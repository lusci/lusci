# Windows NT Batch API
%CD% ;Expands to the current directory string.
%DATE% ;Expands to current date using same format as DATE command.
%TIME% ;Expands to current time using same format as TIME command.
%RANDOM% ;Expands to a random decimal number between 0 and 32767.
%ERRORLEVEL% ;Expands to the current ERRORLEVEL value
%CMDEXTVERSION% ;Expands to the current Command Processor Extensions version number.
%CMDCMDLINE% ;Expands to the original command line that invoked the Command Processor.
APPEND [d:]path[;][d:]path[...] ;Displays or sets the search path for data files. DOS will search the specified path(s) if the file is not found in the current path.
APPEND [/X:on|off][/path:on|off] [/E] ;Displays or sets the search path for data files. DOS will search the specified path(s) if the file is not found in the current path.
ASSIGN x=y [...] /sta ;Redirects disk drive requests to a different drive.
ASSOC [.ext[=[fileType]]] ;Displays or modifies file extension associations.
AT [\\computername] [ [id] [/DELETE] | /DELETE [/YES]] ;Schedules commands and programs to run on a computer.
AT [\\computername] time [/INTERACTIVE] [ /EVERY:date[,...] | /NEXT:date[,...]] "command" ;Schedules commands and programs to run on a computer.
ATTRIB [d:][path]filename [/S] ;Displays or changes file attributes.
ATTRIB [+R | -R] [+A | -A ] [+S | -S] [+H | -H] [drive:][path][filename] [/S [/D]] ;Displays or changes file attributes.
BREAK ;Sets or clears extended CTRL+C checking.
CACLS filename [/T] [/E] [/C] [/G user:perm] [/R user [...]] ;Displays or modifies access control lists (ACLs) of files.
CALL [drive:][path]filename [batch-parameters] ;Calls one batch program from another.
CD [/D] [drive:][path] ;Displays the name of or changes the current directory.
CHCP [nnn] ;Displays or sets the active code page number.
CHDIR [/D] [drive:][path] ;Displays the name of or changes the current directory.
CHKDSK [volume[[path]filename]]] [/F] [/V] [/R] [/X] [/I] [/C] [/L[:size]] ;Checks a disk and displays a status report.
CHKNTFS volume [...] ;Displays or modifies the checking of disk at boot time.
CHKNTFS /D ;Displays or modifies the checking of disk at boot time.
CHKNTFS /T[:time] ;Displays or modifies the checking of disk at boot time.
CHKNTFS /X volume [...] ;Displays or modifies the checking of disk at boot time.
CHKNTFS /C volume [...] ;Displays or modifies the checking of disk at boot time.
CLS ;Clears the screen.
CMD [/A | /U] [/Q] [/D] [/E:ON | /E:OFF] [/F:ON | /F:OFF] [/V:ON | /V:OFF] [[/S] [/C | /K] string] ;Starts a new instance of the Windows command interpreter.
COLOR [attr] ;Sets the default console foreground and background colors.
COMP [data1] [data2] [/D] [/A] [/L] [/N=number] [/C] [/OFF[LINE]] ;Compares the contents of two files or sets of files.
COMPACT [/C | /U] [/S[:dir]] [/A] [/I] [/F] [/Q] [filename [...]] ;Displays or alters the compression of files on NTFS partitions.
CONVERT volume /FS:NTFS [/V] [/CvtArea:filename] [/NoSecurity] [/X] ;Converts FAT volumes to NTFS.  You cannot convert the current drive.
COPY [/D] [/V] [/N] [/Y | /-Y] [/Z] [/A | /B ] source [/A | /B] [+ source [/A | /B] [+ ...]] [destination [/A | /B]] ;Copies one or more files to another location.
DATE [/T | date] ;Displays or sets the date.
DEFRAG <volume> [-a] [-f] [-v] [-?] ;Defragment a disk drive.
DEL [/P] [/F] [/S] [/Q] [/A[[:]attributes]] names ;Deletes one or more files.
DIR [drive:][path][filename] [/A[[:]attributes]] [/B] [/C] [/D] [/L] [/N] [/O[[:]sortorder]] [/P] [/Q] [/S] [/T[[:]timefield]] [/W] [/X] [/4] ;Displays a list of files and subdirectories in a directory.
DISKCOMP [drive1: [drive2:]] ;Compares the contents of two floppy disks.
DISKCOPY [drive1: [drive2:]] [/V] ;Copies the contents of one floppy disk to another.
DOSKEY [/REINSTALL] [/LISTSIZE=size] [/MACROS[:ALL | :exename]] [/HISTORY] [/INSERT | /OVERSTRIKE] [/EXENAME=exename] [/MACROFILE=filename] [macroname=[text]] ;Edits command lines, recalls Windows commands, and creates macros.
ECHO [ON | OFF] ;Turns command echoing on or off.
ECHO [message] ;Displays messages
ENDLOCAL ;Ends localization of environment changes in a batch file.
ERASE [/P] [/F] [/S] [/Q] [/A[[:]attributes]] names ;Deletes one or more files.
EXIT [/B] [exitCode] ;Quits the CMD.EXE program (command interpreter).
EXPAND Source Destination ;Expands one or more compressed files.
EXPAND -r Source [Destination] ;Expands one or more compressed files, renaming expanded files.
EXPAND -D Source.cab [-F:Files] ;Display list of files in source.
EXPAND Source.cab -F:Files Destination ;Expand named files from a .CAB.
FC [/A] [/C] [/L] [/LBn] [/N] [/OFF[LINE]] [/T] [/U] [/W] [/nnnn] [drive1:][path1]filename1 [drive2:][path2]filename2 ;Compares two files or sets of files, and displays the differences between them.
FC /B [drive1:][path1]filename1 [drive2:][path2]filename2 ;Performs a binary comparison of two files and displays the differences between them.
FIND [/V] [/C] [/N] [/I] [/OFF[LINE]] "string" [[drive:][path]filename[ ...]] ;Searches for a text string in a file or files.
FINDSTR [/B] [/E] [/L] [/R] [/S] [/I] [/X] [/V] [/N] [/M] [/O] [/P] [/F:file] [/C:string] [/G:file] [/D:dir list] [/A:color attributes] [/OFF[LINE]] strings [[drive:][path]filename[ ...]] ;Searches for strings in files.
FOR %variable IN (set) DO command [command-parameters] ;Runs a specified command for each file in a set of files.
FOR /D %variable IN (set) DO command [command-parameters] ;Runs a specified command for each file in a set of files.
FOR /R [[drive:]path] %variable IN (set) DO command [command-parameters] ;Runs a specified command for each file in a set of files.
FOR /L %variable IN (start,step,end) DO command [command-parameters] ;Runs a specified command for each file in a set of files.
FOR /F ["options"] %variable IN (file-set | "string" | 'command') DO command [command-parameters] ;Runs a specified command for each file in a set of files.
FORMAT volume [/FS:file-system] [/V:label] [/Q] [/A:size] [/C] [/X]
FORMAT volume [/V:label] [/Q] [/F:size] ;Formats a disk for use with Windows.
FORMAT volume [/V:label] [/Q] [/T:tracks /N:sectors] ;Formats a disk for use with Windows.
FORMAT volume [/V:label] [/Q] ;Formats a disk for use with Windows.
FORMAT volume [/Q] ;Formats a disk for use with Windows. ;Formats a disk for use with Windows.
FTYPE [fileType[=[openCommandString]]] ;Displays or modifies file types used in file extension associations.
GOTO label ;Directs the Windows command interpreter to a labeled line in a batch program.
GRAFTABL [xxx] ;Enables Windows to display an extended character set in graphics mode.
GRAFTABL /STATUS ;Enables Windows to display an extended character set in graphics mode.
GRAPHICS [type] [[drive:][path]filename] [/R] [/B] [/LCD] [/PRINTBOX:STD | /PRINTBOX:LCD] ;Loads a program that can print graphics.
HELP [command] ;Provides Help information for Windows commands.
IF [NOT] ERRORLEVEL number command ;Returns true if the last program run returned an exit code equal to or greater than the number specified.
IF [NOT] string1==string2 command ;Returns true if the specified text strings match.
IF [NOT] EXIST filename command ;Returns true if the specified filename exists.
IF [/I] string1 compare-op string2 command ;Returns true when comparing two strings where compare-op may be one of EQU,NEQ,LSS,LEQ,GTR,GEQ.
IF CMDEXTVERSION number command ;Returns true if the internal version number associated with the Command Extensions is the number specified.
IF DEFINED variable command ;Returns true if the environment variable is defined.
ipconfig /all
ipconfig /displaydns
ipconfig /flushdns
ipconfig /registerdns
ipconfig /release
ipconfig /renew
ipconfig /setclassid
ipconfig /showclassid
LABEL [drive:][label] ;Creates, changes, or deletes the volume label of a disk.
LABEL [/MP] [volume] [label] ;Creates, changes, or deletes the volume label of a disk.
MD [drive:]path ;Creates a directory.
MKDIR [drive:]path ;Creates a directory.
MODE COMm[:] [BAUD=b] [PARITY=p] [DATA=d] [STOP=s] [to=on|off] [xon=on|off] [odsr=on|off] [octs=on|off] [dtr=on|off|hs] [rts=on|off|hs|tg] [idsr=on|off]; Configure serial port
MODE [device] [/STATUS] ;Display device status.
MODE LPTn[:]=COMm[:] ;Redirect printing.
MODE CON[:] CP SELECT=yyy ;Select code page.
MODE CON[:] CP [/STATUS] ;Display code page status.
MODE CON[:] [COLS=c] [LINES=n] ;Configure display mode.
MODE CON[:] [RATE=r DELAY=d] ;Configure typematic rate.
MORE [/E [/C] [/P] [/S] [/Tn] [+n]] < [drive:][path]filename ;Displays output one screen at a time.
MORE /E [/C] [/P] [/S] [/Tn] [+n] [files] ;Displays output one screen at a time.
MOVE [/Y | /-Y] [drive:][path]filename1[,...] destination ;Moves one or more files from one directory to another directory.
MOVE [/Y | /-Y] [drive:][path]dirname1 dirname2 ;Renames a directory.
PATH ;Displays the search path for executable files.
PATH [[drive:]path[;...][;%PATH%] ;Sets a search path for executable files.
PAUSE ;Suspends processing of a batch file and displays a message.
POPD ;Restores the previous value of the current directory saved by PUSHD.
PRINT [/D:device] [[drive:][path]filename[...]] ;Prints a text file.
PROMPT [text] ;Changes the Windows command prompt.
PUSHD [path | ..] ;Saves the current directory then changes it.
RD [/S] [/Q] [drive:]path ;Removes a directory.
RECOVER [drive:][path]filename ;Recovers readable information from a bad or defective disk.
REM [comment] ;Records comments (remarks) in batch files or CONFIG.SYS.
REN [drive:][path]filename1 filename2 ;Renames a file or files.
RENAME [drive:][path]filename1 filename2 ;Renames a file or files.
REPLACE [drive1:][path1]filename [drive2:][path2] [/A] [/P] [/R] [/W] ;Replaces files.
REPLACE [drive1:][path1]filename [drive2:][path2] [/P] [/R] [/S] [/W] [/U] ;Replaces files.
RMDIR [/S] [/Q] [drive:]path ;Removes a directory.
SET [variable=[string]] ;Displays, sets, or removes Windows environment variables.
SET /A expression ;Evaluates a numerical expression, storing the result in a variable.
SET /P variable=[promptString] ;Set the value of a variable to a line of input entered by the user.
SETLOCAL ;Begins localization of environment changes in a batch file.
SHIFT [/n] ;Shifts the position of replaceable parameters in batch files.
SORT [/R] [/+n] [/M kilobytes] [/L locale] [/REC recordbytes] [[drive1:][path1]filename1] [/T [drive2:][path2]] [/O [drive3:][path3]filename3] ;Sorts input.
START ["title"] [/Dpath] [/I] [/MIN] [/MAX] [/SEPARATE | /SHARED] [/LOW | /NORMAL | /HIGH | /REALTIME | /ABOVENORMAL | /BELOWNORMAL] [/WAIT] [/B] [command/program] [parameters] ;Starts a separate window to run a specified program or command.
SUBST [drive1: [drive2:]path] ;Associates a path with a drive letter.
SUBST drive1: /D ;Deletes a substituted (virtual) drive.
TIME [/T | time] ;Displays or sets the system time.
TITLE [string] ;Sets the window title for a CMD.EXE session.
TREE [drive:][path] [/F] [/A] ;Graphically displays the directory structure of a drive or path.
TYPE [drive:][path]filename ;Displays the contents of a text file.
VER ;Displays the Windows version.
VERIFY [ON | OFF] ;Tells Windows whether to verify that your files are written correctly to a disk.
VOL [drive:] ;Displays a disk volume label and serial number.
XCOPY source [destination] [/A | /M] [/D[:date]] [/P] [/S [/E]] [/V] [/W] [/C] [/I] [/Q] [/F] [/L] [/G] [/H] [/R] [/T] [/U] [/K] [/N] [/O] [/X] [/Y] [/-Y] [/Z] [/EXCLUDE:file1[+file2][+file3]...] ;Copies files and directory trees.
# Windows XP/2003 Server API
BOOTCFG /Copy ;Makes a copy of an existing boot entry [operating systems] section for which you can add OS options to.
BOOTCFG /Delete ;Deletes an existing boot entry in the [operating systems] section of the BOOT.INI file. You must specify the entry# to delete.
BOOTCFG /Query ;Displays the current boot entries and their settings.
BOOTCFG /Raw ;Allows the user to specify any switch options to be added for a specified boot entry.
BOOTCFG /Timeout ;Allows the user to change the Timeout value.
BOOTCFG /Default ;Allows the user to change the Default boot entry.
BOOTCFG /EMS ;Allows the user to configure the /redirect switch for headless support for a boot entry.
BOOTCFG /Debug ;Allows the user to specify the port and baudrate for remote debugging for a specified boot entry.
BOOTCFG /Addsw ;Allows the user to add predefined switches for a specific boot entry.
BOOTCFG /Rmsw ;Allows the user to remove predefined switches for a specific boot entry.
BOOTCFG /Dbg1394 ;Allows the user to configure 1394 port debugging for a specified boot entry.
FSUTIL BEHAVIOR QUERY <Option> ;Control file system behavior.
FSUTIL BEHAVIOR SET <Option> <Value> ;Control file system behavior.
FSUTIL DIRTY QUERY <Volume Pathname> ;Query the dirty bit of a file.
FSUTIL DIRTY SET <Volume Pathname> ;Set the dirty bit of a file.
FSUTIL FILE CREATENEW <Filename> <Length> ;Creates a new file of a specified size
FSUTIL FILE FINDBYSID <User> <Directory> ;Find a file by security identifier
FSUTIL FILE QUERYALLOCRANGES OFFSET=<Val> LENGTH=<Val> <Filename> ;Query the allocated ranges for a file
FSUTIL FILE SETSHORTNAME <Filename> <ShortName> ;Set the short name for a file
FSUTIL FILE SETVALIDDATA <Filename> <DataLength> ;Set the valid data length for a file
FSUTIL FILE SETZERODATA OFFSET=<Val> LENGTH=<Val> <Filename> ;Set the zero data for a file
FSUTIL FSINFO DRIVES ;List all drives
FSUTIL FSINFO DRIVETYPE <Volume Pathname> ;Query drive type for a drive
FSUTIL FSINFO VOLUMEINFO <Volume Pathname> ;Query volume information
FSUTIL FSINFO NTFSINFO <Volume Pathname> ;Query NTFS specific volume information
FSUTIL FSINFO STATISTICS <Volume Pathname> ;Query file system statistics
FSUTIL HARDLINK CREATE <New Filename> <Existing Filename> ;Create a hardlink
FSUTIL OBJECTID CREATE <Filename> ;Create the object identifier
FSUTIL OBJECTID DELETE <Filename> ;Delete the object identifier
FSUTIL OBJECTID QUERY <Filename> ;Query the object identifier
FSUTIL OBJECTID SET  <ObjectId> <BirthVolumeId> <BirthObjectId> <DomainId> <filename> ;Change the object identifier
FSUTIL QUOTA DISABLE <Volume Pathname> ;Disable quota tracking and enforcement
FSUTIL QUOTA ENFORCE <Volume Pathname> ;Enable quota enforcement
FSUTIL QUOTA MODIFY <Volume Pathname> <Threshold> <Limit> <User> ;Sets disk quota for a user
FSUTIL QUOTA QUERY <Volume Pathname> ;Query disk quotas
FSUTIL QUOTA TRACK <Volume Pathname> ;Enable quota tracking
FSUTIL QUOTA VIOLATIONS ;Display quota violations
FSUTIL REPARSEPOINT DELETE <Filename> ;Reparse point management
FSUTIL REPARSEPOINT QUERY <Filename> ;Reparse point management
FSUTIL SPARSE QUERYFLAG <Filename> ;Query sparse
FSUTIL SPARSE QUERYRANGE <Filename> ;Query range
FSUTIL SPARSE SETFLAG <Filename> ;Set sparse
FSUTIL SPARSE SETRANGE <Filename> <Beginning Offset> <Length> ;Set sparse range
FSUTIL USN CREATEJOURNAL m=<Max-value> a=<Alloc-delta> <Volume Pathname> ;Create a USN journal
FSUTIL USN DELETEJOURNAL <Flags> <Volume Pathname> ;Delete a USN journal
FSUTIL USN ENUMDATA <File ref#> <lowUsn> <highUsn> <Volume Pathname> ;Enumerate USN data
FSUTIL USN QUERYJOURNAL <Volume Pathname> ;Query the USN data for a volume
FSUTIL USN READDATA <Filename> ;Read the USN data for a file
FSUTIL VOLUME DISMOUNT <Volume Pathname> ;Dismount a volume
FSUTIL VOLUME DISKFREE <Volume Pathname> ;Query the free space of a volume
NET ACCOUNTS [/FORCELOGOFF:{minutes | NO}] [/MINPWLEN:length] [/MAXPWAGE:{days | UNLIMITED}] [/MINPWAGE:days] [/UNIQUEPW:number] [/DOMAIN]
NET COMPUTER \\computername {/ADD | /DEL}
NET CONFIG
NET CONFIG [SERVER | WORKSTATION]
NET CONTINUE service
NET FILE
NET FILE [id [/CLOSE]]
NET GROUP [groupname [/COMMENT:"text"]] [/DOMAIN] ;This command can be used only on a Windows Domain Controller.
NET GROUP groupname {/ADD [/COMMENT:"text"] | /DELETE}  [/DOMAIN] ;This command can be used only on a Windows Domain Controller.
NET GROUP groupname username [...] {/ADD | /DELETE} [/DOMAIN] ;This command can be used only on a Windows Domain Controller.
NET HELP command
NET HELPMSG message#
NET LOCALGROUP
NET LOCALGROUP [groupname [/COMMENT:"text"]] [/DOMAIN]
NET LOCALGROUP groupname {/ADD [/COMMENT:"text"] | /DELETE}  [/DOMAIN]
NET LOCALGROUP groupname name [...] {/ADD | /DELETE} [/DOMAIN]
NET NAME
NET NAME [name [/ADD | /DELETE]]
NET PAUSE service
NET PRINT \\computername\sharename
NET PRINT [\\computername] job# [/HOLD | /RELEASE | /DELETE]
NET SEND {name | * | /DOMAIN[:name] | /USERS} message
NET SESSION [\\computername] [/DELETE]
NET SHARE  sharename
NET SHARE sharename=drive:path [/USERS:number | /UNLIMITED] [/REMARK:"text"] [/CACHE:Manual | Documents| Programs | None ]
NET SHARE sharename [/USERS:number | /UNLIMITED] [/REMARK:"text"] [/CACHE:Manual | Documents | Programs | None]
NET SHARE {sharename | devicename | drive:path} /DELETE
NET START
NET START service
NET STATISTICS
NET STATISTICS [SERVER | WORKSTATION]
NET STOP service
NET TIME [\\computername | /DOMAIN[:domainname] | /RTSDOMAIN[:domainname]] [/SET]
NET TIME [\\computername] /QUERYSNTP
NET TIME [\\computername] /SETSNTP[:ntp server list]
NET USE [devicename | *] [\\computername\sharename[\volume] [password | *]] [/USER:[domainname\]username]
NET USE [devicename | *] [\\computername\sharename[\volume] [password | *]] [/USER:[dotted domain name\]username]
NET USE [devicename | *] [\\computername\sharename[\volume] [password | *]] [/USER:[username@dotted domain name]
NET USE [devicename | *] [\\computername\sharename[\volume] [password | *]] [/SMARTCARD] [/SAVECRED]
NET USE [devicename | *] [[/DELETE] | [/PERSISTENT:{YES | NO}]]
NET USE {devicename | *} [password | *] /HOME
NET USE [/PERSISTENT:{YES | NO}]
NET USER
NET USER [username [password | *] [options]] [/DOMAIN]
NET USER username {password | *} /ADD [options] [/DOMAIN]
NET USER username [/DELETE] [/DOMAIN]
NET VIEW [\\computername [/CACHE] | /DOMAIN[:domainname]]
NET VIEW /NETWORK:NW [\\computername]
NETSH [-a AliasFile] [-c Context] [-r RemoteMachine] [Command | -f ScriptFile]
REG ADD KeyName [/v ValueName | /ve] [/t Type] [/s Separator] [/d Data] [/f]
REG COMPARE KeyName1 KeyName2 [/v ValueName | /ve] [Output] [/s]
REG COPY KeyName1 KeyName2 [/s] [/f]
REG DELETE KeyName [/v ValueName | /ve | /va] [/f]
REG EXPORT KeyName FileName
REG IMPORT FileName
REG LOAD KeyName FileName
REG QUERY KeyName [/v ValueName | /ve] [/s]
REG RESTORE KeyName FileName
REG SAVE KeyName FileName
REG UNLOAD KeyName
SC <server> query [service name] <option1> <option2>...
SC <server> queryex [service name] <option1> <option2>...
SC <server> start [service name] <option1> <option2>...
SC <server> pause [service name] <option1> <option2>...
SC <server> interrogate [service name] <option1> <option2>...
SC <server> continue [service name] <option1> <option2>...
SC <server> stop [service name] <option1> <option2>...
SC <server> config [service name] <option1> <option2>...
SC <server> description [service name] <option1> <option2>...
SC <server> failure [service name] <option1> <option2>...
SC <server> qc [service name] <option1> <option2>...
SC <server> qdescription [service name] <option1> <option2>...
SC <server> qfailure [service name] <option1> <option2>...
SC <server> delete [service name] <option1> <option2>...
SC <server> create [service name] <option1> <option2>...
SC <server> control [service name] <option1> <option2>...
SC <server> sdshow [service name] <option1> <option2>...
SC <server> sdset [service name] <option1> <option2>...
SC <server> GetDisplayName [service name] <option1> <option2>...
SC <server> GetKeyName [service name] <option1> <option2>...
SC <server> EnumDepend [service name] <option1> <option2>...
SC <server> Boot <bad|ok>
SC <server> Lock
SC <server> QueryLock
SYSTEMINFO
SYSTEMINFO [/S system [/U username [/P [password]]]] [/FO {TABLE, LIST, CSV}] [/NH]
TASKKILL [/S system [/U username [/P [password]]]] { [/FI filter] [/PID processid | /IM imagename] } [/F] [/T]
TASKLIST
TASKLIST [/S system [/U username [/P [password]]]] [/M [module] | /SVC | /V] [/FI filter] [/FO {TABLE, LIST, CSV}] [/NH]
# Windows Resource Kit API
adlb /parameter:argument ... ;Active Directory Load Balancing Tool
atmarp [/s] [/c] [/reset] ;Windows NT IP/ATM Information
atmlane ;Windows ATM LAN Emulation Client Information
cdburn <drive> -erase [image [options]]
cdburn <drive> image [options]
chknic ;Check all the Network Adaptors on the local machine whether they are supported by NLB.
clearmem [/q] [/d] [/mNum] [/pNum] [/w] [/b][/tNum] ;Determines the size of the computer's physical memory, allocates enough data to fill it, and references the data as quickly as possible.
COMPRESS [-R] [-D] [-S] [ -Z | -ZX ] Source Destination
COMPRESS -R [-D] [-S] [ -Z | -ZX ] Source [Destination]
confdisk /save <SIF file to save configuration to>
confdisk /restore <SIF file to restore configuration from>
consume RESOURCE [-time SECONDS] ;Universal Resource Consumer - Just an innocent stress program.
creatfil FileName [FileSize]
csccmd /ENABLE
csccmd /DISABLE
csccmd /RESID
csccmd /DISCONNECT:<\\server\share>
csccmd /MOVESHARE:<\\server\share> <\\server\share>
CustReasonEdit [/? | /i | /l | /e fn | /r fn][/s][/m host]
DH [-p n | -p -1 | -p 0 [-k] [-o]] [-l] [-m] [-s] [-g] [-h] [-t] [r n][-f fileName]
DISKRAID [[/s <file-path>] [/v]]
diskuse <path> [switches]
dvdburn <drive> <image> [/Erase] ;Write (burn) Digital Video Disk (DVD) images from image files located on the hard drive to Digital Video Disk (DVD) media.
empty.exe {pid | task-name} ;Frees the working set of a specified task or process, making those page frames available for other processes.
gpotool [/gpo:GPO[,GPO...]] [/domain:name] [/dc:DC[,DC...]] [/checkacl] [/verbose] ;Group Policy Object verification tool
hlscan [/log [location]] ;Hard Link Display, scans the current volume.
hlscan /all [/log [location]] ;Hard Link Display, scans all NTFS volumes on the local system.
hlscan /dir <directory name> [/log [location]] ;Hard Link Display, scans recursively from <directory name>.
hlscan /file <file names> [/log [location]] ;Hard Link Display, scans only files specified by <file names> (non recursively).
ifilttst /i <input file>[...] [/ini <ini file>] [/l [<log file>]] [/d] [/-l] [/-d] [/legit] [/stress] [/v <verbosity>] [/t <threads>] [/r [<depth>]] [/c <loops>]
ifmember [/verbose] [/list] groupname ... ;Show groups of which logged in user is a member.
instexnt install [/interactive] ;Install the AutoExNT service
instexnt remove ;Remove the AutoExNT service
INSTSRV <service name> (<exe location> | REMOVE) [-a <Account Name>] [-p <Account Password>] ;Installs and removes system services from NT.
KERNRATE [-l] [-lx] [-r] [-m] [-p ProcessId] [-z ModuleName] [-j SymbolPath] [-c RateInMsec] [-s Seconds] [-i [SrcShortName] Rate] ;Reports on kernel and user-mode processes to provide information about CPU activity.
KERNRATE [-n ProcessName] [-w] ;Reports on kernel and user-mode processes to provide information about CPU activity.
klist <tickets | tgt | purge> ;View and delete Kerberos tickets granted to the current logon session.
LINKD Source [/D] Destination ;Links an NTFS directory to a target valid object name in Windows 2000.
LINKSPEED { /S system | /DC } [/T value] ;Displays the link speed to the remote system.
list [-s:string] [-g:line#] filename, ... ;Displays and searches one or more text files.
LOGTIME "text string" ;Writes log file LOGTIME.LOG with date and time stamp next to the specified text string.
lsreport [/F filename] [/D start [end]] [/T] [/W] [/?] [serverlist] ;Write information about licenses granted by Terminal Server License Servers.
mcast /send /srcs:SourceAddress /grps:GroupAddress [/intvl:[PacketInterval]] [/numpkts:[NumberPackets]] [/ttl:[TimeToLive]] [/pktsize:[PacketSize]] [/intf:[InterfaceAddress]]
mcast /recv /grps:GroupAddress [/dump:[packettype]] [/runtime:[Duration]] [/intf:[InterfaceAddress]]
memmonitor [-p <pid> | -pn <name> | -ps <svc>] [-wait] [-nodbg] [-int <secs>] [-WS <value>] [-PPool <value>] [-NPPool <value>] [-VM <value>]
MemTriage -m  LOGFILE ;Snapshot system and process information.
MemTriage -p  LOGFILE ;Snapshot kernel pool information.
MemTriage -mp LOGFILE ;Snapshot system, process, pool information.
MemTriage -h  PID LOGFILE ;Snapshot process heap information.
MemTriage -t  RETRY -w MINUTES ;Take R snapshot in every M minutes. Could be used together with -m, -p, -mp and -h.
MemTriage -a  LOGFILE <-pid PID> ;Analyze a log for leaks. Specify PID if it's a heap snapshot.
MemTriage -av LOGFILE ;Same as '-a' but generate a detailed report file
mibcc [-?] [-e] [-l] [-n] [-o] [-t] -[w] [files...] ;Compiles the specified SNMP MIB files.
nlsinfo ;Displays the information about a locale on the target system.
NOW [message to be printed with time-stamp] ;Display Message with Current Date and Time
ntimer [-1 -f -s] name-of-image [parameters]... ;Displays the Elapsed, Kernel, User and Idle time of the image specified in the command line.
ntrights {-r Right | +r Right} -u UserOrGroup [-m \\Computer] [-e Entry] ;Grants/Revokes NT-Rights to a user/group.
oh {+kst | -kst} {+otl | -otl} ;Show the handles of all open windows.
oh [/p [ProcessID [/h]]] [/t TypeName] [/o FileName] [/a] [/s] [/u] [/v] [Name] ;Show the handles of all open windows.
oh /c [/l] [/t] [/all] BeforeLog AfterLog ;Show the handles of all open windows.
pathman [/as Path[;Path[Path ..]]] [/au Path[;Path[Path ..]]] [/rs Path[;Path[Path ..]]] [/ru Path[;Path[Path ..]]] ;Adds or removes components from system or user paths.
permcopy \\SourceServer SourceShare \\DestinationServer DestinationShare ;Copies share-level permissions (Full Control, Read, Change) from one share to another.
perms [Domain\|Computer\]UserName [Path\]FileName [/i] [/s] ;Displays user access permissions for a file or directory on an NTFS file system volume.
pfmon [/n | /l] [/c | /h] [/k | /K] [/pProcessID] [/d] AppCommandLine ;Monitor page faults that occur while an application is running.
pmon ;Displays several measures of the CPU and memory use of processes running on the system.
PrintDriverInfo [/s: ServerName] [/p: PrinterName] [/d: DriverName] [/f: FileName] ;Display information about printer drivers.
qgrep [-B] [-E] [-L] [-O] [-X] [-l] [-n] [-z] [-v] [-x] [-y] [-e String] [-f File] [-i File] [Strings] [Files] ;Search a file or list of files for a specific string or pattern and return the line containing the match.
qtcp [/BNum] [/mNum] [/RNum] [/RNumB] [/e] [/M] [/lNum] [/nNum] [/nNums] [/ni] [/cNum] [/y] [/pNum] [/S{CL|GR}] [/W] [/v] [/F"FileName"] [/A"Path"] [/i] /t HostIPAddress ;Measures end-to-end network service quality for the sending host.
qtcp [/f"FileName"] [/cNum] [/k{0|1|2|3}] [/d] [/N] [/P] [/u] [/qNum] [/S{CL|GR}] [/W] [/v] [/F"FileName"] [/A"Path"] /r ;Measures end-to-end network service quality for the receiving host.
rassrvmon {/s:ServerName | /p:Svchost_Pid} [/t:TimeOffset]
Regfind [-m \\machinename | -h hivefile hiveroot | -w Win95 Directory] [-i n] [-o outputWidth] [-p RegistryKeyPath] [-z | -t DataType] [-b | -B] [-y] [-n] [searchString [-r ReplacementString]]
regini [-m \\machinename | -h hivefile hiveroot | -w Win95 Directory] [-i n] [-o outputWidth] [-b] textFiles...
reportgen [/d:dd/mm/yy] [/t:hh:mm] [/f:minutes] [/c] [/p:{PPP|ARAP}] [/a] [/s]
robocopy Source Destination [File [File]..] [options] ;Robust file copy, provides time-efficient maintenance of mirror images of large folder trees on network servers separated by slow or unreliable wide area network (WAN) links.
rpccfg [/pi PortNumber | PortRange...] | [/pe PortNumber PortRange] [/d {0|1}] [/r] [/q] [/l] [/ha ComputerName {PortNumber | PortRange}] [/hr ComputerName] [/hd] [/hl FileName] [/hs FileName] ;Configures Microsoft Remote Procedure Call (RPC) to listen on specified ports.
rpcdump [/s] [/v] [/i] [/p Protocol] ;Queries Remote Procedure Call (RPC) endpoints and reports on the status of RPC services on the system.
rpcping [options];Troubleshoot connection problems between computers involving remote procedure calls (RPCs).
rpings -p {namedpipes | tcpip | ipx/spx | netbios | vines} ;Server side RPC ping utility.
rqc <ConnName> <TunnelConnName> <Port> <Domain> <Username> <String> ;Remote Access Quarantine Client
rqs /run ;Remote Access Quarantine Server
setprinter [/show] {\\server | \\server\printer | printer} {printer_info_level} [keyword="[- | +]value" ...] [{pause | resume | purge | setstatus}] ;Set configurations or states of local and remote printers.
setprinter {/help | /examples} {printer_info_level}
showacls [/s] [/u:Domain\User] [FileSpec] ;Enumerates access rights for files, folders, and trees.
showpriv SeAssignPrimaryTokenPrivilege
showpriv SeAuditPrivilege
showpriv SeBackupPrivilege
showpriv SeChangeNotifyPrivilege
showpriv SeCreatePagefilePrivilege
showpriv SeCreatePermanentPrivilege
showpriv SeCreateTokenPrivilege
showpriv SeDebugPrivilege
showpriv SeEnableDelegationPrivilege
showpriv SeIncreaseBasePriorityPrivilege
showpriv SeIncreaseQuotaPrivilege
showpriv SeLoadDriverPrivilege
showpriv SeLockMemoryPrivilege
showpriv SeMachineAccountPrivilege
showpriv SeProfileSingleProcessPrivilege
showpriv SeRemoteShutdownPrivilege
showpriv SeRestorePrivilege
showpriv SeSecurityPrivilege
showpriv SeShutdownPrivilege
showpriv SeSyncAgentPrivilege
showpriv SeSystemEnvironmentPrivilege
showpriv SeSystemProfilePrivilege
showpriv SeSystemtimePrivilege
showpriv SeTakeOwnershipPrivilege
showpriv SeTcbPrivilege
showpriv SeUndockPrivilege
sleep time-to-sleep-in-seconds
sleep [-m] time-to-sleep-in-milliseconds
sleep [-c] commited-memory-ratio
sonar [/i] [/s] [/npc] [/rpc] [/u] [file] ;Monitor key statistics and status about members of a file replication service (FRS) replica set.
splinfo [/z] [/v] [/d] [\\UNC_Name] ;Displays information from the print spooler.
srvany ;Enables 32-bit or 16-bit applications to run as services.
srvcheck \\ComputerName ;Displays a list of server shares and who has access.
SRVINFO [[-ns|-d|-v|-s] \\computer_name] ;Remotely gather information about a target server.
SSDFormat [-h] <-d ShutdownDirectory> <-s XSLfile> [ -o OutputDirectory ]
SubInAcl [/option...] /object_type object_name [[/action[=parameter]...]
tail filename ;Print last 10 lines of file.
tail [-n] filename ;Print last n lines of file.
tail [-f] filename ;Print last 10 lines of file and keep checking for new lines.
timeit [-f filename] [-a] [-c] [-i] [-d] [-s] [-t] [-k keyname | -r keyname] [-m mask] [commandline...]
TIMETHIS "command" ;Command timing utility.
timezone [/g | /s StartDate EndDate] ;Daylight Saving Time Update Utility.
tsctst [/a] ;Terminal Server Client License Dump Tool.
vadump [-sv] -p decimal_process_id ;Dump the address space.
volperf /install ;Install shadow copy performance counters.
volperf /uninstall ;Uninstall shadow copy performance counters.
volrest [options] FileName ;Previous Version command-line tool.
vrfydsk [volume[path]] [/V] [/I] [/C] ;Checks a disk and displays a status report.
winhttpcertcfg [-i PFXFile | -g | -r | -l] [-a Account] [-c CertStore] [-s SubjectStr] [-p PFXPassword] ;WinHTTP Certificate Configuration Tool.
winhttptracecfg ;WinHTTP Tracing Facility Configuration Tool.
winhttptracecfg [-e <0|1>] [-l [log-file]] [-d <0|1>] [-s <0|1|2>] [-t <0|1>] [-m <MaxFileSize>] ;WinHTTP Tracing Facility Configuration Tool.
# SysInternals Tools API
accesschk [-s][-i|-e][-r][-w][-n][-v][[-k][-c]|[-d]] [username] <file, directory, registry key, service> ;Check account access of files, registry keys or services.
AdRestore [-r] [searchfilter] ;
Autologon <username> <domain> <password>
Clockres ;View the system clock resolution.
Contig [-v] [-a] [-s] [-q] [existing file] ;Make file contiguous.
Contig [-v] -n [new file] [new file length] ;Make file contiguous.
ctrl2cap /install
ctrl2cap /uninstall
diskext [drive1 [drive2] ...] ;Disk Extent Dumper.
du [[-v] [-l <levels>] | [-n]] [-q] <directory> ;Report directory disk usage.
efsdump [-s] [-q] <file or directory> ;EFS Information Dumper.
handle [[-a] [-u] | [-c <handle> [-y]] | [-s]] [-p <process>|<pid>] [name]
hex2dec [decimal|hex] ;Converts hex to decimal and vice versa.
junction [-s] [-q] <file or directory> ;Windows junction creator and reparse point viewer.
ldmdump /d# ;Logical Disk Manager Configuration Dump.
listdlls [-r] [processname|pid] ;DLL lister.
listdlls [-r] [-d dllname] ;DLL lister.
livekd [-w] [-d] [-k <debugger path>] [debugger options] ;Execute i386kd/windbg/dumpchk on a live system.
logonsessions [/p]
movefile [source] [dest] ;Copies over an in-use file at boot time.
ntfsinfo  <drive letter> ;NTFS Information Dump.
pendmoves ;Display pending file rename operations.
pipelist
ProcFeatures ;Display processor features.
psexec [\\computer[,computer2[,...] | @file][-u user [-p psswd]][-n s][-l][-s|-e][-x][-i [session]][-c [-f|-v]][-w directory][-d][-<priority>][-a n,n,...] cmd [arguments] ;Execute process remotely.
psfile [\\RemoteComputer [-u Username [-p Password]]] [[Id | path] [-c]] ;Lists or closes files opened remotely.
psgetsid [\\computer[,computer2[,...] | @file] [-u Username [-p Password]]] [account | SID] ;Translates SIDs to names and vice versa.
psinfo [-h] [-s] [-d] [-c [-t delimiter]] [filter] [\\computer[,computer[,..]]|@file [-u Username [-p Password]]] ;Local and remote system information viewer.
pskill [-t] [\\computer [-u username [-p password]]] <process ID | name> ;Terminates processes on local or remote systems.
pslist [-d][-m][-x][-t][-s [n] [-r n] [\\computer [-u username][-p password][name|pid] ;Display local and remote process information.
psloggedon [-l] [-x] [\\computername] ;Display who's logged on.
psloggedon [username] ;Display who's logged on.
psloglist [OPTIONS]... <event log> ;Local and remote event log viewer.
pspasswd [\\[computer[,computer,[,...]|Domain]|@file] [-u Username [-p Password]]] Username [NewPassword] ;Local and remote password changer.
psservice [\\Computer [-u Username [-p Password]]] <cmd> <optns> ;Service information and configuration utility.
psshutdown [OPTIONS]... ;Shutdown, logoff and power manage local and remote systems
pssuspend [-r] [\\RemoteComputer [-u Username [-p Password]]] <process Id or name> ;Suspend local and remote processes.
regdelnull <path> [-s] [-r] ;Delete Registry keys with embedded Nulls.
regjump <path> ;Open registry editor.
sdelete [-p passes] [-s] [-q] <file or directory> ;Secure delete.
sdelete [-p passes] [-z|-c] [drive letter] ;Secure delete.
SigCheck InoculateIT ;Returns the signature version for the InoculateIT Engine.
SigCheck Vet ;Returns the signature version for the Vet Engine.
streams [-s] [-d] <file or directory> ;Enumerate alternate NTFS data streams.
strings [-s] [-o] [-n length] [-a] [-u] [-q] <file or directory>
sync [-r | drive letters] ;Disk flusher.
volumeid [drive:] [Id] ;Set disk volume id.
whois domainname [whois.server] ;Domain information lookup utility.
