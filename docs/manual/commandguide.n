Terminal Commands Brief  (draft)
  
  `cd` works in the terminal as normal.
  
  Non interactive shell commands work when typed plainly into terminal.
  Their output doesnt show until the command completes.
  `run,` can be used for commands with output which scrolls, like compilation.
  
  Hit `enter` on empty prompt to cycle to current buffers directory and through MRUs
  
  `sudo chown etc.` works, lusci asks for and uses an asterix hidden password
  
  Interactive shell commands cannot work. 
  Lusci will totally freeze on interactive commands
  terminating them with a process manager restores
  
  All lusci commands are marked with a comma, only the first two letters
  need typed.
  
  new, makes a new file and opens it. Fails if file already exists
  
  open, opens an existing file, Fails if doesnt exist
  
  ssave, saves current document when sudo permissions are required preserving ownership
  
  lua, runs a lua statement such as a lusci global function 
  
  lp, prints return of a lua statement, trys adding return statement if none present
    eg. lp,math.log(16)/math.log(2)
        lp,a="abcdef" return a:sub(2,-2)

  ctl + r - runs selected text as a lua snippet, or if no selection runs the tail
     end of the current buffer that is preceeded by the lowest comment sequence ----
     this allows writing lua snippets in a buffer which can be run quickly
     without commenting out all of the preceeding snippets.
  
  focus, prints list of stats of currently opened buffers, which can be navigated and pruned
    eg. fo,partname  lists open buffers with filepaths containing 'partname'
                     if only one buffer matched then it is focused immediately
    the formatted list of files enable quick focusing and closing of files
    
  fd, same as focus but for all files which have ever opened in the current lusci config
  
  undo, redo, un/redo editor from output 
  clr, clears output
  
  save,  saveas
  osave, save output as
  csave, save clipboard as
  move,  renames the currently open file on disk and in editor database
    
  pallet, flip luminosity of theme keeping colours similar
        allows instant change between white, light, dim, dark and black background
        
  sp, and li, (spot, and list,)
    spot and list are lusci's two primary file navigation commands
    these are both find in file commands, they list out the lines in the terminal
    which contain the search hits. The list can then be scanned by eye
    and each line and file can be opened in the editor by clicking or hitting enter
    on the line, or by using the keypress 'ctrl+[' or 'ctrl+]' which steps through
    each line and file in sequence in the editor.   
    list, searches only the current file, spot, searches files in the directory
    they are commonly triggered by keypress ctrl+h for list, ctrl+shift+h for spot
    the keypresses do a listing for the word under the cursor or the current selection
    the keypresses just prepare the command in the terminal and run it
    these commands can also be entered and controlled manually in the terminal
    they both have a fast basic syntax and by default do case insensitive plain 
    text word searches:
      li,pin   [list word bound occurences of 'pin' in current file ]
      li,pin~  [same but not whole word search, will also hit eg. 'spin']
      sp,pin   [lists the term 'pin' (as word bounded) in files under the current
                directory in the terminal. ~ at the end would also search as non-word ]
      sp,* pin [patterns beginning with * after the comma are taken as the filepatterns
                to include in the search. If no filepattern supplied command automatically
                limits search to currently displayed documents extension ]
      Sp,*.h *.cxx *makefile SprocketB
               [lists case sensitive "SprocketB" in files names *.h *.cxx *makefile 
                Case sensitivity is specified by uppercasing the command (List, or Spot)]
                
      The spot command is performed by grep on linux and findstr in windows. Lusci
      prepares the appropriate command. Options can also be included after file patterns eg.
      Sp,*.lua -G -B3 while.*do
               [greps lua files for the regex term "while.*do" lists B3 lines per hit]
    
    list can also perform text replacements in current file and in multiple files
    more documentation on list and spot to follow...
  
  
File Encryption, lucy enables files to be encrypted, then opened and saved transparently
using a global password defined in config, or unique password typed in 

The terminal runs most system commands including sudo'ing and "save-as-super"


Lusci Lua 
Scites inbuilt Lua 5.3 interpreter.
Run lua scripts on startup, or easily attached to keypresses and having access to
all editors data. Output to editors panels or files. Run just a selected part of a
lua file or the marked tail end of a file for testing snippets.

Terminal

Run noninteractive system commands on windows or bash. On linux sudo commands and 
with luscis 'susave,' command quickly save a file with priveledges without altering 
ownership or permissions (supply sudo password).
