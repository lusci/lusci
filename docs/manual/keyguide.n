Draft Keyguide

#Keyboard Map:Left 
  with (Ctrl), and (Ctrl Shift) below
 
             [Q]         [W]         [E]         [R]         [T]         [Y]     []  [..
(C )        cmmnt      close-bf   ln-blk-ends   -free-      twiddle    -free-
(CS)                    close-all                revert       sort     theme-sw
              [A]         [S]         [D]         [F]         [G]         [H]     []  [..
(C )         ln-ends      save      dupline      find        GOTO        HTLIST
(CS)        sel-ends                                                     RGLIST
        [\]    [Z]         [X]         [C]         [V]         [B]         [N]     []  [..
(C )    bkmk   undo      cut/select    copy        paste      DUP-OUT    new file
(CS)           redo      select-vis                                     see-endings

#Keyboard Map:Right

  ..]  []   [U]        [I]        [O]        [P]        [{]        [}]
(C )        case      RPT-WRD     open    flpath-cpy   jmpl-dw   jmpl-up
(CS)       linejoin   RPT-WDbk               print       bkm-dwn   bkm-up   
             
    ..]   []   [J]         [K]        [L]         [;]         [@]         [~] 
(C )           JP-DWN      JP-UP     cut-lin     SEL-LIN      --         CUR-BK       
(CS)           JPD-OP      JPU-OP    del-lin     SEL-BLK   (lnsplits)   MULT-WRD-ED
             
      ..]    []    [M]           [,]         [.]         [/]         
(C )             quick-cmd    out-switch   SEL-WRD        --
(CS)              copyover     out-shrk    out-exp     (lnjoin)
 

familiar editor hotkeys
  open|save|new file|close     -as common    ctrl + o|s|n|w
  undo|find|copy|cut|paste     -as common    ctrl + z|f|c|x|v
  duplicate            ctrl + d
  cut lines            ctrl + l
  move line up/dwn     alt + up or down
  move word lf/rgh     alt + ctrl + left or right
  comment              ctrl + q
  transpose            ctrl + t
    
missing familiar hotkeys
 select all  ctrl + a  - use ctrl + x instead, which is the cut selection shortcut
                         but when there is no selection, it selects all without cutting.
                         ( this frees ctrl+a for a useful caret movement key )
  revert     ctrl + r  - revert with shift+ctrl+r
  redo       ctrl + y  - redo with shift+ctrl+z
  
modified familiars
  ctrl + o  an existing filename is opened without dialog if one is selected or under caret 
  ctrl + c  if there is no selection to copy, ctrl+c does not copy but instead 
            it re-selects the last pasted or selected text
  ctrl + t  if selection is present it twiddles words or lines in the selection
  ctrl + v  pastes with indent, to paste without indent use ctrl+shift+v
  ctrl + l + shift  
    deletes lines instead of cutting  
  alt + up/down  
    move selection or lines up/down 
  ctrl + shift + up/down or left/right
    expands selection by lines or words


Rapid Hotkey Intro

Lusci default config and hotkeys is intended to work without using mouse or menus,
but also to not clash with them.
It is intended to enable deft vim or emacs style operation without
disturbing basic text editor usage.


Lusci Intro Hotkeys
  caret hop to other end of line or selection        ctrl + a
    hops to line start, or if already at the start to line end
  caret seek to same word down|up                    ctrl + j | k
    when with shift it seeks in the other pane (an ode to vim)
  return caret to last editted position              ctrl + #
    for when place is lost scrolling
  word select (and grow)                             ctrl + .
  line select (and grow)                             ctrl + ;
  repeat a nearby word                               ctrl + i
    (cycles through near words, with shift cycles back)
  toggle focus between editor and terminal           ctrl + ,
  
Routine Hotkeys
  make jump list of occurences in file               ctrl + h
  make jump list of occurences in directory          shift + ctrl + h
    these employ lusci commands list and spot in the
    terminal. these commands can also do replacements
  goto file and line in jump list  (up,dwn)          ctrl + [ , ctrl + ]
    this will jump to file and line positions listed in the terminal
    by luscis list, command, grep and compiler notices 
  switch to most recently dwelled file               ctrl + tab
  bring up file-switcher modal                       ctrl + `

Extra Shortcuts
  toggle theme to previous pallet                    {ctrl + shift + y}
    the themes pallet can be instantly adjusted from light to dark and 
    contrast tweaked etc. in the term with the pallet, command.
  enter multi-type mode                             {ctrl + shift + #}  
    actives multitype on selection or current line
    normal typing changes all matching words in highlighted area
    change all matching characters or wordparts by selecting
    them before typing over selection
    multi-type mode avoids accidental changes by tracking
    current match positions, it works as a handy live replace feature
  insert at selection ends                         {space,braces,quotes,delete}
    typing space,brackets,braces,quotes on a selection inserts them
    at ends and includes them in selection, delete key removes
    a characters at each end. Backspace deletes selection as normal.
  scroll screen without moving cursor.                 {ctrl + up or down} 
    To bring the cursor into a scrolled screen
    press up or down without ctrl. any other keys including left and right cursor
    will return the screen to the cursors position.
  scroll the panel not in focus                     {ctrl + alt + up or down}
    scrolls the panel which is not in focus
  Scite's virtual space selection (very handy) -     {alt + shift + arrows}

Keyboard Other
  shift + enter
    new line with indent, (push current line down)
  ctrl + enter
    autocompletion list
    
keys left free for future and custom shortcuts
  ...     


Summarizing Hotkeys With Arrows:
 {shift} selects   {ctrl} hops over terms   {alt} moves selected text
   these controls work in combination with the {up/down/left/right} keys
 except the combinations:
 {ctrl up/down  and  alt+ctrl up/down}  - these scroll without moving caret
 and 
 {shift+alt+arrows} - these modify selection to 'column mode' (a useful mode)

shift works as normal (selection)
shift+alt works as scite normal (column selection)
shift+ctrl works similar to normal -word and line selection with modifications
alt works to move selection in direction of arrow
ctrl+alt works to move txt by word and line, with refinements
ctrl+arrow works to move caret by word and gap or gap, with advanced word
detection including floating point terms and brackets.
shift+ctrl+up/dwn expands the selection to line starts 
this enables fast selection of lines around caret

Note
For the purposes of ctrl-arrow caret movement and selection, lusci determines 
words differently to other common editors. For example if moving into brackets 
or quotes lusci will hop or select to the matching far bracket.
To move over terms within brackets it is necessary to first cursor inside them.
Also when moving terms by word steps (with the alt key in combination) lusci
hops not only individual words but also the space or separators following them.
When direction is changed a single word or separation sequence only is hopped.
(the state of move direction is represented in which end of the selection the caret
is arranged to be in, or which end of word it is at when there is no selection)
This combination of word+space hop and word-or-space hop, can assist moved words 
(or selected phrases) to be finegaled into correct place.
In practice it should be more intuitive than this note describes and sometimes
cutting or retyping would have been faster, but so it is too with common editor 
ctrl-arrow word hop arrangements.


Missing hotkeys:  
Adjust size of terminal pane  
    The SciTE platform itself will need updated to enable this. 
    Until then it is required to mouse drag to adjust terminal size
 
Change Case
  cycle case of selected text
  try to make work over virtual selections

ctrl + shift + x
  select visible lines, may be useful for limited replacement operations ? 
  
ctrl-r on a jump-line in term to update the line in file to current state
  would be powerful, confirmation of action printed above the current line
  line path detection will need to be improved before performing this.
  
  if fullpath etc is openfiles and dirty 
  file is open and dirty, focus the file
  responses printed as comments below the cursored jumpline 


Modified Hotkeys

ctrl j
  creates a list in the terminal of occurences of selected or word under cursor when pressed
  which can be read and easily hopped through
ctrl shift j 
  does the same for occurences of text in files of current directory using grep    
ctrl [ ]
  jumps editor focus through the search results listed in terminal 
        
ctrl .
  toggles focus between terminal and document
  pressed twice promptly send the terminals caret to the bottom 
  where commands are entered

ctrl tab
  cycles back through most recently used buffers, but does not mark the newly opened 
  buffers as visited, unless dwelled for a while or caret is moved.
  eg. to ctrl-tab back after a sequence to the originating buffer, just move the caret
  and next ctrl tab will return to it. The caretted/editted file will then be the penulimate
  file in the visitation list. This is the best MRU file switching system, it keeps the MRUs
  very nicely.

ctrl \
  sets a bookmark if none nearby or jumps to nearby one
  when pressed on bookmark jumps through most recently dwelled bookmarks throughout files
  
ctrl shift \
  unsets bookmark if on one or jumps to nearby one
  if no bookmark nearby jumps to prev in reverse order
  
ctrl #
  this returns the cursor to the positions which were last edited in the document

ctrl j or k 
  hops up or down to the next occurence of the word under the cursor
  
ctrl `
  shows a userlist of open and recent files, up down and enter or mouseclick switches to
  chosen file. The list indicates which files are dirty and have bookmarks and is quite 
  well formatted and sorted for quick file selection. non cursor or enter key or clicking 
  outside the list closes it without switching. 
  ctrl ` twice can bring up a second list of files which are read from the top of
  a file dedicated for this, set in sciteuserproperties. It can contain common project
  or note files.
  
ctrl a 
  'endhop key' hops between ends of line text or selection.
  moves cursor to the beginning of current lines text after any indent.
  if pressed in that place already it, moves to the end of the lines text
  so 1 press > go start of line, 2 presses > go end of line
  if pressed with a selection it swaps caret and anchor
  
ctrl shift a 
  same as ctrl a but *selects* from cursor to line ends alternately
  so 1 press > select from current to start of line,
     2 press > select from current to end of line

ctrl x
  cuts when a selection is present, when no selection nothing to cut so it selects all
  so 2 presses = select all and cut
ctrl c
  copys selection, when no selection present, it reselects the previous selected or pasted text
ctrl v
  modified to paste text at the cursors indent level,
  also if pasted on top of a selection it copies the selection
  so remember to delete target selections if they should not replace cliptext 
  ctrl shift v pastes normally

ctrl ;
  selects line text without indent, then whole line, then block, then larger block
  also expands a multiline selection to whole lines that is not already whole
  quite occasionally borks selection, wants rewrite.
ctrl ,
  progessive word select
  
ctrl l
  cuts the lines spanned by the selection or just the current line if no selction
  (expands selection to full lines and cuts)
  ctrl shift l does same but just deletes the lines without replacing clipboard
ctrl t twiddle extended
  with no selection twiddles lines as usual
  with selection twiddles around punctuation in the selection, such as ==
  or a coma or space. Mainly for flipping terms, can also cycle through comas
  or spaces.
ctrl shift t
  sorts selected lines or words in selection, to standard and natural sort order
brackets and quotes
  pressing these keys when there is a selection adds the character to the ends of 
  the selection instead of the normal behaviour which is to delete and replace
  the selection. This gives a quick way to put terms in quotes and brackets.
  Delete (not backspace) also deletes the characters at the ends of the selection
  Backspace deletes the selection as normal.
ctrl i
  repeats preceeding word. on subsequent presses it changes to next nearest and so on
  shift ctrl, cycles back in case you tap past one.
  (most useful for just repeating a last word, or another nearby.)
ctrl up or down    
  Scrolls screen without moving cursor. To bring the cursor to a scrolled screen
  press up or down without ctrl. any other keys including left and right cursor
  will return the screen to the cursors position.
ctrl alt up or down
  scrolls the panel which is not in focus
shift return
  makes a new line with same indent as current line, 
  pushes current line down and places cursor at the new lines indent 

ctrl e
  writes or updates a TOC at the top of a notefile ... todo 
    
ctrl shift #
  activates a multiword edit mode on selected area or currentline    
  
ctrl o
  opens filename under cursor
            
ctrl o
  opens path selected or under cursor if not big file, or open dialog if none
  
ctrl p
  pastes current buffers path or cycles back through most-recent-dwell list of buffers
  with shift same but name only

ctrl m 
  sends cursor text to output, with shift opens all output text in a buffer
  sends text to current pos or to line above prompt 
  *arrange for only return on last prompt line to execute in ouput
  so text can be quite safely composed in output, avoiding  

-------------familiar editor hotkeys
  open|save|new file|close     -as common    ctrl + o|s|n|w
  undo|find|copy|cut|paste     -as common    ctrl + z|f|c|x|v
  duplicate           ctrl + d
  cut lines           ctrl + l
  move line up/dwn    alt + up or down
  move word lf/rgh    alt + ctrl + left or right
  comment             ctrl + q
  transpose           ctrl + t
    
missing familiar hotkeys
 select all  ctrl + a  - use ctrl + x instead, which is the cut selection shortcut
                         but when there is no selection, it selects all without cutting.
                         ( this frees ctrl+a for a useful caret movement key )
  revert     ctrl + r  - revert with shift+ctrl+r
  redo       ctrl + y  - redo with shift+ctrl+z
  
modified familiars
  ctrl + o  an existing filename is opened without dialog if one is selected or under caret 
  ctrl + c  if there is no selection to copy, ctrl+c does not copy but instead 
            it re-selects the last pasted or selected text
  ctrl + t  if selection is present it twiddles words or lines in the selection
  ctrl + v  pastes with indent, to paste without indent use ctrl+shift+v
  ctrl + l + shift  
    deletes lines instead of cutting  
  alt + up/down  
    move selection or lines up/down 
  ctrl + shift + up/down or left/right
    expands selection by lines or words


ctrl+u change case of selection or word on caret,

ctrl+t = transponse/twiddle 
  according to selection or none, ctrl+t transposes (swaps,reverses order)
    the pair of letters either side of caret   teh > the
    the letters in a word                      detonated > detanoted  
    the terms in a selection                   selection a in terms the
    terms around = a pivot                     a pivot = terms around
    the lines in a selection             of complete lines
    of complete lines                    the lines in a selection

ctrl+shift+t = sort

---------------------------

# Lusci Hotkey Tricks #

##While Typing On Selection##
  Editors commonly dedicate shortcuts for adding brackets or quotation marks
  at the ends of selected text. Lusci adds brackets
  or quotes to both ends whenever they are typed over selected text.
  Expected editor behaviour is to replace the selected text with 
  any character typed on it. Lusci does this for letters or non-pairing punctuation
  characters. Pairing punctuation marks are added to the ends 
  and selection expands to include them. If delete is typed on a selection
  a character is removed from each end of it. Backspace deletes as normal. 
  >< = selection ends
    >sometext<     pressed = ] or [
    >[sometext]<   pressed = Delete
    >sometext<     pressed = "
    >"sometext"<   pressed = 2
    >2<            ctrl+z ....
    
##State Appropriate Hotkey Responses ##
  Lusci tries to have hotkey functions progress on repeat keypresses 
  eg. the 'line select' hotkey goes on to select paragraphs and blocks 
  on subsequent keypresses and 'word select' hotkey adds words or gaps
  on subsequent presses, in the continuing direction.
  
  The 'sort text' shortcut sorts three ways, into: standard sort order,
  numeric natural order, and random shuffle. It progresses through these 
  orders on subsequent keypresses.
  
  The shortcut 'ctrl+a' jumps to linestart, then lineend, and then
  toggles on subsequent presses. When pressed with shift, it selects
  from current position to linestart, on next press it selects from
  original position to lineend. This may save consuming or reaching for
  a separate hop-to-lineend hotkey. When ctrl+a is pressed without shift
  on a selection, it flips the caret to the other end of selection.
  
  Lusci modifies the standard cut key 'ctrl+x' to perform select_all when it is 
  pressed with no selection. This frees up ctrl+a by replacing the redundant 
  ability to cut an empty selection.
  
  The clipboard can still be cleared for some rare occasion, by ctrl+c on an empty 
  selection. Lusci will also select any text which was last pasted or restore the
  previous selection if ctrl+c on empty selection. Useful for adjusting
  pasted-in blocks, or recovering a selection state that got lost with an
  accidental trackpad touch or keyfuffle.
  
  " While the dilerate edge casing will come as surprise until familiar, 
  their behaviour should neverless fit with the keypresses name.
  So not take long to have as typing reflex. "  
  
  Lusci arranges shortcuts for arrowkeyed selection of words & lines, and moving line or selection
  around by word or line, with a quite consistent combination of the ctrl, shift and alt
  and cursor keys, which also fits the normal expectations that :
  shifted arrows create & maintain selections, and ctrl'ed arrows skip words.

  Luscis word skipping behaviour is more sophisticated than most, words are detected
  by pattern matching so common forms like floating point numbers are recognised.
  When skipping from the start of a word it skips to start of next, when from inside a 
  word or the start of a separator, only the word or separator is traversed. The behaviour is
  bidirectional and maximises ease of moving caret or repositioning selected words
  respecting gaps (which can be a frustrating fiddle in vanilla scite). 
  Luscis wordskip will also 'eat' quotes and brackets if skipping outside them, it
  also 'eats' separating commas etc, if not skipping directly over them. 
  
  A hotkey or terminal commands can adjust brightness, color, contrast and
  flip the theme between light and dark mode. Level of brightness inversion, hue, contrast and gamma
  can be adjusted easily with a terminal command. Favourite configs saved and cycled 
  with hotkey. The ability to instantly adjust to light / dark without going down
  the theme trying choosing and tweaking rabbithole should lead to fewer and more
  refined themes. 
  
  ctrl+tab switches display to the previous Most_Recently_Dwelled file
  ctrl + the_key_above_tab(¬ or ~)
    brings up a file switcher modal which lists recent and nearby files names
    and locations in a quick to read format.
    the list can be cursored-through and entered-on to focus a file,
    pressing the hotkey a second time, displays a different ordering of
    files to switch to, where commonly desired files can also be pinned
    by adding to a config or property file. 

