# Lusci
 
LuSci is an extensive config for SciTE - the Scintilla Text Editor. 

### Overview

* Non-interactive system commands are comfortable to perform in the output pane.
  
* LuSci's terminal (output pane) has numerous built-in commands, which can be increased with basic lua programming experience.

* Many files can be juggled without tabs or session management, with annoyance free file switching and search facilities.
  
* Numerous improvements and additions to common editor hotkeys and navigation features, which can also be customized with modest lua coding experience.
  
* A unique theme is included which can be adjusted on-the-go between dark and light background and shades between, constrast,saturation with a simple terminal command. Or press ctrl+shift+y for default transformation between light and dark configuration.

* Transparent text file encryption to help keep sensitive note files private.

* Developed privately for years, now in the process of fixing up for open source developement.

* Works same on Windows, Linux and Mac, like SciTE 

### This Repo Contents
```
  `dist/`  
      All the lua scripts which lusci requires
  `install/`
      .SciTEUser.properties file and script to run lusci on linux 
  `winstall/`
      SciTEUser.properties file and script to run lusci on windows
  `docs/manual/`
      Hotkey and Command guides
```
    
### Installation

#### Stability

Scite itself is very stable and errors in Lusci tend to be very transient. 
The current state of development should work fairly well on Linux, with Windows requiring a bit more finishing to work fully.

#### Basic separated install
1. Have scite already installed.
2. Extract lusci's directory to somewhere, perhaps into scites installed directory or `~/mylusci`
3. Start scite as a lusci instance by running `./install/run.sh` or `./winstall/run.bat`

or 
#### Install into a fresh scite install
1. Replace the default `.SciTEUser.properties` file with lusci's
2. Add directory `/dist` into scites root directory which may be /usr/share/scite/ on linux
3. Edit `.SciTEUser.properties` to point startup script at `/dist`

Lusci creates a `.filesdb` file and also uses scites session.
  
`.SciTEUser.properties` : Masks default global and user properties for luscis config and colorscheme and theme adjust feature.

`.filesdb` : Contains encoded details of all files lusci has opened
  
#### Contribution

* Lusci is MIT licensed the same as Neil Hodgsons SciTE 
* Feel free to open issues here or use forum for bug, support or suggestions.
  Forum : [lusci.boards.net](https://lusci.boards.net/)

```
----------------------

Draft Keyguide for UK Laptop Layout. 

Other layouts can be set or generated with interactive tool.

# Keyboard Map: Left Side        ( with Ctrl, and Ctrl+Shift below )
 
             [Q]         [W]         [E]         [R]         [T]         [Y]     []  [..
(C )     tog-cmment     close-bf   ln-blk-ends  runluasel   wtiddle      redo
(CS)                   close-all                             sorts     theme-flip
              [A]         [S]         [D]         [F]         [G]         [H]     []  [..
(C )         ln-ends      save     duplicate     find        goto        
(CS)        sel-ends     save as                                         
        [\]    [Z]         [X]         [C]         [V]         [B]         [N]     []  [..
(C )    bkmk   undo      cut/select    copy       paste     mv-to-outpt  new file
(CS)           redo      select-vis             ring-paste              see-endings

#Keyboard Map: Right Side

  ..]  []   [U]        [I]        [O]        [P]         [{]        [}]
(C )       cases     rpt-wrd     open   filepth-cpy   listjmp-dwn  listjp-up
(CS)      linejoin   rpt-wdbk              print      listhead-dw  listhd-up 
             
    ..]   []    [J]        [K]         [L]         [;]        ['@]        [\#] 
(C )          jump-dwn   jump-up     doc-grep   sel-line/s     del      last-edit       
(CS)          out-jmdwn  out-jmpup   dir-grep   sel-block              mult-wrd-edit
             
      ..]    []    [M]           [,]           [.]         [/]         
(C )            quick-cmd     out-switch    sel-word/s     copy  
(CS)                           out-shrk                    paste
 
  
Shift,Ctrl,Alt With Arrows:

{shift} selects   {ctrl} hops over terms   {alt} moves text
  These controls work in combination with the {up/down/left/right} keys
  except the combinations:
{ctrl up/down  and  alt+ctrl up/down} - These scroll without moving caret,
  and 
{shift+alt+arrows} - These modify selection to 'column mode' (a useful mode)

alt+lft/rt       Moves subline-sel by character, indents if no sel or multiline
ctrl+alt+lft/rt   Selects and moves by word and gap
shift+ctrl+lft/rt  Expands selection by word or gap
shift+ctrl+up/dwn  Expands the selection to line starts 
                   (to retract an over-expansion, use shift without ctrl)
Also
ctrl+a  hop caret between selection ends (or line ends if no selection)
crtl+.  select current word     ctrl+; select current line

ctrl+backspace  , ctrl+delete         delete to word ends
ctrl+shift+bckspc , ctrl+shift+del    delete to line ends

ctrl+pgup/dwn          scroll to head/tail of file
shift+ctrl+pgup/dwn    select to head/tail of file 

Lusci navigates these kinds of terms as whole words:
  5.1279324493014e-08 -523457.432857 0xff 0xCAFEBABE 
  regular  εξωτικός  o'clock  $symbol  #symbol
  "quotes from outside" , ( 'and brackets' ) {etc (from outside)}

Ways to select,copy,cut or delete a word
  ctrl+.         selects word around current position or selection
  ctrl+/         copy word around current position or selection
  
  ctrl+shift + left then right   
    expands a selection to left then right ends of word
    
Ways to select,copy,cut or delete a line or lines
  ctrl+'       deletes the current line or selection
  ctrl+;       selects line/s  cumulatively
  
  ctrl+shift+up or down   
    expands selection to beginning or end of line
      
Familiar editor hotkeys
  open|save|new file|close     -as common    ctrl + o|s|n|w
  undo|find|copy|cut|paste     -as common    ctrl + z|f|c|x|v
  duplicate            ctrl + d
  move line up/dwn     alt + up or down
  move word lf/rgh     alt + ctrl + left or right
  comment              ctrl + q
  transpose            ctrl + t
      
Modified familiars
  ctrl + o  an existing filename is opened without dialog if one is selected or under caret 
  ctrl + c  if there is no selection to copy, ctrl+c does not copy but instead 
            it re-selects the last pasted or selected text
  ctrl + t  if selection is present it twiddles words or lines in the selection
  ctrl + v  pastes with indent
  ctrl+shift+v   paste cycles through previously copied clips 
  alt + up/down    move selection or lines up/down 
  ctrl+shift + up/dwn/lft/rght   expands selection only by lines or words

Missing familiar hotkeys
  ctrl + a  select all - use ctrl + x instead, which is the cut selection shortcut
                         but when there is no selection, it selects all without cutting.
                         ( this frees ctrl+a for a useful caret movement key )

Rapid Hotkey Intro

Lusci default config and hotkeys is intended to work without using mouse or menus,
but also to not clash with them. It is intended to enable deft vim or emacs style 
operation without losing basic text editor usage.

Lusci Intro Hotkeys
  caret hop to other end of line or selection        ctrl + a
    hops to line start, or if already at the start to line end
  caret seek to same word down|up                    ctrl + j | k
    when with shift it seeks in the other pane
  return caret to last editted position              ctrl + #
    for when place is lost scrolling
  word select (and grow)                             ctrl + .
  line select (and grow)                             ctrl + ;
  repeat a nearby word                               ctrl + i
    (cycles through near words, with shift cycles back)
  toggle focus between editor and terminal           ctrl + ,
  
Routine Hotkeys
  
  make jump list of occurences in file               ctrl + l
  make jump list of occurences in directory          shift + ctrl + l
    these employ lusci commands list and spot in the
    terminal. these commands can also do replacements
  goto file and line in jump list  (dwn,up)          ctrl + [ , ctrl + ]
    this will jump to file and line positions listed in the terminal
    by luscis list, command, grep and compiler notices 
    
  switch to most recently dwelled file               ctrl + tab
  bring up file-switcher modal                       ctrl + `

Extra Shortcuts
  toggle theme to previous pallet                    {ctrl + shift + y}
    the themes pallet can be instantly adjusted from light to dark and 
    contrast tweaked etc. in the term with the pallet, command.
  multi-type mode                                    {ctrl + shift + #}  
    starts multitype on selection or current line
    normal typing changes all matching words in highlighted area
    change all matching characters or wordparts by selecting
    them before typing over selection
    multi-type mode avoids accidental changes by tracking
    current match positions, it works as a handy live replace feature
  insert at selection ends                         {space,braces,quotes,delete}
    typing space,brackets,braces,quotes on a selection inserts them
    at ends and includes them in selection, delete key removes
    a characters at each end. Backspace deletes selection as normal.
  scroll screen without moving cursor.                 {ctrl + up or down} 
    To bring the cursor into a scrolled screen
    press up or down without ctrl. any other keys including left and right cursor
    will return the screen to the cursors position.
  scroll the panel not in focus                     {ctrl + alt + up or down}
    scrolls the panel which is not in focus
  Scite's virtual space selection (very handy) -     {alt + shift + arrows}

Keyboard Other
  shift + enter
    new line with indent, (push current line down)
  ctrl + enter
    autocompletion list
    
Word Hopping

For the purposes of ctrl-arrow caret movement and selection, lusci determines 
words differently to other common editors. For example if moving into brackets 
or quotes lusci will hop or select to the matching far bracket.
To move over terms within brackets it is necessary to first cursor inside them.
Also when moving terms by word steps (with the alt key in combination) lusci
hops not only individual words but also the space or separators following them.
When direction is changed a single word or separation sequence only is hopped.
(the state of move direction is represented in which end of the selection the caret
is arranged to be in, or which end of word it is at when there is no selection)
This combination of word+space hop and word-or-space hop, can assist moved words 
(or selected phrases) to be finegaled into correct place.
In practice it should be more intuitive than this note describes and sometimes
cutting or retyping would have been faster, but so it is too with common editor 
ctrl-arrow word hop arrangements.
  
Modified Hotkeys

ctrl tab
  cycles back through most recently used buffers, but does not mark the newly opened 
  buffers as visited, unless dwelled for a while or caret is moved.
  eg. to ctrl-tab back after a sequence to the originating buffer, just move the caret
  and next ctrl tab will return to it. The caretted/editted file will then be the penulimate
  file in the visitation list. This is the best MRU file switching system, it keeps the MRUs
  very nicely.

ctrl \
  sets a bookmark if none nearby or jumps to nearby one
  when pressed on bookmark jumps through most recently dwelled bookmarks throughout files
  
ctrl shift \
  unsets bookmark if on one or jumps to nearby one
  if no bookmark nearby jumps to prev in reverse order
  
ctrl #
  this returns the cursor to the positions which were last edited in the document

ctrl j or k 
  hops down(j) or up(k) to the next occurence of the word under the cursor
  (these are common down/up keys in terminal tools) 
  
ctrl `
  shows a userlist of open and recent files, up down and enter or mouseclick switches to
  chosen file. The list indicates which files are dirty and have bookmarks and is quite 
  well formatted and sorted for quick file selection. non cursor or enter key or clicking 
  outside the list closes it without switching. 
  ctrl ` twice can bring up a second list of files which are read from the top of
  a file dedicated for this, set in sciteuserproperties. It can contain common project
  or note files.
  
ctrl a 
  'endhop key' hops between ends of line text or selection.
  moves cursor to the beginning of current lines text after any indent.
  if pressed in that place already it, moves to the end of the lines text
  so 1 press > go start of line, 2 presses > go end of line
  if pressed with a selection it swaps caret and anchor
  
ctrl shift a 
  same as ctrl a but *selects* from cursor to line ends alternately
  so 1 press > select from current to start of line,
     2 press > select from current to end of line

ctrl x
  cuts when a selection is present, when no selection nothing to cut so it selects all
  so 2 presses = select all and cut
ctrl c
  copys selection, when no selection present, it reselects the previous selected or pasted text
ctrl v
  modified to paste text at the cursors indent level,
  also if pasted on top of a selection it copies the selection
  so remember to delete target selections if they should not replace cliptext 
  ctrl shift v pastes normally

ctrl ;
  selects line text without indent, then whole line, then block, then larger block
  also expands a multiline selection to whole lines that is not already whole
  on rare occasion it borks block selection, wants rewrite.
ctrl ,
  progessive word select
ctrl l
  cuts the lines spanned by the selection or just the current line if no selction
  (expands selection to full lines and cuts)
  ctrl shift l does same but just deletes the lines without replacing clipboard
ctrl t twiddle extended
  with no selection twiddles lines as usual
  with selection twiddles around punctuation in the selection, such as ==
  or a coma or space. Mainly for flipping terms, can also cycle through comas
  or spaces.
ctrl shift t
  sorts selected lines or words in selection, to standard and natural sort order
brackets and quotes
  pressing these keys when there is a selection adds the character to the ends of 
  the selection instead of the normal behaviour which is to delete and replace
  the selection. This gives a quick way to put terms in quotes and brackets.
  Delete (not backspace) also deletes the characters at the ends of the selection
  Backspace deletes the selection as normal.
ctrl i
  repeats preceeding word. on subsequent presses it changes to next nearest and so on
  shift ctrl, cycles back in case you tap past one.
  (most useful for just repeating a last word, or another nearby.)
ctrl up or down    
  Scrolls screen without moving cursor. To bring the cursor to a scrolled screen
  press up or down without ctrl. any other keys including left and right cursor
  will return the screen to the cursors position.
ctrl alt up or down
  scrolls the panel which is not in focus
shift return
  makes a new line with same indent as current line, 
  pushes current line down and places cursor at the new lines indent 
    
ctrl shift #
  activates a multiword edit mode on selected area or currentline    
              
ctrl o
  opens path selected or under cursor if not big file, or open dialog if none
  
ctrl p
  pastes current buffers path or cycles back through most-recent-dwell list of buffers
  with shift same but name only

ctrl m 
  sends cursor text to output, with shift opens all output text in a buffer
  sends text to current pos or to line above prompt 
  *arrange for only return on last prompt line to execute in ouput
  so text can be quite safely composed in output, avoiding  

ctrl u change case of selection or word on caret,

ctrl t = transponse/twiddle 
  according to selection or none, ctrl+t transposes (swaps,reverses order)
    the pair of letters either side of caret   teh > the
    the letters in a word                      pupils > slipup  
    the terms in a selection                   selection a in terms the
    terms around = a pivot                     a pivot = terms around
    the lines in a selection             of complete lines
    of complete lines                    the lines in a selection

ctrl shift t  Cycles order of lines or words: Standard sort,natural sort,random

ctrl shift r  Run the selected part of a lua file or the tail end of a file 
              below last `---` mark, for testing lua snippets.

ctrl shift y  Toggles the previous theme pallet (default flips light to dark)

-----------------------------------------------

# Lusci Hotkey Tricks #

##While Typing On Selection##
  Editors commonly dedicate shortcuts for adding brackets or quotation marks
  at the ends of selected text. Lusci adds brackets
  or quotes to both ends whenever they are typed over selected text.
  Expected editor behaviour is to replace the selected text with 
  any character typed on it. Lusci does this for letters or non-pairing punctuation
  characters. Pairing punctuation marks are added to the ends 
  and selection expands to include them. If delete is typed on a selection
  a character is removed from each end of it. Backspace deletes as normal. 
  >< = selection ends
    >sometext<     pressed = ] or [
    >[sometext]<   pressed = Delete
    >sometext<     pressed = "
    >"sometext"<   pressed = 2
    >2<            ctrl+z ....
    
##State Appropriate Hotkey Responses
  Lusci tries to have hotkey functions progress on repeat keypresses 
  eg. the 'line select' hotkey goes on to select paragraphs and blocks 
  on subsequent keypresses and 'word select' hotkey adds words or gaps
  on subsequent presses, in the continuing direction.
  
  The 'sort text' shortcut sorts three ways, into: standard sort order,
  numeric natural order, and random shuffle. It progresses through these 
  orders on subsequent keypresses.
  
  The shortcut 'ctrl+a' jumps to linestart, then lineend, and then
  toggles on subsequent presses. When pressed with shift, it selects
  from current position to linestart, on next press it selects from
  original position to lineend. This may save consuming or reaching for
  a separate hop-to-lineend hotkey. When ctrl+a is pressed without shift
  on a selection, it flips the caret to the other end of selection.
  
  Lusci modifies the standard cut key 'ctrl+x' to perform select_all when it is 
  pressed with no selection. This frees up ctrl+a by replacing the redundant 
  ability to cut an empty selection.
  
  The clipboard can still be cleared for some rare occasion, by ctrl+c on an empty 
  selection. Lusci will also select any text which was last pasted or restore the
  previous selection if ctrl+c on empty selection. Useful for adjusting
  pasted-in blocks, or recovering a selection state that got lost with an
  accidental trackpad touch or keyfuffle.
  
  " While the diliberate edge casing will come as surprise until familiar, 
  their behaviour should neverless fit with the keypresses name.
  So not take long to have as typing reflex. "  
  
  Lusci arranges shortcuts for arrowkeyed selection of words & lines, and moving line or selection
  around by word or line, with a quite consistent combination of the ctrl, shift and alt
  and cursor keys, which also fits the normal expectations that :
  shifted arrows create & maintain selections, and ctrl'ed arrows skip words.

  Luscis word skipping behaviour is more sophisticated than most, words are detected
  by pattern matching so common forms like floating point numbers are recognised.
  When skipping from the start of a word it skips to start of next, when from inside a 
  word or the start of a separator, only the word or separator is traversed. The behaviour is
  bidirectional and maximises ease of moving caret or repositioning selected words
  respecting gaps (which can be a frustrating fiddle in vanilla scite). 
  Luscis wordskip will also 'eat' quotes and brackets if skipping outside them, it
  also 'eats' separating commas etc, if not skipping directly over them. 
  
  A hotkey or terminal commands can adjust brightness, color, contrast and
  flip the theme between light and dark mode. Level of brightness inversion, hue, contrast and gamma
  can be adjusted easily with a terminal command. Favourite configs saved and cycled 
  with hotkey. The ability to instantly adjust to light / dark without going down
  the theme trying choosing and tweaking rabbithole should lead to fewer and more
  refined themes. 
  
  ctrl+tab switches display to the previous Most_Recently_Dwelled file
  ctrl + the_key_above_tab(¬ or ~)
    brings up a file switcher modal which lists recent and nearby files names
    and locations in a quick to read format.
    the list can be cursored-through and entered-on to focus a file,
    pressing the hotkey a second time, displays a different ordering of
    files to switch to, where commonly desired files can also be pinned
    by adding to a config or property file. 


Future hotkeys:   
Adjust size of terminal pane  
    The SciTE platform itself will need updated to enable this. 
    Until then it is required to mouse drag to adjust terminal size
 
ctrl + shift + x
  select visible lines, may be useful for limited replacement operations ? 
  
hotkey on a jump-line in terminal to update the line in file to current state
  would be powerful, confirmation of action printed above the current line
  line path detection will need to be improved before performing this.
  if fullpath etc is in openfiles and dirty 
  if file is open and dirty, focus the file
  responses printed as comments below the cursored jumpline 

====================================================================================

Terminal Commands Brief  (draft)
  
  `cd` works in the terminal as normal.
  
  Simple shell commands work when typed plainly into terminal.
  Their output doesnt show until the command completes.
  `run,` can be used for system commands with output which scrolls, like compilation.
  `sudo,` can be used on linux, lusci asks for and uses an asterix hidden password
  `ssave,` saves current file as sudo without changing ownership or permissions
  
  Hit `enter` on empty prompt to cycle to current buffers directory and through MRUs
    
  No interactive shell commands work. Lusci will hang on interactive commands
  but terminating them with a process manager unhangs the editor.
  
  All lusci commands are marked with a comma, only the first two letters
  need typed.
  
  new, Makes a new file and opens it. Fails if file already exists
  
  open, Opens an existing file, Fails if doesnt exist
  
  ssave, Saves current document when sudo permissions are required preserving ownership
  
  lua, Runs a lua statement such as a lusci global function 
    eg. lu,filterblankline() --will remove blank lines in selection
  
  lp, Prints return of a lua statement, trys adding return statement if none present
    eg. lp,math.log(16)/math.log(2)
        lp,a="abcdef" return a:sub(2,-2)

  To run multiple lines of lua :
  [ctrl+shift+r] - Runs selected text as a lua snippet, or if no selection runs the tail
     end of the current buffer that is marked by the last 4 dashes ----
     This allows writing lua snippets in a file which can be run quickly
     without commenting out all of the preceeding snippets.
  
  focus, Prints list of stats of currently opened buffers, which can be navigated and pruned
    eg. fo,partname  Lists open buffers with filepaths containing 'partname'
                     If only one buffer matched then it is focused immediately
    The formatted list of files enable quick focusing and closing of files
    The filepath match also matches up to three parts separated by spaces      
      eg. fo,three path parts
    
  fd, Same as focus but for all files which have ever opened in the current lusci config
  
  undo, Redo, un/redo editor from output 
  clr, Clears output
  
  save,  Saveas
  osave, Save output as
  csave, Save clipboard as
  move,  Renames the currently open file on disk and in editor database
    
  pallet, Flip luminosity of theme keeping colours similar
        Allows instant change between white, light, dim, dark and black background    
  sp, and li, (spot, and list,)
    spot and list are lusci's two primary file navigation commands
    These are both find in file commands, they list out the lines in the terminal
    which contain the search hits. The list can then be scanned by eye
    and each line and file can be opened in the editor by clicking or hitting enter
    on the line, or by using the keypress 'ctrl+[' or 'ctrl+]' which steps through
    each line and file in sequence in the editor.   
    list, Searches only the current file, spot, searches files in the directory
    They are commonly triggered by keypress ctrl+h for list, ctrl+shift+h for spot
    The keypresses do a listing for the word under the cursor or the current selection
    The keypresses just prepare the command in the terminal and run it
    These commands can also be entered and controlled manually in the terminal
    They both have a fast basic syntax and by default do case insensitive plain 
    Text word searches:
      li,pin   [list word bound occurences of 'pin' in current file ]
      li,pin~  [same but not whole word search, will also hit eg. 'spin']
      sp,pin   [lists the term 'pin' (as word bounded) in files under the current
                directory in the terminal. ~ at the end would also search as non-word ]
      sp,* pin [patterns beginning with * after the comma are taken as the filepatterns
                to include in the search. If no filepattern supplied command automatically
                limits search to currently displayed documents extension ]
      Sp,*.h *.cxx *makefile SprocketB
               [lists case sensitive "SprocketB" in files names *.h *.cxx *makefile 
                Case sensitivity is specified by uppercasing the command (List, or Spot)]
                
      The spot command is performed by grep on linux and findstr in windows. Lusci
      prepares the appropriate command. Options can also be included after file patterns eg.
      Sp,*.lua -G -B3 while.*do
               [greps lua files for the regex term "while.*do" lists B3 lines per hit]
    
    List can also perform text replacements in current file and in multiple files
    More documentation on list and spot to follow...
  
----------------------------------------------------------------------
Next revision of 'lister' ready soon...

Lister 
Luscis list and replace command performs operations using a shorthand notation
that is very simple for common usage, and progressively a little more 
obscure for rarest usage.
It performs listings and replacement of words or patterns in current or multiple files
The List syntax is punctuated by the comma normally. 

Basic operations: 
  li,findthis            -- lists wholeword 'findthis' in selected text or current file
  li,zap,,               -- replaces "zap" with what follows ,, (here it deletes) 
  li,fixthis,,fixedit    -- simply replaces 'fixthis' with 'fixedit' in current text
  Li,FixThis,,FixedIt    -- Capital LI, to match Case Sensitive ( and replace )
  Li,ixThis,,ixedIt,     -- put a comma at the end to match wordpieces
  li,day,                -- will list MonDay TuesdaY etc. 
  Li,Day                 -- will only list Day
  li,oo,,,               -- deletes all oo from wood brood mood shrood...

More:
The default escape is the comma, it can be changed to any of the 
following 6 characters: ,./;'` by putting it immediately at the 
start of the instruction. 
If the find/replace terms contain commas, change the escape to one of the other 5
 . / ; ' ` (these should be unshifted and live close on the keyboard)
If any of those characters is placed before the find term, it becomes the escape for 
that instruction
  li,/,//, /            -- puts a space after all commas in doc, escape is /
  li,,.txt,,.doc        -- need to set comma as escape here or . would be set
  li,.txt,,.doc         -- almost replaces "txt,," with "doc" (error)

The magic term "bird" ~<> following find/replace text, enables multiple operations 
in one line: 
  li,red ~<>,yellow ~<>,grey                         --three find operations
  li,red,,pink ~<>,/yellow//green ~<>,.grey..silver  --three replace operations

The "bird" term can allow options to be set for the find/rep which precedes it:
   
Tildes on the right set alternative match types:
  li,[wh?][ou]?man,,be a person ~<>~   --basic lua pattern find (and plain replace)
  li,([wh?][ou]?man),,be a %1 ~<>~~    --basic lua pattern with captures in replace
  li,(man|woman|human),,be a %1 ~<>~~~ --lua pattern with 1 capture with | separated alts 
@ at signs on the right makes the operation run on multiple files
  li,red,,pink ~<>@5  --replaces word red with pink in the 5 most recent files
  li, ~<>@3           --just prints 3 last most recent filepaths
  li,red,,pink ~<>@@12 --performs replace on 12 most recent filepaths printed in terminal
  li, @@4              --prints 4 most recent filepaths printed in the terminal
  
li,(red|blue|green|yellow),,%1ish ~<>@3~~~ --replaces into redish,blueish,greenish..
                                           --in 3 most recently dwelled files
  
S for save makes the operation save each file after find and/or replaces 
d for dryrun, reports replaces without performing them
s to search and replace within selections only
a to search and replace ignoring selections only
(by default search replace uses selection if it is larger than 2 lines.)

The last bird option applies to all the preceeding find/replace unless it is followed
by a comma.
  li,red,,pink ~<>,/yellow//green ~<>,.grey..silver ~<>@3d  --dryrun all
  li,red,,pink ~<>,/yellow//green ~<>,.grey..silver ~<>@3d, --dryrun only last


---------------------------------------------------------------

File Encryption, lucy enables files to be encrypted, then opened and saved transparently
using a global password defined in config, or unique password typed in. 

  lu,mork("apass") -- Current buffer will be transparently encrypted with password "apass"
  lu,unmork() -- current buffer will no longer be encrypted
  lu,mork() -- If no password, encrypt by the password set in lusci property
  d,thepass -- Supplies (non default) password for scrambled file
  d, -- Leave empty for prompt to supply hidden as ****'s

```