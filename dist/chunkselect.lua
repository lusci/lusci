--growblock hotkey
--grows a selection to block boundaries
--then parent blocks boundaries
--alternately expands top and bottom of selection
--so that expansion does not occur offscreen
--always expands above current block before
--completing

--growline hotkey
--expands an existing multiline selection to whole lines with lineends 
--or pressed without selection it selects line of text excluding the 
--indentation and lineend
--two presses selects whole line including lineend
--three presses selects chunk of lines without gaps and same indent
--four presses selects most lines on screen

local function printl(l,p)
  p=p or editor
  trace(linetxt(p,l))
end

local function listlevels()
  for i=0,editor.LineCount do
    local t=editor:textrange(poslinestfn(i,pan,0,i))
    print(""..(i+1).." "..(editor.FoldLevel[i]&0x0FFF).." "..t:sub(1,20))
  end
end

local function linetype(pan,ln,cmp,den)
  local lex=pan.Lexer
  local dlst = {  --distinct linestyles 
    [0]={1,2} --unset langs
   ,[3]={1,2,3,9} --c 
   ,[17]={1} --conf
   ,[15]={1,2,3,8} --lua
  }

  local a,e=poslinestfn(false,pan,false,ln)
  local dent,tx=pan:textrange(a,e):match("^([, \t]*)([^\r\n]?)")
--tracerr(ln+1,a,e,dent,tx)
  if tx=="" then 
    return not (cmp and cmp~=-1) and not (den and den~=0),-1,0 
  end
  
  local dnt,tabs=string.gsub(dent,"[\t]","")
  tabs = tabs+math.floor(string.len(dnt)/pan.TabWidth) 

  local cstyle=pan.StyleAt[a+#dent]
  local dstyles = dlst[lex] or dlst[0]
  local ds = dstyles[cstyle] or 0

  return not (cmp and cmp~=ds) and not (den and den~=tabs), ds, tabs
end

local function runline(pan,ln,dir,skipgap,blknt)

  local pass,ltb,ntb = linetype(pan,ln)
  local lt,nt=ltb,ntb
 
  repeat
    ln=ln+dir
    pass,lt,nt = linetype(pan,ln,ltb,blknt and ltb<1 and ntb) 
--  tracerr(ln+1,pass,ltb,lt,ntb,nt)
    if not pass and skipgap and lt==-1 and linetype(pan,ln+dir,ltb) then 
      pass=true 
    else
      ltb=lt
    end
    if pass and blknt and editor.FoldLevel[ln]&SC_FOLDLEVELHEADERFLAG ~= 0 then 
      pass=false 
    end
    
  until not pass or ln==0 or ln==pan.LineCount-1 or overloop()
  
  if not pass then ln=ln-dir end 
  return ln
end

function growlines(cpy)
  local pan,b,d,rvs,a,e = get_pane()
  local oa,oe=a,e
  local retract=false
  --discernments
  -- b line, first pos, indented pos , end pos
  local bla,ble,lb = poslinestfn(b,pan)
  local btarg=bla
  local dtarg=poslinestfn(d,pan)
  if d~=dtarg then dtarg=poslinestfn(d,pan,1) end
  
  local bdent,btx = (pan:textrange(bla,ble)):match("^([%s]*)([^\r\n]?)")
  local ind=bla+#bdent
  -- dline first pos
  local dla,dle,ld = poslinestfn(d,pan)
  local dlx=poslinestfn(d,pan,1) 
  dlx=dlx<dle and dle or dlx --case of last line
  
  if     lb==ld and b~=ind and not (b==ind and d==ble) then --square to indent 
    oa,oe=ind,ble  retract=true --prntb("p")
  elseif lb==ld and b<=ind then --square to line anchors
    oa,oe=bla,dlx  retract=true --prntb("q")
  elseif b<btarg or d<dtarg then --square to line anchors
    oa,oe=btarg,dtarg  retract=true --prntb("qqq")
  
  elseif b>bla or d>dla then tracerr("b>bla or d>dla ??")
    if d>dla then d=dlx end  --dunno what this is about
    if b>bla then b=bla end  --
    oa,oe=b,d 
  else  --multiline expansion
    local selscreen=true
    if lb==ld-1 then
      selscreen=false 
      local tlb=runline(pan,lb,-1,false,true)  --nogaps, same ind
      local tld=runline(pan,lb,1,false,true) --tracerr()
      if tlb==tld then 
        if tlb>0 then tlb=tlb-1 else selscreen=true end
      end
      oa=poslinestfn(_,pan,0,tlb)
      oe=poslinestfn(_,pan,0,tld+1)
    end
    if selscreen then
      lb,ld=doclinesvis(pan)
      oa=poslinestfn(_,pan,0,lb+1)
      oe=poslinestfn(_,pan,0,ld-2)
    end 
  end
  
  pan:SetSel(oa, oe)
  recordselnow(pan)
  if cpy then 
    pan:Copy() 
    if retract then pan:SetSel(a,e) end
  else
    recordselnow(pan)
  end
  return true
end


local function runoutblock(pan,ln,di,lva,fla)
  local lvc=lva or pan.FoldLevel[ln]
  local flc=fla or lvc&SC_FOLDLEVELHEADERFLAG~=0
  lvc=lvc&0x0fff
  
  if flc then
    if di<0 then return ln,lvc,flc
    else lvc=lvc+1 end
  end
  
  local e,lvd,fld,pass = pan.LineCount-1
  if ln+di<0 or ln+di>e then
--  print("!",ln+di,lvc,flc) 
    return ln,lvc,flc 
  end
  
  repeat
    ln=ln+di
    lvd=pan.FoldLevel[ln] 
    fld=lvd&SC_FOLDLEVELHEADERFLAG~=0  lvd=lvd&0x0FFF
    if lvc==1024 then 
      pass = lvd==1024 and not fld
    else
      pass = lvd>=lvc or di<0 and fld and lvd==lvc-1
    end
  until not pass or ln==0 or ln==e or overloop()
  
  if not pass then ln=ln-di end
  lva=pan.FoldLevel[ln]
  return ln, lva&0x0FFF, lva&SC_FOLDLEVELHEADERFLAG
end 

function growblock(cpy)
  local pan,b,d,rvs,a,e,bl,dl,dlt,pe = get_pane()
  local lc=pan.LineCount-1
  
  local aln,alv,alf = runoutblock(pan,bl,-1)
  local eln,elv,elf = runoutblock(pan,dl,1)
  
--print("lns",bl,dl,"lvs",alv,elv,"flds",alf,elf,"lns",aln,eln)
  if bl==dlt and bl==dl and b~=pan:PositionFromLine(bl) then --just expand to line
  elseif elv>1025 and aln==bl and eln==dl then --full expand if didnt grow inblock
    aln,alv,alf = runoutblock(pan,bl,-1,elv-1)
    eln,elv,elf = runoutblock(pan,dl,1,elv-1)
  elseif aln==0 then
    eln,elv,elf = runoutblock(pan,tain(dl+1,0,lc),1)
  elseif aln==bl and (eln==dl or eln==dlt) then
    aln,alv,alf = runoutblock(pan,tain(bl-1),-1)
  end
  
  oa=poslinestfn(nil,pan,0,aln)
  oe=poslinestfn(nil,pan,0,eln+1)
    
  if oa~=b or not rvs and oa==b and oe==d then
    oe=d  oa,oe=oe,oa
  end
   
  pan:SetSel(oa, oe)
  recordselnow(pan)
  if cpy then 
    pan:Copy() 
    if retract then pan:SetSel(a,e) end
  else
    recordselnow(pan)
  end
  return true
end
