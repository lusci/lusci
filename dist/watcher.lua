--[[

  Fileswatch
  maintains `filesdb` and `openedfiles`

--]]

-- persistent global values are messed up here.
-- local vars are persistent
-- global vars reset on lua context switches (buffer switch mainly)
-- global table `Lc_` is used to keep persistent vars that might be accessed anywhere
-- most of them are redundant tho ...
--~ local function debu(c) print(c) return end

local Sl = Lc_
local buftim=Lc_.buftim

local filesdb=lc_init('filesdb',{}) --to register as local and global,
local chngdfls=lc_init('chngdfls',{})
local fpstack=lc_init('fpstack', new_stacket(12) )
--~ local fpstack= fpstack or new_stacket(12)

local ffolder,fstatfile = "", GTK and '.LuSciDB' or 'LuSciDB.txt' 
if #props['LuSciDBFile'] > 1 then fstatfile=props['LuSciDBFile'] end
fstatfile,ffolder=matchout(fstatfile,"^(.+)[\\/]")
if ffolder=="" then ffolder=props['SciteUserHome'] end
local fstatpath=ffolder..Lc_.platsep..fstatfile

lc_init('afterclose', false   )
lc_init('opentime', os.time() )
lc_init('recordch', 0 )
lc_init('detailch', 0 )
lc_init('extrarec', 0 )
lc_init('tndg', 0     )
lc_init('tabtime',  0 )
lc_init('tabnumb',  1 )

lc_init('curftime', 0 )
lc_init('curfpath', "" )
lc_init('bbtime',  0 )

local ssub, slen, match = string.sub, string.len, string.match
local insert, floor = table.insert, math.floor

--Lua:print(string.format("<%-10s>", "goodbye")) leftpad 10, no - rightpad
local function strpack(str,len,right)
  local sln=#str or 0
  len = math.floor(len)
  if len<sln then
    local cutl=math.floor((sln-len)/2)
    local cutr=sln-len-cutl
    local cutp=math.floor(sln/2)
    return ssub(str,1,cutp-cutl-2).."(~)"..ssub(str,cutp+cutr+2)
  else
    if not right then
      return str..string.rep(" ",len-sln)
    else
      return string.rep(" ",len-sln)..str
    end
  end
end
  
local function fstatrow( diffs, size, saves, dwelltime, alphtime , ulttime )
  return 
    {['diffs']       = diffs or 0
    ,['size']        = size or 0
    ,['saves']       = saves or 0
    ,['tottime']     = floor(dwelltime or 0) 
    ,['alphtime ']   = floor(alphtime or os.time()) 
    ,['ulttime']    = floor(ulttime or os.time()) }
end

function movestat(str,nobak,nopat)
 
  local fpath=string.match(str,"([^,]+),")
  local apath=string.match(str,",([^,]*[^%s])%s*")
  if fpath==apath or not 
    (string.find(fpath,"^%a?:?[\\/][^,%*]*$") 
    and string.find(apath,"^%a?:?[\\/][^,%*]*$") ) 
  then
    print("\n- Moving "..fpath.."* to "..apath.."*")
    print("\n- Wont move paths (terms seem invalid)")
    
    return
  end
  if not nopat then
    print("\n- Moving "..fpath.."* to "..apath.."*")
  end
  
  if not nobak then 
    print("\n- Backup saved to "..fstatpath..".bak")
    saveallrec(".bak") 
  end
  
  if nopat then
    filesdb[apath]=flatcopy(filesdb[fpath])
    filesdb[fpath]=nil
  else 
    for fn,sr in pairs(filesdb) do
      local pfp=string.gsub(fn, fpath, apath)
      if pfp~=fn then
        if not filesdb[pfp] then 
          filesdb[pfp]=flatcopy(filesdb[fn])
          print(pfp..":1: Was "..fn)	
        else
          print( fn..":1: Ate "..pfp)
          filesdb[pfp].diffs=filesdb[pfp].diffs+filesdb[fn].diffs
          filesdb[pfp].saves=filesdb[pfp].saves+filesdb[fn].saves
          filesdb[pfp].tottime=filesdb[pfp].tottime+filesdb[fn].tottime
          filesdb[pfp].alphtime =filesdb[fn].alphtime 
          filesdb[pfp].ulttime=filesdb[fn].ulttime
          filesdb[pfp].size=filesdb[fn].size or 0
        end
        filesdb[fn]=nil
      end
    end
  end
  saveallrec()
end 

--format details forthe reading and writing routines
local g= { "diffs",9,"size",9,"saves",5,"tottime",9,"alphtime ",10,"ulttime",11 }
local gx={ 1,g[2], 
           1+g[2],  g[2]+g[4], 
           1+g[2]+g[4],  g[2]+g[4]+g[6], 
           1+g[2]+g[4]+g[6],  g[2]+g[4]+g[6]+g[8],
           1+g[2]+g[4]+g[6]+g[8],  g[2]+g[4]+g[6]+g[8]+g[10], 
           1+g[2]+g[4]+g[6]+g[8]+g[10],  g[2]+g[4]+g[6]+g[8]+g[10]+g[12] } 
local s=[[                    ]] --20 s
--

function saveextrarec()
  if toofrequent(6) then return end
  local tout={ "diff     size     save dwell    first     last       path\n" }
  local cnt=0
  for pth,v in pairs(filesdb) do 
    if chngdfls[pth] then
      cnt=cnt+1
      insert( tout,
        table.concat( 
        { v[g[1]] , ssub( s,1,g[2]-slen(v[g[1]]) ) , 
          v[g[3]] , ssub( s,1,g[4]-slen(v[g[3]]) ) , 
          v[g[5]] , ssub( s,1,g[6]-slen(v[g[5]]) ) , 
          v[g[7]] , ssub( s,1,g[8]-slen(v[g[7]]) ) , 
          v[g[9]] , ssub( s,1,g[10]-slen(v[g[9]]) ) , 
          v[g[11]], ssub( s,1,g[12]-slen(v[g[11]]) ) , pth ,"\n" } ) 
      )
      chngdfls[pth]=false
    end
  end
  chngdfls={} Sl.recordch=0  Sl.detailch=0 
  if cnt==0 then return end
  ModeWriteD(fstatpath, table.concat(tout), "a+")
end
                           
function saveallrec(bak)
  if toofrequent(6) then return end
  bak=bak or ""
  local tout={ "diff     size     save dwell    first     last       path\n" }
  for pth,v in pairs(filesdb) do 
    insert( tout,
      table.concat( 
      { v[g[1]] , ssub( s,1,g[2]-slen(v[g[1]]) ) , 
        v[g[3]] , ssub( s,1,g[4]-slen(v[g[3]]) ) , 
        v[g[5]] , ssub( s,1,g[6]-slen(v[g[5]]) ) , 
        v[g[7]] , ssub( s,1,g[8]-slen(v[g[7]]) ) , 
        v[g[9]] , ssub( s,1,g[10]-slen(v[g[9]]) ) , 
        v[g[11]], ssub( s,1,g[12]-slen(v[g[11]]) ) , string.gsub(pth,"%c","") ,"\n"}  ) 
      ) --hack with gsub to stop fnames with newlines getting stuck in database
        --better to stop them getting in... 
    chngdfls[pth]=false
  end
  ModeWriteD(fstatpath..bak, table.concat(tout), "w+")
  chngdfls={} Sl.extrarec=0 Sl.recordch=0 Sl.detailch=0 
  --print('saved all')
end

local function readfstatable()
  --print("\ntableread")
  local e = io.open(fstatpath)
  if not e then return end
  local path=""
  for line in e:lines() do --simple closes itself
--[[ 
  print(line)
    print( ssub(line,gx[1],gx[2]), ssub(line,gx[3],gx[4]), ssub(line,gx[5],gx[6]), 
           ssub(line,gx[7],gx[8]), ssub(line,gx[9],gx[10]), ssub(line,gx[11],gx[12]),
           ssub(line,gx[12]+1)			 )
--]]
    path=ssub(line,gx[12]+1)
    
    if tonumber(ssub(line,gx[9],gx[10])  )==0 or not string.find(path,"^[~%w]?[\\/:]") then
--~       print("iggy",line,"---",ssub(line,gx[9],gx[10]))
      filesdb[path]=nil Sl.extrarec=Sl.extrarec+1
    else
      if filesdb[path] then Sl.extrarec=Sl.extrarec+1 end
      
      filesdb[path]=
        fstatrow( 
        tonumber(ssub(line,gx[1],gx[2])  ),
        tonumber(ssub(line,gx[3],gx[4])  ),
        tonumber(ssub(line,gx[5],gx[6])  ),
        tonumber(ssub(line,gx[7],gx[8])  ),
        tonumber(ssub(line,gx[9],gx[10]) ),
        tonumber(ssub(line,gx[11],gx[12]))
        )
    end
  end
  e:close()
end 
 
function fhistory(num)

  local r,n = {},#fpstack
  n = math.min(n,num or n)
  --current file is not pushed when switching unless switching back
  --after 'hopreset' so push it now. perhaps push it now regardless
  --of hopreset which may sometimes miss.

  if Lc_.hopreset then 
    stacket_push(fpstack,props['FilePath']) 
  end
  for i=1,n do  r[i]=fpstack[i]  end
  
  return r
end

function testhist()
  print("\n"..table.concat(fhistory(5),"\n"))
end

local function cdirt(path,impo)
  chngdfls[path]=true 
  Sl.recordch=Sl.recordch+1
  Sl.detailch=Sl.detailch+2+impo 
end 
   
function onsaveds()
  local path = props['FilePath']
  local ctime=os.time()
  buftim[path]=nudgenm(ctime) 
  
  if not filesdb[path] then
    filesdb[path] = fstatrow( 0, 0, 0, 0, ctime , ctime )
  end
  
  local r = filesdb[path]
  local rd = r.diffs
  if editor.Length ~= r.size then
    rd = rd + 1
  end
  
  r = fstatrow( rd, editor.Length, r.saves + 1, r.tottime+ctime-Sl.curftime, r.alphtime , ctime )

  cdirt(path, 3 )
  wcheckdirt()
  
  Sl.curfpath=path 
  Sl.curftime=ctime 
end 
scite_OnBeforeSave(onsaveds)

Lc_['dirtydocs']={}

local function moddirtydoc(f) 
  Lc_['dirtydocs'][f] = scite.SendEditor(SCI_GETMODIFY) -- is doc dirty
end


local lastpip=os.time()
local tfrequency,tpaused=0,0

function toofrequent(fr)
  local t=os.time()
  local rest=t-lastpip
  lastpip=t
  if rest>1 then tpaused=0 tfrequency=0 return false end
  if rest==1 then 
    tpaused=tpaused+1 
    if tpaused>1 then tfrequency=1 return false end
  end
  if rest==0 then
    tpaused=0
    tfrequency=tfrequency+1
  end
  if tfrequency>(fr or 4) then 
    return true 
  end 
end

local oswitchtime=os.time()+os.clock()

function onopenswitches() 
  local path = props['FilePath']
--~   local otm=os.time()+os.clock()
  if not toofrequent() then --dont hammer on shutdown 
    local f=stacket_peek(fpstack,1)
    if fileexists(f) then 
      scite_OnOpenSwitch(onopenswitches,"rem")
      scite.Open(f)
      --print("oo "..f.." oo")
      moddirtydoc(f)
      scite.Open(path)
      scite_OnOpenSwitch(onopenswitches)
    end
  end
  oswitchtime = otm
  stacket_push(fpstack,path)
--~   print("- stacket_pushed "..path)
  
  local ctime=os.time()
  local r
  if Sl.curfpath == path or not Sl.curfpath then 
    Sl.curfpath = path
    Sl.curftime = ctime
  end

  if ctime-Sl.curftime<3 then 
    --last filerec doesnt need updated
  else
    if not filesdb[path] then
      filesdb[path]=fstatrow( 0, editor.Length, 0, 0, ctime, ctime )
      cdirt(path,4) --print('ch',Sl.detailch,#chngdfls,chngdfls[path])
    else 
      r = filesdb[Sl.curfpath]
      if r == nil then
        if Sl.curfpath~="" then
          print(""..Sl.curfpath.." is not found, while switching to "..path)
        end
      else
        filesdb[Sl.curfpath] 
         = fstatrow( r.diffs, r.size, r.saves, r.tottime+ctime-Sl.curftime, r.alphtime , ctime )
        cdirt(Sl.curfpath,1)
      end
    end
  end --tottime, is not figuredout right
  
  Sl.curftime=ctime
  Sl.curfpath=path

  buftim[Sl.curfpath]=nudgenm(Sl.curftime)
  wcheckdirt()
  
end scite_OnOpenSwitch(onopenswitches)

function wdroprec(path)
  filesdb[path]=nil
  chngdfls[path]=true
  saveallrec()
end

function nudgenm(c)
  Sl.tndg=math.fmod(Sl.tndg+0.001,1)
  c = c or 0
  return Sl.tndg + c
end

function startbuffers()
  ctime = os.time() or 0
  local pa = props['FilePath']
  readfstatable()
  for i = 1,100 do 
    scite.MenuCommand(IDM_NEXTFILE) 
    p = props['FilePath']
    if not filesdb[p] then 
      filesdb[p]=fstatrow( 0, editor.Length, 0, 0, ctime, ctime )
    end
    buftim[p]=filesdb[p].ulttime
--~     print(p,buftim[p])
    if p == pa then break end 
  end
  
  local blink,bsrt={},{}
  local cc=1
  
  for k,v in pairs(buftim) do 
    table.insert(blink,k) 
    if k~='' then table.insert(bsrt,cc) end
    cc=cc+1 
  end
  
--~   print(dump(buftim))
--~   print(dump(blink))
--~   print(dump(bsrt))
  
  table.sort(bsrt, 
    function(x,y) 
--~      print(x,y,blink[x],blink[y],buftim[blink[x]],buftim[blink[y]] )
    return (buftim[ blink[x] ]<buftim[ blink[y] ]) end )
  
  for k,v in ipairs(bsrt) do stacket_push(fpstack,blink[v]) end

  scite.Open(stacket_peek(fpstack,1))

  tweakDisplayProps()
--~   print(editor.CurrentPos)
end scite_OnOpenSwitch("once",startbuffers)

function ac_oncloses()  --collect buffer paths trying turned off... 
--~   print("ac_oncloses_triggered : "..props['FilePath'].."<- fp")
  local p=props['FilePath']
  buftim[p]=false
  stacket_rmv(fpstack,p)
  saveextrarec()
  --chngdfls[p]=true
  --Sl.detailch=Sl.detailch+1
end scite_OnClose(ac_oncloses)

function wcheckdirt()
  --at shutdown all are saved so this only
  --checks to see if much for a resave or 
  --this is to save disk access especially for pen drives.
  if Sl.recordch+Sl.extrarec>32 and Sl.detailch>32 then saveallrec() 
  else if Sl.detailch>48 then saveextrarec() end 
  end
end
                
function ModeWriteD(filepath, filedata, mode)
--~ 	print("#Writing zum")
  local F = io.open(filepath, mode)   -- write data to file
  if not F then
    F = io.open(filepath):write(filedata) io.close(F)
    print("Created '"..filepath) 
    return
  end
  local ok = F:write(filedata) io.close(F)
  if not ok then print("Failed to write data to '"..filepath.."'") end
end	

local function score1(frec)
  local nt=os.time()
  local age=nt-frec.alphtime 
  local lgage=math.log(age/3)
  local last=nt-frec.ulttime
  local lglast=math.log(last/3)
  local active=math.log(frec.tottime+(frec.saves*4)+50)
  
--~ 	print("age "..age)
--~ 	print("last "..last)
--~ 	print("lgage "..lgage)
--~ 	print("lglast "..lglast)
--~ 	print("active "..active)
--~ 	print("score "..(((lgage-lglast)+active)*active))
  
  return 
    (lgage*active)/lglast 
end

local function score3(frec)
  local nt=os.time()
  return --+math.log(frec.tottime)
    10000000-(nt-frec.ulttime)  --1 week 604800  13.4  10weeks 15 
end

local function score2(frec)
  local nt=os.time()
  return 
    math.log(nt-frec.alphtime )/100 --1 week 604800  13.4  10weeks 15
   -math.log((nt-frec.ulttime+200)/3)/5  --1 week 604800  13.4  10weeks 15
   +math.log(frec.tottime*2+(frec.saves*5)+200)/2   --1 min 4, 1 hour 8 , 
end

local function spad(str,len,ind)
  local s="                                                  "
  local spaces=len-string.len(str)
  local prespaces=0
  if not ind then
    prespaces=floor(spaces/2)
    spaces=spaces-prespaces
  end
  
  return string.sub(s,50-prespaces+1)..string.sub(str,1,len)..string.sub(s,50-spaces+1)
end

function purgewlist()

  for p,t in pairs(filesdb) do
    if not fileexists2(p) then
      print("Purging "..p)
      filesdb[p]=nil
    end
  end
  saveallrec()
end

function wsummary(opt,qfind)
  --print("ok")
  local suma={}
  local picker
  local vwall=true
  if opt=="s" then 
    picker="-- [" --delete from records picker
    tscore=score3
  elseif opt=="l" then 
    picker="-- [" --delete from opens
    tscore=score3
  elseif opt=="f" then 
    picker="-~ ["
    tscore=score3
    vwall=false
  end
    
  local qfind2,qfind3
  qfind=qfind or ""
  qfind,qfind2 = matchout(qfind," (.+)$")
  qfind,qfind3 = matchout(qfind," (.+)$")
  --(comma sep fndtxts for multiple part matching)
  
  for p,t in pairs(filesdb) do
--~ 		print(p)
    local tsc=tscore(t)
    if buftim[p] or vwall then 
      if qfind~="" or qfind2~="" then
        if string.find( p:lower(), qfind:lower(),1,true ) 
        and ( qfind2=="" or string.find( p:lower(), qfind2:lower(),1,true ) ) 
        and ( qfind3=="" or string.find( p:lower(), qfind3:lower(),1,true ) )
        then insert(suma, { p , tsc }) end
      else
        insert(suma, { p , tsc })
      end
    end
  end
  
  if (opt=="f") and (#suma==1) then
    scite.Open(suma[1][1])
    --scite.MenuCommand(421)	
  end
  
  table.sort(suma, function(x,y) return (x[2]<y[2]) end )
  
--~ 	for i,v in ipairs(suma) do
--~ 		print(suma[i][2],suma[i][1])
--~ 	end
  
  local tt=os.time()
  local ft=filesdb
  --pathtop is list of best scoring paths
  --for path ordering of suma
  
  if opt=="disable pathckumping" then
    local nsum={}
    local pathtick={}
    local pathtop={}	
    for ij=#suma,1,-1 do 
      v=suma[ij]
      local _,_,pt=string.find(v[1],"^(.-)[^\\/]*$")
      --print(v[1],pt)
      if pt and not pathtick[pt] then
        insert(pathtop,pt)
        pathtick[pt]=true
        --print(pt)
      end
    end
    for ii,pathv in ipairs(pathtop) do
      for i,suv in ipairs(suma) do
        local _,_,pt=string.find(suv[1],"^(.-)[^\\/]*$")
        if pathv==pt then
          insert(nsum,suv)
        end
      end
    end
    suma=nsum
  end
  
  local function printlabs(opt,f)
    if opt=="s" then
      print("+ "..spad(f.." Files" ,9,"left").."      saves  dwell  last first rm paths")
    elseif opt=="l" then
      print("+ "..spad(f.." Files" ,9,"left").."            dwell  last rm paths")
    else
      print("+ "..spad(f.." Files" ,9,"left").."            dwell  last rm paths")
    end
  end
  
--~   print()
--~   printlabs(opt,#suma)
  
  --if fo==1 then pp=#pathtop end
  --print()--print(pathtop[pp])

--~ 	for i,v in ipairs(suma) do
--~ 		print(suma[i][2],suma[i][1])
--~ 	end
  
  for i,sflpath in ipairs(suma) do
--~ 		if i>lscore then break end
    
--~ 		local r=ft[sflpath] ??
    local r=ft[suma[i][1]]
    local fexists=fileexists(sflpath[1])
    local mf=floor
    local _,_,mp=sflpath[1]:find("[\\/]([^\\/]*)$")
    
    local dwelledmins =math.fmod(mf(r.tottime/60),60)
    local dwelledhrs =mf(r.tottime/3600)
    
    if dwelledmins==0 then dwelledmins="" 
    elseif dwelledmins<10 then dwelledmins="0"..dwelledmins..'m'
    else dwelledmins=dwelledmins..'m' end
    
    if dwelledhrs>0 then dwelledhrs=dwelledhrs.."h" else dwelledhrs="" end	
    local dwellti=dwelledhrs..dwelledmins
    
    if dwellti=="" then dwellti="-" end
      
    local sav=r.saves if sav==0 then sav="-" end
    local l
    if fexists then fexists="- " else fexists=": " end
    
    if opt=="s" then l=
    { fexists,spad(mp,15,true)," ",
      spad(sav,6,true), spad(dwellti ,6   ),
      spad(mf((tt-r.ulttime)/86400+0.499) ,6 ),
      spad(mf((tt-(r.alphtime or 0) )/86400+0.499) ,6),
      picker,sflpath[1]," ]"--,suma[i][2]
      }
    elseif opt=="l" then l=
    { fexists,spad(mp,20,true)," ",
      spad(dwellti ,6   ),
      spad(mf((tt-r.ulttime)/86400+0.499) ,6 ),
      picker,sflpath[1]," ]"
      }
    else l=
    { fexists,spad(mp,20,true)," ",
      spad(dwellti ,6   ),
      spad(mf((tt-r.ulttime)/86400+0.499) ,6 ),
      picker,sflpath[1]," ]"
      }
    end
--~ 		print(suma[i][2])
    print(table.concat(l))
  end
  
  if #suma>8 then 
    printlabs(opt,#suma) 
  end
end




------------------------------------------------------
Lc_shortcut("C=Tab switch|ctrltab|Ctrl+Tab")
Lc_shortcut("C=Tab switch back|ctrltab 'b'|Ctrl+Shift+Tab")

Lc_shortcut("Buffers|do_buffer_list|Ctrl+`")

local ctrlastnow=-1
local ctrlasthesh=""

--we skip the subsequent auto stacking of fp by onswitch with stacket_trip 
--so that the quick visiting doesnt alter order
--differences in time and cp are used to detect a dwell
--as stacket_hop was not made when this function was wrote 
--it works well independant of bookmark hopping so may be left as is.

function ctrltab(rvrs)
  local now=os.time()
  local fp=props['FilePath']
  local hesh=fp..editor.CurrentPos..editor.Length
  
  if ctrlastnow==-1 or now>ctrlastnow+6 or hesh~=ctrlasthesh then
    if stacket_peek(fpstack,1)~=fp then
      stacket_push(fpstack,fp) --correct missed auto stack
    end
    stacket_push(fpstack) --push nothing to pop from the top
    stacket_pop(fpstack) --ignore the top entry (current fp)
  end
  
  if rvrs then fp=stacket_unpop(fpstack) else fp=stacket_pop(fpstack) end
  stacket_trip(fpstack)
  ctrlastnow=now

  --disabled fileexists as buffer for missing file needs to be saved or closed..
  if not tryuntitled(fp) then
    if true then -- fileexists(fp) then 
      scite.Open(fp) 
      ctrlasthesh=fp..editor.CurrentPos..editor.Length
    else
      printup("\n- File for buffer in: "..fp.." not found")
      stacket_rmv(fpstack,fp) 
    end 
  end

  return true
end

function tryuntitled(p)
  if not string.match(p,"[\\/]$") then return false end --untitled buffers have only dir as path
  local a=props['FilePath']
  if p==a then return true end
  
  scite_OnOpenSwitch(onopenswitches,"rem")
  for i = 1,100 do 
    stacket_trip(fpstack)
    scite.MenuCommand(IDM_PREVFILE) 
    if props['FilePath']==p then 
      ctrlasthesh=p..editor.CurrentPos..editor.Length
      scite_OnOpenSwitch(onopenswitches)
      return true 
    end
    if props['FilePath']==a then break end
  end
  scite_OnOpenSwitch(onopenswitches)
  return false
end

function printque()
  for i,v in pairs(fpstack) do
    print(i,v)
  end
end

function printup(...)
  output.CurrentPos = poslinestfn(output.CurrentPos,output)
  print(...)  output.CurrentPos=output.Length
end

function gettoppaths()
  local toppaths={}
  local e = io.open(props['luSci.toppaths'])
  if not e then return {} end
  for line in e:lines() do --simple closes itself
    if line:match("[-][-][-][-]") then break end
    local p=fullpathinstr(line) or "#"
    if not p:match("^%s*[#%-]") then insert(toppaths,p) 
    else
--    local dr=line:match("(.*)//.*") or line --strip my comments,  \/janky pattern\/
--    local dr=dr:match("^[%s]*([%a~/][:/%a][%a.@&%-_ ]+[\\/])")
--    if dr then insert(toppaths,dr) end
    end
  end
  e:close()
  return toppaths
end 

function getmruexts(rplfmt)  --rpl format or format for grep
  local ms,seen={},{}
  for c=1,math.min(6,stacket_len(fpstack)),1 do
    local p=stacket_peek(fpstack,c)
    local b,e=matchout(p,"[.][^.]+$")
    if #e>0 then 
      if not rplfmt then e = "*"..e end
    else 
      b,e=matchout(p,"[\\/]([^\\/]+)$")
    end
    if #e>0 and not seen[e] then
      seen[e]=true
      if rplfmt then
        table.insert(ms,'-x '..e)
      else
        table.insert(ms,'--include "'..e..'"')
      end
    end
  end
  return table.concat(ms," ")
end


lc_init('scndgo', false   )


local function dircomp(a,b)
  local ap= match(a or" ","[^\\/]*$")
  local bp= match(b or" ","[^\\/]*$")
  return ap< bp
end

function ldircomp(a,b)
  local ap= match(a or" ","^(.-)[^\\]*$")
  local bp= match(b or" ","^(.-)[^\\]*$")
  return (#ap== #bp) and (ap<bp) 
end

local function drivcomp(a,b) return a:sub(1,2) < b:sub(1,2) end --drive/root

local function driv1comp(a,b)  --d/r w direcory len
  local ap,bp = match(a or" ","[^\\/]*$") , match(b or" ","[^\\/]*$")
  return (a:sub(1,2) == b:sub(1,2)) and (#ap< #bp) 
end

local function driv2comp(a,b)  --d/r w direcory len and alpha
  local ap,bp = match(a or" ","[^\\/]*$") , match(b or" ","[^\\/]*$")
  return (a:sub(1,2) == b:sub(1,2)) and (#ap< #bp) and (ap<bp) 
end

function lencomp(a,b)
  local ap= match(a or" ","^(.-)[^\\]*$")
  local bp= match(b or" ","^(.-)[^\\]*$")
  return #ap< #bp
end

function filcomp(a,b)
  local af= match(a or" ","^.-([^\\]*)$")
  local bf= match(b or" ","^.-([^\\]*)$")
  return af<bf
end




function do_buffer_list()  --~ crazy loony list
  local pane=get_pane()
  local bffrSort ={}
  local bffrsOut ={}
  local targ=65
  local bx,tfl,tpl,tfl2,tpl2,afl,apl=0,0,0,0,0,0,0
  
  if pane:AutoCActive() then
    if scndgo then
      scndgo=nil
      pane:AutoCCancel()
      return true
    else
      scndgo=true
    end
  else
    scndgo=nil
  end
  
  if not scndgo then
    for i,v in pairs(buftim) do
      if v then insert(bffrSort,i) end
    end	
  else
--~ 		scndpths=(props['Lc_paths']):unconcat("|")
    scndpths=gettoppaths()
  end
       
  local gbf=props['FilePath']
  local gdir=props['FileDir']
  local gname=props['FileName'] --(no extension)
  local didfnd=false
  moddirtydoc(gbf)
  
  if scndgo then
    --same name finding--
    
    didfnd=false
    
    for v,_ in pairs(filesdb) do
      local fni= match(v,"([^\\/]*)$") or "" --file name of buff
      fni=match(fni,"^([^.]*)")                 --remove any extension
      if fni==gname then
        if v~=gbf then 
          didfnd=true 
          --print(v)
          if buftim[v] then insert(bffrSort,v) end 
        end 
      end
    end
    
    if didfnd then insert(bffrSort,"      ") end
    didfnd=false

    for _,v in ipairs(scndpths) do
      insert(bffrSort,v)
    end
    
    insert(bffrSort,"      ")
    
    for v,_ in pairs(filesdb) do
      local fpi=match(v,"^(.-)[\\/][^\\/]*$")
--~ 			print(fpi)
      if fpi==gdir then
        if v~=gbf then 
          didfnd=true 
          
          if buftim[v] then insert(bffrSort,v) end
 
        end 
      end
    end
  end
  
  if not scndgo then
    table.sort(bffrSort, dircomp) --all opened buffers
    
    --
    local curpath= bffrSort[1] or "" 
    
    local pathlst,m,n={},1,1
    insert(pathlst,curpath)  --pathlst chunks at a time
    
    local curdir=match(curpath,"^(.-)[^\\/]*$")
    local lstpath=bffrSort[2]
    local igin,xi=0,2
    
    repeat
      local vnd=match(lstpath or"","^(.-)[^\\/]*$")
      if vnd==curdir then 	insert(pathlst,lstpath)  --if lat op buff is same dir as first
      else
        table.sort(pathlst, filcomp) --print('sorting',#pathlst)
        for i,v in ipairs(pathlst) do	bffrSort[igin+i]=v end
        igin=igin + #pathlst
        pathlst={[1]=lstpath}
        curdir=vnd
      end
      xi=xi+1 lstpath=bffrSort[xi] 
    until xi>#bffrSort

    table.sort(pathlst, lencomp)
    table.sort(pathlst, ldircomp)
    for i,v in ipairs(pathlst) do
      bffrSort[igin+i]=v
    end			--	for i,v in ipairs(bffrSort) do
    
    --made a pathlist, and added to bffrSort
    
    --add recently used	
    
    local bbb=fhistory(6)
    
    for i,v in ipairs(bbb) do
      if buftim[bbb[i]] then insert(bffrsOut,bbb[i]) end
    end
    
    if bffrsOut[2] then 
      bffrsOut[2],bffrsOut[1]=bffrsOut[1],bffrsOut[2]
    end
    
    local gdlen=#gdir
    --a back directory of the current 
    local gdhlen=floor((#gdir+1)*2/3)
    local gdh=match(ssub(gdir,1,gdhlen),"^(.-)[^\\/]*$")
    gdhlen=#gdh	
    
    insert(bffrsOut,"      ") --------------
    
    --current directory sorting--
    for i,v in ipairs(bffrSort) do
      local p= match(v,"^(.-)[^\\/]*$")
      if p==gdir then
        --print(p,gdir,v)
        if v~=gbf then 
          didfnd=true
          insert(bffrsOut,v) 
        end
        bffrSort[i]="x" --print("niled!")
      end
    end
    if didfnd then insert(bffrsOut,"        ") end --------------
    didfnd=false
    
    --back directories next, sort by length--
    for i,v in ipairs(bffrSort) do
        --print(v, ssub(v,1,gdhlen),gdh)
      if v~="x" and (gdh==ssub(v,1,gdhlen)) then
        insert(bffrsOut,v)
        didfnd=true
        bffrSort[i]="x" --print("nilled")
      end
    end
    
    if didfnd then insert(bffrsOut,"        ") end --------------
    
    --all the rest--
    table.sort(bffrSort, drivcomp)
    table.sort(bffrSort, driv1comp)
    table.sort(bffrSort, driv2comp)
  end
  --bffrSort=(props['Lc_paths']):unconcat("|")
  --------------------------------------------------
  --Suss out Dialog Widths
  
  bx=#bffrSort
  for _,i in pairs(bffrSort) do
    local p,f= match(i,"^(.-)([^\\/]*)$") --path,file
    local fl=#f --strlen of file 
    local pl=#p 
    afl=afl+fl apl=apl+pl
    
    if fl>=tfl then tfl2=tfl tfl=fl 
    elseif fl>tfl2 then tfl2=fl
    end
    
    if pl>=tpl then tpl2=tpl tpl=pl 
    elseif pl>tpl2 then tpl2=pl 
    end	
  end
  
  local atl=floor(afl+apl)/2   --sub minimum length
  local itl=tfl2+tpl2          --a good length
  
  local zfl,zpl=tfl,tpl
  if (zfl+zpl)>targ then  --average,top file,path length 
    
    targ=((targ+(tfl2+tpl2)*2+(afl+apl)/bx)/4)
    zfl =(targ*(tfl+tfl2*2+afl))/(tpl+tpl2*2+apl)
    zpl =targ-zfl
  else
    local m=targ-(zfl+zpl)
    zpl=zpl+floor(m/4)
    zfl=targ-zpl
    targ=targ-floor(m/4)
  end
  local zwt=zfl+zpl	 --zpathlen -zfilelen
  --^Sussed out Dialog Widths via convoluted reckonings
  ------------------------------------------------------
  
  --reformat entries--
  for i,v in ipairs(bffrSort) do
    if v~="x" then insert(bffrsOut,v) end
  end

  for i,v in ipairs(bffrsOut) do
    local p,f= match(v,"^(.-)([^\\/]*)$")
    local fl=#f
    local pl=#p
    local pfl,ppl=zfl,zpl
    
    if (fl>zfl) and (pl<(zpl-(fl-zfl))) then
      
      ppl=ppl-(fl-zfl)
      pfl=pfl+(fl-zfl)
    end

    if (pl>zpl) and (fl<(zfl-(pl-zpl))) then
      pfl=pfl-(pl-zpl)
      ppl=ppl+(pl-zpl)
    end
     
    if f=="" then f="·.¸¸.·" end
    f=strpack(f,pfl) 		p=strpack(p,ppl)
    local dt="  " 
    if Lc_.dirtydocs[v] then dt="* " end
    if Lc_.bkstack 
    and stacket_has(Lc_.bkstack,v, function (a,fp) return a and a.fp==fp end )
    then  --tag if file has bookmarks
      local bn=stacket_count(Lc_.bkstack,v, function (a,fp) return a and a.fp==fp end )
      dt=string.sub(""..bn..dt,1,2)
    end
      
    bffrsOut[i]="...  "..p..": "..f..dt..">"..v
  end
--~ 		local ad,af=match(a,"^(.-)([^\\]*)$")
--~ 		local bd,bf=match(b,"^(.-)([^\\]*)$")
  pane.AutoCMaxHeight=10
  pane.AutoCMaxWidth=floor(targ+15)
  
  local nn=1
  if pane==output then nn=0 end 
  local c=pane.CurrentPos
  local n=math.floor((pane:PositionFromLine(pane:LineFromPosition(c))))
  pane.CurrentPos=n
  
  scite_UserListShow(bffrsOut,1,
    function (str) 
      local s=str:match("[>](.*)$")
      if s:match("[\\/]$") then -- gettoppaths is not reading directories
        if props['PLAT_GTK'] then trace("ls "..s) else trace("dir/w "..s) end
        if get_pane()==editor then
          scite.MenuCommand(IDM_SWITCHPANE) --switch focus
        end
      else
        sc_open(s) 
      end
      return false 
    end
  )
  
  pane.CurrentPos=c --? moving to push pane view ??

  --see lines vis on pane, 
  --see current pos is below middle, substract 1 line before userlist show
  --otherwise add one
  --rather, just add one unless near bottom of pane
  
--~ 	pane:AutoCSelect(redu)
  return true
end

Lc_shortcut("Closefile|modclosebuf|Ctrl+w")

function modclosebuf()
  
  local p = panefocus()
  scite.MenuCommand(IDM_CLOSE) 
  local s
  local bbb=fhistory(6)
  for i,v in ipairs(bbb) do
    if buftim[bbb[i]] then s=bbb[i] break end
  end 
--~   if not s then scite.MenuCommand(IDM_QUIT) return end 
  scite.Open(s)
  panefocus(p)
  return true
end
