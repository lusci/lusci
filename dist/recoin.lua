local find, match, gsub = string.find, string.match, string.gsub
local insert = table.insert 
local sub = string.sub
local tain,_ = tain
local poslinestfn = poslinestfn

Lc_shortcut("List|listhotkey|Ctrl+l")

function listhotkey() 
      
  local pane,kr,fnp=get_pane()
  if kr==fnp then _,kr,fnp = findonpos("[%w_]+",pane) end
  if not kr then return true end
  local v=pane:textrange(kr,fnp)
  v="List,"..v
--~   tracePrompt(v.."\n","\n>")

  MAReplace(v)
  tracePrompt()
  output:ScrollCaret()
  
  if pane==editor then pane:SetSel(kr,fnp) end
  return true
end

Lc_shortcut("Spot|spothotkey|Ctrl+Shift+l")

function spothotkey() 
      
  local pane,kr,fnp=get_pane()
  if kr==fnp then _,kr,fnp = findonpos("[%w_]+",pane) end
  if not kr then return true end
  local v=pane:textrange(kr,fnp)
    
  if kr==fnp then --use any selection in editor if no word in output
    if editor.CurrentPos ~= editor.Anchor
    then pane,kr,fnp=editor,editor.CurrentPos,editor.Anchor end
  end
  if kr>fnp then kr,fnp=fnp,kr end
  fixprompt("clear")
  
  local fd=props['FileDir']
  local od=output:GetLine(output.LineCount-1):match("/[^;]*") or ""
  if not fd:match(od) then --curfile is not under prompt dir
    outcarets(output.Length)
    rtrace(props['FileDir'].."; Spot,"..pane:textrange(kr,fnp)) --make a swap safe textrange...
  else
    trace("Spot,"..pane:textrange(kr,fnp))
  end
  enter_terminal()
  if pane==editor then pane:SetSel(kr,fnp) end
  return true
end

function xfindpattern(str,ptt,ini,pn)
  
  --string.find(string,patt,init,plain)

  --~ a,b,c,d,e,f,h=xfindpattern("pigglewigglelok","piggle((wiggle|lok))")
  --~ print(a,b,c,d,e,f,g,h)
  --~ >1	12	wiggle	nil	nil	nil	nil
    
  --mtchfn=string.find(pat,"[^%]?(.+[%|].-[^%])")	
  --quote these %(
  local s,u=string.find((string.gsub(ptt,"%%.","%%%%") or ""), "%([^%(%)]-[%|][^%(%)]-%)")
  if s then
    local bg=string.sub(ptt,1,s-1)
    local nd=string.sub(ptt,u+1)
    
    local brak=string.sub(ptt,s+1,u-1)
    local bunc=unconcat(brak,"|")
    local a,b,c,d,e,f,oa,ob,oc,od,oe,of
    local bigs,first=0,1000000
    
    for _,x in ipairs(bunc) do

      a,b,c,d,e,f= find(str,(bg..x..nd),ini,pn)
      if a and (a<first or (a==first and #x>bigs)) then
        bigs=#x 
        first, oa,ob,oc,od,oe,of = a, a,b,c,d,e,f
      end
    end
    
    return oa,ob,oc,od,oe,of
  end
end

--MAReplace and WReplace ...
--MAReplace is a kluge over WReplace
--  parsing user command
--  arranging and reporting on multi file action
--  calling WReplace for each individual action

function MAReplace(c)
  
  local cc=string.lower(c)
  local _,_,isrep=find(c,"^([Ll]is?t?[:,])")
  local _,_,fxx=find(cc,"^lis?t?[:,](~~?)")
  local _,_,fscope=find(cc,"^lis?t?[:,]~~?([%d][%d]?)")
  local _,_,rpexpr=find(c,"^[Ll]is?t?[:,]~~?[%d%~][%d]?[%d]? (.+)")
  if not fxx then WReplace(c) return end 
  
  local farray={}
  local cfp=props['FilePath']
  Lc_.repfiles=Lc_.repfiles or {}
  
  if (not fscope) or fscope=="0" then 
    if Lc_.repfiles[cfp] then 
      farray=Lc_.repfiles[cfp]
    else
      table.insert(farray,cfp)
    end
    print("- File Scope:")
    print(table.concat(farray,"\n"))
    Lc_.repfiles[cfp]=farray
    --print(isrep..rpexpr)
    --~ return false
  elseif find(c,"...?.?[:,]~%d+") then
    farray,farr={},fhistory(tain(tonumber(fscope),1,50),true)
    if fscope then
      for i=#farr,1,-1 do
        insert(farray,farr[i])
      end
      print(table.concat(farray,"\n"))
      print("- These recent files scoped")
      Lc_.repfiles[cfp]=farray
      if not rpexpr then return false end
    end
  else
    farray,farr={},{}--getofiles(tain(tonumber(fscope),1,50))
    if fscope then
      for i=#farr,1,-1 do
        insert(farray,farr[i])
      end
      print(table.concat(farray,"\n"))
      print("- These upscreen files scoped")
      Lc_.repfiles[cfp]=farray
      if not rpexpr then return false end
    end
  end
  
  if not rpexpr then return false end
  
  for i,v in ipairs(farray) do 
    scite.Open(v)
    WReplace(isrep..rpexpr,i~=#farray,true)
  end
  scite.Open(cfp)
  return false
end

 
function WReplace(c,nosumm,multifile)
  
  local find = string.find
  local cc=string.lower(c)

  if not multifile then
    uptrace("\n>"..props["FileDir"].."; "..c.."\n",0,1)
  end

  if (not multifile) and find(cc,"^lis?t?[:,]~[%d%~][%d]?[%d]?%s")
  then MAReplace(c) return end

  local _,_,q=find(c,"[:,](.*)")  q=q or ""
  printwaypoint(q)
    
  local tnam=props['FileNameExt']
  local hfs={lua=1,java=1,js=1,php=1,thtml=1,cxx=1,h=1,py=1,html=1}
  local hasfun=hfs[(props['FileExt'])]
      
  noteSelection()
  
  local stp,fnp=0,editor.Length
  if editor.CurrentPos~=editor.Anchor then
    fnp,stp=editor.CurrentPos,editor.Anchor
    if fnp<stp then fnp,stp=stp,fnp end
    if (fnp-stp<500) and not find(editor:textrange(stp,fnp),"[\r\n]")
    then stp=0 fnp=editor.Length end
  end

  editor:BeginUndoAction()

  local len=string.len
  
  local patrep,plainmtch=nil,true
  local inst=c
  _,_,c=find(c,"[:,](.*)")
  local nocase
  if sub(inst,1,1)=="l" then nocase=true end
  local wrdbnd=true
  local xfindx=false

  if sub(c,1,1)=="¬" then xfindx=true
    c=sub(c,2)
  end

  if sub(c,#c)=="¬" then plainmtch=nil
    c=sub(c,1,#c-1)
  end

  if sub(c,#c)=="¬" then patrep=true
    c=sub(c,1,#c-1)
  end

--~ 	if sub(c,-2)==" ~" then wrdbnd=nil
--~ 		c=sub(c,1,#c-2)
--~ 	else
  if 
     sub(c,#c)=="~" then wrdbnd=nil
    c=sub(c,1,#c-1)
  end 
  
  if sub(c,1,2)=="\\~" then c=sub(c,3) end --an escaped ~
  
  local fndpatw=c
  local reppatw,reppatv
  
  if find(c," ~") then
    fndpatw=match(c,"(.*) ~")
    reppatw=match(c," ~(.*)")
    reppatv=reppatw
    if not reppatw then reppatw="" end
  end
  
  if reppatv and wrdbnd then
    reppatv=wwordpatt(reppatv)
--~ 		print("reppatv#"..reppatv)
--~ 		reppatv="[^%w_]"..reppatv.."[^%w_]"
  end
  
  if fndpatw=="" then fndpatw=nil end 
  if not fndpatw then fndpatw="whaddyamean?" reppatw=nil end
  
  local lenchng=0 --init not used
  
  local se,fwd=false,false
  local nxfndpos
  local otabl={}
  local ortabl={}
  local fndpatcas=fndpatw
  if nocase then fndpatcas= string.lower(fndpatcas) end
  
  local haswdst,haswdfn
  if xfindx or find(fndpatcas,"^[$%w_]") then haswdst = true else haswdst=false end
  if xfindx or find(fndpatcas,"[%w_]$") then haswdfn = true else haswdst=false end
  local xf
  
  --local ooo=sub("~ooOO8O8O8O8O8888888",1,math.floor((len(fndpatw)+1)/2))..sub("8888888O8O8O8O8OOoo~",21-math.floor((len(fndpatw))/2))
  
  local reps,treps=0,0
  local colides=0
  local edst=editor:LineFromPosition(stp)
  local eden=editor:LineFromPosition(fnp)
  
  if not fndpatw then print("- lost innocence") return end

  local mtchst,mtchfn,clnstp,clnfnp,padlntxt,caslntxt,prtlntxt,lndif,prtdif,insr
  local lower=string.lower
  
  if xfindx then tfind=xfindpattern else tfind=find end
  
--~ 	print("fndpatcas#"..fndpatcas) 

--~ 	if wrdbnd then print("wrdbnd") else print("no wrdbnd") end
--~ 	if haswdst then print("haswdst") else print("no haswdst") end
--~ 	if haswdfn then print("haswdfn") else print("no haswdfn") end
--~ 	if fndpatcas then print("fndpatcas#"..fndpatcas) else print("no fndpatcas") end
--~ 	if plainmtch then print("plainmtch") else print("no plainmtch") end
--~ 
--~ 	a,b,c,d=tfind("boot tweek",fndpatcas,0,plainmtch)
--~ 	print(a,b,c,d)
  
--~ 	if true then return end
  
  for cln=edst,eden do
    
    clnstp=editor:PositionFromLine(cln)
    clnfnp=editor.LineEndPosition[cln]
    
    if clnstp<stp and clnfnp>stp then clnstp=stp end
    if clnfnp>fnp then clnfnp=fnp end
    
    if clnstp==0 then padlntxt="\n"..editor:textrange(clnstp,clnfnp)
    else padlntxt=editor:textrange(clnstp-1,clnfnp) 
    end  --one extra char at start to assist word finds
    
    caslntxt, prtlntxt =padlntxt, padlntxt
    lndif, nxfndpos, prtdif=0, 2, 0 
    if nocase then caslntxt= lower(caslntxt) end
  
    if reppatv and not patrep then
      local v=0 
      for _ in string.gmatch(padlntxt, reppatv) do --!
        v = v + 1	
      end
      if v>0 then
        colides=colides+v
        insert(ortabl, tnam..":"..(cln+1)..":"..sub(prtlntxt,2))
      end
    end
    
    repeat --process one line (padlntxt)
        
      mtchst,mtchfn=tfind(caslntxt,fndpatcas,nxfndpos,plainmtch)
      
--~ 			if mtchst then print(nxfndpos,sub(caslntxt,mtchst,mtchfn),fndpatcas) end
      
      if wrdbnd then
        if haswdst and mtchst and find(sub(caslntxt,mtchst-1,mtchst-1),"[%w$_]") 
        then mtchfn=nil end
        if haswdfn and mtchfn and find(sub(caslntxt,mtchfn+1,mtchfn+1),"[%w$_]") 
        then mtchfn=nil end
      end
      
      if mtchfn then nxfndpos=mtchfn+1 
      else
        if mtchst then nxfndpos=mtchst+1 end
      end
        
      if mtchfn then	
        if reppatw then	
          if patrep then 
            if xfindx then
              insr=gsub(sub(padlntxt,mtchst,mtchfn), 
                sub(padlntxt,mtchst,mtchfn), reppatw) 
              or "!!"..sub(padlntxt,mtchst,mtchfn).."!!"
            else
              insr=gsub(sub(padlntxt,mtchst,mtchfn), fndpatcas, reppatw) 
              or "!!"..sub(padlntxt,mtchst,mtchfn).."!!"
            end
            
            prtlntxt=sub(prtlntxt,1,mtchst-1+lndif+prtdif).."@"..insr
                .."@"..sub(prtlntxt,mtchfn+1+lndif+prtdif)
            prtdif=prtdif+2
          else 
            insr=reppatw 
          end
          
          padlntxt=sub(padlntxt,1,mtchst-1)..insr..sub(padlntxt,mtchfn+1) 
          caslntxt=sub(caslntxt,1,mtchst-1)..insr..sub(caslntxt,mtchfn+1) 
          
          lenchng=#insr-(mtchfn-mtchst+1)
          nxfndpos=nxfndpos+lenchng
          if clnstp+mtchst<stp then stp=stp+lenchng end
          if clnstp+mtchst<fnp then fnp=fnp+lenchng end
        end
        
        reps,treps=reps+1,treps+1
      end	
    until not mtchst --(no more finds in line)
    
    if hasfun and find(padlntxt,"function") then 
      --function: test1.lua:3:
      ffunc=tnam..":"..(cln+1)..":"..sub(prtlntxt,2)
      fll=cln
    end
    
    if reps>0 then
      if ffunc and not (fll==cln) then
        insert(otabl, ffunc)
        ffunc=nil
      end
      
      insert(otabl, tnam..":"..(cln+1)..":"..sub(prtlntxt,2))
      if reppatw then
        editor:SetSel(clnstp,clnfnp) 
        editor:ReplaceSel(sub(padlntxt,2))
      end
    end
    reps=0
    
  end --all lines done	

  editor:EndUndoAction()
  
  local tr="Text"
  if wrdbnd then tr="Word" end
  
  if treps==0 then
    if (not nosumm) then 
      print("-"..tr.." not found: '"..fndpatw.."'")
    end
  else
    print(table.concat(otabl,"\n"))
    
    if not reppatw then
      print("-"..tr.." '"..fndpatw.."' found "..treps.." times")
    else
      print("-"..tr.." '"..fndpatw.."' replaced with '"..reppatw.."' "..treps.." times")
      if colides>0 then
        print(table.concat(ortabl,"\n"))
        print("-Colliding with "..colides.." previous occurences of '"..reppatw.."'")
      end
    end
  end

  restoreSelection()	
  return treps
end



function wwordpatt(wrd)
  local ffwd,afwd
  
  if string.find(wrd,"^[$%w_]")
  then ffwd="%f[$%w_]" else ffwd="" end
  if string.find(wrd,"[%w_]$")
  then afwd="%f[^%w_]" else afwd="" end
    
  return ffwd..string.gsub(wrd, "([%(%)%.%%%+%-%*%?%[%^%$])", "[%%%1]")..afwd
end	
