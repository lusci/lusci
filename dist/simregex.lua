--[[

  simple regex convert from pattern
  convert backslash escapes to pattern escape \w \d etc to %w%b etc
  * - + ? remain as is
  \b word boundary translates to %f[%w] and %f[^%w] depending on
  the two character classes it is placed next to
  case insensitive must put literal word chars into classes [aA][bB] etc
  and must add chars and ranges within classes [a-z] becomes [a-zA-Z]
  helper function xfind allows for one ored capture (abc|def|zzzz)
  
  this may be expanded to allow for more
  and for capture repeats (abc|def){1,4}
  but simple usage is of most immediate use to many who know basic regex 
  
  (?:pat)	non-capturing group
  unescape \'s
  
  may bodge single letter with frontiers:
  (?!pat)	negative lookahead assertion
  (?<!pat)	negative lookbehind assertion
  (?=pat)	positive lookahead assertion
  (?<=pat)	positive lookbehind assertion
  
  crgx = rfinder("/boobo/gi")
  rets = crgx(haystack)
  
  function rgfinder(c,anchor)
  
    local r
    
    r=function(x) return string.find(x,rpatt,start) end
    or
    r=function(x) return xfind(x,rpatt,start) end
    
    return r
  end 
  
  rgmatcher
  rgsuber
  
]]

function rgfinder(c)

  --return progressively more complex solvers depending on
  --complexety of regex pattern...
  r=function(x) return string.find(x,c) end
  return r
end

local b=rgfinder("aaa")
local c=rgfinder("bb")
print( b("bbaabbaabaaabaaba") ) 
print( c("bbaabbaabaaabaaba") ) 
--~ print(  string.find("bbaabbaabaaabaaba","aaa") ) 