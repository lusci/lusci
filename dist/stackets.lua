--stacket idea: a stack-set; stack containing a set. set members have unique values so pushes of existing 
--values remove the value below. Otherwise like a normal stack. this stacket is for cycling through 
--most recent visited lists so popping does not remove elements and pop position resets to top on push.
--elements are lost by dropping off the end of max size or having value removed by stacket_rmv
--stacket_hop pops values in order but also checks if the last hopped value was dwelled on
--if it was dwelled on it is pushed so that it is the first value hopped to
--the last hopped value is considered dwelled if older than 6 seconds or if hopreset is true
--which is set in lusci_command's onkey processing 

--to be made more correct, pop should remove elements

Lc_=Lc_ or {} Lc_.stackets={}
local stackets = Lc_.stackets

function new_stacket(n,eq) -- n is max entries or 64
  local q={} table.insert(stackets,q)
  q.c=1  q.s=false  q.t=-0.1  q.n=n or 64
  q.v=nil  q.eq=eq or false
  return q
end

function Stacket(...)         --maybe nicer to write astacket:hop() ...
  local st=new_stacket(...) 
  st.push=stacket_push st.pop=stacket_pop st.unpop=stacket_unpop st.pick=stacket_pick
  st.hop=stacket_hop st.unhop=stacket_unhop st.pre=stacket_pre st.trip=stacket_trip
  st.poke=stacket_poke st.has=stacket_has st.len=stacket_len
  st.rmv=stacket_rmv st.under=stacket_under
  return st
end

function stacket_push(q,v,eq) -- q is the stacket, v is val to push, pushing nil resets pop
  if eq~=nil then q.eq=eq end
  if q.s then q.s=false return end
  q.c=1 if v==nil then return end 
  local z=q.n
  for i=1,q.n do
    if q[i]==nil or q[i]==v or q.eq and q.eq(q[i],v) then z=i break end
  end
  if z>1 then
    for i=z,1,-1 do 
      q[i]=q[i-1] 
    end
  end
  q[1]=v 
end
  
function stacket_under(q,v) -- v is val to add to last in stack
  if q.s then q.s=false return end
  if v==nil then return end 
  q[tain(stacket_len(q),0,q.n)]=v
end
  
function stacket_pop(q,n) --n rtrns nth entry without popping, n<1 then from bottom
  if n then 
    if n<1 then n=stacket_len(q)-n end
    return q[tain(n,1,q.n)]
  else
    local ri=q.c
    q.c=1+(q.c)%q.n               --advance q.c
    if q[q.c]==nil then q.c=1 end --stack not full
    return q[ri]
  end
end

function stacket_unpop(q) 
  local ri=q.c
  q.c=(q.n-1+q.c)%q.n 
  if q[q.c]==nil then q.c=1 end 
  return q[ri]
end

function stacket_trip(q)  q.s=true  end --causes next push to be ignored, for alttab mru switch 

function stacket_peek(q,n)  return q[n]  end  --access  -- change this to stacket_pick
function stacket_pick(q,n)  return q[n]  end  --access  -- change this to stacket_pick
function stacket_poke(q,n,v) q[n]=v  end      --access 

function stacket_has(q,v,eq) --search stacket for value v, returns index or false
  eq=eq or q.eq             --eq is transient here 
  for i=1,#q do
    if q[i]==v or eq and eq(q[i],v) then return i end
  end
  return false
end

function stacket_count(q,v,eq) --search stacket for value v, returns true or false
  eq=eq or q.eq                --eq is transient here
  local c=0
  for i=1,#q do
    if q[i]==v or eq and eq(q[i],v) then c=c+1 end
  end
  return c
end

function stacket_len(q)
  for i=0,#q-1 do
    if q[i+1]==nil then return i end
  end
  return #q
end

function stacket_rmv(q,v,eq)
  eq=eq or q.eq  --eq is transient here
  if v==nil then return end
  local c=0 
  for i=1,q.n do 
    if q[i]~=v and not (eq and eq(q[i],v)) then c=c+1 q[c]=q[i] end
  end
  while c<q.n do  c=c+1 q[c]=nil  end
  return
end

function stacket_pre(q,v) --resets stacket_hop time and hopreset
  Lc_.hopreset=false
  q.t , q.v = os.time() , v 
end

function stacket_hop(q,d,eq) --pushes previous hop if hopreset or last hop time > 6
  if eq~=nil then q.eq=eq end
  if type(d)=='function' then eq=d d=6 end
  local ct=os.time()
  if q.v and (Lc_.hopreset or q.t-ct > (d or 6)) then
    stacket_push(q,q.v) --pushing previously cached pop
  end 
  Lc_.hopreset=false
  local cv = stacket_pop(q)
  q.t , q.v = ct , cv
  return cv 
end

function stacket_unhop(q,d,eq) --hop just reversed, untested, might get stuck
  if eq~=nil then q.eq=eq end
  if type(d)=='function' then eq=d d=6 end
  local ct=os.time()
  if q.v and (Lc_.hopreset or q.t-ct > (d or 6)) then
    stacket_push(q,q.v) --
  end 
  Lc_.hopreset=false
  local cv = stacket_unpop(q)
  q.t , q.v = ct , cv
  return cv 
end
