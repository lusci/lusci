--make instruction store instruction and wait for apply hotkey
--this hotkey will restore previous when repeated, so the user
--is protected for borking and not knowing the hotkey
--count hotkey frequency with welfords algorythm
--and advance the hotkey message

local function isqrt(x) 
  if x<0 then return -math.sqrt(-x) else return math.sqrt(x) end
end

local function flip(x,pivot)                   --'pivot' 0.5 results same as 1-x
  if x>pivot then                              --pivot value is equal in and out eg 
    return (pivot - pivot*x) /(1-pivot)        --pivot 0.74 results:
  else                             --{ 0.0=1.0, 0.25=0.91, 0.5=0.83, 0.75=0.75, 1.0=0.0 }
    return 1 -x/pivot + x     --function reverses through 0to1 and drags 0.5 to the pivot
  end
end


local function saturate(r,g,b,factor) 
  local a=(r+g+b)/3
  return a+(r-a)*factor, a+(g-a)*factor, a+(b-a)*factor
end

local function asaturate(r,g,b,ba,ra,ga) 
  local rr,rg,rb,a=r,g,b
  if ra then a=(r+g)/2 rr=a+(r-a)*ra end
  if ga then a=(g+b)/2 rg=a+(g-a)*ga end
  if ba then a=(b+r)/2 rb=a+(b-a)*ba end
  return rr,rg,rb 
end

--fuzzflip averages 2 flips for smoother transform
--distinction of output vals is reduced around pivot but increased at extremes
local function fuzzflip(x,pivot,fuz)
  return ( flip(x,pivot-fuz) + flip(x,pivot-fuz) )/2
end 

local function skew(x,pinch) --this might do same as 1-flip pivot=pinch
  if x<pinch then
    return x*pinch*2
  else
    return 2*pinch-2*pinch*x+2*x-1 
  end
end

local function alterbell(x,spower) --change x by a rough bell shaped distribution
  local kr=(math.random()+math.random()+math.random())/3 - 0.5
  return x + x*kr*spower + (kr*spower)
end

local function xprcnt(c)
  for k,v in pairs(c) do 
    if tonumber(v) then c[k]=tonumber(v)/100 else c[k]=false end 
  end
  return c
end

local function paldump(p)
  local t=dump(p)
  if t=="{  }" then t=" -clear-" else
    t=t:gsub("[=]"," ")
    t=t:gsub("[{}%[%]\"']","")
  end
  return t
end

local function lumtrans(c,p)

  local e=0.00001
  
--~   print(dump(p))
  local r=tonumber( "0x"..c:sub(1,2) )/255 +e
  local g=tonumber( "0x"..c:sub(3,4) )/255 +e
  local b=tonumber( "0x"..c:sub(5,6) )/255 +e
  
  if p.ingamma then
    r=(r^p.ingamma)
    g=(g^p.ingamma)
    b=(b^p.ingamma)
  end
  
  local a=c:sub(7,8)
  
  local rl,gl,bl = 20,34,10 --standard1 chroma bright weights
  rl,gl,bl = 10,35,3.5 --standard2 bright weights
  rl,gl,bl = 18,35,7 --standard3 bright weights
  rl,gl,bl = 17,33,10 --myadjust0 bright weights

  if p.random then 
    if math.random() < math.sqrt(0.3+p.random) then r=alterbell(r,p.random) end 
    if math.random() < math.sqrt(0.3+p.random) then g=alterbell(g,p.random) end 
    if math.random() < math.sqrt(0.3+p.random) then b=alterbell(b,p.random) end 
  end
  
  local mxlm=rl+gl+bl
  
  local alm= r*rl + g*gl + b*bl -- ab luminosity
  local lm = alm/mxlm -- will be between 0 and 1
  local tl = lm
  
  local lfuzz=(p.fuzz or 0)

  if p.flip and p.flip>0 then
    local tflip=tain(p.flip, 0.02, 1.9)/2
    tl=fuzzflip( tl, tflip, lfuzz/10 ) 
  end
  
--~   if p.rand then  tl=alterbell(tl,p.rand)  end
  if p.skew then  tl=skew(tl,p.skew/2)  end
  if p.contrast then tl=0.5 +(tl-0.5)*p.contrast end
  if p.level then  tl=tl+(p.level-1)  end
 
  local k = tl/lm
  local x= (tl-lm)

  if p.saturate and p.saturate>0 and not (p.flip and p.flip>0) then
    r,g,b=saturate(r,g,b,isqrt(p.saturate))
  end 
  
  r,g,b = asaturate(r,g,b,p.bsat,p.rsat,p.gsat)
  
--~   if false then
  if tl>lm then 
    r,g,b=r*k,g*k,b*k 
  else
    r, g, b=(r*k+r+x)/2, (g*k+g+x)/2, (b*k+b+x)/2 --lum reductn increases sat a bit
  end
--~   end
  
  local rep=0
  while ( math.max(r,g,b)>1 or math.min(r,g,b)<0 ) and rep<10 do
    local rr=r-tain(r,0,1) r=tain(r,0,1)
    local gg=g-tain(g,0,1) g=tain(g,0,1)
    local bb=b-tain(b,0,1) b=tain(b,0,1)
    
    rep=rep+1
    local y=(250-rep)/250
    rr=rr*(rl/(gl+bl))*y
    gg=gg*(gl/(rl+bl))*y
    bb=bb*(bl/(gl+rl))*y
    
    r=r+gg+bb  g=g+rr+bb  b=b+rr+gg
  end
  
  if p.saturate then
    r,g,b=saturate(r,g,b,isqrt(p.saturate))
  end 


  local s=255.5

  if p.ingamma then
    r=tain(r,0,1)^(1/p.ingamma)
    g=tain(g,0,1)^(1/p.ingamma)
    b=tain(b,0,1)^(1/p.ingamma)
  end
  
  if p.gamma then
    r=tain(r,0,1)^(p.gamma)
    g=tain(g,0,1)^(p.gamma)
    b=tain(b,0,1)^(p.gamma)
  end
  
  r=math.floor(tain(r,0,1)*s) 
  g=math.floor(tain(g,0,1)*s) 
  b=math.floor(tain(b,0,1)*s)
    
  return string.format("%x", 256*256*256 + r*256*256 +g*256 +b):sub(2)..a 
end


local defpal--={}
--[[
{
  ["flip"]=110,["fuzz"]=15,["saturate"]=145
}
]]

local palletsets = palletsets or lc_init('palletpresets', {} ) -- (like this to survive reloads) 

local function parsepallset(c,underpal) --returns prepared pallet keys from string
  local p,x,any={},nil,nil

  for _,k in ipairs({ "flip","fuzz","contrast","level","gamma","ingamma","skew","random","saturate","bsat","rsat","gsat" }) do
    x,c = strkeywithnum(c,k,2)
    
    if x then  p[k]=x any=true  end
     
    if underpal and x==nil and c and underpal[k]~=nil then
      x=underpal[k]
      --print("this is happening: "..k.."="..x)
    end
    
  end
  if not strblank(c) then
    --print("+ Unparsed input: '"..c.."'")
  end
  return any and p , c -- returns nil if no keys
end

--~ print("aaaaaa",dump(parsepallset('{ ["level"]=0.013, ["ingamma"]=-1.0 }')))

function strkeywithnum(c,d,n) --first nmbr in c after n chars of key d

  local d=string.sub(d,1,n or #d)
  local e,x= matchout(c, "%f[%a]"..d.."[%a]*[%s:=,\'\"%]]*(-?[%.%d]*)")
  if #e == #c then x=nil end --nothing matched
  return x,e
end 

presetreset=false

function readpalprops(sav) --returns lines of props with presets removed and updated palletsets
  local ufp= props["SciteUserHome"]..(GTK and "/.SciTEUser.properties" or "\\SciTEUser.properties") 
  local ul=filelines(ufp)

  local c,pa,pe,pw = 1

  if not sav then palletsets={} end
    
  while c<#ul do
    if string.match(ul[c],"# [-][-] Preset.*:%d+") then 
      if not sav then 
        local n = tonumber(string.match(ul[c],"# [-][-] Preset.-:(%d+)")) or 9 
        local pp=parsepallset(ul[c])
        palletsets[n]=pp or {}
      end
      table.remove(ul,c) c=c-1
    else
      c=c+1
    end
  end
  
  return ul
end

--[[
  write props and apply
  has 2 jobs, 
    write palletsets properly so it can be read by ~ul
    write colors mapped by palpset 0
    rsat is gsat  gsat is bsat bsat is rsat
]]

--apply palset0 write props and palset0
function writepropsapply0(ul) --ul is the props with psets removed ,readpalprops(sav)
  local ufp= props["SciteUserHome"].."/.SciTEUser.properties"
  local duepal= palletsets[0]
  if duepal and duepal.random then 
    math.randomseed(tonum(duepal.random)*723486789121) 
  end
  
  local c,pa,pe,pw = 1

  while c<#ul do c=c+1
    if string.match(ul[c],"#//# [pP]allet [sS]tart") then  pa=c break  end
  end

  if not pa then 
    prnte("-No '#//# Pallet Start' statement found in ",ufp) return
  end
    
  while c<#ul do c=c+1
    if string.match(ul[c],"#//# [pP]allet [eE]nd") then  pe=c break  end
  end
  
  if not pe then 
    prnte("-No '#//# Pallet End' statement found in ",ufp) return
  end
  
  --wipe section before rewrite
  while c<#ul do c=c+1
    if string.match(ul[c],"#//# [pP]allet [rR]ewrite") then 
      for r=c,pe+1,-1 do table.remove(ul,r) end
      break 
    end
  end
  
  -- for pallet start to end read lines and insert converted matches to rewrite section 
  local ic=pe
  
  local pp=xprcnt(flatcopy(palletsets[0]))
--~   print(dump(pp))
  for l = pa+1,pe-1,1 do
    local prfxd,colr=string.match(ul[l],"([^#~ ]+=[#])(%w+)")
    if duepal then
      pe=pe+1 -- does for loop recheck pe ? no its ok, pe was end of srclines to cpy
      if colr then --if a color is there, insert its result by palpset[0] 
        table.insert(ul,pe, prfxd..lumtrans(colr,pp) )
      else --if not color insert the line whatever it is comment or summin 
        table.insert(ul,pe,ul[l])
      end
    end
  end
  
  table.insert(ul,pe+1,"#//# Pallet Rewrite End")
  
  --rewrite all presets to before pal-end
--~   print("wrt ths in props",dump(palletsets))
  for i,s in pairs(palletsets) do 
    local ss="# -- Preset :"..i.." "..dump(s)
    table.insert(ul,ic,ss)  ic=ic+1
    --print("wrote "..ss)
  end
  
  local fr=table.concat(ul,"\n")
  
  local wasopen = Lc_.buftim[ufp]  --unused (?)
  local cfp=props['FilePath']
  
  local ff=panefocus()
  scite_OnOpenSwitch(onopenswitches,"rem") --remove openswitch hook
  
  sc_open(ufp)
  editor:SetText(fr) 
  scite.MenuCommand(IDM_SAVE)
  sc_open(cfp)
  
  scite_OnOpenSwitch(onopenswitches)
  panefocus(ff)
  
end


function listpallsets()
  local c
  local r={}
  
  table.insert(r," 0 (active): "..paldump(palletsets[0]))
      
  for i,s in ipairs(palletsets) do
    table.insert(r," "..i.."         : "..paldump(s)) c=true
  end

  if not c then table.insert(r," No Pallets have been saved ") end
  table.insert(r," 9 (toggle): "..paldump(palletsets[9]))
  return table.concat(r,"\n")
end

--ultimate palset is always saved to ps:0
--pa,  blank switches to previous set
--pa,%n switches to saved set
--pa,s %n saves current to slot %n

--pa,, switches to passthrough   ++
--pa, fl 55 li 5 etc, adds params to current
--pa,, fl 55 clears current and adds only this
 
-- 'current pallet filter (no.1) redefined, use ctrl+y to toggle test
-- 'current pallet filter (no.1) adjusted, use ctrl+y to toggle test
-- 'present pallet filter saved slot 1, use `pa,1` to apply

local applypset=defpal

function applyorrevert()
  
  if slowhammer() then return true end
  if not applypset then
    local ul=readpalprops() 
    palletsets[0],palletsets[9]=palletsets[9],palletsets[0]  --set prev as 0
    writepropsapply0(ul)
  end
  
  if applypset then
    local ul=readpalprops()
    palletsets[0],palletsets[9] = flatcopy(applypset),palletsets[0]
    writepropsapply0(ul)
    applypset=nil
  end
  
end

local nagged=0
function nagorapply()
  nagged=nagged+1
  if nagged==1 then
    print("--- Use `ctrl+shift+y` to toggle changes to theme")
  else
    applyorrevert()
  end
end

Lc_shortcut("toggle pallet|applyorrevert|Ctrl+Shift+Y")

function purgevals(t,a,b,c)
  for k,v in pairs(t) do
    if v==a or v==b or v==c then
      t[k]=nil
    end
  end
  return t
end

function themecommand(c) 

  print()
  local x,op,op2,clearpre --named operations fudged into writepropsapply0

  local ul = readpalprops() -- ul is prop lines, palletsets is updated
  
  local pl=flatcopy(palletsets[0]) -- the present config

  c=string.lower(c)
  
  if c:match("[%S]")==nil then --nothing but space
    print(palhelpmsg)
    return true
  end
  
  c,x = matchout(c,"^,") --comma at start of input
  
  local c,x = matchout(c,"^ ?li?s?t? ?$")
  if x~="" then trace( listpallsets() ) return end
  
  c,x= matchout(c,"^s ?([1-8]) ?")
  if #x>0 then 
    local op2=tonum(x)
    op2=tonum(x)
    if op2~=tain(op2,1,8) then
      print("- Save to slots 1-8 ('"..op2.."' is invalid)")
    else
      local ovw=flatcopy(palletsets[op2]) --backup the slot
      palletsets[op2]=purgevals( flatcopy(palletsets[0]) ,"",0 )
      listpallsets()
      if palletsets[op2] then  prnte("+ Overwrote no."..op2.." "..dump(ovw))  end
      writepropsapply0(ul)
      prnte("- Current Pallet saved to slot no."..op2)
    end
    return
  end

  local basepset=flatcopy(palletsets[0])

  c,x= matchout(c,"^l ?([1-8]) ?")
  if #x>0 then 
    local op2=tonum(x)
    if palletsets[op2] then 
      applypset=flatcopy(palletsets[op2])
      basepset=applypset
      prnte("- Loaded Pallet no."..op2.." "..dump(palletsets[op2]))
    else
      listpallsets()
      prnte("- Pallet no."..op2.." is missing")
    end
  end
  
  c,fclr= matchout(c,"^ ?clr ?")
  c,fadd= matchout(c,"^ ?add ?")
  
  if fclr~="" then basepset={} end
  
  local pdue,c=parsepallset(c)
  
  local remains=c:match("%w+")
  if c:match("%w+") then
    prnte("+ Unknown input :"..remains)
    prnte(palhelpmsg)
    return true
  end
  
  applypset=flatcopy(basepset,pdue) 
  applypset=purgevals( applypset ,"",0 )
  
  nagorapply()
end


palhelpmsg=[[- Redefine theme pallet filters with statements:
    pa,clr (..following filters *reset* the current )
    pa,add (..following filters *modify* the current )
  Default filter values are 100, eg: 
    pa,add gamma 110 flip 120 cont 80 satu 80
    pa,s5    save the current values to slot5
    pa,l3    load slot3 to the current
    pa,list  list the saved filtersets
  Available filters: 
    fuzz contrast level gamma ingamma skew random saturate rsat bsat gsat
    flip 100 --invert luminosity 100 is default pivot
    contrast level saturate 100 --adjust 100 is default value 
    rasat basat gasat --saturate ignoring r,g or b 100 is default
    ]]

--[[ 

--~ themecommand(" ")

--~ themecommand("flip=0.55 fuzz=-0.05 level=-0.053 ingamma=1.3")
--~ themecommand(",level=0.013 ingamma=1.0")

--Constrast increased
--~ a.skew=0.55 a.fuzz=0.1 a.level=0.043 a.gamma=0.95 a.contrast=1.06 writepropsapply0(a)

--Elevated background and saturation
--~ a.skew=0.55 a.fuzz=0.1 a.level=0.043 a.gamma=0.95 a.contrast=1.00 writepropsapply0(a)

--~ a.skew=0.55 a.fuzz=0.1 a.level=0.1 a.gamma=0.9 a.contrast=0.8 writepropsapply0(a)

--~ a.flip=0.46 a.fuzz=0.0 a.level=0.00 a.gamma=1.4 a.contrast=1.00 writepropsapply0(a)
--~ a.fuzz=0.0 a.level=0.09 a.gamma=1 a.contrast=1 a.skew=0.5 writepropsapply0(a)
--~ a.flip=0.5 a.fuzz=0.0 a.level=-0.0 a.gamma=1.25 a.contrast=1 writepropsapply0(a)
--~ a.flip=0.4 a.fuzz=0.2 writepropsapply0(a)
--~ a.skew=0.5 writepropsapply0(a)
--~ writepropsapply0() 

--Nicest flip
--~ a.flip=0.55 a.fuzz=-0.005 a.level=-0.03 a.ingamma=1.2 a.contrast=1.0 writepropsapply0(a)

--~ a.flip=0.55 a.fuzz=-0.005 a.level=-0.03 a.ingamma=1.2 a.contrast=1.0 writepropsapply0(a)
--~ a.flip=0.55

--~ writepropsapply0(a)

--nice silvered flip
--~ a.flip=0.55 a.fuzz=0.0 a.level=-0.25 a.ingamma=1.2 a.contrast=1.05 writepropsapply0(a)
--~ a.flip=0.55 a.fuzz=0.0 a.level=-0.25 a.ingamma=1.1 a.contrast=1.05 a.skew=0.55 writepropsapply0(a)

--nice silvered flip
--~ a.rand=0.00000 a.flip=0.56 a.fuzz=0.0 a.level=-0.25 a.ingamma=1.1 a.contrast=1.05 a.skew=0.55 writepropsapply0(a)


--~ writepropsapply0()
]]
