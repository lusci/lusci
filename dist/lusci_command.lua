Lc_.hopreset=false --globals reset to init value on bufferswitch

local lscmds=lc_init("lscmds",{})
local lsdesc=lc_init("lsdesc",{})
local kbtype=Lc_.kbtype
local kvkycd=Lc_.kvkycd

local kyvala=tonumber(kvkycd['a'])
local kyvalA=tonumber(kvkycd['A'])
local kyvalz=tonumber(kvkycd['z'])
local kyvalZ=tonumber(kvkycd['Z'])
local kyvaltab=tonumber(kvkycd['tab'])
local kyvalenter=tonumber(kvkycd['enter'])
local kyvalleft=tonumber(kvkycd['left'])
local kyvaldown=tonumber(kvkycd['down'])
local kyvalup=tonumber(kvkycd['up'])
local kyvalright=tonumber(kvkycd['right'])
kyvalcrpre=math.min(kyvaldown,kyvalup,kyvalleft,kyvalright)-1
kyvalcraft=math.max(kyvaldown,kyvalup,kyvalleft,kyvalright)+1
local kyvalcrpre=kyvalcrpre
local kyvalcraft=kyvalcraft

local interposekey=false 

local insert=table.insert
local concat=table.concat

function Lc_shortcut( cmdstr )
  local c=unconcat(cmdstr,"|")
--~   prnte(cmdstr,dump(c))
  if string.sub(cmdstr,-1)=="|" then c[3]=c[3].."|" end --separator is the hotkey 
  if #c<3 then print("- Lusci command error:",desc,fun,kyp,k) return end 
  local kks,kkc,kka,kkk = "","","",""
  local desc,fun,dkeys = c[1],c[2],c[3]
  
  local e,x,f=dkeys:match("^([^'\":]*)([%S])(%s?%a%a:.+)$")  --split kbtype cookies
  if f then 
    f=f:match((kbtype or "%a%a")..":(.)")  --isolate key for kbtype
    if f then dkeys=e..f else dkeys=e..x end     --recreate dkeys
  end
  
  local tk=unconcat(dkeys,"+")
  local e=tk[1]
  
  if #tk==2 and e:match("^[%u%s]+$") then
    kks=e:match("S?") kkc=e:match("C?") kka=e:match("A?")
  else
    for i=1,#tk-1 do 
      e=string.lower(tk[i]) --  lowercase for Tab etc and keys without shift
      if e:find("shift") then kks="S" 
      elseif e:find("ctrl") then kkc="C" 
      elseif e:find("alt") then kka="A"
      end 
    end
  end

  kkk=string.lower(tk[#tk])
  
  if #kkk>1 or kks=="" then 
    kkk=tostring(kvkycd[kkk]) 
  else 
    kkk=tostring(kvkycd[string.upper(kkk)])     --correct case of shifted keys
  end
  
  k=kks..kkc..kka..kkk
  
  local ff=unconcat(fun," ")
  ff[2] =ff[2] or "" 
  
  lscmds[k]=load("return "..ff[1].."("..ff[2]..")")
  lsdesc[k]=desc.."|"..dkeys.." "
end

function dumpkeytrap() prntd(lsdesc) end

function setinterposekeys(fnc) 
  if fnc==nil then return interposekey else interposekey=fnc end
end

function luscikeytrap(key, shift, ctrl, alt, ankg, cur)

--~   if interposekey then print(key) end 
  if interposekey and interposekey(key, shift, ctrl, alt) then return true end
  
  --no commands on unmodded alphabet
  if not(alt or ctrl) then
    if (key>=kyvala and key<=kyvalz) or (key>=kyvalA and key<=kyvalZ) then 
      Lc_.hopreset=true  return
    end
    if key==kyvalenter and not shift then 
      Lc_.hopreset=true
      if Lc_['dabtime'] and Lc_['dabtime']+600<os.time() 
      then Lc_['dabra']=nil Lc_['dabtime']=nil end --pw timeout
      
      return do_enter()
    end
  end 
 
  if key==kyvaltab and ctrl and not shift then Lc_.hopreset=true end

  if key>kyvalcrpre and key<kyvalcraft then
    if shift then 
      recordselnext(editor.Focus and editor or output , key)
    elseif not ctrl then
      Lc_.hopreset=true
    end
  end
  
  local kcd={}
  
  if shift then insert(kcd,"S") end
  if ctrl then insert(kcd,"C") end
  if alt then insert(kcd,"A") end
  insert(kcd,key)
  
  local fnc=lscmds[concat(kcd)]
  if fnc then return fnc() end	
end

scite_OnKey(luscikeytrap)

function do_enter() 
  
  if editor:AutoCActive() or output:AutoCActive() then return false end
  
  if output.Focus then
    noteSelection()
    return enter_terminal()
  end 

  local cp=editor.CurrentPos
  local ap,ep,lnn=poslinestfn(cp,editor)
  local bt=editor:textrange(ap,cp)
  
--~ eat trailing whitespace to fix auto indent after newline
--~   if string.find( string.sub(ct,(cp-ap)) , "%s+") then
--~ 	  ct=string.sub(ct,(cp-ap))
--~ 		editor:remove(cp,ep)
--~ 	end

  local ind,oud=0,0
  
  for _ in string.gmatch(bt, "%{") do ind=ind+1 end
  for _ in string.gmatch(bt, "%}") do ind=ind-1 end
  if string.match(bt,"^%s*%}")  then ind=ind+1 end --sloppy mg

  local mt=string.match(bt,"^%s*%/%/%/?%~?%s*") or string.match(bt,"^%s*") or ""
--~ 	if mt then mt= string.sub(mt,1,cp-ap) end
  
  while ind<0 do --? removing whitespace from proceeding?
    if     string.sub(mt,1,1)=="\t" then   mt=string.sub(mt,2)
    elseif string.sub(mt,1,2)=="%s%s" then mt=string.sub(mt,3) --adjust to props tabsize ? 
    else ind=0 
    end
    ind=ind+1
  end
    
  local nlch=nlchar()
  
  editor:insert(cp,nlch)
  editor.CurrentPos=cp+#nlch
  editor.Anchor=editor.CurrentPos

  if #mt~=0 then
    for i=1,ind do
      editor:Tab()
    end
    editor:insert(editor.CurrentPos, mt  )
    editor.CurrentPos=editor.CurrentPos+#mt
    editor.Anchor=editor.CurrentPos
  end
  
  enviscp(editor)
  return true 
end

function keysay(a,b,c,d,e,f,g)
  print(a,b,c,d,e,f,g)
end
 
--~ scite_OnKey(keysay)
--~ scite_OnKey(keysay,'remove')

local function edatpos(c,pos,act)
  if not act or act=="" then return c,pos end
  c=c or "" pos=pos or 0
  if pos>#c then pos=#c end
  local b=c:sub(1,pos) or "" 
  local d=c:sub(pos+1) or ""
  if act==-1 then d=d:sub(2)
  elseif act==-2 then b=b:sub(1,#b-1) pos=tain(pos-1)
  else b = b..act pos=pos+#act
  end
  return b..d,pos
end

Lc_shortcut("tipcmd|tipcmd|ctrl+m")

local tipcmdin,tipcmdpos="",0
function tipcmd(key, shift, ctrl, alt)
  if not setinterposekeys() then
    if output.Focus then return true end
    setinterposekeys(tipcmd)
    editor:CallTipShow( editor.CurrentPos, "; _"..tipcmdin)
    return true
  end
  
  local ky,lit=tostring(key),nil
  local ku=Lc_.vkkycd[ky]
 
  if ku=='space' then lit=" "
  elseif ctrl and ku=="v" then 
    lit=read_clipboard():match("[^%s\r\n]*")  ku=""
  elseif ku == 'tab' then lit="\t"
  elseif ku == 'delete' then lit=-1
  elseif ku == 'backspace' then lit=-2
  elseif ku == 'right' then tipcmdpos=tain(tipcmdpos+1,0,#tipcmdin) lit=nil
  elseif ku == 'left' then tipcmdpos=tain(tipcmdpos-1,0) lit=nil
  elseif ku == 'up' or ku=='down' then lit=""
  elseif ku == 'enter' or ku=='escape' then
    setinterposekeys(false)
    if ku=='enter' then
      trace(tipcmdin)
      local p=panefocus()
      panefocus(output)
      do_enter()
      panefocus(p)
    end
    editor:CallTipCancel()
    tipcmdin,tipcmdpos="",0
    return true
  elseif not ctrl and ku and #ku==1 then
    lit=ku
  else
    return true
  end
 
  if lit~="" then  tipcmdin,tipcmdpos = edatpos(tipcmdin,tipcmdpos,lit)  end

  local ct=tipcmdin:sub(1,tipcmdpos).."_"..tipcmdin:sub(tipcmdpos+1)

  --(calltip text doesnt update if length doesnt change)
  if lit==nil then  editor:CallTipShow( editor.CurrentPos, "; "..ct.." ")  end
  
  editor:CallTipShow( editor.CurrentPos, "; "..ct )
  return true
end


function grabpwd(passnm, msg, fnbk)
  print()
  setinterposekeys(grabbingpwd)  Lc_[passnm]=""  Lc_['pztxt']=passnm --passnm is the pswd name
  Lc_['pzfnc']=fnbk --this is a callback
  trace(msg) --'msg' is a msg on intitial call
  return true
end

--get password, allow sudo commands with echo <password> | sudo -S <command>
function grabbingpwd(key, shift, ctrl, alt)

  local ky,lit=tostring(key),nil
  --print(ky)
  if ky==kvkycd['space'] then lit=" " 
  elseif ky==kvkycd['backspace'] and #Lc_[Lc_.pztxt]>1 then 
    output:DeleteBack()
    Lc_[Lc_.pztxt] = Lc_[Lc_.pztxt]:sub(1,#Lc_[Lc_.pztxt]-1)
  elseif ky==kvkycd['enter'] or ky==kvkycd['escape'] then
    setinterposekeys(false)
    clearcurline(output)
    local ps = Lc_[Lc_.pztxt]  Lc_[Lc_.pztxt]=nil
    if ky==kvkycd['enter'] then
      Lc_['pzfnc'](ps)
    else
      print("- Input Cancelled")
    end
  else
    Lc_[Lc_.pztxt] = Lc_[Lc_.pztxt]..(lit or Lc_.vkkycd[ky] or "")
    trace("*") --..Lc_.pztxt
  end

  return true
end --salt passwords with startuptime to hide in memory? use fcrypt to encrypt and decrypt

--~ Lc_shortcut("testo|grabby|ctrl+shift+e")
function grabby()
  grabpwd("testo","\nenter the testo password:",function(c) print("twas:"..c) end )
end