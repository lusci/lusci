--twiddle should be transpose (drags letter forward) & selection twiddle
--sort should go near case

--todo: separate 'sort'
--make ops work with multiselection

Lc_shortcut("ctrlt twiddle|twiddle|ctrl+t")
Lc_shortcut("ctrlt twiddle|twiddle 'sort'|ctrl+shift+t")

function twiddle(sortit) --twiddles selection or sorts
  local pan,ca,ce,rv,_,_,bl,dl,dlt,pe = get_pane()

  if ca==ce then --no sel so quick twiddle requirde 
    local d=tain(ca+1,0,pe)
    filtertext(function(c) return c:sub(2,2)..c:sub(1,1) end ,"-", tain(ca-1),d)
    pan:SetSel(d,d)
    return true 
  end

  local multi=pan.Selections>1
  local bricks,mortar,tarfirst = {},{}
  local sels,xa,xe
  
  if multi then
    sels=getmulsel(pan)
    for i=1,sels.n do 
      if sels[i][1]~=sels[i][2] then
        bricks[i] = pan:textrange(sels[i][1],sels[i][2]) 
      end
    end
  else
    
    local txt=pan:textrange(ca,ce)
    local tar=nil
    xa,xe="",""

    local yca,ycc,ylnn=poslinestfn(ca,pan) --ugly detection of full line range
    local zca,zcc,zlnn=poslinestfn(ce,pan) --make rangelineendpos(c[,e])...
    
    if ce==zcc then zca,zlnn=zcc,zlnn+1 end
    if zlnn-ylnn > 1 and ca==yca and ce==zca then -- divby lines when sel at line ends
      tar=nlchar()
      txt,xa=matchout(txt,"^"..tar)
      txt,xe=matchout(txt,tar.."$")
      bricks=unconcat(txt,tar)
      for c=1,#bricks-1 do mortar[c]=tar end
    else
      
      local lft={"{","%(","%[","<"}
      local rgt={"}","%)","%]",">"} 
      local mid={"%}%{","%)%(","%]%[","><"} 

      if #txt>3 then
        local ya,ye, yc = txt:sub(1,1),txt:sub(-1),txt:sub(2,#txt-1)
        for i=1,#lft do
          if ya:find(lft[i]) and ye:find(rgt[i])
          and ( yc:find(mid[i]) or not ( yc:find(lft[i],1,true) or yc:find(rgt[i],1,true) )) then 
            xa,xe,txt=ya,ye,yc
            break
          end
        end
      end

      local onetar={"else","==","<=","~=",">=","%)%(","><","%]%["," = ","=",",","||","|",":",";","~",">","<","%+","/","%*","%^","&"," or "," and ","%.","%-","_","%.%."," "}
      local numtar={"[\n\r%s]+",",","|",":",";"}
      
      local sing,mul,cnt={},0,0
      
      for i,v in pairs(onetar) do
        cnt = select(2, txt:gsub("%s*"..v.."%s*","%1") )
        if cnt==1 then 
          local ga,ge=txt:find("%s*"..v.."%s*")
          if ga~=1 and ge~=#txt then sing[#sing+1]="%s*"..v.."%s*" end
        end
        if cnt>1 then mul=mul+cnt end
      end
--    prntb(#sing,mul,#txt)
      if sing[1] and #sing<5 and mul<2*#sing then
        tar=sing[1]
      else
        local cntm,top={},0
        for i,v in pairs(numtar) do
          cntm[i] = select(2, txt:gsub("%s*"..v.."%s*","%1") )
          if cntm[i]>top then top=cntm[i] tar="%s*"..v.."%s*" end
        end
      end
      
--    prntb("tar ! ",tar)
      
      if not tar then --didnt find a sing or numtar
        bricks=unconcat(txt,"")
      else
        local b=1
        repeat
          local sa,se=txt:find(tar,b)
          if sa then 
            if sa>1 then 
              bricks[#bricks+1]=txt:sub(b,sa-1) 
            else
              tarfirst=true --important for recombine
            end
            mortar[#mortar+1]=txt:sub(sa,se)
            b=se+1
          else
            bricks[#bricks+1]=txt:sub(b,#txt)
            b=#txt+1
          end
        until b>#txt or overloop()
      end 
--    prntb(bricks,mortar) 
--    if true then return true end 
    end 
  end
   
  if sortit then togglesort(bricks) else array_flip(bricks) end

  if not multi then 
    bricks=array_mingle(bricks,mortar,tarfirst)
    pan:ReplaceSel(xa..table.concat(bricks)..xe)
    if rv then ca,ce=ce,ca end
    pan:SetSel(ca,ce)
  else
    pan:BeginUndoAction()
    for i=1,sels.n do 
      if sels[i][1]~=sels[i][2] then
        pan:remove(sels[i][1],sels[i][2]) 
        pan:InsertText(sels[i][1],bricks[i])
      end
    end
    pan:EndUndoAction()
    setmulsel(sels)
  end

  return true
end

function togglesort(t) -- sorts shuffled >natural >standard >shuffled
  local std= standard_order(t)
  --standard -> shuffled
  if has_order(std)==1 then  return array_shuffle(t)  end

  local nat = natural_order(t)
  --nat -> std
  if has_order(nat)==1 then  return array_permute(t,std) end
  
  --shf -> nat
  return array_permute(t,nat) 
end

--
  
Lc_shortcut("toggle comments|togglecomment|ctrl+q")
--
--no selection toggles line
--subline selection stream comments ?
--multiline selection toggles by first line

function linecomment_ftype()
  local cmts={}
  cmts.lua="--"
  return cmts[ props['FileExt'] ] or "//"
end

function togglecomment()
  local pan,b,d,rv,a,c=get_pane()
  local obuf,jumping,otxt=props['FilePath']
  
  if pan==output then
    
    otxt,jumping = matchout( poslinetxt(output,c) ,"[^:]*:%s*%d+:")
    if not jumping then return true end
    
    local fp,ln = matchout( jumping ,":%s*(%d+):")
    
    if ln=="" then return true end
    ln=tonumber(ln)
    lc_open(fp,ln)
--  scite.MenuCommand(IDM_SWITCHPANE) --may not need to switch...
    pan,b,d,rv,a,c=get_pane(editor)
--  local ap,ep=poslinestfn(output.Length,output)
  end
      
  local la,lx,bl = poslinestfn(b,pan)
  local lx,le,dl = poslinestfn(d,pan)
  if d==lx and b~=d then le=lx end
     
  local rmv=false
  local q=linecomment_ftype()
  local qw=#q
  local qp="^"..txttopatt(q)
  
  local tw=pan.TabWidth
  local ut=editor.UseTabs
  local tsp=string.rep(" ",tw)
  local rps=string.rep(" ",qw)
  if ut then rps="\t" end

  local tx=pan:textrange(la,le)
 
  if tx:find(q,1,true) == 1 then rmv=true end
  
  local a,e,x= tx:find("^([^\r\n]*\r?\n?)",1)
  local cnv={}
   
  while e and e>=a do
    if overloop() then return true end
    if rmv then  --remove all first comments
      if x:find(qp) then
        if x:find("^~",qw+1) then
          x=x:sub(2+qw)         --clean this old style of toggle comment
        elseif x:find("^ %f[^ ]",qw+1) then
          x=x:sub(2+qw)         --single space used when no indent
        elseif ut and qw<tw then --use tabs
          x=x:sub(1+qw)
        else
          x=rps..(x:sub(1+qw)) --replace comment with rps
        end 
      end 
    else --splosh
      local ta,te = x:find("^\t*")
      local tblen=te-ta+1
      local sa,se = x:find("^ *",te+1)
      local splen=se-sa+1 + tblen*tw

      if not x:find(qp) then --no patt in line
        if splen<qw then
          if x:find("[^\r\n]",se ) then
            x=q.." "..x
          else
            x=q.." "..x
          end
        else
          if not ut then
            x=q..string.rep(" ",splen-qw)..(x:sub(splen+1)) 
          else 
            if tw>qw then x=q..x end
            if tw==qw then x=q..x:sub(2) end
            
            if tw<qw then 
              if x:find("^%s",2) then
                x=q..x:sub(3)
              else
                x=q..x:sub(2)
              end
            end 
          end 
        end --Weeeeeeee....
      end 
    end
    cnv[#cnv+1]=x
    a,e,x= tx:find("^([^\r\n]*\r?\n?)",e+1)
  end
  
  local u=table.concat(cnv)
  if tx=="" then u=q end  --odd case of press on an empty line
  inserttxt(la,le,u,pan)

  if jumping then
    local cxx=poslinetxt(editor)
    local a,e = poslinestfn(output.CurrentPos,output)
--  print(jumping..cxx)
    inserttxt( a + string.len(jumping),e, cxx, output)
--  scite.MenuCommand(IDM_SWITCHPANE)
--  lc_open(obuf)
  end

  return true
end

Lc_shortcut("line down|enterline|Shift+Enter")

function enterline()
  local pane,b,d,rv=get_pane()
  local push,ct=nlchar()
 
  if false and pane==output then --normal behaviour in output 
    output:ReplaceSel(nlchar()) 
  return true end 
  
  pane:BeginUndoAction()
  
  if b~=d then ct=taketxt(pane,b,d) d=b end --take to put selection on new line

  local clf,cla = poslinestfn(nil,pane)
  local crr=pane:textrange(clf,cla)
  
  local indy=nearbyindent(pane,b)
  
  if not ct and not crr:match("%S") and b-clf~=#indy then push="" end
  
  if push=="" then pane:SetSel(clf,cla) else pane:SetSel(clf,clf) end
  
  pane:ReplaceSel(indy..push)
  
  if ct then 
    local p,q=clf+#indy 
    local clen=inserttxt(p,p,ct,pane)
    q=clf+#indy+clen
    if rv then p,q = q,p end
    pane:SetSel(p,q)
    noteSelection()
  else
    pane:SetSel(clf+#indy,clf+#indy)
  end
  pane:EndUndoAction()
  return true
end


-- 
-- 0.1 000 12 abra 0123 001 peep 100 
-- //\\Oo/\\\  crab