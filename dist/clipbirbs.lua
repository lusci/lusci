
Lc_shortcut("copy|mod_copy|Ctrl+c")
Lc_shortcut("cut|mod_cut|Ctrl+x")
--records copyposition for selection restoration function
--and records new clipboard strings

local clippet= clippet or Stacket(24)
Lc_.clippet=clippet

local maxcliptxtsz=2200000
local bufanc,bufcrp = Lc_.bufanc, Lc_.bufcrp

local function recordpastsel(pan,a,c)
  local g = pan==editor and (props['FilePath'] or "nil") or "o\t"
  local la,lc=pan:LineFromPosition(a),pan:LineFromPosition(c)
  bufanc[g] = { [1]=la,[2]=a-pan:PositionFromLine(la) }
  bufcrp[g] = { [1]=lc,[2]=pan.LineEndPosition[lc]-c }
end

function mod_cut()
  local pan,f,a,rv = get_pane()
  if pan.Selections > 1 then return false end
  
  if pan.Anchor == pan.CurrentPos then
    pan.Anchor=0  pan.CurrentPos=pan.Length 
    return true
  end
  mod_copy()
  pan:ReplaceSel("")
  return true
end

function mod_copy()  --restores sel if nosel, copies and caches copied text
  local pan,a,c,rv = get_pane() 
  
  --restore prev selection if none
  local g = pan==editor and (props['FilePath'] or "nil") or "o\t"
  if pan.Anchor==pan.CurrentPos and bufanc[g] then 
    pan:SetSel(
      pan:PositionFromLine(bufanc[g][1]) + bufanc[g][2]
     ,pan.LineEndPosition[bufcrp[g][1]] - bufcrp[g][2]
    )
    return true
  else
    pan:Copy()
    local tr=pan:GetSelText()
    if tr and #tr<maxcliptxtsz then clippet:push(tr) end
    return true 
  end
end

Lc_shortcut("paste prev clipboards|paste_prev|Ctrl+Shift+V")

function paste_prev()
  local pan,pf,pa,rv=get_pane()
  local g,fc=nil,0
  
  if pf==pa then
    clippet:hop(-1)
    clippet:push()
    g=clippet:hop()
  else
    g=clippet:hop()
--  if g==pan:textrange(pf,pa) then g=clippet:hop() end
  end
  
  if not g then g="" tracerr("clipboard cache error") end
  
  pan:ReplaceSel(g)
  pan:SetSel(pf,pf+#g)
  return true
end

--useful
--editor:ClearSelections()
--editor:SetEmptySelection(position caret)

Lc_shortcut("Code Paste|code_paste|Ctrl+v")
Lc_shortcut("Code Paste|code_paste|Ctrl+shift+?")
-- Lc_shortcut("Code Paste|code_paste|Ctrl+shift+~ us:|")

--[[
Code Paste, inserts lines at the carets indent level
]]

function code_paste ()
  
  local pane=get_pane()
  if pane.SelectionMode~=0 or pane.Selections > 1 then return false end
 
  get_pane():BeginUndoAction()
  
  local tabw=pane.TabWidth
  local apos,cpos=pane.Anchor,pane.CurrentPos
  local ax,cx,nosel=apos,cpos,apos==cpos
  
  local olcount=pane.LineCount
  local wassel=(cpos~=apos)
  local sflipped=false
      
  if cpos<apos then
    cpos,apos = apos,cpos
    sflipped=true
  end

  local ditch
  if pane:PositionFromLine(pane:LineFromPosition(cpos))==cpos and cpos==apos then
    ditch=true
  end
    
  local undercopy
--~   if pane:LineaposFromPosition()~=pane:LineFromPosition(cpos) then
--~   if pane:LineFromPosition(apos)~=pane:LineFromPosition(cpos) then
  if apos~=cpos and not props['dont_undercopy_paste'] then
    undercopy=pane:textrange(apos,cpos)
    if cpos-apos<200 and not string.find(undercopy,"[%w%p]") then
      undercopy=nil
    end
  end

  local destin = indent_of_range(pane,lineancpos(pane,apos),apos)
  
  pane:Paste()
  pane:EndUndoAction()
  pane:BeginUndoAction()
  
  local pastend = pane.CurrentPos --it will be this if multiline..
  local multipaste=pane:LineFromPosition(apos)~=pane:LineFromPosition(pane.CurrentPos)	
  local stopStall = pane.CurrentPos-apos > 1000000
  
  --if a singleline pasted clear selection, return false end
  if (pane.LineCount)==olcount or stopStall or ditch or not multipaste then
    if not multipaste then --(still after clear first sel)
      pane.Anchor,pane.CurrentPos=pastend,pastend
    else
      pane.Anchor,pane.CurrentPos=pastend,pastend
    end
    recordpastsel(pane,apos,pastend)
        
    if undercopy then pane:CopyText(undercopy) end
    pane:EndUndoAction()
    return true
  end

  local pEnd=pastend

  --isolate pasted text with newlines (for tab shifting )
  pane:AddText("\n")
  pane:InsertText(apos,"\n")

  local pBeg=apos+1
  local pEnd=pastend	-- +1 ??
  
  if nosel then 
    destin = indent_of_range(pane,lineancpos(pane,apos),apos) 
  end
  --print("apos:",apos,lineancpos(pane,apos))
  --if true then return true end
  --note indent of first paste line       --caution fine wove spagetti...
  local fpasti=indentinposline(pane,pBeg)
  
  --start of 2nd paste line
  local en=pane:PositionFromLine(pane:LineFromPosition(pBeg)+1) 
  local fpasti2=indentinposline(pane,en)
  --print(" destin:",destin,"fpasti:",fpasti,"fpasti2:",fpasti2)
  --if true then return end
  --use 2nd line i when 1st seems clipped
  --better to read the line and estimate if it indents...
  if fpasti==0 and fpasti2>1 then fpasti=fpasti2 end 
                        
  local lenA=pane.Length
  local shiA=destin-fpasti
  
  local tEnd --sometime pEnd is not on newline when end of paste was?? fdg..
  if pane:PositionFromLine(pane:LineFromPosition(pEnd))==pEnd then
    tEnd=pEnd
  else
    tEnd = pane:PositionFromLine(pane:LineFromPosition(pEnd)+1)
  end
  
  tab_mov(pane,pBeg,tEnd,shiA)	
  tab_mov(pane, pane:PositionFromLine(pane:LineFromPosition(pBeg))
              , pane:PositionFromLine(pane:LineFromPosition(pBeg)+1)
              , 0-destin) --move first line by -first.indent

  local lendiff=pane.Length-lenA
    
  pEnd=pEnd+lendiff
  
  pane:SetSel(pEnd+1,pEnd+2) pane:Clear() --take out the newlines
  pane:SetSel(apos,apos+1) pane:Clear()
  
  --if on a newline ,insert the indent:destin
  local ls = pane:LineFromPosition(pEnd+1)
  if (pane:PositionFromLine(ls)-pEnd)==0 and (pane.LineEndPosition[ls]-pEnd)>0 then
    pane:SetSel(pEnd,pEnd)
    
    if destin>15 then
      print("-pasting without tabs:"..destin) --huh
    else
    for i=1,destin do
      pane:Tab()
    end
    end
--~ 		pane:ReplaceSel(
--~ 		string.sub("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t",0,destin)
--~ 		string.sub("                                                                                      ",0,destin*pane.TabWidth)
--~ 		)  --if pane use tabs, if not... or do that operation using pane tab?
  end
  
  pane:EndUndoAction()
  if undercopy then pane:CopyText(undercopy) end
  pane:SetSel(pEnd,pEnd)
  recordpastsel(pane,pBeg-1,pEnd)
  return true
end

function indent_of_range(pane,a,c) --counts indents to a position through letters and wspace
  local gark,cnttab=string.gsub(pane:textrange(a,c),"[\t]","")
  local indy=cnttab+math.floor(string.len(gark)/pane.TabWidth)	--calcued tab indents at anchor
  return indy
end

function tab_mov(pane,a,c,indy) --mov on a selection
  if a>c then a,c=c,a end
  pane:SetSel(a,c)
  indy=math.floor(indy)
  while indy~=0 do
    if indy>0 then
      pane:Tab() indy=indy-1
    else
      pane:BackTab() indy=indy+1
    end
  end
end
