-- LuSci (Luscious)
-- Extman is a Lua script manager for SciTE. It enables multiple scripts to capture standard events
-- without interfering with each other. For instance, scite_OnDoubleClick() will register handlers
-- for scripts that need to know when a double-click event has happened. (To know whether it
-- was in the output or editor pane, just test editor.Focus).  It provides a useful function scite_Command
-- which allows you to define new commands without messing around with property files (see the
-- examples in the scite_lua directory.)
-- extman defines three new convenience handlers as well:

-- scite_OnWord (called when user has entered a word)
-- scite_OnEditorLine (called when a line is entered into the editor)
-- scite_OnOutputLine (called when a line is entered into the output pane)

-- this is an opportunity for you to make regular Lua packages available to SciTE
--~ package.path = package.path..';C:\\lang\\lua\\lua\\?.lua'
--~ package.cpath = package.cpath..';c:\\lang\\lua\\?.dll'
-- useful function for getting a property, or a default if not present.

--~ if true then return end
 
Lc_={} 

function lc_init(gkey,initval) --returns ref to global which will not reset on srcfile reruns
  Lc_[gkey] =Lc_[gkey] or initval
  return Lc_[gkey]
end

_G['GTK'] = props['PLAT_GTK']=="1"
Lc_['platsep'] = (GTK and "/") or "\\"
platsep = Lc_['platsep']

Lc_['bufanc']={} --selection anchors, a table entry for each buffer of last selected anchor
Lc_['bufcrp']={} --selection currentPoss..
Lc_['buftim']={} --dictionary of opened buffers with times, used by various

--reorganise these globals, put all standard lusci globals in _ls
-- to make 
--_ls.sela
--_ls.sele
--_ls.opnbffrs
--_ls.kvkycd _ls.vkkycd  keycodetoval valtokeycode > kctv vtkc simpler 

local _MarginClick,_DoubleClick,_SavePointLeft = {},{},{}
local _SavePointReached,_Open,_SwitchFile = {},{},{}
local _BeforeSave,_Save,_Char = {},{},{}
local _Word,_LineEd,_LineOut = {},{},{}
local _OpenSwitch = {}
local _UpdateUI = {}
local _Key = {}
local _DwellStart = {}
local _Close = {}
local _undispatch = {}
local append  = table.insert
local find = string.find
local size = table.getn
local sub  = string.sub
local gsub = string.gsub

local function DispatchOne(handlers,arg)
  for i,handler in pairs(handlers) do
    local fn = handler
    if _undispatch[fn] then
      handlers[i] = nil
      _undispatch[fn] = nil
    end
    local ret = fn(arg)
    if ret then return ret end
  end
  return false
end

local function Dispatch4(handlers,arg1,arg2,arg3,arg4)
  for i,handler in pairs(handlers) do
    local fn = handler
    if _undispatch[fn] then
      handlers[i] = nil
      _undispatch[fn] = nil
    end
    local ret = fn(arg1,arg2,arg3,arg4)
    if ret then return ret end
  end
  return false
end

-- the standard SciTE Lua callbacks are used to call installed extman handlers
function OnMarginClick()    return DispatchOne(_MarginClick)   end
function OnDoubleClick()    return DispatchOne(_DoubleClick)    end
function OnSavePointLeft()   return DispatchOne(_SavePointLeft)  end
function OnSavePointReached() return DispatchOne(_SavePointReached) end
function OnChar(ch)         return DispatchOne(_Char,ch)  end
function OnSave(file)       return DispatchOne(_Save,file) end
function OnBeforeSave(file) return DispatchOne(_BeforeSave,file) end
function OnSwitchFile(file) return DispatchOne(_SwitchFile,file) end
function OnOpen(file)       return DispatchOne(_Open,file) end
function OnClose()          return DispatchOne(_Close)  end

function OnUpdateUI() 
  if editor.Focus then return DispatchOne(_UpdateUI)
  else
    return false
  end
end

function OnUserListSelection(tp,str)
  if _UserListSelection then
    local callback = _UserListSelection
    _UserListSelection = nil
    return callback(str)
  else return false end
end

function OnKey(key,shift,ctrl,alt)   return Dispatch4(_Key,key,shift,ctrl,alt)  end
function OnDwellStart(pos,s)         return Dispatch4(_DwellStart,pos,s)        end

-- may optionally ask that this handler be immediately
-- removed after it's called
-- ***
local function append_unique(tbl,fn,rem)
  local once_only
  if type(fn) == 'string' then
    if fn == 'once' then once_only = true end
    fn,rem = rem,nil
    if once_only then  --puts fn in a table called remove as key and value
      _undispatch[fn] = fn
    end
  else
    _undispatch[fn] = nil --if 2nd param not string, third param is ignored
  end
  local idx
  for i,handler in pairs(tbl) do
    if handler == fn then idx = i break end  --looks for fn in sched table
  end
  if idx then
    if rem and tbl[idx] then --third param not null, remove from sched table
      table.remove(tbl,idx)
    end
  else
    if not rem then 
      append(tbl,fn)
    end
  end
end

-- this is how to register handlers with extman
-- parameters:
-- ( fn       ) to schedule a function
-- ('once',fn ) to schedule function once
-- (fn,'rem'  ) to remove function from schedule

function scite_OnMarginClick(fn,rem)      append_unique(_MarginClick,fn,rem)   end
function scite_OnDoubleClick(fn,rem)      append_unique(_DoubleClick,fn,rem)   end
function scite_OnSavePointLeft(fn,rem)    append_unique(_SavePointLeft,fn,rem) end
function scite_OnSavePointReached(fn,rem) append_unique(_SavePointReached,fn,rem) end
function scite_OnOpen(fn,rem)             append_unique(_Open,fn,rem)       end
function scite_OnSwitchFile(fn,rem)       append_unique(_SwitchFile,fn,rem) end
function scite_OnBeforeSave(fn,rem)       append_unique(_BeforeSave,fn,rem) end
function scite_OnSave(fn,rem)             append_unique(_Save,fn,rem)     end
function scite_OnUpdateUI(fn,rem)         append_unique(_UpdateUI,fn,rem) end
function scite_OnChar(fn,rem)             append_unique(_Char,fn,rem)     end
function scite_OnOpenSwitch(fn,rem)       append_unique(_OpenSwitch,fn,rem) end
function scite_OnKey(fn,rem)              append_unique(_Key,fn,rem)        end
function scite_OnDwellStart(fn,rem)       append_unique(_DwellStart,fn,rem) end
function scite_OnClose(fn,rem)            append_unique(_Close,fn,rem)      end

Lc_['extman_bffrlast']={}
local function opswitch(f)
  if #(Lc_.extman_bffrlast)==0 or Lc_.extman_bffrlast[1]~=props['FilePath'] then 
    Lc_.extman_bffrlast[1]=props['FilePath'] 
    DispatchOne(_OpenSwitch,f)
  end
end

scite_OnOpen(opswitch)
scite_OnSwitchFile(opswitch)

-- the handler is always reset!

local word_start,in_word,current_word
-- (Nicolas) this is in Ascii as SciTE always passes chars in this "encoding" to OnChar
local wordchars = '[A-Za-zÀ-Ýà-ÿ]'  -- wuz %w

local function on_word_char(s)
  if not in_word then
    if find(s,wordchars) then
      -- we have hit a word!
      word_start = editor.CurrentPos
      in_word = true
      current_word = s
    end
  else -- we're in a word
    -- and it's another word character, so collect
  if find(s,wordchars) then
    current_word = current_word..s
  else
    -- leaving a word; call the handler
    local word_end = editor.CurrentPos
    DispatchOne(_Word, {word=current_word,
        startp=word_start,endp=editor.CurrentPos,
        ch = s
      })
    in_word = false
  end
end
-- don't interfere with usual processing!
return false
end

local last_pos = 0
local path_pattern
local tempfile
local dirsep

if GTK then
  tempfile = '/tmp/.scite-temp-files'
  path_pattern = '(.*)/[^%./]+%.%w+$'
  dirsep = '/'
else
  tempfile = '\\scite_temp1'
  path_pattern = '(.*)[\\/][^%.\\/]+%.%w+$'
  dirsep = '\\'
end

--------------------------------------------------------------
function runcommand(cmd,pth,grab)
   local r,ls,outn={},{},0
   if pth and #pth then  cmd='cd '..quodir(pth)..' && '..cmd  end
   
   local handle=io.popen(cmd)
   if not handle then
     r={[3]=-1}
   else
     for line in handle:lines() do
       if not grab then  print(line) else ls[#ls+1] = line end
       outn=outn+1
     end
     r={handle:close()}
     if outn==0 then  end --do something for no output?
   end
   if grab then  r[1]=table.concat(ls,"\n") end
   return r
end

function quodir(d)
  d=d or ""
  if d:find("[&]") then 
    tracerr(" Odd path seen :",d)
    d='hazardch4r' 
  end
  local x = GTK and "/" or "[\\/]"
  a=d:match("^([~.]"..x..")") or "" 
  d=d:match("^[~.]"..x.."(.*)") or d
  if not d:find("^[\"']") then d='"'..d..'"' end
  return a..d
end

function navdir(dd,ad) --dd is test dir, ad is origin dir
  if not dd then return false,1 end
  ad=ad~="" and ad
  if not ad then
--  print("seeing",dd)
    if not dd:find("^%w?:?[\\%/~]") then return false,1 end
    ad=dd
  end
--print(dd,ad)
  if not GTK then dd=slashdir(dd) ad=slashdir(ad) end
--print(dd,ad)
  local r = runcommand( --run cd to due dir 
    "cd "..quodir(dd)..((GTK and " && pwd") or " && echo %cd%") 
    ,ad,true            --from ad dir
  )
--print( r[0],r[1],r[2],r[3])
  return r[3]==0 and r[1], r[3] -- rtns: false | directory, errcode
end

function dircheck(cd,ad) return navdir(cd,ad) end

function slashdir(p) return p:gsub("[^"..platsep.."/]$","%1"..platsep) end

function fileexists(f) 
  if not f then return false end
  local fio = io.open(f,"r")
  if fio then
    local _,b=fio:read()
    fio:close()
    if b==nil then return true end --b should only exist on file problem ?
  end
  return false
end

function sendscite(m,p)
  if not (p and p==output) then scite.SendEditor(m) else scite.SendOutput(m) end
end

--[[--------------------------------------------------]]

function quote_if_needed(target)
  if not find(target,'%s') or find(target,"^[\"\'].+[\"\']$") then return target end
  return '"'..target..'"'
end

function path_of(s)
  local _,_,res = find(s,path_pattern)
  if _ then return res else return s end
end

local extman_path = path_of(props['ext.lua.startup.script'])
-- print("extman_path",extman_path)
local lua_path = props['ext.lua.directory']
-- print("lua_path",lua_path)
if lua_path =="" or not lua_path then
  lua_path = extman_path
end
-- print("lua_path",lua_path)

function dirmask(mask,isdir)
  local attrib = ''
  if isdir then
    if not GTK then
      attrib = ' /A:D '
    else
      attrib = ' -F '
    end
  end
  if not GTK then
    mask = gsub(mask,'/','\\')
    return 'dir /b '..attrib..quote_if_needed(mask)
  else
    return 'ls -1 '..attrib..quote_if_needed(mask)
  end
end

function scite_FileExists(f)
  local f = io.open(f,'r')
  if not f then return false else f:close() return true end
end

-- use this to launch Lua Tool menu commands directly by name
function scite_MenuCommand(cmd)
  if type(cmd) == 'string' then
    cmd = name_id_map[cmd]
    if not cmd then return end
  end
  scite.MenuCommand(cmd)
end

local loaded = {}
local current_filepath

function haspath(fp) 
  if GTK then return string.find(fp,"^/") and true
  else return string.find(fp,"^%w:") and true end
end

function scite_dofile(fp,force) --old dofile that doesnt do files already done
  if not haspath(fp) then fp = lua_path..Lc_.platsep..fp end
  if scite_FileExists(fp) then
    if force or not loaded[fp] then
      dofile(fp)
      loaded[fp] = true
    end
    return true
  else
    print("scite_dofile fail: "..fp)
    return false
  end
end

function OpenCmd()
  os.execute("start /D"..props['FileDir'])
end

function OpenExplorer()
  os.execute("explorer "..props['FileDir'])
end

if not dircheck(lua_path) then
  print('Error: directory "'..lua_path..'" not found')
end

scite_dofile('exkeycode.lua')
scite_dofile('stackets.lua')
scite_dofile('lusci_gen.lua')
scite_dofile('lusci_sci.lua')
scite_dofile('lusci_command.lua')
scite_dofile('watcher.lua')    --seen file database and tab switcher 
scite_dofile('bookmarkhop.lua')
scite_dofile('terminal.lua')
scite_dofile('multiword.lua')
scite_dofile('carethops.lua')
scite_dofile('arrowfun.lua')
scite_dofile('chunkselect.lua')
scite_dofile('twiddles.lua')   --twiddles or sorts lines or passages
scite_dofile('clipbirbs.lua')  --ctl c x v mods
scite_dofile('selbrak.lua')
scite_dofile('autocom.lua')
scite_dofile('recoin.lua')     --the list, command
scite_dofile('wordstep.lua')   --hotkey cycles through nearby words
scite_dofile('palletprox.lua')
scite_dofile('zlot1.lua')

scite_dofile('t/tallytext.lua')

scite_dofile('fcrypt.lua')     --transparent file encryption 
scite_dofile('arcfour.lua')    --encrypter lib for fcrypt

scite_dofile('surveykeycodes.lua')

--scite_dofile('carecrow.lua')
--scite_dofile('withattr.lua')
--scite_dofile('propzup.lua') -- a non general tool for crunching props file
--scite_dofile('switch_headers.lua')
--scite_dofile('ext_select.lua')
--scite_dofile('snippet.lua')
--scite_dofile('indent.lua')
--scite_dofile('eliza.lua')
--scite_dofile('inactive.lua')
--scite_dofile('SciTE_ExportBase.lua')
--scite_dofile('SciTE_ExporterHTML.lua') 
--scite_dofile('SciTEHexEdit.lua')

print("                       *** megabytes free ***     ")
