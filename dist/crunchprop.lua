-- for removing duplicate declarations and comments in props before a manual re-ording

function crunchprops()

  local th={}
  --store unique line nos of last (re)declaration of prop or commented prop
  --or other line string
  for i = 0,editor.LineCount-1 do
    local l=editor:GetLine(i)
    if l then
      local eq=l:match("^[^=]*[=]")
      if not eq then eq=l:match("^[#][^=]*[=]") end
      if not eq then eq=l end
      th[eq]=i 
    end
  end

  local ou={}
  --rebuild editors line list ommiting all the early repeated prop declarations
  for k,v in pairs(th) do
    local x=editor:GetLine(v)
    ou[v]=x:match("[^\r\n]*")
  end
  
  for i = 0,editor.LineCount-1 do
    if ou[i] then print(ou[i]) end
  end
  
end

--crunchprops()
