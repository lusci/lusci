--merges duplicate prop def by replacing above with below
--and copies chunks of comments above the floated def
--this may make a bit of a mess but can consolidate an unweildy hodge podge of props

function propzup() --consolidates duplicate properties and floats comments with them

  local function pmatch(x,y)
    if not string.match(x,"[=]") then return false end --exceptions, is blank, has no equals,
    if string.match(x,"^[^=]+[=]")==string.match(y,"^[^=]+[=]") then return true end
  end

  local m=panelines(editor)
  
  for c=#m,2,-1 do
  
    for b = c-1,1,-1 do 
      if pmatch(m[c],m[b]) then
        m[b]=m[c]
        table.remove(m,c)
        
        for cc=c-1,3,-1 do 
   
          if string.sub(m[cc],1,1)=="#" then
            if m[cc]~=m[b-1] then
              table.insert(m,b-1,m[cc])
              table.remove(m,cc+1)
            else
              table.remove(m,cc)
            end
          else
            break
          end
          
        end --next cc
        
        break
      end --if match
     
    end --next b
  
  end --next c
 
  editor:SetText(table.concat(m,""))
end


--[[
  //int rc=19,gc=38,bc=7; - example reference values
  int rc=22,gc=34,bc=8; //must sum(64). ratios reduced to improve bounds
  
  int span=xcontrast; //64 max/full             63
  int sign =1; if(span<0){ sign=-1; } 
  int lift=-6;  //32 max/full                   8
  //int drksater=36; //16 resolution    36
  
  int ave=(r+g+b)/3;
  int l=( r*rc +g*gc +b*bc )/64 ; //lum, max 4088
  int lif=sign*((((span-64)*16)*lift)/64);
  long LtargLum= 2044+(((l-2044-lif/2)*(long)span)/64)+lif+(xshift*60); //target lum;
  if (LtargLum<1) LtargLum=1;
  if (LtargLum>4088) LtargLum=4088;
  int targLum=(int)LtargLum;
  //targetlum = hlflum+(thislum-halflum)*i;	
  //int targLum= 2044+(((l-2044)*span)/64); //target lum;
]]

--[[
z="ffffff"
print(z,lumtrans(z,64,8,0)   )
print(z,lumtrans(z,64,-60,0) )
print(z,lumtrans(z,10,8,0)   )
print(z,lumtrans(z,-64,8,0)   )
z="eeeeee"
print(z,lumtrans(z,63,8,0)   )
print(z,lumtrans(z,63,-60,0) )
print(z,lumtrans(z,10,8,0)   )
print(z,lumtrans(z,-60,8,0)   )
z="888888" 
print(z,lumtrans(z,63,8,0)   )
print(z,lumtrans(z,63,-60,0) )
print(z,lumtrans(z,10,8,0)   )
z="111111" 
print(z,lumtrans(z,63,8,0)   )
print(z,lumtrans(z,63,-6,0)  )
print(z,lumtrans(z,10,8,0)   )
                             
z="cc77ff" 
print(z,lumtrans(z,163,8,0)   )
print(z,lumtrans(z,163,-6,0)  )
print(z,lumtrans(z,10,8,0)   )

]]

--[[
li,mip
li,mip,
li,mip ,pop
li,mip ,pop,

li,mip,,pop
li,mip,,pop,

if first char after li, is punctuation, that is the sep char

li,,mip,,pop   --no change comma still escape char
li,:mip::pop:  --colon is escape character
li,,:mip,,:pop  --comma is escape
if your list/replace terms include commas then set a different escape char which they do not contain such as semicolon or forward slash

li,/mip,//mop,

So the rule is simple, comma is the escape separator but if the first character of term is non alphnumeric then it becomes the escape character. This lets commas to be used simply for most cases , and allows an alternative escape to be designated immediately for cases containing commas or starting with punctuation. 
]]