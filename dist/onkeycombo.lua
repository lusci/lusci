local kvkycd=kvkycd
local kyvala=tonumber(kvkycd['a'])
local kyvalA=tonumber(kvkycd['A'])
local kyvalz=tonumber(kvkycd['z'])
local kyvalZ=tonumber(kvkycd['Z'])
local kyvaltab=tonumber(kvkycd['tab'])
local kyvalenter=tonumber(kvkycd['enter'])
local kyvalleft=tonumber(kvkycd['left'])
local kyvaldown=tonumber(kvkycd['down'])
local kyvalup=tonumber(kvkycd['up'])
local kyvalright=tonumber(kvkycd['right'])
kyvalcrpre=math.min(kyvaldown,kyvalup,kyvalleft,kyvalright)-1
kyvalcraft=math.max(kyvaldown,kyvalup,kyvalleft,kyvalright)+1
local kyvalcrpre=kyvalcrpre
local kyvalcraft=kyvalcraft

local interposekey=false 
function setinterposekeys(f) 
  if f==nil then return interposekey else interposekey=f end
end

function luscikeytrap(key, shift, ctrl, alt, ankg, cur)

--~   if interposekey then print(key) end 
  if interposekey and interposekey(key, shift, ctrl, alt) then return true end
  
  --no commands on unmodded alphabet
  if not(alt or ctrl) then
    if (key>=kyvala and key<=kyvalz) or (key>=kyvalA and key<=kyvalZ) then 
      Lc_.hopreset=true  return
    end
    if key==kyvalenter and not shift then 
      Lc_.hopreset=true
      if Lc_['dabtime'] and Lc_['dabtime']+600<os.time() 
      then Lc_['dabra']=nil Lc_['dabtime']=nil end --pw timeout
      
      return do_enter()
    end
  end 
 
  if key==kyvaltab and ctrl and not shift then Lc_.hopreset=true end

  if key>kyvalcrpre and key<kyvalcraft then
    if shift then 
      recordselnext(editor.Focus and editor or output , key)
    elseif not ctrl then
      Lc_.hopreset=true
    end
  end
  
  local kcd={}
  
  if shift then insert(kcd,"S") end
  if ctrl then insert(kcd,"C") end
  if alt then insert(kcd,"A") end
  insert(kcd,key)
  
  local fnc=lscmds[concat(kcd)]
  if fnc then return fnc() end	
end

scite_OnKey(luscikeytrap)

