--deranged code by strainer
--delete and rewrite entirely using scites multiple carets

local find = string.find 
local match= string.match
local gsub = string.gsub 
local insert = table.insert 
local sub= string.sub
local tain= tain
local poslinestfn = poslinestfn
--abc abbbbc aabcc a a abc abc[] abc() abc_abc
--typical localslocal tdum,tdee,swp  --tweedles, swapped
local tsline,tslpos  --thisline thislineposition
local tpath          --this path
local str            --a string
local pane           --the pane
local _,u,v,w,a,b    --transients
local hit, fpos, cnt --hit, found pos, count
local multirun
local rcmark=11 --recoin mark
--[[

hotkeyed renaming tool, recoin
keying on a selected word, lists the word in current and recent files

todo
Will enter file recoin mode..
leaving the word via cursors leaves the mode
replacing word
]]

local function domarkers(a,e)
    scite.SendEditor(SCI_SETINDICATORCURRENT, 0)
    scite.SendEditor(SCI_INDICATORCLEARRANGE, 0, editor.Length)
    scite.SendEditor(SCI_INDICSETALPHA, 0, 70)
    scite.SendEditor(SCI_INDICSETOUTLINEALPHA, 0, 160)
    scite.SendEditor(SCI_INDICSETUNDER, 0, 1)
    scite.SendEditor(SCI_INDICSETFORE, 0, 0x3f7f7f) --bgr
    scite.SendEditor(SCI_INDICSETSTYLE, 0, 7)
    if e and e>a then scite.SendEditor(SCI_INDICATORFILLRANGE, a, e - a) end
end

local function domarkersx(st,fn)
--~ 	editor:MarkerDefine(rcmark, SC_MARK_CIRCLE, 0)
  editor:MarkerDeleteAll(rcmark)
  if not st then return end

  editor:MarkerSetBack(rcmark, 0x00ccff)
  editor:MarkerSetAlpha(rcmark, 40)
  editor.MarginMaskN[1] = 33554431-(2^rcmark)
  
  for line = editor:LineFromPosition(st),
             editor:LineFromPosition(fn-#nlchar()) 
  do 
    editor:MarkerAdd(line, rcmark)
  end
end

--[[
]]
function recoin_hotkey() 
    
--~ 	print('hotkey')
  
  if (get_pane())==output then return end
  
  Lc_.recoin=Lc_.recoin or {}

  local kr,cp,bcp = editor.Anchor, editor.CurrentPos, editor.CurrentPos
  if kr>cp then kr,cp=cp,kr end
      
  local u,v,kln=poslinestfn(kr)
  local ws,w,cln=poslinestfn(tain(cp,v))
    
  local hassel= (cln==kln and kr~=cp)
  
  local ttime=os.time()
  
  local cfp= props['FilePath'] or "nil"
  local rcfp= Lc_.recoin[cfp]
  
  if not rcfp then           --init single line or scope
    Lc_.recoin[cfp]={}
    rcfp=Lc_.recoin[cfp]
    rcfp['hottime']=ttime
    scite_OnChar(recoin_onChrKy)
    scite_OnKey(recoin_onChrKy)
    rcfp.wdst= math.floor((kr+cp)/2)
    rcfp.wdfn= rcfp.wdst

    if canscope or not rcfp.mskst then
      --tweak selection if ends on start of newline
      --if ws==cp then cp=cp-#(nlchar()) cln=cln-1 w=cp end
      if kln~=cln then
        rcfp.mskst=u  rcfp.mskfn=w
      else
        if cp-kr<4 then
          rcfp.mskst=ws  rcfp.mskfn=w
        else
          rcfp.mskst=kr  rcfp.mskfn=cp
        end
      end
--~ 			rcfp['multiline']=true
      cp=bcp editor.CurrentPos=bcp
      editor.Anchor=bcp
      hassel=false
    else	
      rcfp.mskstl=kln rcfp.mskfnl=cln
      rcfp.mskst=editor:PositionFromLine(kln) 
      rcfp.mskfn=editor.LineEndPosition[cln]
    end
    
  --continue with, subsequent press behaviour:
  --no rcfp, no multiline,
  elseif ttime-rcfp['hottime']<3 and not rcfp['multiline'] then
    
    rcfp['multiline']=true
    rcfp.mskstl=kln rcfp.mskfnl=cln
    local mnln = editor.FirstVisibleLine
    local mxln = mnln+editor.LinesOnScreen-2
    local stick=-1
    repeat
      local f,a,cln=poslinestfn(nil,nil,nil,rcfp.mskfnl+1)
--~ 			print(stick,f,a,cln)
      if stick==cln then break else stick=cln end
      if editor:findtext("\\w+", SCFIND_REGEXP, f, a) then 
        rcfp.mskfnl=rcfp.mskfnl+1
      else break end
    until rcfp.mskfnl==mxln
    stick=-1
    repeat
      local f,a,cln=poslinestfn(nil,nil,nil,rcfp.mskstl-1)
      if stick==cln then break else stick=cln end
      if editor:findtext("\\w+", SCFIND_REGEXP, f, a) then 
        rcfp.mskstl=rcfp.mskstl-1 
      else break end
    until rcfp.mskstl==mnln
    
    rcfp.mskst=editor:PositionFromLine(rcfp.mskstl) 
    rcfp.mskfn=editor.LineEndPosition[rcfp.mskfnl]

  elseif not hassel then
    return closerecoin()
  else
    rcfp.wdst= kr
    rcfp.wdfn= cp
  end

  recoin_setmask(
    rcfp, rcfp.mskst, rcfp.mskfn,
    rcfp.wdst,
    rcfp.wdfn,
    hassel
  )
--~ 	rcfp.wdst,rcfp.wdfn=caret_land(editor,cp,"[^$%w_]",true)
end	

Lc_shortcut("Recoin|recoin_hotkey|Ctrl+shift+~ us:|")

function recoin_setmask(rcfp,mskst,mskfn,wst,wfn,hassel)

--~ 	print('setmask',mskst,mskfn,wst,wfn,hassel)
  
  mskst=mskst or rcfp.mskst
  mskfn=mskfn or rcfp.mskfn

  wst= wst or editor.Anchor
  wfn= wfn or editor.CurrentPos
  if wst==wfn then 
    wst,wfn,xx=dcrnword(wst)
--~ 		print("q:"..xx..":")	
  end
  if wst>wfn then wst,wfn=wfn,wst end
  
  wrd=editor:textrange(wst,wfn)	
  
--~ abrac  abrac  abrac  abrac  abrac ebra aabrac --~ 	print("w:"..wrd..":")

  rcfp['bmap']=recoin_mask(mskst,mskfn, wst,wfn, hassel, editor)
  
  rcfp['wrd']=wrd rcfp['wdst']=wst rcfp['wdfn']=wfn
  rcfp['mskst']=mskst rcfp['mskfn']=mskfn
  rcfp['mskstl']=editor:LineFromPosition(mskst) 
  rcfp['mskfnl']=editor:LineFromPosition(mskfn-#nlchar())
  
  domarkers(rcfp.mskst,rcfp.mskfn)
end
--abc abc abc abc abc

function recoin_mask(dst,dov,wst,wfn,hassel,pane)
--~ 	print("msk ran")
  wst= wst or editor.Anchor
  wfn= wfn or editor.CurrentPos
  local fq,aq=poslinestfn(wfn)
  if wst==wfn then --or wst<fq or wst>aq then 
    wst,wfn=dcrnword(math.floor((wst+wfn)/2))
--~ 		print("w2:"..editor:textrange(wst,wfn)..":")	
  end 
  if wst>wfn then 
    wst,wfn=wfn,wst 
    rcfp['wdst']=wst rcfp['wdfn']=wfn
  end
  if dst>dov then dst,dov=dov,dst end
  
  pane = pane or editor 
  local sdt=pane:textrange(dst,dov)
--~ 	print("tr:"..sdt..":")
--~ 	print(wst,wfn)
--~ 	print("wrt:"..pane:textrange(tain(wst,wfn),wfn)..":")
  wst=wst-dst+1 wfn=wfn-dst 
  local wrd=sdt:sub(wst,wfn)
--~ 	print("wrd:"..wrd..":")
--~ 	print(wst,wfn)
  
  
  local dtln=#sdt
  local nk,mtchs=1,{}

  local ffwd=wrd:sub(1,1)
  local afwd=wrd:sub(-1,-1)

  if hassel then
    ffwd,afwd="",""
  else 
    ffwd,afwd="%f[%w_]","%f[^%w_]" 
  end
  
  local patt=ffwd..string.gsub(wrd, "([%(%)%.%%%+%-%*%?%[%^%$])", "%%%1")..afwd

  repeat
    
    local st,fn = string.find(sdt, patt , nk )

    if st then
      if st>wfn or fn<wst then 
        insert(mtchs,st) insert(mtchs,fn) 
--~ 				trace(":m:"..sdt:sub(st,fn)..":")
      end
      nk=fn+1
    else
      nk=dtln
    end
  until	nk >= dtln
--~ 	print("msk ret")

  return mtchs
end

local function isoutwd(kp,wst,wfn)  return kp<=wst or kp>wfn  end

function dcrnword(pos,pane,wchr)
  pane=pane or editor
  pos= pos or pane.CurrentPos
  wchr=wchr or "%w$_"
  wchr="["..wchr.."]"
  local lf,la=poslinestfn(pos)
  
  local ltxt=pane:textrange(lf,la)
--~ 	print("ltxt:"..ltxt..":")
  local ltl=#ltxt
  local subp=pos-lf
  local qfp,qap=subp,subp
  
  while qfp>0 and string.sub(ltxt,qfp,qfp):find(wchr) do
    qfp=qfp-1
  end
  
  while qap<ltl and string.sub(ltxt,qap+1,qap+1):find(wchr) do
    qap=qap+1
  end
  
--~ 	print("ss:"..string.sub(ltxt,qfp+1,qap)..":")
--~ 	print("pt:"..pane:textrange(lf+qfp,lf+qap)..":")
  
  return lf+qfp, lf+qap, string.sub(ltxt,qfp+1,qap)
end

function closerecoin()
  domarkers()
  Lc_.recoin[(props['FilePath'] or "nil")]=nil
  scite_OnChar(recoin_onChrKy,'remove')
  scite_OnKey(recoin_onChrKy,'remove')
  return true
end

local kvkycd=Lc_.kvkycd

function recoin_onChrKy(char, shift, ctrl)

  --dreadfulfunction called onchar and onkey
  --onkey happens first
  local charx=tostring(char)
  if charx==kvkycd['escape'] then return closerecoin() end 
  
  if get_pane()==output then return end
  local rcfp= Lc_.recoin[props['FilePath'] or "nil"]
      
  if not rcfp then
    if not Lc_.recoin['paused'] then
--  			uptrace("+ Recoin quick is paused\n",1,0)
      Lc_.recoin.paused=true
    end
    return --
  else
    if Lc_.recoin.paused then
--~ 			uptrace("- Recoin quick is active\n",1,1)
      Lc_.recoin.paused=false
    end
  end
    
  local f,c,a
  local bwdst,bwdfn=rcfp.wdst,rcfp.wdfn
  local kp,cp=editor.Anchor,editor.CurrentPos
--~ 	rcfp['lastkp2']=rcfp['lastkp'] rcfp['lastcp2']=rcfp['lastcp']
  local _,_,cl=poslinestfn(cp)
  
  if hotkeyed  -- (ctrl and char==83) 
  or cl<(rcfp.mskstl-1) 
  or cl>(rcfp.mskfnl+1) 
  or (rcfp.mskst>cp+1) 
  or (rcfp.mskfn<cp-1) 
  then 
    --cp~=tain(cp,rcfp.mskst,rcfp.mskfn) then
    return closerecoin()
  end
  
  local retv=false

  if char=="autoc" then return end
  
  if charx==kvkycd['space'] and shift then --space and shift
    editor:ReplaceSel(" ")
    rcfp.mskfn=rcfp.mskfn+1
    recoin_setmask(rcfp,rcfp.mskst,rcfp.mskfn)
    return true
  end
--~   prnte("swthis",char,kvkycd['backspace'],char==kvkycd['backspace'] ) 
  if charx==kvkycd['backspace'] then --backspace
--~     prnte("saw backspace")
    local jbk=0
    
    if kp~=cp then
      if kp>cp then kp,cp=cp,kp end
      recoin_setmask(rcfp,rcfp.mskst,rcfp.mskfn,kp,cp,true)
      bwdst,bwdfn=rcfp.wdst,rcfp.wdst  --?
      editor:remove(kp, cp)
      editor:GotoPos(kp)
      rcfp.mskfn=rcfp.mskfn-cp+kp
    elseif ctrl then
      f,a=dcrnword(editor,cp-1)
      if f>a then a,f=f,a end
      if isoutwd(cp,bwdst,bwdfn) or isoutwd(f,bwdst,bwdfn) then 
        recoin_setmask(rcfp) 
        bwdst,bwdfn=rcfp.wdst,rcfp.wdfn
      end
      bwdfn=bwdfn-math.abs(cp-f)
      editor:SetSel(f,cp)
      editor:DeleteBack()
      rcfp.mskfn=rcfp.mskfn-math.abs(cp-f)	
    else --just backspace
      
      if isoutwd(cp,bwdst+1,bwdfn) then 
--~ 				print("outwd")
        recoin_setmask(rcfp) 
        bwdst,bwdfn=rcfp.wdst,rcfp.wdfn
      end
      
      editor:DeleteBack()
      jbk=cp-editor.CurrentPos
      cp=cp-jbk
      
      bwdfn=bwdfn-jbk
      rcfp.mskfn=rcfp.mskfn-jbk

    end
    
    if editor.CurrentPos<rcfp.mskst then
      rcfp.mskst=rcfp.mskst-jbk
      rcfp.wdst=rcfp.wdst-jbk
      rcfp.wdfn=rcfp.wdfn-jbk
      return true 
    end
    
    retv=true
  
  --fwd del w ctrl 
  --bwd= remved string as stored in prev msk, also
  --on outwd resetmask after edit, reset beforewd
  elseif charx==kvkycd['delete'] then --forward delete
    if ctrl then
      f,a=dcrnword(editor,cp+1)
      bwdfn=bwdfn-math.abs(cp-a)
      editor:remove(cp, a)
      rcfp.mskfn=rcfp.mskfn-a+cp
      if isoutwd(cp,bwdst,bwdfn) then 
        recoin_setmask(rcfp) 
        bwdst,bwdfn=rcfp.wdst,rcfp.wdfn
        end
    else
    --all ops are complicated byhow 
    --we are performing the singular edit before
    --the operation to use the mask to edit the whole
    
      bwdfn=bwdfn-1
      editor:remove(cp, cp+1)
      rcfp.mskfn=rcfp.mskfn-1
      if isoutwd(cp,bwdst,bwdfn-1) then 
        recoin_setmask(rcfp) 
        bwdst,bwdfn=rcfp.wdst,rcfp.wdfn
      end
    end
    retv=true
  
  elseif type(char)=='number' then 
    rcfp['lastkp']=kp rcfp['lastcp']=cp
    return retv
        
  end
  --ends the onkey processing
  
  rcfp.rekeyed=0
  if type(char)=='string' then 
    
    bwdfn=bwdfn+#char
    
    if rcfp.lastkp~=rcfp.lastcp then --key is pressed on a selection
      editor:Undo()
--~ 			print("undid")
      kp=rcfp.lastkp local cpq=rcfp.lastcp
      if kp>cpq then kp,cpq=cpq,kp end
      recoin_setmask(rcfp,rcfp.mskst,rcfp.mskfn,kp,cpq,true)
      bwdst,bwdfn=rcfp.wdst,rcfp.wdst+#char
      editor:SetSel(kp, cpq)
      editor:ReplaceSel(char)
      editor.CurrentPos=kp+#char
      rcfp.mskfn=rcfp.mskfn+#char-cpq+kp	
    elseif isoutwd(cp,bwdst,bwdfn) then 
--~ 		elseif isoutwd(cp-#char,bwdst,bwdfn) then 
      editor:remove(cp-#char, cp)  --caution remove on f==a
--~ 			print("tis so")
      recoin_setmask(rcfp)
      bwdst,bwdfn=rcfp.wdst,rcfp.wdfn+#char
      editor:insert(cp-#char,char)
      editor.CurrentPos=cp
      rcfp.mskfn=rcfp.mskfn+#char
    else
      rcfp.mskfn=rcfp.mskfn+#char
    end
  
  end
  
  rcfp['lastkp']=rcfp['lastcp']
  
--~ 	print("multi",bwdst,bwdfn)
  if bwdst>bwdfn then bwdst,bwdfn=bwdfn,bwdst end
  applymultitxt(rcfp,bwdst,bwdfn)	
--~ 	print("backfrom multi")
  return retv
end

function applymultitxt(rcfp,cwdst,cwdfn) 

  local bmap=rcfp.bmap	
  local cp=editor.CurrentPos
  local cwrd=editor:textrange(cwdst,cwdfn)

  local wrdif=#cwrd-#rcfp.wrd	
  
  local txi=editor:textrange(rcfp.mskst,tain(rcfp.mskfn,rcfp.mskst))

--~   print("APPLY ",cwrd,wrdif)
--~   print(dump(rcfp))
  
  -- aabra  aabra  aabra  aabra aaabra aaabra aaabra --~ 	print("\n:::::::\n"..rcfp.wrd..">"..cwrd..":")
--~ 	print(":::::::\n"..txi..":\n - - ")

  local mcp=cp-rcfp.mskst
  local mout={}
  local onshft,inshft,cpshft =0,0,0
  local m,stp,fp,lp = 1,0,0,-1 
  local un = true
  
  --mask works on unaltered clip,
  --which only needs shifted by wrdif
  --on cords after cp

  --mask is adjusted to fit recoined out
  --an accumulating shift is applied
  --to every stp and fp following
  editor:BeginUndoAction()
  while m<#bmap do
    if bmap[m]>=mcp then   --if point in map is after caretposition
      inshft=wrdif
    else
      cpshft=cpshft+wrdif
    end
    
    fp=bmap[m]+inshft
    stp=bmap[m+1]+inshft

    insert(mout,txi:sub(lp+1,tain(fp-1,0)))
    insert(mout,cwrd)
    lp=stp
    
    bmap[m]=bmap[m]+onshft+inshft
    onshft=onshft+wrdif
    bmap[m+1]=bmap[m+1]+onshft+inshft
    
    m=m+2
  end
  insert(mout,txi:sub(stp+1))
  
  mcp=cpshft+mcp+rcfp.mskst
  
  local moo=table.concat(mout)
  editor:SetSel(rcfp.mskst,rcfp.mskfn)
  editor:ReplaceSel(moo)
  editor:SetSel(mcp,mcp)
--~ 	print(table.concat(mout))
  editor:EndUndoAction()
  
  rcfp.mskfn=rcfp.mskst+#moo
  rcfp.wrd=cwrd rcfp.wdst=cwdst+cpshft rcfp.wdfn=cwdfn+cpshft
  
  txi=editor:textrange(rcfp.mskst,rcfp.mskfn)
--~   print("its::"..editor:textrange(rcfp.wdst,rcfp.wdfn))
--~ 	print(txi..":")

  domarkers(rcfp.mskst,rcfp.mskfn)
end



--[[

local mwrdtbl,xwrdi = {},1        -- multiword table, hotword index 
local mrnga,mrnge,mrncp,rnudge = 1,1,1,0   -- mul range anchor,end,curpos
local mwrdc,mwrdb = "",""         -- mul word current, before 
local xwrda,xwrde = -1,0        -- mul word anchor,end
local mwpan,mwbuf                 -- multiword panel (editor output) buffer
local zwrd="esCaPe~C0ok1e"       -- no one will type that
local nomul=false

-- aaa aaa aaa aa
-- ficklecafed ficklecafed ficklecafed ficklecafed ficklecafed crokko po
local function paintrange(pan,a,e)

  local cla=tain((a or 0)-200)    -- avoid full sweep of large files
  local clb=tain((e or pan.Length)+200,0,pan.Length)
  
  local send = pan==editor and scite.SendEditor or scite.SendOutput
  send(SCI_SETINDICATORCURRENT, 0) -- use different number for overlapping indications? 
  send(SCI_INDICATORCLEARRANGE, cla, clb)
  send(SCI_INDICSETALPHA, 0, 35)
  send(SCI_INDICSETOUTLINEALPHA, 0, 125)
  send(SCI_INDICSETUNDER, 0, 1)
  send(SCI_INDICSETFORE, 0, 0x00aaff) -- bgr
  send(SCI_INDICSETSTYLE, 0, 7)
  if e and e>a then send(SCI_INDICATORFILLRANGE, a, e - a) end

  mrngax=poslinestfn(a,pan,-1)
  _,mrngex=poslinestfn(e,pan,1)

end

local function stopmwrd()
  paintrange(mwpan)
  --clear interpose
  setinterposekeys(false)
  mwpan,mwbuf=false,false
end

--a a a a a a aaaaa
--replace wrds in the mw range with due wrd except the curpos wrd
--move curpos after so it is in same place after
--due wrd may be +1 or -1 size, it may go to 0, but not under 
--if it goes to 0 dont destroy mwt , incase fresh word is typed in place




-- ogot mazoo ogot mekka zam, zam, ooorphrpm zam,
-- ogot mazoo ogot mekka z z ooorphrp

--, ogot, mazul, flibbit, correp, last, last, meek, rumnopcalibrum,um,

local function updtmwt(wrdd,wrdb,arp,crp) --dueword , beforword , caret position
  if xwrd==zwrd then return end  -- do nothing if new table found no hits
  if wrdb~=xwrd then prnte("wrdb~=xwrd:",wrdb,xwrd) return end
  xwrd=wrdd
  mwpan:BeginUndoAction()
  --
  -- p , i, r   pristine , intermediate, resulting
  
  local ld,lb,lz = #wrdd, #wrdb, #wrdd-#wrdb
  prnte("#wrdd-#wrdb",#wrdd-#wrdb)
  local mvtb,mvcr,skpc = 0,0,0  -- move tbl vals, move carets, skip caret word

  for i,tp in pairs(mwrdtbl) do
    mwrdtbl[i] = tp+mvtb+skpc -- table after updates and key edit
--~     prnte("mrnge",mrnge)
            
    if i~=xwrdi then
      mwpan:SetSel(tp+mvtb,tp+mvtb+lb)
      mwpan:ReplaceSel(wrdd) 
      mvtb=mvtb+lz
--~       mrnge=mrnge+lz            -- range after updates
      
      if tp<arp then --cursor moved for key edit every tp less than anchor 
        mvcr=mvcr+lz
      end
    else
      skpc=lz
    end
    
  end
  
  xwrda=xwrda+mvcr
  xwrde=xwrde+mvcr+lz
  
  if crp==ape then
    mwpan:GotoPos(crp + mvcr)
  else
    mwpan:SetSel(arp+mvcr, crp + mvcr)
  end
  
  mwpan:EndUndoAction()
--~   paintrange(mwpan,mrnga,mrnge)
  return  -- pos to put caret in when scite is allowed to handle keypress
end

--
--makes a list of matches in the range

local function newmwt(xwrda,xwrde,flags,crp)
  local wrd = mwpan:textrange(xwrda,xwrde)
  local z, x, a,e,d = 0, 0, mrnga,mrnge,1
  print("NEWMWT")
  local ttt=""
  while d and z<1000 do
    b,d=mwpan:findtext(wrd, flags ,a,e+1)  -- e+1 tried for an offbyone? 
    if d then 
      x=x+1 mwrdtbl[x]=b a=d
      ttt=ttt..mwpan:textrange(b,d)
      if b==xwrda then
        xwrdi = x
        prnte("xwrdi (xwrd index) :"..xwrdi)
      end 
    end -- should a=d+1 ?
    z=z+1
  end 
  if z>1000 then prnte("NEWMWT overflow!!! wrd:"..wrd..": b,d",b,d) end
  
  if x<2 then xwrda,xwrde,xwrd=-1,-1,zwrd print("MWT invalid") end --invalidate 
  --clear rest of table
  while x<#mwrdtbl do  x=x+1 mwrdtbl[x]=nil  end
  xwrd = wrd
end
--   aa;aaa bb cc aaaaa bbcc aa;aaa aa;aaa bb cc
--correct mwt after the wrd has changed
local function mwtshft(crp,x) --function not used ?
  local xs=0
  for i,v in pairs(mwrdtbl) do
    if v<crp then crp=crp+x end --only word anchors before cp will move cp
    mwrdtbl[i]=v+xs       --xs is initially 0 as first anchor not moved
    xs=xs+x 
  end
  return crp --rtrn probably not required as updatemwt moves cp
end

local function mwtaftcp(crp,x,z) --used by backspace, move entries by x 
  local bv=-1000 -- preceeding entry's value
  for i,v in pairs(mwrdtbl) do 
    if v>=crp then --do? shift back entry that is anchored on current position ?
      if bv+z >=v+x then 
        print("invalidated mwrange for xwrd:"..xwrd)
        xwrd=zwrd 
      return end --? hit previous wd so invalidate mwrange
      mwrdtbl[i]=v+x 
    end 
    bv=v
  end
end

----------------------------------------------------------------------------
-- we just make list of current word in range locwds
-- which is the start positions of the current word in the document, in the range
-- if has selection, make selection the current word
--   make new locwds of selection but dont require separation
-- if edit not occuring during current locwds
--   get word it is occuring at,
--     make new locwds...
--   if not occuring in word, allow normal edit (return false)
-- calculate what editted word will be 
--   use locwds to change all other occs and change cp and ank
--   return false to allow normal edit






Lc_shortcut("matchededit|startmultiword|Ctrl+shift+;")

function startmultiword()
  prnte("mw started")
  --calculate range
  local pan,ap,ep,rv=get_pane()
  local tma,tme=poslinestfn(ap,pan)
  if ep==pan.Length then pan:AppendText(" ") end
  if ap==ep then  ap,ep = tma,tme 
    if ap==ep then
      tma,tme=poslinestfn(ap,pan,-1)
      if tma then ap=tme end 
      tma,tme=poslinestfn(ap,pan,1)
      if tma then ep=tma end
--~       pan:insert(ap," ") ep=ep+1
    end
  end 

  --pad the range to include a gap because vhard to track indicator end 

  if (ap==tma or (pan:textrange(tain(ep-1),ep)):match("%S"))
    and (pan:textrange(ep,ep+1)):match("[%s%c]") 
  then
    ep=ep+1
  end

  if ap==ep then return true end

--highlight range
  mwpan,mwbuf=pan,props['FilePath']
  mrnga,mrnge=ap,ep
  paintrange(mwpan,mrnga,mrnge)
  xwrda,xwrde,xwrd = -1,-1,""
--register range
--interpose okmultiword
  setinterposekeys(okmultiword)
  mwpan.Anchor=mwpan.CurrentPos
  return true
end

--recalc mwmap when ?
--when edit outside lastwrd
--when lastwrd empty
--after shift-space



--~ okmultiword 
--~   every press
--~    discernments

--~    key is cursor
--~ 
--~    edit key pressed
--~      inhotword -inhwrng & hotword is same 
--~      exhotword -exhwrng or hwrd is diff or 
--~ 
--~ 
--~ 
--~    key is insert character
--~    key is insert clipboard 
--~    key is delete character
--~    key is backspace
--~    key is cut 

--~ tttt  kb kk dd
--~ t  bb c  dd
--~  wwwwwwwwwwww wweddddddwwwwwwwwwwwwxxxxxx ddxxxxxx
--~ aaabe aaabe aaabe aaabe abc aabcaaaab aaabe bb baaaaaaa bb
--~     aaaa aaa 
--~ t


local function fixrange(sl,dif)
  if sl<=mrnga then  mrnga=mrnga+dif  end  -- < or <= ??
end

function okmultiword(key, shift, ctrl, alt)
  local pan,sl,sr,rv=get_pane() --pane selection left ,sel right, anchor caret reversed
  local issel = sl~=sr
  local crp = rv and sr or sl   --caret position (nominal sel right)
  local anp = rv and sl or sr   --anchor position (nom sel left)
  
  if pan~=mwpan or props['FilePath']~=mwbuf then return false end
  --paintrange(mwpan,mrnga,mrnge) --dev

  --   aZb  asdz aZz aZzz  aZzz   aZb equi

  local inde = pan:IndicatorEnd(0, mrnga) -- scite indicator tracks range end
  prnte("okmul range enddiff",mrnge-inde)
  mrnge=inde
  prnte(pan:textrange(mrnga,mrnge))
  
  if crp>=mrnge then
    local _,mrngex=poslinestfn(mrnge,pan,1)
    if crp > mrngex then 
      return stopmwrd() 
    else
      return false
    end
  elseif crp<mrnga then
    if crp < poslinestfn(mrnga,pan,-1) then
      return stopmwrd()
    else
      return false
    end 
  end

  if (key>kyvalcrpre and key<kyvalcraft) --cursors 
  then    return false 
  end
  
--~   local mrngf=pan:IndicatorEnd(0,pan.CurrentPos)
  -- ABRxxxABABRxxxAdA B suBsuB Rxxxeflox opopoRxxxt ABRxxxABABRxxxAdA B suBsuB Rxxxeflox opopoRxxxt ABwfg ABcdefg Rxxxccaaafg Rxxxccaaafg
  
  prnte("\n+ sr xwrdaa xwrde xwrd pantxt",sr,xwrda,xwrde,xwrd,pan:textrange(tain(xwrda),tain(xwrde)))
   
  if issel then -- makenewlist of selected thing
    xwrd,xwrda,xwrde = pan:textrange(sl,sr),sl,sr
    newmwt(xwrda,xwrde,SCFIND_MATCHCASE,sl)
    prnte("selction mwt of:"..pan:textrange(sl,sr))
  elseif xwrd==zwrd or crp~=tain(crp,xwrda,xwrde) or pan:textrange(xwrda,xwrde) ~= xwrd 
  
  
  then 
    prnte("rebottle")
    if xwrd==zwrd then prnte("but xwrd==zwrd") end
    if crp~=tain(crp,xwrda,xwrde) then prnte("crp~=tain(crp,xwrda,xwrde)",crp,xwrda,xwrde) end
    if xwrda>-1 and pan:textrange(xwrda,xwrde) ~= xwrd then prnte("pan,xwrd,",pan:textrange(xwrda,xwrde),xwrd) end
    
    
    xwrd,xwrda,xwrde=findonpos("[%w_]+",pan) --to make new list of word thing 
--~     if not xwrd then xwrd,xwrda,xwrde = findonpos("[%p]+",pan) end --punctuation?? space?? 
    if not xwrd then prnte("zwrd")  xwrd=zwrd  xwrda,xwrde=-1,-1  else
      prnte("new mwt of:"..xwrd)
      newmwt(xwrda,xwrde,SCFIND_MATCHCASE + SCFIND_WHOLEWORD,crp)
    end
  end
  
  key=tostring(key)
  ckey=vkkycd[key]
  lckey= (ckey and #ckey) or 0
  if ckey==nil then prnte("nilc >:"..key..":<") return false end

  --sp ttb btck  sp t delete
    
  wrdcp=crp-(xwrda or 0)--cp in word
  
  local wrdx=xwrd
  local wrdb=xwrd:sub(1,wrdcp)
  local wrdd=xwrd:sub(wrdcp+1,#xwrd)
  
  prnte("xwrd :"..wrdb.."@"..wrdd..":")
  
  if lckey==1 then --ckey is most any character
    if shift then ckey=string.upper(ckey) prnte("shifted char") end 
    
    if issel then 
      wrdx=ckey
      prnte("axmwrd:"..wrdx..":") 
    else --sel replaces
      wrdx=wrdb..ckey..wrdd     --no sel inserts
      prnte("tobe :"..wrdx)
    end
    
    if ctrl then
      if ckey == "v" then ckey=Lc_.clippet:pop(1)
      elseif ckey == "z" then stopmwrd() return
      elseif ckey == "x" then fixrange(sl,math.abs(sl-sr)) return
      else ckey = "" end
    end
    --acc ebf def acc ebf ghi def acc ebf def acc ebf deghiccbcc ebf deghiccbcc ebf def
    if xwrd==zwrd then 
      if not issel then 
        fixrange(sl,lckey)
      else
        fixrange(sl,lckey+sl-sr)
      end
      if sl==mrnge and sr==mrnge then paintrange(mwpan,mrnga,mrnge+1) end 
      prnte("noword issel",issel)
      return false 
    end
--~     inwrda nshft noht  inwrd pshift ht   ouwrd nsft noht ouwrd pshft ht 
  elseif key==kvkycd['space'] or key==kvkycd['enter'] then
    if key==kvkycd['space'] then ckey=" " else ckey=nlchar() end 
    if sl==mrnge and sr==mrnge then paintrange(mwpan,mrnga,mrnge+#ckey) end
 
    if issel then 
      wrdx=" " 
--~     elseif (#wrdd==0 or #wrdb==0) and not shift then 
    -- abcc  abcc  abcc  abcc 
--~     elseif (#wrdd==0 or #wrdb==0) ~= shift then 
    elseif not shift then 
      xwrd=zwrd       --reset wrdx and ignore 
      return false 
    else
      if xwrd==zwrd then 
        return false 
      end
      wrdx=wrdb..ckey..wrdd
      prnte("wrdx is!!",wrdx)
    end 
  elseif shift and key==kvkycd['tab'] then xwrd=zwrd return false
  elseif key==kvkycd['tab'] then
    if issel then wrdx="\t"
    elseif #wrdd==0 or #wrdb==0 or xwrd==zwrd then 
      xwrd=zwrd 
      return false 
    else
      wrdx=wrdb.."\t"..wrdd
    end 
  elseif key==kvkycd['delete'] then
    if issel or #wrdd==0 then wrdx="" --delete selection just deletes selection
      xwrd=zwrd 
      return false 
    end 
    wrdx = wrdb..wrdd:sub(2)   --else wrdx is calculated here
  elseif key==kvkycd['backspace'] then --backspace selection deletes selection everywhere,
    if issel then 
      wrdx=""               --maintain mask to allow recreation ?? yes
    elseif #wrdb==0 then    --drag wrdx back so not to alter a joined word 
      xwrda=xwrda-1 xwrde=xwrde-1 
      mwtaftcp(sr,-1,#wrdx)  --but matches in table after cp need moved
      --paintrange(mwpan,mrnga,mrnge)
      return false 
    end
    if xwrd==zwrd then  return false  end
    wrdx = wrdb:sub(1,#wrdb-1) .. wrdd
  end
  -- aaaaabcd ab cd ab cd aaaaabcd 
  
  
  prnte("axmwrd:"..wrdx..":")
  
  --if true then return false end
  
  if xwrd~=zwrd and wrdx~=xwrd then 
    prnte("updating xwrd >:"..xwrd..":<","wrdx >:"..wrdx..":<",xwrda,xwrde)
  updtmwt(wrdx,xwrd,sl,sr) end
  
end

-- aaaaa aaaaa aaaaa aaaaaaaabcdaaa aaaaaaaaaaabcdaaaaaaa aaaaaaaaaaaaaaaaaaabeep
-- ddi ddddggg bbw bbw hh dddddggg
--[[
atrotty booooooo cobling boooooooz atrotty
b331z atr1tty c1bling atr1tty b331131f1fz
c1blibb331131f1fz/1tty wwr1bb33b33b331131f1fz/bhijr bhijr stostsstopmwrstopmwrd;bhijr
bhijr coorrppppy bhijr coorrppppy bhijr
coorrppppy bhijr cccoorrrpppcoorrppppyppy


vvv QQQoo owwwQQ QQhhoo vvvs
QQQoo owwwQeeee vvvs owwwQQ QQbovvvvvsvvvvsvsvsvvso Wqqweeee


aasssaaagaaaaa beee c beee aasssaaagaaaaa
beee c aasssaaagaaaaa c beee
c aasssaaagaaaaa beee aasssaaagaaaaa aassab aassa aassaa c

o dasaby crabsss dasaby o ,; ,dasaby crabsss o crabsss dasaby ,; ,crabsss dasaby o dasaby crabsss ,; , ,; ,aa bbbbbbbbbbbbbbbbbb cccccc bbbbbbbbbbbbbbbbbb aa ,; , bbbbbbbbbbbbbbbbooorphrpho aa cccccc bbbbbbbbbbbborphbbbbb ,mekka ogotcmazoocogotcc bbbbbbooorphrphobbb aa bccccccccccborphbbbbbbbmekkabogot mazoocogotccc ,; ,,a babby crabs babby a
babby crabs a crab babby
crabs babby a babby crabs

ttjav ttjoo cco ttjojjtttthjhorroo cops ttrav cop ttrrrroo
cco ttrav ttrrrroo ttracopso


andyyyes blindman copyes blindman andyyyes
blindman copyes andyyyes copyes blindman
copyes andyyyes blindman andyyyes copyes


  croat 
 croat  croat 
croat    croat

a b cl,,  a
 cl,, a cl,, 
cl,, a  a cbbb


a  cc  a
 aaaa cc 
aaaaaaa ccc

]]