

function tallytext()
  local tx=editor:GetText()
  local sto,stofn={},{}
  fishtxt(tx,"%w+",stofn,"function %w+")
  fishtxt(tx,"%w%w*:[%w:]*%w",sto)
  fishtxt(tx,"%w%w*[.][%w.]*%w",sto)
  
  local rsto = rendersto(sto)
  local rstof = rendersto(stofn)
  return table.concat(rsto,nlchar())..nlchar()..table.concat(rstof,nlchar())
end

function stoword(sto,tx)
  if not sto[tx] then sto[tx]=0 end
  sto[tx]=sto[tx]+1
end
  
function fishtxt(tx,patt,sto,patt2)
  local fin,s,c,e,c2,e2=false,1
  while not (overloop() or fin) do
   
    if patt2 then c2,e2=string.find(tx,patt2,s) end
    
    c,e=string.find(tx,patt,s)
    if c2 and c2==c then e=e2 end
    
    if c then stoword(sto,tx:sub(c,e)) s=e+1 
    else fin=true end
  end
end

function keylist(dic) local t={} for k,v in pairs(dic) do t[#t+1]=k end return t end 

function rendersto(sto)
  local frq=keylist(sto)
  local x=array_ordinals(#frq)
--prntd(x)
--prntd(frq)
--nilcheck(sto,"sto")
  
  local function cmp(a,b) 
--print(a,b,frq[x[a]],frq[x[b]],sto[frq[x[a]]],sto[frq[x[b]]])
 
  return sto[ a ] > sto[ b ] 
  end
  
  table.sort(frq,cmp)
  
  local ucas={}
  for i=1,#frq do
    local sl=string.lower(frq[i])
    if not ucas[sl] then ucas[sl]={} end
    table.insert(ucas[sl],i)
  end
  
  local ut = {}
  for i=1,#frq do
    local c=frq[i]      --word tx 
    local s=ucas[string.lower(c)]
    for j=1,#s do
      ut[#ut+1]=" "..sto[frq[ s[j] ]].." "..frq[ s[j] ]
    end
    ucas[string.lower(c)]={}
  end
  
  return ut
end

function nilcheck(t,n)
  for k,v in pairs(t) do
    if v==nil then print(n.." has nil on key "..k) end
  end
end

-- print(tallytext())

-- function FUNCTION FUNCTION FUNCTION FUNCTION functION functION FUNction