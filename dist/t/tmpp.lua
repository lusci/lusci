--[[
function test(...)
  for i,v in ipairs({...}) do
  print(v)
  end
end

--~ test(1,2,3,4)
rpl -mwF flexister fileexisterr *.lua
zaba
function flexister(f) 
  if not f then return false end
  local fio = io.open(f,"r")
  if not fio then 
    return false
    else 
--~     print(io.type(fio))
    print(fio:read())
--~     print(b=="Is a directory")
    fio:close() return true
  end
end

flexister("/usr/share/scite/scitejoin/lua/tmpp.lua")
]]

--fuz never resets, lives in `Lc_`
local fuz=initsl('fuz',{['a']=0}) 

--foz persists as global until buffer switch
--~ foz=foz or {['a']=0}
--~ local foz=foz

--foz resets on each run
local foz
if foz==nil then foz={['a']=0} end

fuz.a=(fuz.a)+1
foz.a=(foz.a)+1
--~ print(fuz.a,foz.a)


--~ props['command.21.*.properties']="touch voom"
--~ RunToolCmd(21)
--string.gmatch(editor:GetText(), "name....CDATA.([%w ]-).+CDATA.(.+)[%]][%]]...body")

--~ runcmd("echo yeah yeah yeah")
function deedee()
  local tang= editor:GetText()
  for nm,tx in string.gmatch(tang, "name[^C]+CDATA.([^%]]+)%][^C]*CDATA.([^%<]+)") do
    print("\n#//#//#//#//#//#//#//#//#")
    print("#//# Clip Name ~<( "..nm.." )>~")
    print("")
    print(tx)
  end 
end

Lc_.deedee= deedee

--~ print( o=runcommand( "stat "..props['FilePath'] ,nil,true)[1] )
