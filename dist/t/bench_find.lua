--*
local t=os.clock()

local a=0
function zibay()
  while a<150000000 do a=a+1 end
end


--*
local t=os.clock()

--there is no sig dif in times for string comparison and number comparison
--the structs are compared in roughly equal time
--use string comparison for clarity

local c="aa"
local b="aa"
local j=1
function zub(a,z)
  if c==z then a=a+1 end
  if b=="aa" then a=a+1 end
  return a
end
function zab(a,z)
  if 1==z then a=a+1 end
  if j==1 then a=a+1 end
  return a
end

function zibob()
  while a<50000000 do a=zub(a,"aa") end
end
function zibab()
  while a<50000000 do a=zab(a,1) end
end

zibob() 
print(os.clock()-t)

zibab() 
print(os.clock()-t)

--*

local t=os.clock()

local tax="qqqqwwwwwwwwwwwwwwwwweeeeeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrrrrrtttttttttttttttttttttttyyyyyyyyyyyyyyyyyyyyyyuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq1111111111111111111111111111122222222222222222222222222222222222233333333333333333333333333333333333344444444444444444444444444444444445555555555555555555555555555555555555556566666666666666666666666666666666666434444444444444444444444444444444abcabcabcabcdefghijkldefghijkldefghijkldefghldefghldefghghldefghghldefghijklmnopqrstuvwxtulmnopqrstuvwxtulmnopqrstuvwxtuvwxtuvwxyz"

tax=tax.."r"..tax.."3123"..tax.."7389"..tax
tax=tax.."2e3e"..tax.."1237"..tax.."8320"..tax
tax=tax.."29993e"..tax.."1222237"..tax.."8300020"..tax
print(#tax)
function uncap_test(a)
  globy={string.find(tax,".-a.-g.-r.*z",a%38000)}
  if globy[1]==1 then return 1 end
  return (a and 1) or 1 
end

function reffed_cap_test(a)
  globy={string.find(tax,"(.-a)(.-g)(.-r).*z",a%38000)}
  if globy[4]=="Never" then return 1 end
  return (a and 1) or 1 
end

function unreffed_cap_test(a)
  globy={string.find(tax,"(.-a)(.-g)(.-r).*z",a%38000)}
  if globy[1]=="Never" then return 1 end
  return (a and 1) or 1 
end

pat="(.-a)(.-g)(.-r).*z"

function pat_test(a,pat)
  globy={string.find(tax,pat,a%38000)}
  if globy[1]=="Never" then return 1 end
  return (a and 1) or 1 
end

reps =50000
function reps_cap()
  local a=0
  while a<reps do 
    a=a+reffed_cap_test(a) 
  end
  print("dmp",dump(reffed_cap_test(a)))
end

function reps_ucap()
  local a=0
  while a<reps do 
    a=a+unreffed_cap_test(a) 
  end
  print("dmp",dump(unreffed_cap_test(a)))
end

function reps_dcap()
  local a=0
  while a<reps do 
    a=a+unreffed_cap_test(a) 
  end
  print("dmp",dump(unreffed_cap_test(a)))
end

reps_cap() 
print("refed",os.clock()-t)
t=os.clock()

reps_ucap() 
print("unrefed",os.clock()-t)
t=os.clock()

reps_dcap() 
print( "uncapped", os.clock()-t )


--saw overall slowdowns factor between 1.2 and 2.0 for various
--captured patterns, slowndown to 2 with difficult long unique captures
--(surprisingly low)
