
function unconcata(hay, sep) -- fast section disabled, needs tested 
  local fi, sep, tbl, pattern = 1, sep or "", {}

  if #sep>1 then -- do slower multichar *pattern* separator
  
    local aa,cc = string.find(hay,sep) 
    local dd = 0 
    while cc do
      tbl[#tbl+1] = hay:sub(dd,aa-1)
      dd = cc+1
      aa,cc = string.find(hay,sep,dd)
    end
    tbl[#tbl+1] = hay:sub(dd)
    
    return tbl
  end
  
  --fast single char non-pattern separator 
  if sep=="" then pattern ="(.)" else 
    if sep:find("[%[%]]") then sep="%"..sep end
    pattern = string.format("([^%s]*)",sep)
  end
  
  if hay:sub(-1)==sep then hay=hay:sub(1,#hay-1) end --no separator on end
  for c in string.gmatch(hay, pattern) do
    tbl[fi] = c  fi=fi+1
  end
   
  return tbl
end

-- print(dump(unconcata("appo]doolo]coppo%]","%]")))

--*


--local function string:unconcat(sep)
function unConcatt(hay, sep)
    
  local fi, ult, sep, tbl, pattern = 1, 1, sep or "", {}

  if #sep>1 then
  
    local aa,cc= string.find(hay,sep) 
    local dd=0 
    while cc do
      tbl[#tbl+1]=hay:sub(dd,aa-1)
      dd=cc+1
      aa,cc= string.find(hay,sep,dd)
    end
    tbl[#tbl+1]=hay:sub(dd)
    
    return tbl
  end
    
  if sep~="" 
  then pattern = string.format("([^%s]*)[%s]?", sep,sep)
  else pattern ="(.)" end
  
  if hay:sub(#hay-#sep+1)~=sep 
  then ult=0 end
  
  local function cc(c)  tbl[fi] = c  fi=fi+1  end
  hay:gsub(pattern, cc)
  
  if ult==0 then tbl[#tbl]=nil end
  return tbl
end

--[[
local h=unConcatt("bbccaaeeaahhaajja","aa")

print("woop")
for a,v in ipairs(h) do
  print(a,v)
end

]]

--[[
mytable = {}
mymetatable = {}
setmetatable(mytable,mymetatable)

local debugttables={}
local debugttabmeta = {
  
  __index = function (t,k)
    local m=[
    'pop'=stacket_pop
   ,'hop'=stacket_hop
   ]
    if m[k] then return m[k](t)
  
    local tb=debug.traceback():match("^.-[\r\n]+.-[\r\n]+\t?(.-)[\r\n]+")
    prntb("--- dbg table element : " .. tostring(k) .." accessed ".."\n"..tb)
    if debugttables[t] then return debugttables[t][k] end
  end,

  __newindex = function (t,k,v)
    local tb=debug.traceback():match("^.-[\r\n]+.-[\r\n]+\t?(.-)[\r\n]+")
    prntb("--- dbg table element : " .. tostring(k) ..
                         " updated to " .. tostring(v).."\n"..tb)
    if not debugttables[t] then debugttables[t]={} end
    debugttables[t][k]=v
  end
}

]]
--[[

Lc_['stackets'] = stackets or {}

function new_stacket(n) -- n is max entries or 64
  local q={} table.insert(stackets,q)
  q.c=1 q.s=false q.t=-0.1 q.v=nil q.n=n or 64
  return q
end

function stacket(...)         --not used, maybe nicer to write astacket.hop() ...
  local st=new_stacket(...)   --would only adds 10 fnc refs to each stacket table 
  local function o(s,f,...) return f(s,...) end
  st.push=o(st,stacket_push) st.pop=o(st,stacket_pop) st.hop=o(st,stacket_hop)
  st.unpop =stacket_unpop
  st.trip=stacket_trip st.pick=stacket_pick st.poke=stacket_poke
  st.has=stacket_has st.len=stacket_len st.rmv=stacket_rmv st.pre=stacket_pre
  st.unhop=stacket_unhop st.under=stacket_under 
  return st
end

function stacket_push(q,v,eq) -- q is the stacket, v is val to push, pushing nil resets pop
  if q.s then q.s=false return end
  q.c=1 if v==nil then return end 
  local z=q.n
  for i=1,q.n do
    if q[i]==nil or q[i]==v or eq and eq(q[i],v) then z=i break end
  end
  if z>1 then
    for i=z,1,-1 do 
      q[i]=q[i-1] 
    end
  end
  q[1]=v 
end
  
function stacket_under(q,v,eq) -- v is val to add to last in stack
  if q.s then q.s=false return end
  if v==nil then return end 
  q[tain(stacket_len(q),0,q.n)]=v
end
  
function stacket_pop(q,n) --q is the table, n returns nth entry without popping, like peek
  if n then return q[tain(n,1,q.n)] else
    local ri=q.c
    q.c=1+(q.c)%q.n               --advance q.c
    if q[q.c]==nil then q.c=1 end --stack not full
    return q[ri]
  end
end

function stacket_unpop(q,n) 
  if n then return q[tain(n,1,q.n)] else
    local ri=q.c
    q.c=(q.n-1+q.c)%q.n 
    if q[q.c]==nil then q.c=1 end 
    return q[ri]
  end
end

function stacket_trip(q)  q.s=true  end --causes next push to be ignored, for alttab mru switch 

function stacket_peek(q,n)  return q[n]  end  --access  -- change this to stacket_pick
function stacket_pick(q,n)  return q[n]  end  --access  -- change this to stacket_pick
function stacket_poke(q,n,v) q[n]=v  end      --access 

function stacket_has(q,v,eq) --search stacket for value v, returns index or false
  for i=1,#q do
    if q[i]==v or eq and eq(q[i],v) then return i end
  end
  return false
end

function stacket_count(q,v,eq) --search stacket for value v, returns true or false
  local c=0
  for i=1,#q do
    if q[i]==v or eq and eq(q[i],v) then c=c+1 end
  end
  return c
end

function stacket_len(q)
  for i=0,#q-1 do
    if q[i+1]==nil then return i end
  end
  return #q
end

function stacket_rmv(q,v,eq)
  if v==nil then return end
  local c=0 
  for i=1,q.n do 
    if q[i]~=v and not (eq and eq(q[i],v)) then c=c+1 q[c]=q[i] end
  end
  while c<q.n do  c=c+1 q[c]=nil  end
  return
end

function stacket_pre(q,v) --resets stacket_hop time and hopreset
  Lc_.hopreset=false
  q.t , q.v = os.time() , v 
end

function stacket_hop(q,d,eq) --pushes previous hop if hopreset or last hop time > 6
  if type(d)=='function' then eq=d d=6 end
  local ct=os.time()
  if q.v and (hopreset or q.t-ct > (d or 6)) then
    stacket_push(q,q.v,eq) --pushing previously cached pop
  end 
  Lc_.hopreset=false
  local cv = stacket_pop(q)
  q.t , q.v = ct , cv
  return cv 
end

function stacket_unhop(q,d,eq) --hop just reversed, untested, might get stuck
  if type(d)=='function' then eq=d d=6 end
  local ct=os.time()
  if q.v and (hopreset or q.t-ct > (d or 6)) then
    stacket_push(q,q.v,eq) --
  end 
  Lc_.hopreset=false
  local cv = stacket_unpop(q,eq)
  q.t , q.v = ct , cv
  return cv 
end
]]

a= Stacket()
a:push(22)
print(a:pop())