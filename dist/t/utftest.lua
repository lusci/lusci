---------------
--lookups from https://gist.github.com/natanael-b/3de34eff407477cc519eb6a309ebaf14

local utf_lower = { 
 [192]=224, [193]=225, [194]=226, [195]=227, [196]=228, [197]=229, [198]=230, [199]=231, 
 [200]=232, [201]=233, [202]=234, [203]=235, [204]=236, [205]=237, [206]=238, [207]=239, 
 [208]=240, [209]=241, [210]=242, [211]=243, [212]=244, [213]=245, [214]=246, [216]=248, 
 [217]=249, [218]=250, [219]=251, [220]=252, [221]=253, [222]=254, [376]=255 }

local utf_upper = { 
 [224]=192, [225]=193, [226]=194, [227]=195, [228]=196, [229]=197, [230]=198, [231]=199, 
 [232]=200, [233]=201, [234]=202, [235]=203, [236]=204, [237]=205, [238]=206, [239]=207, 
 [240]=208, [241]=209, [242]=210, [243]=211, [244]=212, [245]=213, [246]=214, [248]=216, 
 [249]=217, [250]=218, [251]=219, [252]=220, [253]=221, [254]=222, [255]=376 }
                     
local function utfcaseup(c,lo)
  if lo then c=string.lower(c) lo=utf_lower
  else c=string.upper(c) lo=utf_upper end
  if not c:find("[\128-\244]") then return c end
  
  local r= {}
  for _,i in utf8.codes(c) do  r[#r+1] = utf8.char( lo[i] or i ) end
  return table.concat(r)
end

function upperutf8(c)  return utfcaseup(c)  end
function lowerutf8(c)  return utfcaseup(c,"low") end

dx="asFGuˀŋĸßeð¶←øþŋŋþłĸˀĦŊŦĦ↑ÞØ ̛Ħª®Ŧ¥"
print(upperutf8(dx))
print(lowerutf8(dx))
dx="aabracadax JUTSYEHI"
print(upperutf8(dx))
print(lowerutf8(dx))

for k,v in pairs(utf_lower) do
  print(k,v,utf8.char(k),utf8.char(v))
end


-- Asfguˀŋ. Ð¶←øþŋ. ŋþłĸˀĦŊŦĦ↑þø ̛Ħª®Ŧ¥  asfguˀŋĸße.Ð¶←øþŋŋþłĸˀĦŊŦĦ↑þø ̛Ħª®Ŧ¥   asfguˀŋĸßeð¶←øþ.ŋŋþłĸˀĦŊŦĦ↑þø ̛Ħª®.Ŧ¥Asfguˀŋĸßeð¶←øþŋŋþłĸˀĦŊ.ŦĦ↑ÞØ ̛Ħª®Ŧ¥ 