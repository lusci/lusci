--[[
function dod(x)
  local g=os.time()
  local k=10000
  for i=1,x do k=math.sqrt(k+1.23456789) end
  print(k,os.time()-g)
end


dod(25000000)
protect(20,50)
dod(25000000)
unprotect()

]]


function array_permute(tbl,ndx)
  
  local cv,bi,di
  for ci=1,#tbl-1,1 do 
    cv,bi = tbl[ci],ci 
    while ndx[bi]~=bi do
      di,ndx[bi] = ndx[bi],bi
      if di ~= ci then
        tbl[bi],bi = tbl[di],di
      else
        tbl[bi] = cv
      end
    end
  end
  
  return tbl
end


function array_shuffle(t,b,d)
  b=b or 1  d=d or #t
  local c,e=b,#t
  
  while c<e do 
    d=math.random(c,e)
    t[c],t[d] = t[d],t[c]
    c=c+1 
  end

  return t
end

protect(10,20)

for ch=1,500,1 do

a,b={},{}

for i=1,ch,1 do
  a[i]=i b[i]=i
end

array_shuffle(a)

table.sort(b, function(x,y) return (a[x]<a[y]) end )

array_permute(a,b)
trace(ch..":")

for i=1,ch-1,1 do
  if a[i] and not (a[i]<a[i+1]) then
    print(dump(a))
  end
end

end

unprotect()


editor:CopyText(GetClipboard()..sel())