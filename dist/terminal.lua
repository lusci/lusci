function tweakDisplayProps()
  scite.SendEditor(2655,1,1) --always highlight caret line
  scite.SendOutput(2655,1,1) --SetCaretLineVisibleAlways
  
  scite.SendEditor(SCI_SETEXTRADESCENT,props['editor.ExtraDescent'] or 0) --props wouldnt set on gtk
  scite.SendEditor(SCI_SETEXTRAASCENT,props['editor.ExtraAscent'] or 0) 
  scite.SendOutput(SCI_SETEXTRADESCENT,props['output.ExtraDescent'] or 0) 
  scite.SendOutput(SCI_SETEXTRAASCENT,props['output.ExtraAscent'] or 0) 
  trace("+READY.\n"..props['FileDir'].."; ")
end

local comments={}
local wedgedonce,prmptonce=false,false --if this is below calling function, it is lost?!
--local variables must be declared above functions they are used by
--or that function references a global...

--lua (scite?) scope rules:
--local variables are persistent, they must be declared in file *above* the functions
--that use them, or else those functions will reference a global of the same name
--global variables can be accessed by all files
--in scite global variables are reset to their initial state after each process completes
--the contents of tables are not reset, so scite persistent global values can be 
--kept inside globally scoped tables
 
cdstack=new_stacket(16)
--
function fixprompt(empty)

-- clears empty space
-- separates dirprompt and commands
-- if dir is invalid
-- trace new prompt with commands if not 'empty'
  clipoutwhite()
-- check directory is valid

  local pan,redo,strike=output,false,true
  local lt,la,le = poslinetxt(pan,pan.Length)
  local lft = lt:match("^([^;]*)[;]%s*")
  local rgh = lt:match("^[^;]*[;]%s*(.+)") or lt
  
  if not lft then strike=false end
  local dk=dircheck(lft)
  if dk==false or dk~=lft then
    lft=(dk or props['FileDir'])
    redo=true
  end
  
  rgh=string.gsub(" "..rgh,"^ +"," ")
  if empty and rgh~=" " then rgh=" " redo=true end
  
  if redo then
    local nl,x=nlchar(output),0
    if strike then 
      placetxt(pan,"-",la,la) x=1
    else
      rgh=" "
    end
    placetxt(pan,nl,le+x,le+x)
    pan:SetSel(le+x+#nl,le+x+#nl)
    trace(lft..";"..rgh)
    return true
  end
end

Lc_shortcut("prini|fixprompt|Ctrl+8")
Lc_shortcut("prini|fixprompt '1'|Ctrl+9")

function enter_terminal()
   
  local pane=output
  local forcerun=false
  wedgedonce,prmptonce=false,false
  
  local cp = pane.CurrentPos
  local lanc,lend,le=poslinestfn(cp,pane)
  local bulkln=pane:textrange(lanc,lend)

  if enter_above(bulkln,pane,le,lanc,lend,cp) then return true end
  
  if fixprompt() then return true end
  
  pane:SetSel(pane.Length,lend)
  pane:Clear()
      
  -- >>> or >3 finds up 3x  .> echos place ,> deletes back to place 
  local _,_,tt,tn=string.find(bulkln,"; ?[.,]?(>+)(%d*)") 
  if not tt then _,_,tt,tn=string.find(bulkln,"^ *(>+)(%d*)") end 
  if tt then 
    if tn=="" then tn=#tt end -- >>> or >3 mean the same
    local _,fnd,a,e=rgxfindup(output,"^>[\\/].+; ?.+$",tonum(tn)) fnd=fnd:sub(2)
    if not a then fnd,a = "",lend-#(bulkln:match(";.*$") or bulkln) end
    if string.find(bulkln,"; ?[,]>+") then -- ',' to delete back to prmpt
      pane:SetSel(a,lend) pane:ReplaceSel(fnd)
    elseif not string.find(bulkln,"; ?[.]>+") then -- no '.' to change whole line
      pane:SetSel(lanc,lend) pane:ReplaceSel(fnd)
    else --just grab optxt to replace
      fnd = matchout(fnd,"^([^;]*)") --fnd includes ;
      local a,e,fdir=string.find(bulkln,"^([^;]*)")
      pane:SetSel(lanc,lend) pane:ReplaceSel(fdir..fnd)
    end
    return true
  end
  
  local optxt, left_op = matchout(bulkln,"^([^;]*)[;]%s*")
  local hadleft = left_op==true
  local left_op,path = matchout(left_op,"[%w]?[%/:][%s%w~\\/:@_#.%-]*")
  
  if optxt:match( "^%s*$" ) then
    if not stacket_has( cdstack,props['FileDir'] ) 
      then stacket_push( cdstack,props['FileDir'] ) end
    rtrace(""..stacket_pop( cdstack).."; " ) --cycle previous dirs
    return true
  end

  if path=="" then path = props['FileDir'] end 
  stacket_push(cdstack,path)
  
  --better to generate this from an alias definition file...
  local breevies={
    ['d']='dir/w',['cd..']='cd ..',['..']='cd ..', ['s']='lu,wsummary("s")', ['l']='lu,wsummary("l")',
    ['api,']='lu,lc_open(props["APIPath"])',
    ['abr,']='lu,lc_open(props["AbbrevPath"])',
    ['usr,']='lu,lc_open(props["SciteUserHome"].."/.SciTEUser.properties")',
    ['glo,']='lu,lc_open(props["SciteDefaultHome"].."/SciTEGlobal.properties")',
    ['?scd']='\n/mnt/o/hub/plat/win/scitejoin/scdoc; dir',
    ['?']='type /mnt/o/hub/plat/win/scitejoin/lua/help.out',
    ['?git']='type /mnt/o/hub/plat/win/scitejoin/lua/git.out',
    ['?list']='type /mnt/o/hub/plat/win/scitejoin/lua/list.out',
    ['f']='li,function'
    }

  --replace aliases
  for i,v in pairs(breevies) do
    --print(i,v,inp)
    if optxt==i then
      pane:SetSel(cp-#optxt ,cp)
      pane:Clear()
      trace(v)
      optxt=v
    end
  end
    
  local function tmpifempty(c)
    if not c or c=="" then 
      local rndcrono=math.floor(os.clock()*1000000+os.time()/10)%100000000
      c="tmp_"..rndcrono.."."..props['FileExt'] 
    end
    return c
  end
    
  local function checkandopen(c,job)
    if not fileexists(c) then 
      job()
      stacket_push(Lc_.fpstack,props['FilePath'])
      scite.Open(c)
    else
    print("\n- A file:"..c.." is here already") end
  end

  local function runandopen(c,job)
    if job then job() end 
    if fileexists(c) then 
      stacket_push(Lc_.fpstack,c)
      scite.Open(c)
    else
      print("\n- file:"..c.." was not created") 
    end
  end
  
  local lccoms={
    
    ["^(cd[ .][^&|;]+)$"] = function(c)
      if c:match("^cd [-]$") then 
        rtrace(""..stacket_peek(cdstack,1).."; ")
      else
        c=c:match("cd ?(.*)")
        local cdir,ecode=navdir(c,path)
        if cdir then 
          path = cdir 
        else print("\n-Err Code "..ecode) end
      end
    end,
    ["^ ?[#] ?(.*)$"] = function(c) --stub feature for entering comments, incmplt
      if hadleft then 
        rtrace(""..stacket_pop(cdstack).."; #\n")
      end
      trace("# "..c)
      if #c>0 then
        trace("\n# ")
      end
      return true
    end,
    ["^[Rr][Ee][Nn]?[Aa]?[Mm]?[Ee]?[,](.*)$"] = function(c) --renames and reopens current file 
      c=fullpathinstr(c,path)
      if fileexists(c) then 
        print("\n- Cancelled move to "..c.." - already exists")
      else
        local t=props['FilePath']
        if runcommand("mv '"..t.."' '"..c.."'",path,true)[3] ~= 0 then
          print("\n- Failed to Move to "..c)
        else 
          if bkstackmove then bkstackmove(c,t) end --rename the files bookmark stacket
          scite.MenuCommand(IDM_CLOSE)
          movestat(t..","..c, 'nobak', 'nopat')
          sc_open(c)
        end
      end
    end,
    ["^[Pp][Aa][Ll]?[Ll]?[Ee]?[Tt]?[,](.*)$"] = function(c) 
      themecommand(c)
    end,
    ["^[Oo][Pp][Ee]?[Nn]?[,](.*)$"] = function(c) 
      c=fillpath(c,path)
      if fileexists(c) then scite.Open(c) else
      print("\n- No file:"..c.." was there") end
    end,
    ["^[Nn][Ee][Ww]?[,](.*)$"] = function(c)
      c=fillpath(tmpifempty(c),path)
      checkandopen(c, function() runcommand("touch "..c,path,true) end ) 
    end,
    ["^[Aa][Ss][Aa]?[Vv]?[Ee]?[,](.*)$"] = function(c) --save as (cat) 
      c=fillpath(tmpifempty(c),path)
      runandopen(c, function() iocatfile(c,editor) end     )
    end, 
    ["^[Oo][Ss][Aa]?[Vv]?[Ee]?[,](.*)$"] = function(c) --output save as (cat)
      c=fillpath(tmpifempty(c),path)
      runandopen(c, function() iocatfile(c,output) end     )
    end,
    ["^[Cc][Ss][Aa]?[Vv]?[Ee]?[,](.*)$"] = function(c) --clipboard
      c=fillpath(tmpifempty(c),path)
      runandopen(c, function() iocatfile(c,Lc_.clippet:pop(1)) end )
    end, 
    ["^[Rr][Uu][Nn]?[,](.*)$"] = function(c) --this is too do sysrun? - not impemented
      optxt=c forcerun=true
      return true
    end, 
    ["^[Ll]([UuPp])[Aa]?[,](.*)$"] = function(b,c)  --lu,lp,lt = lua,print,printtable
      freshline()
      local plain=string.find(b,"[Uu]") 
      protect()
      local rd=dumpt(luaeval(c,plain))
      unprotect() 
      local er=errofeval(rd)
      if er then print(er)
      elseif rd~="" then 
        print( "---  >\t"..rd ) 
      end 
    end,
    ["^([Ll][Ii][Ss]?[Tt]?[,])(.*)$"] = function(b,c) --list 
      local g=unconcat(c,"';,") --bodge,should send whole command to wrep and do sub-parsing there
      for i,c in ipairs(g) do
        WReplace(b..c)
      end
    end,
    ["^([Ss][Pp][Oo]?[Tt]?[,])(.*)$"] = function(b,c)
      local rc="grep " local oc=" -nHI" --recursive with line nb and filenm prefixed
      if b:match("^s") then oc=oc.."i" end  --case insense
      local fps,opp2,opp="","","" 
      while c:match("^[*][%S]* ") do
        c,fps=matchout(c,"^([*][%S]*) ")
      end
      if c:match("^[-][rR]?[ABC] ?%d ?") then
        c,opp2 = matchout(c,"[-]([rR]?[ABC] ?%d) ")
      end
      if c:match("^[-][rRiEGP] ?") then
        c,opp = matchout(c,"[-]([rRiEGP]+) ?")
      end
      if not opp:match("[EGP]") then oc=oc.."F" end
      if not opp:match("[rd]") then oc=oc.."R" end
      opp = matchout(opp,"d")
      local c,nmw = matchout(c,"~$") -- postfixed ~ means dont match whole word
      if nmw=="" then oc=oc.."w" end 
      if fps=="" and getmruexts then fps=getmruexts() --depends on watcher.lua
      else fps='--include "'..fps..'"' end 
      pane:SetSel(cp-#optxt ,cp)  pane:Clear() 
      optxt='grep '..fps..oc..opp..opp2..' "'..c..'" '..'.'
      trace(optxt)
      print() printwaypoint(c) --forcerun=true only 'forcerun' if directory is large
      return true
    end, 
    ["^([Zz][Pp][,])(.*)$"] = function(b,c)
      
      -- [I] dont check binaries, [r] recursive [l] filename only
      local rc="grep " local oc="" --recursive with only filename output?
--    if b:match("^s") then oc=oc.."i" end  --case insense
      
      local fps,opp2,opp="","","" 
      while c:match("^[*][%S]* ") do
        c,fps=matchout(c,"^([*][%S]*) ")
      end
      if c:match("^[-][rR]?[ABC] ?%d ?") then
        c,opp2 = matchout(c,"[-]([rR]?[ABC] ?%d) ")
      end
      if c:match("^[-][rRiEGP] ?") then
        c,opp = matchout(c,"[-]([rRiEGP]+) ?")
      end
      if not opp:match("[EGP]") then oc=oc.."F" end
      if not opp:match("[rd]") then oc=oc.."R" end
      opp = matchout(opp,"d")
      local c,nmw = matchout(c,"~$") -- postfixed ~ means dont match whole word
      local ff,rr=matchout(c," ~(.+)")
      local ff2=ff
      if nmw=="" then 
        oc=oc.."w" 
        ff2="\\b"..ff.."\\b"
      end 
      if fps=="" and getmruexts then fps=getmruexts() --depends on watcher.lua
      else fps='--include "'..fps..'"' end 
      pane:SetSel(cp-#optxt ,cp)  pane:Clear() 
      
      optxt='grep '..fps.." -nHI"..oc..opp..opp2..' "'..ff..'" '..'.'
      optxt=optxt.." && "..'grep '..fps.." -Il"..oc..opp..opp2..' "'..ff..'" '..'.'
      optxt=optxt.." | xargs sed -i 's/"..ff2.."/"..rr.."/g'"
      trace(optxt)
      
      print() printwaypoint(c) --forcerun=true only 'forcerun' if directory is large
      return true
    end, 
    ["^[Ff][Oo][Cc]?[Ss]?[,](.*)$"] = function(c) print() wsummary("f",c) end, 
    ["^[Ff][Dd][,](.*)$"] = function(c) print() wsummary("s",c) end, 
    ["^[Ff][Dd][Mm][Vv][,](.*)$"] = function(c) movestat(c or "") end, --mv files in db 
    ["^[Uu][Nn][Dd]?[Oo]?[,][%s]*$"] = function (c) 
      noteSelection(editor)
      editor:Undo()
      restoreSelection(editor)
    end,
    ["^[Rr][Ee][Dd]?[Oo]?[,][%s]*$"] = function(c) 
      noteSelection(editor)
      editor:Redo()
      restoreSelection(editor)
    end,
    ["^[Cc][Ll][Rr][,][%s]*$"] = function(c) 
      output:SetText("")
      ACoutCache={} --autocompletes cache
    end, 
    ["^[Gg][Oo][Tt]?[Oo]?[,](%d%d?%d?%d?%d?%d?%d?)[%s]*$"] = function(c) 
      editor:GotoPos(editor:PositionFromLine(tain(c-1)))
    end,
    ["^[Ss][Ss][Aa]?[Vv]?[Ee]?[,]([%s%w~\\/:@_#.%-]*)"] = function(c) 
      if Lc_['dabra'] then
        Lc_['dabtime']=os.time() --refresh pw timeout
        print()
        sudosave(Lc_['dabra'],c)
        prmpt(""..props['FileDir'].."; ")
        return "leave"
      else
        Lc_['dbltapo']="true" Lc_['dbltapp']=props['FileDir']
        grabpwd( 
          "sudop","- Input sudo password:",
          function(ps)
            Lc_['dabtime']=os.time()
            sudosave(ps,c)
            prmpt(""..props['FileDir'].."; ")
            return true
          end
        )
        return "leave"
      end
    end,
    ["^[Dd][,](.*)$"] = function(c) 
      if not c or #c==0 then
        grabpwd(
          "demorkp","\n- Input files password:",
          function(ps)
            demork(ps)
            prmpt(""..props['FileDir'].."; ")
            return true
          end
        )
        return "leave"
      else
        pane:SetSel(cp-#optxt ,cp)  pane:Clear() print()
        demork(c) 
      end
    end --decrypt
  }
  
  for pat,fn in pairs(lccoms) do
    local s,e,a,b = string.find(optxt,pat)
    if e then 
      prefixcmdline() cp=cp+1
      local r= fn(a,b)
      if not r then 
        prmpt(""..path.."; ")
        return true
      elseif r == "leave" then
        return true
      else
        break
      end
    end 
  end
      
  prefixcmdline() cp=cp+1
  freshline()
  if forcerun then
    --if path ~= props['FileDir'] then trace("cd "..path.." && ") end
    --trace(optxt)
    --output:GotoPos(poslinestfn(output.CurrentPos,output,-1)) --no effect ntrly
    gocmd(optxt,path)
    return true
  end
  
  if GTK and (optxt:find("^sud?o?,") or optxt:find("^sudo ")) then
    optxt=matchout(optxt,"^sud?o?,? ?")
    if Lc_['dabra'] then
      Lc_['dabtime']=os.time() --refresh pw timeout
      optxt='echo "'..Lc_['dabra']..'" | sudo -S '..optxt
    else
      Lc_['dbltapo']=optxt  Lc_['dbltapp']=path
      --eat a line here!
      clipoutwhite()
      grabpwd("sudop","- Input sudo password:",sudoincllbck)
      return true
    end
  end

  if optxt:match("^ ?([^ ]+[,])") then  --reject unknown comma terms
    print("- Lusci Command '"..optxt:match("^ ?([^ ]+[,])").."' unknown")
  else
    local ret=runcommand(optxt,path)
    
    if ret[3]~=0 then print(errorspec(ret)) end
  end
  prmpt(""..path.."; ")
  return true
end

function enter_above(bulkln,pane,le,lanc,lend,cp)

  if output:LineFromPosition(output.Length)-output:LineFromPosition(cp)>20 
  or string.find(output:textrange(lend,output.Length),"[%w%p]") then
    
    local formpath=bulkln:match("%s[-][-~]%s%[([%a/~].+)%s%]$") --matches lusci filelist table
    local nik=pane:textrange(cp-1,cp+1) --when cursor is in these characters remove file

    if formpath then	
      
      if nik=="--" then
        if Lc_.buftim[formpath] then closebuf(formpath) end
        wdroprec(formpath) 
      end
      if nik=="-~" then closebuf(formpath) end
      if formpath and (nik=="-~" or nik=="--") then
        
        local lcp = cp - lanc
        local ql = pane:PositionFromLine(le+1)
        local nikdw = (ql>0 and pane:textrange(ql+lcp-1,ql+lcp+1)) or ""
        ql = pane:PositionFromLine(le-1)
        local nikup= (ql>0 and pane:textrange(ql+lcp-1,ql+lcp+1)) or ""
        
        lend= ( le~=pane.LineCount-1 and pane:PositionFromLine(le+1) ) or lend

        pane:DeleteRange(lanc,lend-lanc ) --~  pane:LineDelete()
        if nikdw~=nik and nikup==nik then cp=ql else cp=lanc end

        pane:SetSel(cp+lcp,cp+lcp) 
        return true
        
      end
      sc_open(formpath)
      panefocus(output)
      enviscp(output)
      return true
    end

    openlinefile(pane,pane.CurrentPos)
    return true
  else
    return false
  end
end

function sudoincllbck(ps)
  local op ='echo "'..ps..'" | sudo -S '..Lc_.dbltapo
--~   print(op)
  local ret=runcommand(op,Lc_.dbltapp)

  if ret[3]~=0 then print("-Exit Code "..ret[3]) else
    Lc_['dabra']=ps
    Lc_['dabtime']=os.time()
  end
    
  prmpt(""..Lc_.dbltapp.."; ")
  return true
end

function sudosave(pw,pth)
  
  local sd='echo "'..pw..'" | sudo -S '

  local pfp,ret = props['FilePath']
  if pth then pfp =fillpathinstr(pth,props['FileDir'],"noexist") end
  --hasty modif to saves as path...
  if not file_exists(pfp) then
    ret=runcommand(sd..'touch "'..pfp..'"')
    ret=runcommand(sd..'chmod 744 "'..pfp..'"')
    if ret[3]~=0 then 
      prnte("-Exit Code "..ret[3].." (Error creating "..pfp.." ) " ) 
      return
    else
      prnte("-File created as root (744)")
    end 
  end
          
  ret=runcommand( "stat -c%a "..pfp ,nil,true) --should return only files prm '775'
  if ret[3]~=0 then 
    prnte("-Exit Code "..ret[3].." (Error stat-ing "..pfp.." ) " ) 
    return
  end

  local prmssns=ret[1]
  
  ret=runcommand(sd..'chmod 666 "'..pfp..'"')  --enable writes
  if ret[3]~=0 then 
    prnte("-Exit Code "..ret[3].." ( Error chmod-ing "..pfp.." ) " ) 
    return
  end
  Lc_['dabra']=pw
  scite.MenuCommand(IDM_SAVE)
  ret=runcommand(sd..'chmod '..prmssns..' "'..pfp..'"')
  trace("-SuSaved: "..pfp) --tracing to hide an extra cr somewhere later
end

Lc_shortcut("Toggle Terminal|toggterm|Ctrl+,")

function toggterm() --toggles terminal
  
  local ovis=caretisvis(output)
--~ 	local evis=caretisvis(editor)
  
  if get_pane()~=output then  --from editor
    
    --note time and fromeditor
    Lc_.togtime=os.time()
    scite.MenuCommand(IDM_SWITCHPANE)
    output:ScrollCaret()

--~ 		output:GotoPos(output.CurrentPos)
  else --goto editor
    if (os.time()-(Lc_.togtime or -1000))<4 then
      Lc_.togtime=-1000
      
      if( output.CurrentPos==output.Length 
        and output.Anchor==output.Length
        and isposvisible(output,output.Length)) 
      then
        scite.MenuCommand(IDM_SWITCHPANE)
      else
        output.CurrentPos,output.Anchor=output.Length,output.Length
        local ap,ep=poslinestfn(output.Length,output)
        if ap==ep then 
          prmpt(""..props['FileDir'].."; ")
        end
        output:ScrollCaret()
      end
      
      return true
    end
    
    if output.CurrentPos<output.Length then  -- floating upslide
      scite.MenuCommand(IDM_SWITCHPANE) 
      --replace with better
      editor:ScrollCaret()
    else
      --output:ScrollCaret()
      scite.MenuCommand(IDM_SWITCHPANE)
      editor:ScrollCaret()
      if ovis then output:ScrollCaret() end
    end
  end
  return true
end
 
function openlinefile(pane,cp)
  pane=pane or editor.Focus and editor or output
  cp=cp or pane.CurrentPos
  local lx,lanc,lend = poslinetxt(pane,cp)
  
  local ptu=pathabove(lanc,pane) or "" 
  local bulk,lineno = matchout(lx,"%s*:%s*(%d+):.*")
  
  local fp= fullpathinstr(bulk,ptu)

  if fp then  lc_open(fp,lineno)  return end
  
  --[[
  local onlynm = bulk:match("^%s*([^\\/]+)$")
  if not onlynm then onlynm = bulk:match("^[.][/]([^\\/]+)$") end
  if onlynm then lc_open(ptu..onlynm,lineno) return end
  
  local bulk = matchout(bulk,"[/][/*]") --remove sequences not found in filenames

  local flnm,pth = matchout(bulk,'%s*([%w~/][%w~/:][%s%w~/:@_#.]*[\\/])') --do a better find-paths in string
  flnm=matchout(flnm,"%s*[%])}:;]$") --clip noise
  _,lineno = matchout(lineno,"%d+")
  
  --if contains tabs or spaces just take string under caret
  if flnm:match("\t") or flnm:match("  ") then flnm=findonpos("[%w:@_#.]+",pane,cp) end
  
  --todo: get a filepath from upscreen, or use cwd
  if flnm then lc_open(pth..flnm,lineno) else prnte("-no filename") end
  --]]
  
end

function prefixcmdline(c)
  if wedgedonce then return end
  wedgedonce=true
  local cp=output.CurrentPos
  lanc,lend,le=poslinestfn(cp,output)
  output:insert(lanc,c or ">")
  output:SetSel(cp+1,cp+1)
end

function prmpt(p)
  if prmptonce then return end
  prmptonce=true
  freshline(p)
end

function clipoutwhite()
  local _,_,_,s=rgxfindup(output,"\\S",1,output.Length)
  if s then 
    if output:textrange(s-1,s)==";" then s=s+1 end
    removetxt(output, s,output.Length )
  end
end

function freshline(c) --ensures new line and traces
  local a,e=poslinestfn(output.CurrentPos,output)
  if output.CurrentPos~=a then output.CurrentPos = e  print()  end
  if c then trace(c) end
end

function rtrace(s,up,ins) --inserts at beginning or replaces line of cp or previous line
  if not up then up=0 end
  local cl=output:LineFromPosition(output.CurrentPos)-up
  if cl<0 then cl=0 end
  local lee
  if ins then lee=output:PositionFromLine(cl)
  else lee=output.LineEndPosition[cl] end
  output:SetSel( output:PositionFromLine(cl),lee )
  output:ReplaceSel(s)
end

function uptrace(txt,upmv,updl) --deletes and inserts line from the bottom of output
  upmv = upmv or 1  updl= updl or 0

  output:SetSel(
    output.LineEndPosition[
    tain(output:LineFromPosition(output.Length)-(upmv+updl),0) ],
    output.LineEndPosition[
    tain(output:LineFromPosition(output.Length)-upmv,0) ]
  )
  output:ReplaceSel(txt)
  output.CurrentPos, output.Anchor= output.Length, output.Length
  output:ScrollCaret()
end

function tracePrompt(a,b,c) --aft str,fore str, dirsup
  trace( (b or "")..( stacket_peek(cdstack,1) or props['FileDir'] ).."; "..(a or "") )
  output:ScrollCaret()
  enviscp(output)
end 

function closebuf(fp)
  local s,p=props["FilePath"],panefocus()
  scite.Open(fp)
  --local pause=editor:GetCurLine()
  scite.MenuCommand(IDM_CLOSE)
  if s~=fp then scite.Open(s) end
  panefocus(p)
end

Lc_shortcut("Save|savebuffer|Ctrl+s")

function savebuffer()
  local pfp=props['FilePath']
  local p =panefocus()
  local a,c=p.Anchor,p.CurrentPos
--noteSelection(p)
  if not file_exists(pfp) then
    print("- Creating file ! - "..pfp)
  end
  if true then  --touch to see if permitted, and to create if missing
    ret=runcommand('touch "'..pfp..'"')
    if ret[3]==1 then
      prmptonce=false
 
      if Lc_['dabra'] then
        Lc_['dabtime']=os.time() --refresh pw timeout
        print()
        sudosave(Lc_['dabra'])
        prmpt(""..props['FileDir'].."; ")
      else
        Lc_['dbltapo']="true" Lc_['dbltapp']=props['FileDir']
        trace("\n- No permission - trying 'sudo save' > ")
        grabpwd( 
          "sudop","- Input sudo password:",
          function(ps)
            Lc_['dabtime']=os.time()
            sudosave(ps)
            prmpt(""..props['FileDir'].."; ")
            return true
          end
        )
      end
    else
      scite.MenuCommand(IDM_SAVE)
      panefocus(p) --this 
      return true
    end
  end
  panefocus(p) --this 
  p:SetSel(a,c) --need to restore multisel...
--restoreSelection(p)
  return true
end


Lc_shortcut("Open|openfilehot|Ctrl+o")

function openfilehot()
  
  local pane,bpos,dpos,rv=get_pane()
  local s,txt
  if bpos==dpos then _,bpos,dpos = findonpos("~?[\\/:%w_.]+",pane) end
  
  pane:SetSel(bpos,dpos)
  
  local txt=pane:textrange(bpos,dpos)
 
  if not txt or txt=="" then
    local tt=pane:GetLine(pane:LineFromPosition(pane.CurrentPos))
    txt= fullpathinstr(tt)
 
    if not txt then
      txt=tt:match("([%w][^;(\"':>]+[%w])[%s]*")
    end
    if not txt then
      txt="missing"
    end 
  end
  local lnum
  txt,lnum=matchout(txt,":(%d+) *:.*")
  lnum=tonum(lnum) if lnum==0 then lnum = nil end
  
  local sezpath = dircheck( pathabove2(pane,bpos) )
  txt = fillpath(txt,sezpath or props['FileDir']) --need look up for filepath
  
  if fileexists(txt) then
    lc_open(txt,lnum)
    return true
  else
    prntb ("-Not a file here [ "..txt.." ]")
--  tracePrompt()
    return true
  end
end

local ntSel 
                              
function noteSelection(pane)  --stashes selection and view to be restored after actions of function 
  pane=pane or editor
  local c,a=pane.CurrentPos , pane.Anchor
  local swp=c<a
  if swp then a,c=c,a end
  local cl,al=pane:LineFromPosition(c) , pane:LineFromPosition(a)

  local ntSel = {
    ['topl'] =pane.FirstVisibleLine,
    ['acol'] =a -pane:PositionFromLine(al),
    ['aline']=al,
    ['ccol'] =pane.LineEndPosition[cl]-c,
    ['cline']=cl,
    ['swp']=swp,
    ['same']= cl==al 
  }
    
  Lc_['nt']=ntSel
end

function restoreSelection(pan) -- 
  local ntSel=Lc_['nt']
  if not ntSel then return end
  pan=pan or editor
  pan.Anchor =
  tain(ntSel.acol+pan:PositionFromLine(ntSel.aline),	0
  , pan.LineEndPosition[ntSel.aline])
  if ntSel.same then 
    pan.CurrentPos = pan.Anchor
  else
    pan.CurrentPos =
      tain(pan.LineEndPosition[ntSel.cline]-ntSel.ccol,	0
      ,pan.LineEndPosition[ntSel.cline])	
    if ntSel.swp then pan.Anchor,pan.CurrentPos=pan.CurrentPos,pan.Anchor end
  end
  pan.FirstVisibleLine=tain(ntSel.topl)
end

--
function recordselnext(pan,key,a,c)  -- tracks selection after keypress
  a=a or pan.Anchor  c=c or pan.CurrentPos
  local kvkycd=Lc_.kvkycd
  local la,lc,f = pan:LineFromPosition(a),pan:LineFromPosition(c)

  key=""..(key or -1) 
  if key==kvkycd['right'] then c=tain(c+1,0,pan.Length)
  elseif key==kvkycd['left'] then c=tain(c-1,0,pan.Length)
  elseif key==kvkycd['up'] or kvkycd['down'] then
    c=c-pan:PositionFromLine(lc)
    if key==kvkycd['up'] then lc=tain(lc-1,0)
    elseif key==kvkycd['down'] then lc=tain(lc+1,0,pan.LineCount) end
    c=c+pan:PositionFromLine(lc)
    c=tain( c, pan:PositionFromLine(lc), pan.LineEndPosition[lc] )
  end
  
  if a==c then return end
  
  local g = pan==editor and (props['FilePath'] or "nil") or "o\t"
  Lc_.bufanc[g] = { [1]=la,[2]=a-pan:PositionFromLine(la) }
  Lc_.bufcrp[g] = { [1]=lc,[2]=pan.LineEndPosition[lc]-c }
end 

function recordselnow(pan,a,c,la,lc) --to be called by selection setting things
  local g = pan==editor and (props['FilePath'] or "nil") or "o\t"
  a=a or pan.Anchor  c=c or pan.CurrentPos
  if a==c then return end
  la=la or pan:LineFromPosition(a) lc=lc or pan:LineFromPosition(c)
  Lc_.bufanc[g] = { [1]=la,[2]=a-pan:PositionFromLine(la) }
  Lc_.bufcrp[g] = { [1]=lc,[2]=pan.LineEndPosition[lc]-c }
end


local WinErrs={
  [1]='Incorrect function',
  [2]='File not found',
  [3]='Path not found',
  [5]='Access denied',
  [9009]='Command not found',
  [221225495]='Insufficient memory',
  [3221225786]='Process terminated',
  [3221225794]='Launch error',
}
local GtkErrs={
  [1]='Basic Fail Code 1',
  [2]='No result or access',
  [15]='Process terminated',
  [123]='No result err 123',
  [126]='Permission required',
  [127]='Command not found',
  [128]='Problem argument'
}

local MacErrs={}
function errorspec(r)
  local e,t=r[3],GTK and GtkErrs or WinErrs
  if t[e] and props['print_exit_codes_only'] ~="1"
  then e=t[e]
  else e="Error Code "..e end
  return "-"..e
end

--make a print_delayed command, which waits until next keystrike, 
--or after commands complete ?

Lc_shortcut("lua run sel|luarunsel|Ctrl+r")

local function outlast()
  
end

local function traceup(s)  --buffer insert to line with selection/s update
  if not s then return end
  local pan,b,d,r,a,c=get_pane(output)
  local ap,ep=poslinestfn(pan.Length,pan,-1)
  pan:SetSel(ep,ep)
  pan:ReplaceSel(nlchar()..s)
  local df=#(nlchar())+#s
  pan:SetSel(a+df,c+df)
  --print(c) --print it 1 up
end

function clipoutp()
  local a,e=poslinestfn(output.Length,output)
  local z=output:textrange(a,output.Length)
  output:SetSel(a,output.Length)
  output:ReplaceSel("")
  return z -- "boo"
end

function luarunsel()
  local pan,b,d = get_pane()
  if b==d and pan==output then pan,b,d = get_pane(editor) end 
  if b==d then --get text under magic char in tail-test file
    local fnd,str,a,e = rgxfindup(pan,{'^\\-\\-\\*'},1,pan.Length)
    if fnd then
      b,d = a,pan.Length        --to end of doc after mage
    else
      --b,d = poslinestfn(b,pan)  --current line
      b,d = 1,pan.Length  --whole file
    end
  end
  
  --empty lines are added so traced linenos match
  local emln=string.rep( nlchar(),pan:LineFromPosition(tain(b)) )
  local s=pan:textrange(b,d)
  local oprompt=clipoutp()
 
  print( ">"..oprompt.."lua,clip:"..s:match("%w[^\r\n]*") ) 
  protect()
  local rd=dumpt(luaeval(emln..s,"plain"))
  unprotect() 
  local er=errofeval(rd)
  if er then print(er)
  elseif rd~="" then 
    print( "---  >\t"..rd ) 
  end 
  trace(oprompt)
  enviscp(output)
  return true
end


function movetoout() --pastes selection from one pane to the other
  
  local pane,ap,cp,rv=get_pane()
  local tx,ql 
  
   --rtrns ps: lineSt, linefn, line
  
  if ap==cp then  ap,cp=poslinestfn(cp,pane)  end
    
  tx=pane:textrange(ap,cp)

  --if not tx:match("[\r\n]$") then tx=tx..nlchar() end
  
  if pane==editor then
    tx=matchout(tx,"[\r\n]?[\r\n]?$")
    traceup(tx)
  else
    editor:insert(editor.CurrentPos,tx)
    enviscp(editor)
  end
  
  return true
end

Lc_shortcut("Move selection|movetoout|ctrl+b")
