-- to wrap selection with character such as "" or ()

function selwrap(b)
  local sub=string.sub
  local pane=editor if output.Focus then pane=output end
  if pane.Anchor==pane.CurrentPos or pane.SelectionMode~=0
    or (Lc_.recoin and Lc_.recoin[(props['FilePath'] or "nil")])
  then 
    return false
  end
  
  local f,a=pane.Anchor,pane.CurrentPos
  local ff=f
  
  if f>a then a,f=f,a end
  if b=="S" then b="  " end
  if b=='D' then
    local ff,aa=tain(f+1,0,a),tain(a-1,f)
--~ 		local ff,aa=f=1,a-1 --tain(f+1,0,a),tain(a-1,f)
    if aa-ff<1 then tracerr(aa,ff) return false end
    pane:BeginUndoAction()
    pane:ReplaceSel(pane:textrange(ff,aa))
    pane:EndUndoAction()
    a=a-2
  else
    pane:BeginUndoAction()
    pane:insert(a, sub(b,2,2))
    pane:insert(f, sub(b,1,1))
    pane:EndUndoAction()
    a=a+2
  end
    
  if ff==f then pane:SetSel(f,a) else pane:SetSel(a,f) end
  
  return true
end

Lc_shortcut("Selection quote  |selwrap \"\'\'\"|'")
Lc_shortcut("Selection dbquote|selwrap \'\"\"\'|Shift+\"")
Lc_shortcut("Selection tkquote|selwrap '``'|`")
Lc_shortcut("Selection parenth|selwrap '()'|Shift+)")
Lc_shortcut("Selection parenth|selwrap '()'|Shift+(")
Lc_shortcut("Selection brack  |selwrap '[]'|]")
Lc_shortcut("Selection brack  |selwrap '[]'|[")
Lc_shortcut("Selection bracket|selwrap '<>'|Shift+>")
Lc_shortcut("Selection bracket|selwrap '<>'|Shift+<")
Lc_shortcut("Selection brace  |selwrap '{}'|Shift+}")
Lc_shortcut("Selection brace  |selwrap '{}'|Shift+{")

Lc_shortcut("Selection brace  |selwrap '**'|Shift+*")
Lc_shortcut("Selection brace  |selwrap '__'|Shift+_")
Lc_shortcut("Selection brace  |selwrap '~~'|Shift+~")

Lc_shortcut("Selection space  |selwrap 'S'|Space")
--Lc_shortcut("Selection tab  |selwrap '\t\t'|Tab") --messes moving by tab
Lc_shortcut("Delete selection ends|selwrap 'D'|Delete")
