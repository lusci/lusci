
--~ Lc_shortcut("jumpup|jumpfile|Ctrl+[")
--~ Lc_shortcut("jumpdown|jumpfile 'dwn'|Ctrl+]")
Lc_shortcut("jumpup|qjumpfile|Ctrl+[")
Lc_shortcut("jumpdown|qjumpfile 'up'|Ctrl+]")
Lc_shortcut("jumpup|qjumpfile 'x'|Ctrl+shift+{")
Lc_shortcut("jumpdown|qjumpfile 'upx'|Ctrl+shift+}")

Lc_shortcut("curscrollup|curpanescroll 'up'|Ctrl+Up")
Lc_shortcut("curscrolldwn|curpanescroll|Ctrl+Down")
Lc_shortcut("otrscrollup|otrpanescroll 'up'|Ctrl+Alt+Up")
Lc_shortcut("otrscrolldwn|otrpanescroll|Ctrl+Alt+Down")

Lc_shortcut("vertscrolljump|vertscrolljump 'u'|Up")
Lc_shortcut("vertscrolljump|vertscrolljump|Down")


Lc_shortcut("select all|selectall|ctrl+shift+K")
Lc_shortcut("hop to lineends|endhop|ctrl+a")
Lc_shortcut("hop to lineends|endhop 's'|ctrl+shift+a")

function selectall()
  local p=get_pane()
  p.Anchor=0  p.CurrentPos=p.Length 
  return true
end
                         
function endhop(slct)
  local pan,aa,ee,rv,ank,cur=get_pane()
  
  if ank~=cur and not slct then
    pan:SetSel(cur,ank)
    return true
  end
  
  local ns= aa==ee 
  
  if (not rv) and pan:LineFromPosition(ee)~=pan:LineFromPosition(aa) then 
    aa,ee=ee,aa 
  end
  local ln=pan:LineFromPosition(aa)

--if rv then aa,ee=ee,aa end

  local lx,la,le = poslinetxt(pan,aa) 
  
  aa=aa-la  ee=ee-la  le=le-la  ank=ank-la  cur=cur-la --everything anchored at la
--~   if ee>le then ee=le

  local da,de=0,le
  if pan==editor then da=lx:find("[^%s]") or 1  da=da-1
  else _,da=lx:find(";%s?") 
    if da==le then da=lx:find("[^%s]") da=da-1 end
  end
  
  if not da then da=0 end

  if not slct then 
    if     da==0 and aa==0 then ank=de
    elseif aa==0 or aa>da then ank=da
    elseif aa<da then ank=0
    elseif aa==da then ank=de end 
    if ns then pan:GotoPos(la+ank) --a normal hop each end
    else pan:SetSel(la+ee,la+ank) end
--~     else pan:SetSel(la+ank,la+ee) end
  else
    if aa==0 and ee==da then ank,cur=da,de 
    elseif aa<da then ank,cur = 0,da
    elseif aa==da and ee<de then ank,cur=de,ee
    elseif ee==de and aa>da then ank,cur=da,de
    elseif aa==ee then ank,cur=da,aa
    else
      cur,ank=pan.CurrentPos-la,pan.Anchor-la
    end 
    pan:SetSel(la+cur,la+ank)
  end
  
  --pan:AddText(" ") this didnt work to update cursors-vertical-tracking-column
  --pan:DeleteBack()
  seatcaret(pan)
  return true
end
--[[
function block_end_ln(pan)
  -- let SciTE handle the curly braces case
  if pan.Lexer == SCLEX_CPP then
    scite.MenuCommand(IDM_MATCHBRACE)
  else
    local line = pan:LineFromPosition(pan.CurrentPos)
    local lno
    if fold_line(line) then
      lno = pan:GetLastChild(line,-1) 
    else
      lno = pan.FoldParent[line] 
      if lno == -1 then
          lno = pan.FoldParent[line-1]
      end
    end
    if lno == -1 then lno=line end 
  end
  return lno
end
              
function blkendhop(slct)
  local pan,aa,ee,rv,ank,cur=get_pane()
  
  --cp hops to line end, anchor remains if not select
  --if cp at line end, cp hops to block end
  --if cp at block end anchor hops to block start
  
  local ns= aa==ee 
  
  if (not rv) and pan:LineFromPosition(ee)~=pan:LineFromPosition(aa) then 
    aa,ee=ee,aa 
  end
  local ln=pan:LineFromPosition(aa)

--if rv then aa,ee=ee,aa end

  local lx,la,le = poslinetxt(pan,aa) 
  
  if cur~=le then pan.CurrentPos=le return true end
  
  local bxl = block_end_ln(pan)
  
   
  aa=aa-la  ee=ee-la  le=le-la  ank=ank-la  cur=cur-la --everything anchored at la
--~   if ee>le then ee=le

  local da,de=0,le
  if pan==editor then da=lx:find("[^%s]") or 1  da=da-1
  else _,da=lx:find(";%s?") 
    if da==le then da=lx:find("[^%s]") da=da-1 end
  end
  
  if not da then da=0 end

  if not slct then 
    if ank~=cur then --this all is fine... correct behaviour emerges ...
      pan:SetSel(la+ank,la+cur)
      return true
    elseif da==0 and aa==0 then ank=de
    elseif aa==0 or aa>da then ank=da
    elseif aa<da then ank=0
    elseif aa==da then ank=de end 
    if ns then pan:GotoPos(la+ank) --a normal hop each end
    else pan:SetSel(la+ee,la+ank) end
--~     else pan:SetSel(la+ank,la+ee) end
  else
    if aa==0 and ee==da then ank,cur=da,de 
    elseif aa<da then ank,cur = 0,da
    elseif aa==da and ee<de then ank,cur=de,ee
    elseif ee==de and aa>da then ank,cur=da,de
    elseif aa==ee then ank,cur=da,aa
    else
      cur,ank=pan.CurrentPos-la,pan.Anchor-la
    end 
    pan:SetSel(la+cur,la+ank)
  end
  
  --pan:AddText(" ") this didnt work to update cursors-vertical-tracking-column
  --pan:DeleteBack()
  return true
end
]]

function qjumpfile(u)
  local u,leap=matchout(u,"x")
  local up = u=="up"
  
  if leap=="x" then
    local cc = output.CurrentPos
    if not up then cc=poslinestfn(cc,output,1) end
    patts={'~<[(] .* [)]>~'} --listing header
    a,e=findfirstup(output,patts,cc,nil,not up) --may have to arrange cp here...
    if a then 
      local t=poslinestfn(a,output,(up and 1) or -1)
      output:GotoPos(t)
    end
  end
  
  if u=='up' then scite.MenuCommand(IDM_PREVMSG)
  else scite.MenuCommand(IDM_NEXTMSG) end
  return true
end

function otrpanescroll(up) 
  scite.MenuCommand(IDM_SWITCHPANE)
  curpanescroll(up) 
  scite.MenuCommand(IDM_SWITCHPANE)
  return true
end

local scrolltime,scrollspeed,scrollquick,scrollfine=os.time(),1.1,1,false
function curpanescroll(up)
  if up then
    --following gotcha 'not scrollquick' is true or false never == -1
    --  if not scrollquick==-1 then scrollspeed=1.2 end
    --'scrollquick ~= -1' wouldn't work as scrollquick might be nil or false
    if not (scrollquick==-1) then scrollspeed=1.5 end
    scrollquick=-1 
  else
    if not (scrollquick==1) then scrollspeed=1.5 end
    scrollquick=1 
  end
  local ttime=os.time()
    
  local p = panefocus()
  local fvis=p.FirstVisibleLine
  local svis=p.LinesOnScreen
  local nvis=p.LineCount
  local lcnt=p:VisibleFromDocLine(nvis)+1
  if math.abs(fvis-(scrollfine or -10))>=1 then 
    scrollfine=fvis scrollspeed=1.1 
  else
    fvis=scrollfine 
  end
  
  local dist= up and scrollfine or math.abs(lcnt- svis-scrollfine) 
  local dampend=3
  
  local inc1=0.000035
--~   scrollspeed
  local increase=(3000+dist+lcnt/4)*inc1 
  local tdif=ttime-scrolltime
  if tdif==0 then scrollspeed=(scrollspeed)*1.025+increase end
  if tdif>1 then scrollspeed=scrollspeed/(1+tdif*2) end
  
  scrolltime=ttime
  
  local limiter= math.max(1,math.sqrt(dist*(dist+math.sqrt(lcnt)))/dampend )-1
  scrollspeed=(scrollspeed+3*tain(scrollspeed,1.1,limiter))*0.25
  
  scrollfine = scrollfine + scrollquick*scrollspeed

--~   print(p.FirstVisibleLine,limiter,scrollspeed,inc1,scrollfine)
  scrollfine=tain(scrollfine,0,lcnt)
  p.FirstVisibleLine = math.floor(scrollfine+0.4)
  return true
end

function vertscrolljump(up)
  local p = panefocus() local b,d = doclinesvis(p)
  local c=p:LineFromPosition(p.CurrentPos)
  local l=vislinefrompos(p)  --print(scrollquick)
  if c==tain(c,b-1,d+1) and not scrollquick then return false end
  scrollquick=false
  if up then l=b+math.floor((d-b)/4)
  else l=b+math.floor(((d-b)*3)/4) end
  c=p:PositionFromLine(p:VisibleFromDocLine(l))
  p:SetSel(c,c) seatcaret(p)
  return true
end
