-- Lua Generic Helper Routines

function luaeval(s,plain)
  local func, err, rets
  if not plain then func, err = load("return "..s) end
  if not func then func, err = load(s) end
  if func then
    return select(2,pcall(func)) --table.unpack borks nil returns
  else
    return "ErR0R:::"..err  --magic cookie needed to communicate error
  end
end

function errofeval(e)
  local r=e:match("^\"ErR0R:::(.+)\"")
  if not r then r=e:match('^"(%[string ".*"%]:%d+:.*)"') end
  return r
end

function matchout(hy,nd,plain) --returns the matched portion and hy with matched portion removed.
  --if the pattern contains captures it returns the captures concatenated instead
  --of the matched portion (all matched text is discarded irregardless of captures). 
  --todo add paramter to mask quotations...
  if not hy or not nd then return hy, "" end
   
  local s,e,cap,cap2,cap3 = string.find(hy,nd,1,plain) --  nd can be ^.. or ..$

  if cap then cap = cap..(cap2 or "")..(cap3 or "") end
  
  if e then
    return string.sub(hy,1,s-1)..string.sub(hy,e+1) , cap or string.sub(hy,s,e) 
  else
    return hy , ""
  end
end

function strslash(s,e) --rts str with slashe control chars and extra eg. e="'" 
  e=e or ""
  e="[%z\r\n\t"..e.."]"
  local z={["\t"]="\\t",["\r"]="\\r",["\n"]="\\n",["'"]="\\'",['"']='\\"',["\z"]="!#NULL0#!"}
  return s:gsub(e,function(g) return z[g] end)
end

local dmdeep,dmtbls
local function dumper(o,d,a)

  if not d then  dmdeep=0  else  dmdeep=dmdeep+3  end
    
  if type(o) == 'table' then
    if dmtbls[o] then return "#!"..tostring(o).."#!" end
    dmtbls[o]=true
    local s,ss = {},{}
    if dmdeep>200000 then return "#!OOME!#" else
      dmdeep=dmdeep+500
      local iks={}
      for k,v in ipairs(o) do
        local rx=dumper(v,1,a)
        table.insert(s, (#rx>32 and "\n " or "")..'['..tostring(k)..']=' .. rx )
        iks[k]=true
      end
 
      for k,v in pairs(o) do
        if not iks[k] then
          if type(k)=='string' then k = '"'..strslash(k,'"')..'"' 
          else k = tostring(k) 
          end
          local rx=dumper(v,1,a)
          table.insert(ss, (#rx>32 and "\n " or "")..'['..tostring(k)..']=' .. rx )
        end
      end

      dmdeep=dmdeep-500
    end
    local jn=""
    if #ss>0 and #s>0 then jn =", --\n  " end
    return '{ '..table.concat(s,", ")..jn..table.concat(ss,", ").. ' }'
  else
    dmdeep=dmdeep-2
    if type(o)=='string' and dmdeep~=-2 then
      return "'"..strslash(o,"'").."'"
    else
      return tostring(o)
    end
  end
end

function dumpt(...)  --dump prams with tabs
  local l,u,un={},{...},select("#",...)
  for i=1,un do  l[i]=dump(u[i])  end 
  return table.concat(l ,"\t")
end

function dumpr(...)  --dump with tabs without quotes
  local l,u,un={},{...},select("#",...)
  for i=1,un do 
    l[i] = type(u[i])=="string" and strslash(u[i]) or dump(u[i]) 
  end
  return table.concat(l ,"\t")
end

function dump(...)  --dump prams with commas
  local r,o,n={},{...},select("#",...)
  dmdeep,dmtbls = 0,{}
  for i=1,n do 
    if type(o[i])=="string" then 
      r[#r+1] = '"'.. strslash(o[i],'"') ..'"'
    else 
      r[#r+1]=dumper(o[i]) 
    end
  end
  if #r==0 then  return "nil"  else  return table.concat(r," , ")  end
end


function flatcopy(t,u) --u optional table to fold into t
  local t2 = {}
  if type(t) ~= 'table' then return t or u and flatcopy(u) end
  for k,v in pairs(t) do  t2[k] = v  end
  if u then for k,v in pairs(u) do  t2[k] = v  end  end
  return t2
end

function emptytable(f) --unbenched experimental quick table clear 
  local b = next(f) local c = next(f,b)
  while b do f[b], b, c = nil, c, next(f,c) end
  return f
end

function ipaircopy(table,num) 
  local r={}
  if not table then return r end
  for i=1,num or #table do r[i]=table[i] end
  return r
end

function xortrue(a,b)  return a and not b or b and not a  end
function tonum(v,nilval)  return tonumber(v) or nilval or 0  end
function ftoi(v)  return math.tointeger(math.floor(v))  end
function strblank(c)  return c:match("%S") == nil  end --string is blank

function txttopatt(c) --lua pattern expression, 
  if not c then tracemore("invalid parameter here") end
  return c:gsub("[%^%$%(%)%%%.%[%]%*%+%-%?]","%%%0")
end

function tain(v,bot,top) --con(tain) v=tain(v,[low or 0,[high or no high]])
  bot=bot or 0 
  v = v==v and v or bot --sheds damn nans
  if v<bot then v=bot end
  if top and v>top then v=top end
  return v
end

function dictlen(d,nofalse) --counts dictionary entries in table
  local n=0
  if type(d) ~= 'table' then return 0 end
  for k,v in pairs(d) do 
    if v or not nofalse then
      if type(k)~='number' or k<1 or k~=math.tointeger(k) then n=n+1 end 
    end
  end
  return n
end

function unconcat(hay, sep, eschar) -- hay, sep = 1 plain char or pattern [,escape char]
  local sep, tbl, insert, patt   =   sep or "", {}, table.insert

  if #sep>1 or eschar then 
    return unconcat_(hay,sep,eschar) 
  end 
  
  if #sep==0 then patt ="(.)" else
    patt="([^"..sep:gsub("%W","%%%0").."]*)"
  end
  
  for c in hay:gmatch(patt) do  insert(tbl,c)  end
  return tbl
end

function unconcat_(hay, septt, eschar)  --unconcat (hay,sep pattern[, escape char]) 
  local insert, tbl, emtc = table.insert, {} 
  septt=septt or "[,.| ]+"
  if eschar and #eschar>0 then
    eschar = eschar:gsub("%W","%%%0")
    septt = eschar.."*"..septt
    emtc = "("..eschar.."+)"..septt
  end
  
  local ank,fndst,fndy,fnda,fnde = 1,1,#hay
  while fndst do
    fnda,fnde = hay:find(septt,fndst)
    if fnde then
      fndst=fnde+1
      fndy= fnda-1
      if eschar and fnda~=fnde then -- room for escape
        local esca,esce,escm=string.find(hay:sub(fnda,fnde),emtc)
        if esce and (#escm)&1==1 then
          fndy = nil --septt was escaped 
        end
      end
    else --no thing left
      if ank<=#hay then fndy=#hay end
    end
    
    if fndy then 
      insert(tbl,hay:sub(ank,fndy))
      ank=fnde and fnde+1 --fnde may be nil 
      fndst=ank           --nil ends while loop
    end 
  end
  return tbl
end

--unicode iterator from lua manual

function utftotable(txt) 
  local utab={} 
  for c in string.gmatch(txt, "([%z\1-\127\194-\244][\128-\191]*)") do 
    table.insert(utab,c) 
  end
  return utab
end

function hasutf(txt)  return txt:find("[%z\194-\244][\128-\191]*")  end

-- lookups from https://gist.github.com/natanael-b/3de34eff407477cc519eb6a309ebaf14

local utf_lower = { 
 [192]=224, [193]=225, [194]=226, [195]=227, [196]=228, [197]=229, [198]=230, [199]=231, 
 [200]=232, [201]=233, [202]=234, [203]=235, [204]=236, [205]=237, [206]=238, [207]=239, 
 [208]=240, [209]=241, [210]=242, [211]=243, [212]=244, [213]=245, [214]=246, [216]=248, 
 [217]=249, [218]=250, [219]=251, [220]=252, [221]=253, [222]=254, [376]=255 }

local utf_upper = { 
 [224]=192, [225]=193, [226]=194, [227]=195, [228]=196, [229]=197, [230]=198, [231]=199, 
 [232]=200, [233]=201, [234]=202, [235]=203, [236]=204, [237]=205, [238]=206, [239]=207, 
 [240]=208, [241]=209, [242]=210, [243]=211, [244]=212, [245]=213, [246]=214, [248]=216, 
 [249]=217, [250]=218, [251]=219, [252]=220, [253]=221, [254]=222, [255]=376 }
                     
local function utfcaseup(str,cas)
  if cas then str=string.lower(str) cas=utf_lower
  else str=string.upper(str) cas=utf_upper end
  if not hasutf(str) then return str end
  
  local r= {}
  for k,v in utf8.codes(str) do  r[#r+1] = utf8.char( cas[v] or v ) end
  return table.concat(r)
end

function upperutf8(str)  return utfcaseup(str)  end
function lowerutf8(str)  return utfcaseup(str,"low") end

function rndstr(n,s) --rts rnd string len n of "s" or lowcase alphabet
  n=n or 6 
  s=s or "abcdefghijklmnopqrstuvwxyz"
  local l,r = #s,{}
  if hasutf(s) then
    s=utftotable(s)
    l=#s
    for c=1,n do  r[c]=s[math.random(l)]  end
  else
    for c=1,n do
      local p=math.random(l)
      r[c]=s:sub(p,p)
    end
  end
  return table.concat(r)
end

function nearnuff(a,b,c) --c roughly translates to machine epsilons, when a&b have same sign
  c = (a+b)*(c or 10)    --when c=10 returns true if a,b are less than 8 machine eps apart 
                         --false if more than 16 eps apart, and either if between
  return c + (a-b) == c  --for 0.9 to equal 1, c needs to very large, around 6e+14 
end

function array_shuffle(t,b,d) --shuffles b to d of t
  b=b or 1  d=d or #t
  local c,e=b,d
  while c<e do 
    d=math.random(c,e)   t[c],t[d] = t[d],t[c]   c=c+1 
  end
  return t
end

function array_mingle(tc,tx,first)
  local x,c = 1 , (first or #tc==0) and 1 or 2
  while x<=#tx do
    table.insert(tc,c,tx[x])
    if c<#tc then c=c+2 else c=c+1 end
    x = x+1
  end
  return tc
end

function array_permute(tbl,prm) -- inplace reorder tbl by prm
  local av,ci,di
  for ai=1,#tbl-1,1 do 
    av,ci = tbl[ai],ai    -- note anchor tbl value and its index
    while prm[ci]~=ci do  -- Continue swapping elements until a walk is completed
      di = prm[ci]        -- note the due tbl index for this current element
      prm[ci] = ci        -- reset current prm index as it will be stepped next
      if di ~= ai then    -- If dest index is not the same as the anchor index, 
        tbl[ci],ci = tbl[di],di -- move current position to due position 
      else
        tbl[ci] = av      -- end of walk so put anchor value into current
      end
    end                   -- try another walk at next anchor
  end
  return tbl
end

function array_flip(t)
  for i=1, #t//2 do  t[i],t[#t-i+1] = t[#t-i+1],t[i]  end
  return t
end

function array_ordinals(n) -- return array {1,2,3,4...}
  local ix={}
  for i=1,n do ix[i]=i end
  return ix
end

function has_order(t) --rts 0 for bad order, 1 ascending,-1 descending

  local b,d,c=1,1,0
  for i=2,#t do
    if not (t[i] and t[i-1]) then b,d=0,0  --no order if false elements
    elseif t[i]==t[i-1] then c=c+1        --equal
    elseif t[i] >t[i-1] then d=d+1       --ascending
    else b=b+1 end                      --descending
  end

 return (d+c)==#t and 1 or (b+c)==#t and -1 or 0
end

function natural_order(t) --returns natural sort index
  local t2={}             --eq vals are also sorted by index
  local function cmp(a,b) 
    local bisnum = type(t2[b])=='number'
    if type(t2[a])=='number' then 
      if not bisnum then 
        return false
      else return t2[a]<t2[b] or (t2[a]==t2[b] and a<b) end
    elseif bisnum then return true
    else 
      return t2[a]<t2[b] or (t2[a]==t2[b] and a<b)
    end
  end

  for i=1,#t,1 do 
    local u=t[i]:match("^%s*([^%s]+)")
    u = u and tonumber(u)
    t2[i]=u or t[i]:gsub("%d+",function(d) return string.format("%020d",d) end ) 
  end
  ix=array_ordinals(#t)
  table.sort(ix,cmp)
  return ix
end

function standard_order(t)       --return standard sort index
  local ix=array_ordinals(#t)    --eq vals are also sorted by index
  local function cmp(a,b) return t[a] < t[b] or (t[a]==t[b] and a<b)end
  table.sort(ix,cmp)
  return ix
end

function table_extend(t,x) -- array appends x to t and combines dictionary 
  local e=#t
  for c=1,#x,1 do t[e+c]=x[c] end
  for k,v in pairs(x) do 
    if type(k)~='number' or k<1 or k~=math.tointeger(k) then 
      t[k]=v
    end 
  end
  return t
end