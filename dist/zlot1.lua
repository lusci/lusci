
local find = string.find
local match=string.match

--?
local pane           --the pane
local hit, fpos, cnt --hit, found pos, count

function seemspath(str)
  local s=(str or ""):match("^[%s]*([%a~]?:?[\\/][\\/%w.@~&%-_ ]*[%w.@~&-_])[%s]*$")
  return s
end

Lc_shortcut("pastepathcycle|pastepath|Ctrl+p")  --dumb
local lasttap,thistap=lasttap or 0,thistap or 0
function pastepath()
    
  local pan,cp,cq,rv=get_pane()
  local w,a,e=findonpos("%f[A-Z/][A-Z/][\\/:%w_\128-\244.]+",pan,cq)
  
  local bb=fhistory(10)
  local nx
  for k,v in ipairs(bb) do
    if w==v then 
      if k==8 then nx=bb[1] else nx=bb[k+1] end
      pan:SetSel(a,e)
      pan:ReplaceSel(nx or "")
      return true
    end
  end
  if a then pan:SetSel(a,e) end
  pan:ReplaceSel(bb[1])
  return true
end

--scite_Command("|abbrevhk")
--quick note for an abbreviation catcher, to indent it

--function abbrevhk()
  --get term behind caret
  --match inside $[]
  --case this or that setting replacement
  --if no replacement, try the props
  --if replacement set, replace it and return false
  --if not return true
--end

--~ scite_Command("Open API|OpenAPIFile|Ctrl+Shift+n")
--~ function OpenAPIFile()
--~ 	scite.Open(props["APIPath"])
--~ end

Lc_shortcut("Open Abrev|OpenAbrev|Ctrl+Shift+B")
function OpenAbrev()
  scite.Open(props["AbbrevPath"])
  return true
end

--~ scite_Command("Open Atrium|OpenAbrev|Ctrl+Shift+m")
--~ function OpenAbrev()
--~ 	scite.Open(props["AbbrevPath"])
--~ end

Lc_shortcut("Undo|modundo|Ctrl+z")
Lc_shortcut("Undo|altundo|Ctrl+shift+Z")

function altundo()
  if editor.Focus then output:Undo() else editor:Undo() end
  return true
end
 
function modundo()
  
  if editor.Focus then editor:Undo() return true end
  output:Undo() -- reduce times undo lands caret at start of line
  
  local s,f=poslinestfn(output.CurrentPos,output)
  if s==output.CurrentPos then output:Undo() end
  return true
end

function exporthtml()
  exporters:SaveToHTML()
  print()
end

--~ Lc_shortcut("Lc_ down|slidemove 's'|Alt+/")
--~ Lc_shortcut("Lc_ up|slidemove|Alt+'")

function slidemove(smaller) -- this made output bigger and smaller
                            -- scite executable needs modded to work
  if smaller=="s" then
    local vis=caretisvis(output)
    scite.MenuCommand(424)
    if vis then
      output:ScrollCaret()
    end
  else
    local vis=caretisvis(editor)
    scite.MenuCommand(423)
    if vis then
      editor:ScrollCaret()
    end
  end
  return true;
end

--[[Lc_shortcut("Paste across|pasteintty|Ctrl+Shift+M")
function pasteintty()
  local panea,paneb=editor,output 
  if output.Focus then panea,paneb=paneb,panea end
  
  if not panea.Anchor == panea.CurrentPos then
    paneb:AddText(panea:GetSelText())
  else
    paneb:AddText("\n")
  end
  return true
end
]]
--scite_Command("New Test|outtoeditor|Ctrl+Alt+t")

function outtoeditor() --cant style new untitled buffer ?
  scite.Open("")
  scite.MenuCommand(IDM_LANGUAGE + 9)

  editor:BeginUndoAction()

  editor:AppendText(output:textrange(0,output.Length))
  editor:Colourise(0, editor.Length)
  
  editor:EndUndoAction()

  output:BeginUndoAction()
  output:SetSel(0,output:PositionFromLine(output.LineCount-1))
  output:Clear()
  output:GotoPos(output.Length-1)
  output:EndUndoAction()
end

Lc_shortcut("Quit|scitequit|Alt+w")

function scitequit()
  scite.MenuCommand(IDM_QUIT)
  return true
end

function homeendkeyp( homek )
  local pane=get_pane()
  if homek then
    local edg=pane:PositionFromLine(pane:LineFromPosition(pane.CurrentPos))
    if	pane.CurrentPos== edg then
      pane:GotoPos(0)
    else
      sendscite(pane,2453)
    end
  else
    local edg=pane.LineEndPosition[pane:LineFromPosition(pane.CurrentPos)]
    if pane.CurrentPos==edg then
      pane:GotoPos(pane.Length)
    elseif find(pane:textrange(pane.CurrentPos,pane.CurrentPos+1),"%s")
    and pane.CurrentPos==pane:PositionFromLine(pane:LineFromPosition(pane.CurrentPos))
    then
      sendscite(pane,2453)
    else
      sendscite(pane,2451)
    end
  end
end

--~ Lc_shortcut("Home Key|homeendkeyp 1|Home")
--~ Lc_shortcut("End Key|homeendkeyp|End")    --stupid already configable

Lc_shortcut("Leap up|fleap 1|Ctrl+k")
Lc_shortcut("Leap up other|fleap 2|Ctrl+Shift+K")
Lc_shortcut("Leap down|fleap 3|Ctrl+j")
Lc_shortcut("Leap down other|fleap 4|Ctrl+Shift+J")

--~ function findupdownxe() 	findupdownx("n")      1  end  --up
--~ function findupdownxed() 	findupdownx("n",true) 3  end  --down
--~ function findupdownxo() 	findupdownx("y")      2  end
--~ function findupdownxod() 	findupdownx("y",true) 4  end

local twofind
local lastfind
local fclock

--quick jump back find in either pane
function fleap(m)

  --print(m)
  local pane=get_pane()
  local pane2=pane
  local fdown
  if m==3 or m==4 then fdown=true end
  if m==2 or m==4 then if pane2==output then pane2=editor else pane2=output end end
  
  local ecp=pane.CurrentPos
  local eap=pane.Anchor
  if eap>ecp then eap,ecp=ecp,eap end

  local sely=pane:textrange(eap,ecp)
  if sely=="" then sely=get_word(pane) end

  if (sely or "")=="" then sely=lastfind else lastfind=sely end
  if (sely or "")=="" then return false end

  pane=pane2 --twist for otherpane
  ecp=pane.CurrentPos
  eap=pane.Anchor
  if eap>ecp then eap,ecp=ecp,eap end

  local fna,fnc
  
  if fdown then
    fna,fnc = pane:findtext(sely,0,ecp)
    if (not fna) and (not (twofind and (os.clock()-(fclock or 100000)>0.11))) then
      twofind=true fclock=os.clock()
      return false
    end
    
    if not fna then fna,fnc = pane:findtext(sely,0,0)
      if fna==nil then print("\n+Couldnt find "..sely) tracePrompt() return false end
    end
  else
    --complicated upwards
    fna,fnc = pane:findtext(sely,0,0)

    if fna==nil then print("\n+Couldnt find "..sely) tracePrompt() return false end

    local limi=eap-1
    if fna>=eap then
      if (not (twofind and (os.clock()-(fclock or 100000)>0.11))) then
        twofind=true fclock=os.clock()
        return false
      end
      limi=pane.LineEndPosition[pane.LineCount-1]
    end

    local la,lc,lx  --last good find
    la=fna
    lc=fnc
    lx=0

    --~ 		if true then print("la "..la.." ,lc "..lc.." ,fna "..fna) end

    while fna and la<limi and lx<100 do
      la=fna --record last, and find next
      lc=fnc
      lx=lx+1
      fna, fnc = pane:findtext(sely,0,la+1,limi )
    end

    --~     if true then print("la "..la.." ,lc "..lc.." ,lx "..lx) end

    if fna==nil then
      fna=la fnc=lc
    end
  end
  
  twofind=nil
  pane:GotoPos(fna)
  pane:SetSel(fna,fnc)
  return true
end


--bruce dobson's fix indentation

function fixIndentation()
  local tabWidth = editor.TabWidth
  local count = 0
  if editor.UseTabs then
    local preindentSize=-1
    -- for each piece of indentation that includes at least one space
    for m in editor:match("^[\\t]*[ ]+[\\t]+", SCFIND_REGEXP) do

      local indentSize = editor.LineIndentation[editor:LineFromPosition(m.pos)]
      
      if string.find(m.text,"%{[\\t ]*$") then
        preindentSize=preindentSize+tabWidth
      elseif string.find(m.text,"^[\\t ]*%}[\\t ]*$") then
        preindentSize=preindentSize-tabWidth
      end
      
      if preindentSize>-1 then indentSize=tain(indentSize,0,preindentSize) end
      
      local spaceCount = math.mod(indentSize, tabWidth)
      local tabCount = (indentSize - spaceCount) / tabWidth
      local fixedIndentation = string.rep('\t', tabCount) .. string.rep(' ', spaceCount)

      if fixedIndentation ~= m.text then
        m:replace(fixedIndentation)
        count = count + 1
      end
    end
  else
    -- for each piece of indentation that includes at least one tab
    for m in editor:match("^[\\t ]*\t[\\t ]*", SCFIND_REGEXP) do
      -- just change all of the indentation to spaces
      m:replace(string.rep(' ', editor.LineIndentation[editor:LineFromPosition(m.pos)]))
      count = count + 1
    end
  end
  return count
end
    
fixind = fixIndentation
-----------------------------------------------------
-- Strip Trailing Spaces
-- Filename endings set here or in scite properties:
-- Luax.StripTrailSpaceEnds=.lua|.foo|readme
-- Luax.StripTLeaveEmpties=1   to leave empty lines
-- Luax.StripTReportStrips=1   to leave empty lines

local function isTrailingFile()
  -- register here the extensions (endings) that shall be parsed
  tt=props["luax.StripsTrailSpaceEnds"].."|"
  if tt=="|" then tt=".lua|.py|" end

  return find(tt, ((match( props["FilePath"],"[.].+$") or "-").."|") )
end

function stripTrailSpaces()
  local ed=editor
  --(dunno syntax for testing props simply)
  local LeaveEmptyLines = true
  if tonumber(props['luax.StripsTLeaveEmpties'])==0 then LeaveEmptyLines=false end
  local ReportStrips=true
  if tonumber(props['luax.StripsTReportStrips'])==0 then ReportStrips=false end

  if not isTrailingFile() then return end
  noteSelection()
     
  local icount=fixIndentation()

  local hit,fpos,cnt = -1, 0,  0
  
  hit,fpos=ed:findtext("[\t ][\t ]+$", SCFIND_REGEXP, hit+1 )
--~   hit,fpos=ed:findtext("[\t ]+$", SCFIND_REGEXP, hit+1 )
  while hit do

    if LeaveEmptyLines and ed:PositionFromLine(ed:LineFromPosition(hit))==hit
    then
      hit=fpos --skip
    else
      if hit+1~=fpos then ed:remove(hit+1,fpos) end
--~       if hit~=fpos then ed:remove(hit,fpos) end
      cnt=cnt+1
    end

    hit,fpos=ed:findtext("[\t ][\t ]+$", SCFIND_REGEXP, hit+1 )
--~     hit,fpos=ed:findtext("[\t ]+$", SCFIND_REGEXP, hit+1 )
  end

  restoreSelection()
  
  if ReportStrips and cnt>5 then
    trace("\n- "..cnt.." runs of trailing spaces were stripped")
  end

  if ReportStrips and icount > 5 then
    trace("\n- Fixed indentation for " .. icount .. " line(s).")
  end

  if ReportStrips and (icount>0 or cnt>0) then
    print() tracePrompt()
    output:ScrollCaret()
  end
end
scite_OnBeforeSave( stripTrailSpaces )

function hardStrip()
  local ed=editor
  --(dunno syntax for testing props simply)
  local LeaveEmptyLines=false --true
  if tonumber(props['luax.StripsTLeaveEmpties'])==0 then LeaveEmptyLines=false end
  local ReportStrips=true
  if tonumber(props['luax.StripsTReportStrips'])==0 then ReportStrips=false end

  noteSelection()
     
  local icount=fixIndentation()

  local hit,fpos,cnt = -1, 0,  0
  
--~ 	hit,fpos=ed:findtext("[\t ][\t ]+$", SCFIND_REGEXP, hit+1 )
  hit,fpos=ed:findtext("[\t ]+$", SCFIND_REGEXP, hit+1 )
  while hit do

    if LeaveEmptyLines and ed:PositionFromLine(ed:LineFromPosition(hit))==hit
    then
      hit=fpos --skip
    else
--~       if hit+1~=fpos then ed:remove(hit+1,fpos) end
      if hit~=fpos then ed:remove(hit,fpos) end
      cnt=cnt+1
    end

--~ 		hit,fpos=ed:findtext("[\t ][\t ]+$", SCFIND_REGEXP, hit+1 )
    hit,fpos=ed:findtext("[\t ]+$", SCFIND_REGEXP, hit+1 )
  end

  restoreSelection()
  
  if ReportStrips and cnt>5 then
    trace("\n- "..cnt.." runs of trailing spaces were stripped")
  end

  if ReportStrips and icount > 5 then
    trace("\n- Fixed indentation for " .. icount .. " line(s).")
  end

  if ReportStrips and (icount>0 or cnt>0) then
    print() tracePrompt()
    output:ScrollCaret()
  end
end


--mystery debug function from somewhere ..
function bug(name)
  if not Lc_['dobug'] then return end
  
  local info = debug.getinfo(2)

  if info.what == "C" then   -- is a C function?
    print("C function debug?:")
    return
  end
  local value, found

  -- try local variables
  local i = 1
  while true do
    local n, v = debug.getlocal(2, i)
    if not n then break end
    if n == name then
      value = v
      found = true
    end
    i = i + 1
  end
  if found then return value end

  -- try upvalues
  local func = info.func
  i = 1
  while true do
    local n, v = debug.getupvalue(func, i)
    if not n then break end
    if n == name then return v end
    i = i + 1
  end
  local val=getfenv(func)[name]
  if output.CurrentPos~=output:PositionFromLine(output.Length) then
    print()
  end
  print(string.format("%s:%d:%s-%s",
        info.short_src, info.currentline,info.namewhat or "glob",info.name or "anon"))
  print(string.format("%s='%s'",name,val))
end
