
--most word ops currently using terminstring to discern words but its
--inefficient when searching left or right from a word boundary
--as terminstring discerns words also from within word
--to discern from one boundary to next l/r just search patterns
--on a sub string clipped at the boundary and (a/pre)ppend $ or ^ the needles
--list of word and gap needles to be expanded and improved, possibly
--altered for filetypes

--word select is currently not working right at end of document


--write tabshove l/r attached to pgup/dwn
--set configureables:
-- props wordshove autoselect 0,1,2,3 
--  (0:sel wd, 1:selwd and shove immediate, 2:slwd and gap, 3:slwd and gap and shv immdt) 
-- props lineshove autoselect 0,1
--  (0:sel ln, 1:sel ln and shove immediate) 

--lineshove mod?
--an indented sel is moved up/down to indent level
--of surrounding lines? -could work perfectly neatly, 
--better just to have reindenting attached to pgup/dwn or somewhere ?

--~ function catchtab(lft)
--~   local pan,c,d = get_pane()
--~   if c==d then return false end
--~   return shovelinetab(lft)
--~ end 

function shovelinetab(lft) 
  local pan,c,d,rv = get_pane()
  local nosel = c==d
  local ca,ce,cn=poslinestfn(c,pan)
  local da,de,dn=poslinestfn(d,pan)
  if cn==dn or d>da then da,de=poslinestfn(d,pan,1) end
  
  pan:SetSel(ca,da)
  if lft then pan:BackTab() else pan:Tab() end
  if cn~=dn and false then  --allowing sel expansion for multiline, maybe dont... 
    if rv then pan:SetSel(pan.CurrentPos,pan.Anchor) end
    return
  end
  local pan,cp,dp = get_pane() --all this to calc for unexpanded multiline sel
  local xa,xe=poslinestfn(cp,pan)
  local clenincrease= (xe-xa) - (ce-ca)
  local cm,dm = tain(c + clenincrease,ca) , d + (dp-da)
  if rv then cm,dm=dm,cm end
  if nosel then dm=cm end
  pan:SetSel(cm,dm)
  recordselnow(pan,cm,dm)
  return true 
end

function moveandsel(p,a,c,d,sflip,nosel) -- pane,anchor,current, due anchor
  if d==tain(d,a,c) and d~=math.max(a,c) then err() return end
  p:SetSel(a,c)
  local tx,tn=p:GetSelText() --is tn same as abs a-c ?
  p:BeginUndoAction()
  p:ReplaceSel("")
  if d>a then d=d-math.abs(a-c) end 
  p:SetSel(d,d)
  p:ReplaceSel(tx)
  p:EndUndoAction()
  
  if nosel then      p:SetSel(d+nosel,d+nosel)
  elseif sflip then  p:SetSel(d+tn,d) 
  else               p:SetSel(d,d+tn) 
  end
  
  enviscp(p)
  return sflip and p.CurrentPos or p.Anchor
end

function wordandgap(pan,c,lft,behind)
  local mt,d,a = oneinterval(pan,c,lft) -- returns anchor and d is in direction
  local xmt,xd
  if not behind then
    xmt,xd = oneinterval(pan, d, lft)
    if mt~=xmt then d=xd end
  else
    xmt,xd = oneinterval(pan, a, not lft)
    if mt~=xmt then  a=xd 
--~     d,a = a,d 
    end
  end
  
--if not lft then c,d=d,c end
  return a,d
end

function shoveover(ops)  -- alt lf rg
  --if not sel then sel=curword and return --this is word a select
  local lft=ops:sub(1,1)=="l"
  local wds=ops:sub(2,2)=="w"
 
  local pan,c,d = get_pane() -- 
  local nosel --not currently implemented here ...
  
  local a,b = poslinestfn(c,pan)
  
  if c==d or d~=tain(d,a,b) then --no sel or multiline
    shovelinetab(lft and "lft")  --doshovetab instead
    return true
  end
  
  local dest = (lft and c-1) or d+1 
  
  if not wds and dest~=tain(dest,a,b) then   --eeby 
    if dest<a then 
      if pan:textrange(d,b):match("%S") then pan:insert(d," ") pan:SetSel(c,d) end 
    else 
      pan:insert(c," ") pan:SetSel(c+1,d+1)
    end
  else 
    if wds then
      _,dest=wordandgap(pan, (lft and c) or d, lft)
    end
    moveandsel(pan,c,d,dest,lft)
    pan,c,d = get_pane(pan)
    a,b = poslinestfn(c,pan)
    if not pan:textrange(d,b):match("%S") then
      pan:remove(d,b)
    end
  end
  recordselnow(pan)
  return true
end

function wordshove(lft)  -- alt lf rg
  --if not sel then sel = curword and return -- this is a word select
  local mtype
  local pan,b,d,rv,a,c = get_pane()
  if b==d then
    
  --begin complication to drag gap behind if near end of line
    local gapbehind
    local la,le=poslinestfn(c,pan)
    if (b-la>le-b and not lft) or (b-la<le-b and lft) then 
      gapbehind=true 
    end
    
    b,d=wordandgap(pan,b,lft,gapbehind)
    --end complication, seems sucess
    
    pan:SetSel(b,d)
    pan,b,d,rv,a,c = get_pane() --continue do one leap...
    --return true
  end
  
  --move and word gap when going with selection
  --one just word/gap when going against
  --words to push dont come to me boo easy hoo

  --caret leaving selection
  --  see what wordtype behind caret
  --  hop indirection to other side of that wordtype (perhaps over an opposite kind)
  
  --interval selection, hope to endof on caretcrossing    other side of selection
  
  local xtype,xc,xa,stage=oneinterval(pan,(lft and b) or d,lft)
  
  if xortrue(lft, c>a) then  --caret is leaving selection
    if xtype==oneinterval(pan, a, lft, stage) then --same wordtype behind caret
      xtype,xc,xa = oneinterval(pan, xc , lft, stage)
    end
  end
  moveandsel(pan,b,d,xc,lft)
  recordselnow(pan)
  return true
end

local hopbp,hoplft,hopflp

function ctrlmoveright(lft)
  local pan,b,d,rv,a,c = get_pane()
  local flp=props['FilePath']
  
  if b~=d then
    ctrlselright(lft,true)
    return true
  end

  local onestep = false
  if flp==hopflp and c==hopbp and hoplft~=lft then onestep = true end
  hopflp=flp
  local mtype,dp,xp,stage=oneinterval(pan,c,lft)
  if not dp then return true end --an edge of txt case to improve ** 
  if xp==c and not onestep then --try adding a gap if at word end
    
    local wd,dp2,xp2,mtype2 = awordscan(stage.txt,dp-stage.ap,lft) 
    if dp2 then dp2,xp2=dp2+stage.ap,xp2+stage.ap end
    if not lft then  dp2,xp2=xp2,dp2  end
--~     print(wd,mtype2,mtype)
    if (mtype==0 and mtype2==1) or (mtype==1 and mtype2==0) 
     and pan:LineFromPosition(dp)==pan:LineFromPosition(dp2) then 
      dp=dp2 
    end
  end
  
  pan.CurrentPos,pan.Anchor = dp,dp
  hopbp=dp hoplft=lft 
  if onestep then hopbp=0 end
  seatcaret(pan)
  enviscp(pan)
  return true
end

function ctrlselright(lft,edge)
  local pan,b,d,rv,a,c = get_pane()

  local x = (lft and b) or d
  if edge then x = c 
  else
    if xortrue(rv,lft) 
    and (d-b>30 or pan:LineFromPosition(d)~=pan:LineFromPosition(b)) then
      pan.Anchor,pan.CurrentPos = c,a
      seatcaret(pan) enviscp(pan)
      return true
    end
  end
  
  local mtype,dp,xp=oneinterval(pan,x,lft)
   
  if b==d and mtype~=0 --and math.abs(dp-xp)<2
  then
    mtype,dp,xp=oneinterval(pan,dp,lft)
  end
--[[
  if b~=d and mtype==0 and xp==b then --try adding a gap if at word end
  local   mtype2,dp2,xp2=oneinterval(pan,dp,lft)
    if mtype2==1 and pan:LineFromPosition(dp)==pan:LineFromPosition(dp2) then 
      dp=dp2 
    end
  end
]]--
  if dp then
    if not edge then
      if lft then 
        pan.Anchor,pan.CurrentPos = d,dp 
      else 
        pan.Anchor,pan.CurrentPos = b,dp 
      end
    else
      if lft then pan.CurrentPos = dp else pan.CurrentPos = dp end
    end
  end
  enviscp(pan)
  return true
end

--the select has to be able to select line with ends
--for duping and cutting
--so make it so that when both sides are selled it adds the ending
--it adds the ending if the start is selected already...
local shoveleft,shovelast = true

function shovedown(up)
  local pan,b,d,rv,a,c = get_pane()
  local ap,ep,lnn=poslinestfn(b,pan)
  local nlc,olin= #nlchar(), pan.LineCount
  local flipsel,dontsel

  if up and pan.LineCount==1 then  return true  end
--~   if bott then pan:AppendText(nlchar()) end
  if lnn>olin-3 and not up then  pan:AppendText(nlchar())  end
  
  if b==d then 
    dontsel=b-ap 
    b=ap d=poslinestfn(b,pan,1)
    shoveleft =true 
  else
    if not (shovelast and shovelast==b) then
      shoveleft = b==ap
    end
  end
  
  if up and lnn>0 then
    ap,ep = poslinestfn(b,pan,-1)
    shovelast=moveandsel(pan,b,d,shoveleft and ap or ep,flipsel,dontsel)
  end

  if not up then
    ap,ep = poslinestfn(d,pan,1)
    shovelast=moveandsel(pan,b,d,shoveleft and ap or ep,flipsel,dontsel)
    if lnn==olin-2 then
      pan:remove(pan.Length-nlc,pan.Length)
    end
  end
  
--~   if bott then  pan:remove(pan.Length-nl,pan.Length)  end
  recordselnow(pan)
  return true
end
--       aaaa2   22aa  a2a  3.8571428571814e-11

function ctrlseldwn(up)
  local pan,c,d,rv,a,e = get_pane() --check all get_panes, a,e only return when rv true
  local ap,ep,lnn
  
  _,_,ap=poslinestfn(c,pan)
  _,_,ep=poslinestfn(d,pan)

  local diflin = math.abs(ap-ep)

  --when caret is on c rv is true (this is upline) 
  if diflin>1 and xortrue(rv,up) then --xor
    pan:SetSel(e,a) --swap selends if up xor reverse
    enviscp(pan)
    return true
  end
  
  if up then
    ap,ep,lnn=poslinestfn(c,pan)
    if c ~= ap then c=ap else 
      c=poslinestfn(c,pan,-1) 
    end
    pan:SetSel(d,c)
  else
    ap,ep,lnn=poslinestfn(d,pan)
    if d ~= ep and c >= ap then d=ep else --  c >= ap or c> ap ? 
      ap,ep=poslinestfn(d,pan,1)
      d=ap>d and ap or ep --(catch for last line) 
    end
    pan:SetSel(c,d)
  end
  seatcaret(pan)
  enviscp(pan)
  return true 
end

function ctrlselpdwn(up) --for ctrlsel pageupdwn, not done yet..
  local pan,c,d,rv,a,e = get_pane() --check all get_panes, a,e only return when rv true
  local ap,ep,lnn
  
  _,_,ap=poslinestfn(c,pan)
  _,_,ep=poslinestfn(d,pan)

  --[[--go 2/3rd page up to see, but caret needs to stick when possible
  --calc lines on screen ... 
  local sctop,scbot = doclinesvis(pan)
  
  doclinefrompos(pan,c) -- for position for line
  
  ]]
  
  local diflin = math.abs(ap-ep)

  --when caret is on c rv is true (this is upline) 
  if diflin>1 and xortrue(rv,up) then --xor
    pan:SetSel(e,a) --swap selends if up xor reverse
    enviscp(pan)
    return true
  end
  
  if up then
    ap,ep,lnn=poslinestfn(c,pan)
    if c ~= ap then c=ap else 
      c=poslinestfn(c,pan,-1) 
    end
    pan:SetSel(d,c)
  else
    ap,ep,lnn=poslinestfn(d,pan)
    if d ~= ep and c > ap then d=ep else 
      ap,ep=poslinestfn(d,pan,1)
      d=ap>d and ap or ep --(catch for last line) 
    end
    pan:SetSel(c,d)
  end
  
  enviscp(pan)
  return true 
end

function ctrlpgup(p)
  local pan,c,d,rv,a,e = get_pane()
  
  if p:sub(2,2)=="o" then pan = pan==editor and output or editor end
  
  if p:sub(3,3)=="s" then
    if p:sub(1,1)=="d" then
      d=pan.Length
      if c==0 then c,d=d,c end
      pan:SetSel(d,c)
    else
      c=0
      if d==pan.Length then c,d=d,c end
      pan:SetSel(c,d)
    end
  else
    if p:sub(1,1)=="d" then
      pan.FirstVisibleLine=pan.Length
    else
      pan.FirstVisibleLine=0
    end
  end
end


local function charsinway(hx,pat,cp,lft)
  if not lft then cp=cp+1 end
  return string.match(hx:sub(cp,cp),pat)
end


-- %b%(%)  - 'balanced string' %bxy finds match enclosed by x and y
-- %f%w    - frontier of %w
-- frontier can be used as 1 char negative lookback or forward
-- %f[%w']%w - means no wordchar or quote before the matched wordchar
-- %w%f[^%w'] - means no wordchar or quote after the matched wordchar

-- pattern list, more specific patts are searched first
-- and most commonly matched are ranked top for quickest

-- this patt seems to match them accented and unicodes.."[%w\128-\244]+" 
-- investigate better? [%z\1-\127\194-\244][\128-\191]* unicode compatible match
-- from...offical lua doc? pattern needs %z to work ??
-- doesnt seem to need %z sofar ...

-- improved float detection:
-- 3.0     3.1416     314.16e-2     0.31416E1     34e1 
-- 0x0.1E  0xA23p-4   0X1.921FB54442D18P+1 12   123   1

local xxx=props['chars.accented'] or ""
xxx="" -- slow pattern not needed sofar

local awords={ 
  {['t']=2,['p']="  +"}                                         -- long space 
 ,{['t']=1,['p']="[.,]? "} 
 ,{['t']=0,['p']="%f[%w']%a+'%a+%f[^%w']"}                      -- wasn't it's you're o'clock
 ,{['t']=0,['p']="%f[%$%#%%&%w_\128-\244][%a_\128-\244][%w_\128-\244"..xxx.."]*"} -- not var words 
 ,{['t']=0,['p']="%f[%$%#%%&].[%a_\128-\244][%w_\128-\244"..xxx.."]*"}         -- $v4r_names 
 ,{['t']=0,['p']="%f[%w%-%+][%-%+]?0[xX]%x%.?[%x]*[Pp%d%+%-]*%f[%X]"} -- hex & 0xfloat
 ,{['t']=0,['p']="%f[%w%-%+][%-%+]?%d+%.?%d*[Ee][%+%-]?%d%d?%f[%D]"}  -- float
 ,{['t']=0,['p']="%f[%w%-%+][%-%+]?%d+%.?%d*%f[%D]"}                  -- -32.678
 ,{['t']=0,['p']="<[%a_][%a_]-/?>"}                  -- <nogaptags/?>
 ,{['t']=1,['p']="%f[*/~|<>:;,.%-+^]. ?%f[^ =*/~|<>:;,.%-+^]"} --single puncs 
 ,{['t']=3,['p']="\r\n?"  }          -- lin \n mac \r or win \r\n
 ,{['t']=3,['p']="\n"  }             -- no way to combine without eating 2 lines
 ,{['t']=2,['p']="%f[^\r\n][\t]+"  }  -- eat multiple tabs at line start
 ,{['t']=1,['p']="[\t]"  }            -- singular tabs after
-- |= ||= ^= := -= += == ~= <= >= = ===
 ,{['t']=1,['p']="%f[*/=|:+~<>^%-][^=]?=[>=]?%f[^=>]"} 
--&&  || ++ -- ** // :: .. ... 
 ,{['t']=1,['p']="%f[&|+%-*/.:].[&|+%-*/.:][.]?%f[^&|+%-*/.:]"} 
 ,{['t']=1,['p']='%f[\\\"]\"[^,%c]-%f[\\\"]\"' ,['a']='\"'}  -- dbl quotes without comma
 ,{['t']=1,['p']="%f[\\\']'[^,%c]-%f[\\\']'"   ,["a"]="\'"}  -- sgl quotes without comma
-- "   \"  \"  "
-- brace match leftwise doesnt work fully as braces right of cp confuse
-- if terminway(tx,pt,cp,lftwz) included option for end-match-on-cp
-- it could test brace match is for cp.

--~  ,{['t']=0,['p']="%b''" ,['a']='[\']' }  --matched pair quote, without comma bodge
 ,{['t']=0,['p']="%b<>" ,['a']='[%<%>]' }
 ,{['t']=0,['p']="%b()" ,['a']='[%(%)]' }   -- match balanced, but testing 
 ,{['t']=0,['p']="%b[]" ,['a']='[%[%]]' }  --doesnt match first leftwise...
 ,{['t']=0,['p']="%b{}" ,['a']='[%{%}]' }  --research with literal examples... 
}

local function txtstage(pan,c,lft) --limit sampling and stagger sampling to save hashing
  
  if (lft and c==0) or ((not lft) and c==pan.Length) then return false end
  
  --6 lines each way as max stage for multiline brace match 
  local ap,_=poslinestfn(c,pan,-6)
  local _,ep=poslinestfn(c,pan,6)
  
  --1 line including bounding endings as minimal stage
  local _,apl=poslinestfn(c,pan,-1)
  local epl,_=poslinestfn(c,pan,1)
  
  local stepgrain=64 
  local halfstage=128
  if c==nil then tracerr() end
  local cstep=math.floor(c/stepgrain)*stepgrain
  local cmin,cmax=tain(cstep-halfstage),tain(cstep+halfstage,0,pan.Length)
  
  apl,epl = tain(apl,cmin),tain(epl,0,cmax)
    
  local xx=pan:textrange(lft and c-1 or c,lft and c or c+1)

  if lft and xx:match("[\"'%]}>%)]") or (not lft) and xx:match("[\"'%[{<%(]") then
    --do multiline to span quotes and things
    if lft then ep = epl else ap=apl end -- snip but only to line ends to save hashes 
  else
    ap,ep=apl,epl --use the line limited stage for non quotes etc 
  end
  --returns txt,ap,ep,c-ap,
  return pan:textrange(ap,ep),ap,ep,c-ap,xx
end

-- oneinterval uses awordscan
-- wordandgap wordshove ctrlselright wordsomeway ctrlmoveright uses oneinterval

-- oneinterval discerns 1 term in direction, relys on aword scan to do all word discernment
function oneinterval(pan,c,lft,s) --returns matchtype,leading cpos,trailing cpos
  
  if not s then 
    s={} s.txt,s.ap,s.ep,s.cp,s.xx = txtstage(pan,c,lft)
  else
    s.cp=c-s.ap
  end
  if not s.txt then return -1,s.cp,s.cp,"" end
  local wd,pa,pe,mtype = awordscan(s.txt,s.cp,lft)
  
  return mtype,lft and s.ap+pa or s.ap+pe, lft and s.ap+pe or s.ap+pa , s
end

function awordscan(hx,cp,lft)
  local wd,pa,pe,mtype
  local xp,xx
  if lft then xp=cp else xp=cp+1 end
  local  xx=hx:sub(xp,xp) --first char in direction

  if xx=="" then return "",cp,cp,4 end --problematic edge...
   
  for k,pat in pairs(awords) do
    if (not pat.a) or (pat.a and xx:find(pat.a)) then
--~       if pat.a then prnte(pat.p,xx:find(pat.a)) end
      wd,pa,pe=terminway(hx,pat.p,cp,lft,pat.a)
--    print("a"..pat.p.."b") 
    end
 
    if wd then 
      --reduce to run of repeated letter if very long word
      --if pat.t==0 and pe-pa>16 then ...
      mtype=pat.t
--    print("a2"..pat.p.."b")
--~       print('"'..pat.p..'"')   --prints the pat that matched
      break  end
  end 
 
  if not wd then 
    wd,pa,pe=terminway(hx,"[%"..xx.."]+",cp,lft)
--  print("[%"..xx.."]+")
    mtype=2
  end
 
  if not wd then --last ditch 1 char move 
    pa=cp + (lft and -1 or 1) 
    pe=pa
    mytpe=2
  end

  return wd,pa,pe,mtype
end

function wordsomeway(pan,c,lft,either) --returns matchtype,leading cpos,trailing cpos

  --looks for a prime word type in direction,
  --tries other direction if none found
  --returns opposite direction when none found in either --(todo resolve)
  if pan.Length==c then lft=true end --test fix for word sel at end of file
  
  local hx,ap,ep,cp,xx=txtstage(pan,c,lft)
  if not hx then return -1,c,c end
  
  local wd,pa,pe,mtype=awordscan(hx,cp,lft)

  if either then --looks other way when typicl wd not here 
    local xwd,xpa,xpe,xmtype=awordscan(hx,cp,not lft) 
    if xmtype<mtype or (xmtype<mtype and #xwd>#wd) then --takes typical wd in other side
      wd,pa,pe,mtype=xwd,xpa,xpe,xmtype
    else              --absorbs space, has problems...
--    pa,pe = math.min(pa,xpa) , math.max(pe,xpe)
--    wd=hx:sub(pa,pe) --might be off !!! , hmm not returning wd...
--    mtype=3
--    print("joined non words")
    end
  end
                       --lft /direction, may be redundant here
  return ap+pa , ap+pe , mtype , lft and ap+pa or ap+pe, lft and ap+pe or ap+pa
end



function ctrldel(lft)
  local pan,b,d,rv,a,c,_ = get_pane()
  if lft then d=b b=wordsomeway(pan,b,lft)
  else b=d _,d=wordsomeway(pan,d,lft)
  end
  pan:remove(b,d)
--~   if lft then c,d= c-e+a,d-e+a end
--~   editor:SetSel(c,d) --editor:remove moves selection well
  seatcaret(pan)
  return true
end

function wordsel(cpy)
  local pan,b,d,rv,a,c,_ = get_pane()
  local lft
  if b==d then 
    local oo,pp=poslinestfn()
    lft = oo~=pp or false
    local wb,wd,typ=wordsomeway(pan,c,lft,"either")
    
    if b-wb<wd-b then wb,wd=wd,wb end
    pan:SetSel( wd,wb )
    
    if cpy then
      pan:Copy()
      pan:SetSel( a,c )
    else
      recordselnow(pan)
    end
    return true
  end

  if cpy then --maybe move this ...
    pan:Copy()
    return true
  end
  
  if c<a then lft=true end

  local wb,wd,typ=wordsomeway(pan,c,lft) 
  local wb2,wd2,typ2=wordsomeway(pan,a,not lft)
  
  if  (wb==c and wd2==a and not lft) or (lft and wd==c and wb2==a) 
  then --whole words extending by another on caret side
    b,d=math.min(wb,wd,b),math.max(wb,wd,d)
  else --pad caret and anchor to word endings
    if wb~=c then
      b,d=math.min(wb,wd,b),math.max(wb,wd,d)
    end
    if wd2~=a then
      b,d=math.min(wb2,wd2,b),math.max(wb2,wd2,d)
    end
  end
  
  if lft then
    pan:SetSel( d,b )
  else
    pan:SetSel( b,d )
  end
  recordselnow(pan)
  return true
end

Lc_shortcut("ctrldel|ctrldel 'lft'|ctrl+backspace")
Lc_shortcut("ctrldel|ctrldel|ctrl+delete")
-- Lc_shortcut("wordsel|wordsel|ctrl+.")


Lc_shortcut("cyclecase|fselevocase|Ctrl+u")
Lc_shortcut("linejnsmpl|linejnsmpl|Ctrl+shift+u")

--todo....
--ctrl+alt pgdw to remove linebreaks in selection or adds next line
--to current and selects, maintains indent&comment openers
--if selection
--  if linebreaks
--    decrease lnbks / increase linelength
--  else
--    add next line, or increase/justify line ?
--else
--  add next line
--  select

--ctrl+alt pgup to split line at space closest to middle, maintaining indent
--and comment, or increases how many lines selection, spans, or decrease linelength 
--
--


--~ Lc_shortcut("cyclecase|linesplits|Ctrl+shift+u")

--function linesplits(f)
--  return filtertext(linesplit,"s") --filters (s)el or (w)ord 
--end

function linejnsmpl()
  local pan,b,d,rv,a,c = get_pane()
  local ap,ep,lnn=poslinestfn(c,pan)
  if c-ap<ep-c then
    local _,epb=poslinestfn(c,pan,-1)
    local f=ap-epb
    if f<1 then return true end
    pan:remove(epb,ap)
    pan:SetSel(a-f,c-f)
    recordselnow(pan)
  else
    local apd=poslinestfn(c,pan,1)
    if apd-ep<1 then return true end
    pan:remove(ep,apd)
  end
  return true
end

function fselevocase(f)
  return filtertext(evocase,"sw") --filters (s)el or (w)ord 
end

function evocase(c)

  local cz=0
  local d,f,n,m
  --0 all lower
  --1 cap start and periods
  --2 Capitalize first letters
  --3 capitalize all

  --if not c:match("[%a]") then  return c  end
  --if not c:match("[\192-\247][\128-\191]?[\128-\191]?") then  return c  end

  f = upperutf8(c)
  if c==f then  return lowerutf8(c)  --c was all upper
  else
    d = lowerutf8(c)
    if c==d then --c was all lower
      --beginning of passage
      d = c:gsub("^[^%l]*%l", upperutf8)
      d = d:gsub("^[^%l]*[\192-\247][\128-\191]?[\128-\191]?[\128-\191]?", upperutf8) 

      --after periods,  d is (1)
      d = d:gsub("[.]%s-%a", upperutf8)
      d = d:gsub("[.]%s-[\192-\247][\128-\191]?[\128-\191]?[\128-\191]?", upperutf8)
      
      return d --return sentence cap
    end
    
    e = d:gsub("%f[%w_%-\192-\247].[\128-\191]?[\128-\191]?[\128-\191]?", upperutf8)      --e is (2)
    if c~=e then  return e  end 
    return f --returns all upper
  end
end


--[[
function otrpanescroll(up) 
  scite.MenuCommand(IDM_SWITCHPANE)
  curpanescroll(up) 
  scite.MenuCommand(IDM_SWITCHPANE)
  return true
end

function curpanescroll(up)
  local p = panefocus()   local l = p.FirstVisibleLine
  if up then p.FirstVisibleLine=tain(l-1) 
  else p.FirstVisibleLine=tain(l+1,0,p.Length) end
  return true
end
--]]

--tada

Lc_shortcut("ctrlmoveright|ctrlmoveright|Ctrl+Right")
Lc_shortcut("ctrlmoveleft|ctrlmoveright 'left'|Ctrl+Left")

Lc_shortcut("ctrlsel right|ctrlselright|Ctrl+Shift+Right")
Lc_shortcut("ctrlsel left|ctrlselright 'left'|Ctrl+Shift+Left")

Lc_shortcut("ctrlsel dwn|ctrlseldwn|Ctrl+Shift+Down")
Lc_shortcut("ctrlsel up|ctrlseldwn 'up'|Ctrl+Shift+Up")

Lc_shortcut("ctrl    pgdn|ctrlpgup 'd--'|Ctrl+Pgdn")
Lc_shortcut("ctrl    pgup|ctrlpgup '---'|Ctrl+Pgup")
Lc_shortcut("ctrlalt pgdn|ctrlpgup 'do-'|Ctrl+Alt+Pgdn")
Lc_shortcut("ctrlalt pgup|ctrlpgup '-o-'|Ctrl+Alt+Pgup")
Lc_shortcut("ctrlsel pgdn|ctrlpgup 'd-s'|Ctrl+Shift+Pgdn")
Lc_shortcut("ctrlsel pgup|ctrlpgup '--s'|Ctrl+Shift+Pgup")
-- --o --s
-- c pgup  view to top   c shift pgup  sel to top
-- c pgdn  view to bot   c shift pgdn  sel to bot
Lc_shortcut("shove down byline|shovedown|Alt+Down")
Lc_shortcut("shove up   byline|shovedown 'up'|Alt+Up")

Lc_shortcut("shove rght bychar|shoveover 'r'|Alt+Right")
Lc_shortcut("shove left bychar|shoveover 'l'|Alt+Left")

--shove ~ Lc_shortcut("rght byword|shoveover 'rw'|Ctrl+Alt+Right")
--~ Lc_shortcut("shove left byword|shoveover 'lw'|Ctrl+Alt+Left")
Lc_shortcut("shove rght byword|wordshove|Alt+Ctrl+Right")
Lc_shortcut("shove left byword|wordshove 'lft'|Alt+Ctrl+Left")

--~ Lc_shortcut("shove linetab|shovelinetab|Alt+Pgdn")
--~ Lc_shortcut("shove linetab|shovelinetab 'lft'|Alt+Pgup")

--~ Lc_shortcut("shove linetab|catchtab|Tab")         --does shovelinetab if selection
--~ Lc_shortcut("shove linetab|catchtab 'lft'|Shift+Tab") --else returns false to scite

--  delete and cut word or selection
--  delete and cut line or lines
--  select or copy word or selection
--  select or copy line or lines

-- word del cut, should cut word touched
-- if in space, only cut behind caret
-- dont cut outside line, even if empty
-- 
-- word sel should not select outside line unless empty

function delcutword(ct)
  local pan,b,d,rv,a,c,_ = get_pane()
  local lft,wb,wd,wtyp
  if b==d then 
    wb,wd,wtyp=wordsomeway(pan,c,"lft","either")
    if b-wb<wd-b then wb,wd=wd,wb end
    local lb,ld= poslinestfn(b,pan)
    wb=math.max(lb,wb) wd=math.max(lb,wd)
    if wtyp~=0 then wd=c end
    pan:SetSel( wd,wb )
  end
  if ct then pan:Cut() else pan:Clear() end
  return true
end

function delcutlineorsel(ct)
  local pan,b,d,rv,a,c,bl = get_pane()
  if b==d then 
    a,c=pan:PositionFromLine(bl),pan:PositionFromLine(bl+1)
    if a==c then  a=pan:PositionFromLine(tain(bl-1))  end  --at eof 
    if a==c then  c=pan.Length  end                         --1 line file
    pan:SetSel( a,c )
  end
  if ct then pan:Cut() else pan:Clear() end
  return true
end

-- line delete and cut should clear the same text
-- so should just do the whole line or lines 
-- put cursor at indent of the raised lines

function delcutlines(cut)
  local pan,b,d,rvs,oa,oe = get_pane() --these are misnamed wrong but wrking
  
  --discernments
  -- b line, first pos, indented pos , end pos
  local dlx=poslinestfn(d,pan) 
  if b~=d and d==dlx then d=poslinestfn(d,pan,-1) end
  
  local ala,ale = poslinestfn(b,pan)
  local bdent = (pan:textrange(ala,ale)):match("^([%s]*)")

  local bla,ble = poslinestfn(b,pan)
  local dla=poslinestfn(d,pan,1) 

  pan:SetSel( bla,dla )

  if cut then pan:Cut() else pan:Clear() end
  
  ala,ale = poslinestfn(nil,pan)
  ala=math.min(ala+#bdent,ale)
  pan:SetSel( ala,ala )

  return true
end
-- Lc_shortcut("del word or sel|delcutword|Ctrl+'")
Lc_shortcut("del line or sel|delcutlineorsel|Ctrl+'")
-- Lc_shortcut("cut word or sel|delcutword 'c'|ctrl+shift+?")

-- Lc_shortcut("del line or lns|delcutlines|ctrl+'")
-- Lc_shortcut("cut line or lns|delcutlines 'c'|ctrl+shift+@ us:\" uk:@")

Lc_shortcut("sel word or next word|wordsel|ctrl+.")
Lc_shortcut("cpy word or sel|wordsel 'c'|Ctrl+/")

Lc_shortcut("sel line or lns|growlines|Ctrl+;")
Lc_shortcut("sel block|growblock|ctrl+shift+:")

