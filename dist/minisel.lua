-- Abandoned mess...

--~ Lc_shortcut("minsel|minsel|ctrl+,")

--minsel selects at least two characters
--if caret contacts a word it is selected
--if caret is in whitespace it is selected
--if caret is within quotes or brackets the content is selected 
--if a selection already exists, it grows on either side, up to the end of line
--growing by word or space or punctuation or quote or bracket block
-- "    \"
--  "  (   \"    " ][    ] " s   `s"
--

-- bracket nest search from a position
-- establishes the bounding brackets of the position
-- scan left should count away faced brackets up and to faced brackets down
-- till value of 1 toward face is reached for any bracket type 
-- then scan right should count same of that bracket type.
-- match is found on value of 1


function growsnest(pane,c)
  local pan,cp,cq,rv=get_pane(pane)
  if rv then cp,cq=cq,cp end
  c=c or cp

  local ap,ep=poslinestfn(c,pan)
  local t,a,e=xpndtopend(pane,c)
  if c+1==tain(c+1,ap,ep-1) then
    local tk,ak,ek=xpndtopend(pane,c+1)
    if ak and ak==c+1 then t,a,e=tk,ak,ek end
  end
  if c-1==tain(c-1,ap+1,ep) then
    local tk,ak,ek=xpndtopend(pane,c-1)
    if ek and ek==c-1 then t,a,e=tk,ak,ek end
  end
  if t then print(pan:textrange()) end
  return t,a,e
end

function findfore(tx,pt,ps)
  local i, tz,z = ps or 1
  while i<=#tx do
    local s=tx:sub(i,i)
    if s=="\\" then  i=i+1
    elseif s==pt then
      tz=tx:sub(i,i) z=i break
    end
    i=i+1
  end
  return tz,z
end
 
--[[

remapped entropy

 minisel,
   wordterms: values, number, hex, string, bracket
   search for word term at cursor,
     if cursor within term select term, cursor on opposite end
     if cursor on edge select term and 
   
 ctl shft arrow, reverses selection if present
 
 if in a word, just selects word
 if edge of word, selets word and separators
 if has just word, 
   add separators on far side
 if has word and sep
   add word and sep on far side
   
   
 later tweak ...
 also ctrl rightlft will include comma or space on far side of jumped word
 so that it can be cursd back in case unwanted
 and this must hold for shirt ctrl rightlft also
 look for scite prop tweak behavior of this
 
 
 wordhopping
   when at start of word, hops through word and separators until start of next word
   when at end of word hops through
--]]

local function addseps(a,e,pan,j,k,n)
  local sx="[^ \t] ?[,.+-=/:%@? >][=<:]?[ ]?"
  if j==-1 then 
    if findonpos(sx,pan,a) then
--~       print("a")
      local _,a2=findonpos(sx,pan,a)
--~       print("a2",a2)
      if a2 and a2<a then  n=n-1 a=a2  else j=0 end
    end
  elseif j==1 then
    if findonpos(sx,pan,e) then
--~       print("b")
      local _,_,e2=findonpos(sx,pan,e)
      if e2 and e2>e then  n=n-1 e=e2  else j=0 end
    end
  end
  if n>0 and k==-1 then 
    if findonpos(sx,pan,a) then
--~       print("c")
      local _,a2=findonpos(sx,pan,a)
      if a2 and a2<a then a=a2 else k=0 end
    end
  elseif n>0 and k==1 then
    if findonpos(sx,pan,e) then
--~       print("d")
      local _,_,e2=findonpos(sx,pan,e)
      if e2 and e2>e then e=e2 else k=0 end
    end
  end
  if not j==0 and k==0 then print("sepadded",_) end
  return a,e,j,k
end


-- the rules - obey !
-- when no selection, sel word and any sep on farside, put cp on farside
-- when selection, dont move anchor unless no more cp side words and seps

-- minsel is intended to *compliment* the capability of crtl+cursor movement

-- ctrl+cursor jumps to the start of next word or term,
-- it always jumps across word and separations:
--  if leaping from start of word, jumps to start of next
--  if leaping from end of word, jumps to end of next
--  if leaping from inside word, jumps to end of same word
--  if leaping from separation-space jumps to far end of first word 
--  but if leaping back into selection, jumps only word or sepspace
-- when ctrl+cursor with selection, maintain anchor even if no shift
-- default ctrl+cursor behaviour needs lusci-patched to this though

-- minsel selects only the term 
-- minsel and ctrl-curs to use termspan
-- termspan determines the extents of a term surrounding, preceeding or following a position
-- termspan(pane,cp,ap,rght)
--  possible terms
--   words, literals, tags, quotes, sepspace
--  params
--   pane:pane, cp: curpos, 
--   ap : anchor pos for selection expansion behaviour
--    expand a quote or bracket to twin
--[[
function termspan(pane,c,d,rght)

  local w,a,e,j,k
  local ap,ep=poslinestfn(c,pan)
  local hx=pan:textrange(ap,ep) 

  local iwords={
    "[-+]?[%d]+[.]?[%d]*e?[+-]?[%d]?[%d]?"
   ,"[%w_]+"
  }
  
  for _,pat in pairs(iwords) do
    w,a,e=findonpos(pat,pan,c)
    if w then break end
  end

]]

function minsel()
  
  local pan,x,y,rv=get_pane()
  local c,d=y,x 
  if rv then c,d=d,c end
  
--~   if c~=d then cd end
  
  local w,a,e,j,k
  local ap,ep=poslinestfn(c,pan)
  local hx=pan:textrange(ap,ep) 
  local oksep=true --gortape to hide addsel maybe doesnt position right ...
   -- "            ","             "
  local iwords={
    "[-+]?[%d]+[.]?[%d]*e?[+-]?[%d]?[%d]?"
   ,"[%w_]+"
   ,"\"[^,\"]+\""
   ,"'[^,']+'"
   ,"[(][^(]+[)]?'"
   ,"[(]?[^)]+[)]'"
   ,"[[][^%]]+[%]]?'"
   ,"[[]?[^%]]+[%]]'"
  }
  
  for _,pat in pairs(iwords) do
    w,a,e=findonpos(pat,pan,c)
    if w then break end
  end
  
  print("\nintitally found:>"..(w or "~nil~").."<:")
  
  if not a then 
    local cp=c-ap
    hx=string.gsub(hx,"\\[%p]","oo")
    a,e = quotecrack(hx,cp) 
    --1-
    if a then
      if e-a>2 and cp+2>a and cp<e then a=a+1 e=e-1 oksep=false end
      a,e = a+ap-1-1,e+ap-1+1 
 
    else
      a,e = quotecrack(hx,tain(c-1)-ap)
      if e==cp then
        a,e = a+ap-1-1,e+ap-1+1
      else
        a,e = quotecrack(hx,tain(c+1)-ap)
        if a==cp+2 then
          a,e = a+ap-1-1,e+ap-1+1
        end 
      end
    end 
  end
  
  if oksep and (a and c==a or c==e) then
    print("addsep",c,a,e)
    if c==a then -- add seps other side first
      a,e,j,k=addseps(a,e,pan,1,-1,1)
    else
      a,e,j,k=addseps(a,e,pan,-1,1,1)
    end
  end
  print("faaaa",a,e)
  if not a or e-a<2 then 
    w,a,e=findonpos("[%t ][%t ]+",pan) --just grab whitespace

    if not (a and e-a>1) then 
      a,e = brackcrack(hx,c-ap) 
      if a then a,e = a+ap-1-1,e+ap-1+1 end
    end
    rv = a and (c-a < e-c)
  end
  
  if c-a > e-c or rv then 
    pan:SetSel(math.max(d,e),math.min(a,d))
  else 
    pan:SetSel(math.min(a,d),math.max(d,e))
  end
  
  return true
end



--[[
]]
          
          
-- (ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""}}}]]])))>>>>>>>>"`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""}}}]]])))>>>>>>>>"`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""}}}]]])))>>>>>>>>"`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""}}}]]])))>>>>>>>>"`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""}}}]]])))>>>>>>>>"`   <<<<<<<<<((([[[{{{{{[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`(ab"[["[c`}defg)}<hi["""[[j}">klmn'op)}`
        --]]

--///////unfinished...

local quots={["\""]="\"",["'"]="'",["`"]="`"}
local braks={["["]=1,["("]=2,["{"]=3,["<"]=4, ["]"]=1,[")"]=2,["}"]=3,[">"]=4}
local brakp={["["]=1,["("]=1,["{"]=1,["<"]=1, ["]"]=-1,[")"]=-1,["}"]=-1,[">"]=-1}
local sub=string.sub
-- 
  
function brackcrack(hx,mi) --finds nearest enclosing braks apparent in string
 
  if not string.find(hx,"[%[%({<]") then return end
  
  local a,b,c,d,e = 1,mi,mi,mi,#hx
  local bnr,bnl,bnv = {0,0,0,0} , {0,0,0,0} , {true,true,true,true}
  
  while b>0 and d<=e do    -- mi,mi+1,mi-1,mi+2,mi-2, ... 
    local q=sub(hx,c,c)    -- l , r  , l  , r  , l  , r...
    if braks[q] and bnv[braks[q]] then  --k is the latest brak of interest
      local k = braks[q] 
                                      
      if c>mi then 
        bnr[k]=bnr[k]-brakp[q]
      else 
        bnl[k]=bnl[k]+brakp[q]
      end
      
      if bnr[k]==1 or bnl[k]==1 then  -- a brak is reached to lev 1 for a side
        local u,v,pol,step,nst        -- it needs searched in other, once and never again
        if bnr[k]==1 then       -- search left side
          u=b-1 v=1 nst=bnl[k]
        else                    -- search right side
          u=d+1 v=-1 nst=bnr[k]
        end
        -- (      )
        while u==tain(u,a,e) do
          local hu=sub(hx,u,u)
          if braks[hu]==k then nst=nst+v*brakp[hu] end
          if nst==1 then
            if v==-1 then return b+1,u else return u+1,d end
          end
          u=u-v
        end 

        bnv[k] = false --do that brak no more
      end
    end
    if d-mi > mi-b then b=b-1 c=b else d=d+1 c=d end
  end

end

function quotecrack(hx,c) --finds nearest enclosing quots apperent in string
  c=c 
  if not string.find(hx,"[\"'`]") then return end
  local quotp={["\""]=0,["'"]=0,["`"]=0}
  for i=1,#hx do
    local q=sub(hx,i,i)
    if quots[q] then
      if i>=c then if quotp[q]>0 then return quotp[q]-1,i end
      elseif quotp[q]==0 then quotp[q]=i
      else quotp[q]=0 end
    end
  end
end
