--saves to exkeycodes.lua file which is opened by extman
--puts keycode tables into global table named [platform].keycodes
--option to run from commandline and update table on the fly

Lc_['ckey_oxo'] = -1
Lc_['dkeys_oxo'] = {}
Lc_['fnstash'] = OnKey
Lc_['fnstash2'] = OnChar

local kbtype
local keys = { 
 "a" , "A"  , "0" , 
 "\\" , "|" , "`" , "¬" , 
 "," , "." , "<" , ">" , "/" , "?" , ";" , ":" , 
 "!", '"' , "£" , "$" , "%" , "^" , "&", "*" , "(" , ")" , 
 "[" , "]" , "{" , "}" , "'" , "@" , "#" , "~" , 
 "-" , "=" , "_" , "+", "backspace", 
 "delete", "insert", 
 "home", "end", "pgup", "pgdn", "left", "right", "up", "down", 
 "tab", "space", "enter", "escape" }

local keys2 = { 
 "a" , "A"  , "0" , 
 "`" , "~" , 
 "," , "." , "<" , ">" , "/" , "?" , ";" , ":" , 
 "!", '@' , "#" , "$" , "%" , "^" , "&", "*" , "(" , ")" , 
 "[" , "]" , "{" , "}" , "\\" , "|" , "'" , '"'  , 
 "-" , "=" , "_" , "+", "backspace", 
 "delete", "insert", 
 "home", "end", "pgup", "pgdn", "left", "right", "up", "down", 
 "tab", "space", "enter", "escape" }

--~ local keys = { "pgup","pgdn","shift pgup","shift pgdn" }
--~ if true return end

function surveykeycodes()
  prnte("--- (Press alt anytime to cancel)")
  prnte("--- To begin recording keycodes, please press")
  prnte("--- -shift- in combination with, the key below the escape key ...")
  grabkeys()
  --prnte("-Keycodes have been reconfigured, restart if not applied")
  return true
end

local prevk,prevkk
function grabkeys(k,s,alt,char)
  
--~ 
  _G['OnKey'] = OnKeyxx
  _G['OnChar'] = OnCharxx
  
  if not s then s ="-1" else s ="1" end
  if not k then k ="-1" return true end
  if k == 65505 then return true end
  if k == 65507 then return true end
  if k == 65513 then return true end
  if k == 16 then return true end --win shift only 
  
--~   trace("\n"..k.." "..s)
  
  if ckey_oxo == 0 then
    if char=="~" then keys=keys2     prnte("--- US layout assumed.") kbtype="us"
    elseif char=="¬" then keys=keys  prnte("--- UK layout assumed.") kbtype="uk"
    else 
      prnte("--- Cancelled: wrong key pressed or lusci needs updated for your layout")
      _G['OnKey'] = Lc_['fnstash']
      _G['OnChar'] = Lc_['fnstash2']
      return true
    end
    trace("\nNow press the following keys to detect keycodes ...") 
  end
  
  if alt or ckey_oxo ~= 0 then
    if alt or prevk==k and pprevk==k then
      _G['OnKey'] = Lc_['fnstash']
      _G['OnChar'] = Lc_['fnstash2']
      if alt then
        print("-Quiting as -alt- key was pressed")
      else
        print("-Quiting as a key was pressed thrice")
      end
      return true
    end
    dkeys_oxo[ keys[ckey_oxo] ] = k
    pprevk,prevk=prevk,k
    trace(""..keys[ckey_oxo].." as "..dkeys_oxo[ keys[ckey_oxo] ].." == "..k)
  end
  
  ckey_oxo = ckey_oxo + 1
  lim=false
  if ckey_oxo < #keys+1 then
    trace("\n"..(keys[ckey_oxo] or "err").." ")
  else
    local a="bcdefghijklmnopqrstuvwxyz"
    local A="BCDEFGHIJKLMNOPQRSTUVWXYZ"
    local n="123456789"
  
    for i=1,#a do
      dkeys_oxo[a:sub(i,i)]=dkeys_oxo["a"]+i
      dkeys_oxo[A:sub(i,i)]=dkeys_oxo["A"]+i
    end

    for i=1,#n do
      dkeys_oxo[n:sub(i,i)]=dkeys_oxo["0"]+i
    end
    
    _G['OnKey'] = Lc_['fnstash']
        
    return savekeycodes()
  end
 
  return true
end

--~ local e=0
--~ for k,v in pairs(Lc_) do

--~   print(k,v)
--~   e=e+1
--~   if e > 200 then break end
--~ end

function savekeycodes()

  local inv={}
  local strs = {}
  local strs2 = {}
  
  for k,v in pairs(dkeys_oxo) do 
    
    inv[tostring(v)]=k
    
    if k=="\\" then
      k = '"\\\\"'
    elseif k=='"' then
      k ="'"..'"'.."'"
    else
    
      k ='"'..k..'"'
    end
    table.insert(strs, "["..k.."]='"..v.."'")
    table.insert(strs2, "['"..v.."']="..k)

  end
    
  local op={}
  table.insert(op,"-- Platform keycodes autogen by surveykeycodes.lua")
  table.insert(op,"")
  table.insert(op,"Lc_['kbtype']="..kbtype)
  table.insert(op,"Lc_['kvkycd']={"..table.concat(strs,", ").."}")
  table.insert(op,"Lc_['vkkycd']={"..table.concat(strs2,", ").."}")
  table.insert(op,"")
  
  print()
  print(table.concat(op,"\n"))
  print()
    
  --lufwrite("exkeycode.lua", table.concat(op,"\n"))
  prnte("-Keycodes have been reconfigured, restart if not applied")
  --scite_dofile('exkeycode.lua',"force")
  return true 
end

local kwas,swas,awas
function OnKeyxx(key,shift,ctrl,alt)
  kwas,swas,awas=key,shift,alt
end 

function OnCharxx(char)
  return grabkeys(kwas,swas,awas,char)
end 

--savekeycodes()
