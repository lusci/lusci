
function get_word(p)
  local wb,wd,typ=wordsomeway(p,p.CurrentPos,false,"either")
  if wb>wd then wb,wd=wd,wb end
  return p:textrange(wb,wd)
end

function panelines(p)
  local m,p = {} , p or (editor.Focus and editor or output)
  for c=1,p.LineCount do m[c]=p:GetLine(c-1) end
  return m
end

function filelines(p)
  local fio,ret = p and io.open(p,"r"),{}
  if fio then
    ls = fio:lines()
    for l in ls do table.insert(ret,l) end
    fio:close()
  end
  if #ret==1 then  ret = unconcat(ret[1],"\r")  end  --i saw csv file with only \r's

  return ret
end

function gsubfilter(a,b,ops) --apply gsub to selection from commandline
  return filtertext( function(c) return string.gsub(c,a,b) end ,ops)
end
--lp,filtertext( function(c) return string.gsub(c,nlchar().."[\r\n]+",nlchar()) end ,"esa")

function deletewhiteright(ops)
  return gsubfilter(nlchar().."[\r\n]+",nlchar() ,ops)
end

function addlinestop(ops)
  return gsubfilter("(%f[\r\n.][\r\n]+)",".%1" ,ops)
end

function filtertext(fn,opts,aa,cc) --filters opts="sw" or (s)el or (w)ord or (l)ine or (a)ll 
  
  opts=opts or "esa"
  local os,ow,ol,oa=opts:match("s"),opts:match("w"),opts:match("l"),opts:match("a")
  local oe,oo=opts:match("e"),opts:match("o") --(o)utput (e)ditor
  
  local pan,b,d,rv,a,c ,x,y = get_pane(oe and editor or oo and output)
  
  if os and pan.Selections>1 then
    filtermultisel(fn,pan)
    return true
  end
--
  if aa then x,y=aa,cc
  elseif os and b~=d then x,y=b,d
  elseif ol then x,y=poslinestfn(b,pan)
  elseif ow then x,y=wordsomeway(pan,b,lft)
--elseif oa then x,y=0,pan.Length
  else x,y=0,pan.Length  --apply to whole text and undo less janky than opt error 
  end

  local fta=pan:textrange(x,y)
  local ftd=fn(fta)
  if fta~=ftd then 
    pan:SetSel(x,y)
    pan:ReplaceSel(ftd)
    d=d-#fta+#ftd
    if rv then pan:SetSel(d,b)
    else pan:SetSel(b,d) end
  end
  return true
end  --work out how to filter multiple sels and virtual sels...
 
--[[
editor:SetEmptySelection( position pos) -- Set caret to a position, while removing any existing selection.
editor.MultipleSelection -- Set whether multiple selections can be made
editor.Selections read-only
editor.SelectionEmpty read-only
editor:ClearSelections( ) -- Clear selections to a single empty stream selection
editor:SetSelection( int caret, int anchor) -- Set a simple selection
editor:AddSelection( int caret, int anchor) -- Add a selection
editor.MainSelection -- Set the main selection
position editor.SelectionNCaret[int selection] -- Which selection is the main selection
position editor.SelectionNAnchor[int selection] -- Which selection is the main selection
]]

--rewrote due to crashes on selectionN activities(?)

function getmulsel(pan)
  local s,sels = pan.Selections,{}
  sels.rvs=false
  
  for i=0,s-1 do
    local a,c = pan.SelectionNAnchor[i],pan.SelectionNCaret[i]
    if(c<a) then a,c=c,a sels.rvs=true end
    table.insert(sels,{a,c})
--  prntb(a,c)
  end

  sels.rc = pan.RectangularSelectionCaret
  sels.rcv = pan.RectangularSelectionCaretVirtualSpace
  sels.ra = pan.RectangularSelectionAnchor
  sels.rav = pan.RectangularSelectionAnchorVirtualSpace
  sels.m = pan.MainSelection
  sels.p = pan  sels.n = s
  sels.x = false
  
--prntb(sels.ra,selsrav,sels.rc,sels.rcv)
  return sels
end

--version of function mungs rectangular and multi selection
--(the setting of rectangular attributes clears the numeral selections)
function setmulsel(sels) 
  local pan=sels.p
  for i=1,sels.n do
    if i==1 then
      if sels.rvs then sels[i][1],sels[i][2]=sels[i][2],sels[i][1] end
      pan:SetSelection(sels[i][2],sels[i][1]) -- selsel(caret,anchor)
--    prntb(sels[i][2],sels[i][1])
    else
      if sels.rvs then sels[i][1],sels[i][2]=sels[i][2],sels[i][1] end
      pan:AddSelection(sels[i][2],sels[i][1])
--    prntb(sels[i][2],sels[i][1])
    end
  end

  pan.MainSelection=sels.m 
  if sels.ra then
    pan.RectangularSelectionAnchor=sels.ra
    pan.RectangularSelectionAnchorVirtualSpace=sels.rav
    pan.RectangularSelectionCaret=sels.rc
    pan.RectangularSelectionCaretVirtualSpace=sels.rcv
  end
end

function filtermultisel(fn,pan) 

  local sels=getmulsel(pan)
  local s,m,x, a,c,ins = sels.n, sels.m, 0

  pan:ClearSelections()
  pan:BeginUndoAction()
    
  for i=1,sels.n do
    
    a,c = sels[i][1], sels[i][2]
    a,c = a+x, c+x 
   
    ins = fn( pan:textrange(a,c) )
    pan:remove(a,c) pan:InsertText(a,ins)
    
    sels[i][1],sels[i][2] = a, a+#ins
    
    if c-a~=#ins then --inserted difrnt length so
      sels.ra=false   --invalidate rectangle sel mode
      x=x -c+a +#ins  --update cumulative shift
    end
  end
  
  pan:EndUndoAction()

  setmulsel(sels)
end

function seatcaret(p) --so caret wont return to other column when moved vertically
  sendscite(SCI_CHOOSECARETX,p)
end

function get_pane(p) --rt: pan,lfps,rtps,rv,anc,cur,bln,dlnadj,dltrue,plen
  if not p then  p=editor if output.Focus then  p=output end end
  local b,d,r=p.Anchor,p.CurrentPos,false
  if p.CurrentPos<p.Anchor then b,d,r=d,b,true end
  local bl,dl=p:LineFromPosition(b),p:LineFromPosition(d)
  local dbac = bl~=dl and d==p:PositionFromLine(dl) and 1 or 0
  return p,b,d,r,p.Anchor, p.CurrentPos, bl, dl-dbac, dl, p.Length
end

function read_clipboard()
  local p,nl,r = editor, "\n", nil
  local a,c,l,v = p.Anchor,p.CurrentPos, p.Length, p.FirstVisibleLine
  
  scite.SendEditor(SCI_SETUNDOCOLLECTION,false)
  p:AppendText(nl)  --add newline in case rectangular paste might add indents
  p:SetSel(l+#nl,l+#nl)
  p:Paste() 
  r = p:textrange(l+#nl,p.Length)
--p:DeleteRange(l,p.Length)  --DeleteRange doesnt work on output
  p:SetSel(l,p.Length)  p:Clear() 
  p:SetSel(a,c)
  p.FirstVisibleLine=v
  scite.SendEditor(SCI_SETUNDOCOLLECTION,true)
  return r
end

--[[position editor.TargetStart -- Sets the position that starts the target which is used for updating the document without affecting the scroll position.
position editor.TargetEnd -- Sets the position that ends the target which is used for updating the document without affecting the scroll position.
editor:TargetFromSelection( ) -- Make the target range start and end be the same as the selection range start and end.
editor.SearchFlags -- Set the search flags used by SearchInTarget.
editor:SearchInTarget( string text) -- Search for a counted string in the target and set the target to the found range. Text is counted so it can contain NULs. Returns length of range or -1 for failure in which case target is not moved.
editor:ReplaceTarget( string text) -- Replace the target text with the argument text. Text is counted so it can contain NULs. Returns the length of the replacement text.
]]

function placetxt(pan,txt,b,d)
  d=d or b txt=txt or ""
  pan:SetSel(b,d) pan:ReplaceSel(txt)
end

function inserttxt(b,d,txt,pan,a,c) --replace doc text range and maintain selection/s
  txt,pan=txt or "" , pan or editor --use 'Target' instead of selection 
  a,c=a or pan.Anchor,c or pan.CurrentPos
  local rv=false
  local nosel = a==c
  if a>c then rv=true a,c=c,a end
  b,d=math.min(b,d),math.max(b,d)
  
  pan.TargetStart=b
  pan.TargetEnd=d
  local txlen=pan:ReplaceTarget(txt)
  --pan:SetSel(b,d)
  --pan:ReplaceSel(txt)
  local n=txlen-(d-b) --negative when txt reduced doclen
  --but when selection ends are within modified textrange
  --we dont know whether to move them or not
  if a>d then 
--  if a<d then a=tain(a+n,b,txlen+b) else 
    a=tain(a+n,b) 
--  end
  end
  if c>=b then 
--  if c<d then c=tain(c+n,b,txlen+b) else 
    c=tain(c+n,b) 
--  end
  end
  if nosel then a=c end
  pan:SetSel(a,c)
  return txlen
end

function removetxt(pan,b,d)
  if(b>d) then b,d=d,b end
  pan:DeleteRange(b,d-b)
end 

function taketxt(pan,b,d) 
  local pan,a,c,rv=get_pane(pan)
  b,d=math.min(b,d),math.max(b,d)
  
  local r=pan:textrange(b,d)
  editor.TargetStart=b
  editor.TargetEnd=d
  editor:ReplaceTarget("")
  local n=b-d
  if a>b then a=a+n end
  if c>b then c=c+n end
  if rv then a,c=c,a end
  pan:SetSel(a,c)
  return r
end

function clearcurline(pane,whole,line_offset)
  pane=pane or editor
  local cp=pane.CurrentPos
  local ln= pane:LineFromPosition(cp)
  ln=tain(ln+(line_offset or 0), 0,pane.LineCount ) 
  local ep=pane.LineEndPosition[ln]
  --ep = not whole and ep or tain(pane:PositionFromLine(ln+1),ep,pane.Length)
  pane:SetSel(pane:PositionFromLine(ln),ep)
  pane:DeleteBack()
end

function poslinestfn(cp,pane,offset,ln) --rtrns ps: lineSt, linefn, line
  offset= offset or 0
  pane=pane or editor 
  cp=cp or pane.CurrentPos
  ln=ln or pane:LineFromPosition(tain(cp,0,pane.Length))
  ln=ln+offset
  if ln<0 then
    return 0,0,0
  elseif ln>pane.LineCount-1 then
    return pane.Length,pane.Length,pane.LineCount-1
  else
    return  pane:PositionFromLine(ln), pane.LineEndPosition[ln], ln
  end
end 

function poslinetxt(pane,cp) --rtns: linetxt no cr, ancpos, endpos
  cp = cp or pane and pane.CurrentPos
  local a,e = poslinestfn(cp,pane)
  return pane:textrange(a,e),a,e
end

function linetxt(p,l) --rts: line txt with line ending
  local e
  if l==p.LineCount then e=p.Length else e=p:PositionFromLine(l+1) end
  return p:textrange(p:PositionFromLine(l),e)
end

function lineancpos(p,c,l)
  if not p then p=editor.Focus and editor or output end
  if not l then l=p:LineFromPosition(c or p.CurrentPos) end
  return p:PositionFromLine(l)
end

function indentinposline(p,c,l)
  if not p then p=editor.Focus and editor or output end
  if not l then l=p:LineFromPosition(c or p.CurrentPos) end
  return tain(0,math.floor(p.LineIndentation[l] / p.TabWidth),50)
end

function nearbyindent(pane,cp)
  local la,le = poslinestfn(cp,pane)
  local crr = pane:textrange(la,le)
  
  local _,_,a = rgxfindup(pane,"\\S",1,la)
  local ltx = pane:textrange(poslinestfn(a or la,pane)) 
  local indy = ltx:match("^([%s]*)")
  
  _,_,a = rgxfinddwn(pane,"\\S",1,la)
  ltx = pane:textrange(poslinestfn(a or la,pane))
  local indy2 = ltx:match("^([%s]*)")

  if #indy2>#indy then indy = indy2 end 
  return indy
end 

function dolua(f)
 prnte("-Dolua: "..f.."")
 dofile(f)
 tracePrompt()
end
    
function gocmd(c,path) --little tested bodge to type commands like make in the tty
  -- possible to set cwd before running go ?
  local filextnsn = matchout(props['FileNameExt'],"^[^.]*[.]")
  local gx='command.go.*.'..filextnsn
  local ap=props[gx] --the default configured go command
  props[gx]="cd '"..path.."' && "..c  --swap in the typed command
  scite.MenuCommand(IDM_GO)  --run go
  props[gx]=ap       --swap back the default command
end


local hammertime,hammered = 0,0
function slowhammer(t)
  local ct=os.time()
  if hammered>(t or 4) or ct-hammertime> 1 then 
    hammered=0 hammertime=ct
    return false 
  end
  hammered=hammered+1
  return true
end

function nlchar(p)
  --track buffers apparent line endings and auto
  --warn on discrepancies
  --get scite to do a couple of quick plain searches for \r and \n
  --from positions, length_o_file/5
  --if not all same
  --make line list of ending discrepancies,
  p = p or (editor.Focus and editor or output) 
  if p.EOLMode==0 then return "\r\n" end
  if p.EOLMode==1 then return "\r" end
  return "\n" 
end

function caretisvis(pane)
  local c=pane:LineFromPosition(pane.CurrentPos)
  return c==tain(crr,doclinesvis(pain))
end

function doclinesvis(p) --display lines are +1 indexed, this returns +0x 
  p=p or editor
  local a=p.FirstVisibleLine
  local b=p.LinesOnScreen
  local c=p:DocLineFromVisible(a)
  local d=p:DocLineFromVisible(a+b)
  return c,d
end

function vislinefrompos(p,c) --display lines are +1 indexed, this returns +0x 
  p = p or editor  c = c or p.CurrentPos
  return p:VisibleFromDocLine(p:LineFromPosition(c)) 
end

function doclinefrompos(p,c)
  p = p or editor  c = c or p.CurrentPos
  return p:LineFromPosition(c)
end
 
function isposvisible(p,c)
  p = p or editor  c = c or p.CurrentPos
  local b,d = doclinesvis(p)
  local l = doclinefrompos(p,c)
  return l >=b and l <=d
end


function enviscp(pan,l) -- fudged function, alse recrds sel
  pan=pan or editor
  local foc = (editor.Focus and editor or output)
    
  local a,c=pan.Anchor,pan.CurrentPos
  if l then 
    a=pan:PositionFromLine( tain(l,0,pan.LineCount-1) ) c=a 
  end 
  pan:SetSel(a,c)
  pan:ScrollCaret()
  
  --help avoid prob scrolling output ?
  if pan==output then output.FirstVisibleLine=output.FirstVisibleLine+1 end
  if a~=c then recordselnow(pan) end
--[[ 
  scitelua.n:183:editor.FirstVisibleLine -- Scroll so that a display line is at the top of the display.
  scitelua.n:255: SCROLLING AND AUTOMATIC SCROLLING
  scitelua.n:256:editor:LineScroll( int columns, int lines) -- Scroll horizontally and vertically.
  scitelua.n:257:editor:ScrollCaret( ) -- Ensure the caret is visible.
  scitelua.n:261:editor.HScrollBar -- Show or hide the horizontal scroll bar.
  scitelua.n:262:editor.VScrollBar -- Show or hide the vertical scroll bar.
  scitelua.n:263:editor.XOffset -- Get and Set the xOffset ( ie, horizontal scroll position).
  scitelua.n:264:editor.ScrollWidth -- Sets the document width assumed for scrolling.
  scitelua.n:265:editor.ScrollWidthTracking -- Sets whether the maximum width line displayed is used to set scroll width.
  scitelua.n:266:editor.EndAtLastLine -- Sets the scroll range so that maximum scroll position has the last line at the bottom of the view ( default). Setting this to false allows scrolling one page below the last line.
  ]]--
end

function findonpos(pt,p,c) --just wraps terminstring for pane access instead of string
  local pan,cp,cq,rv=get_pane(p)
  if rv then cp,cq=cq,cp end
  if c then cp=c end
  local ap,ep,lnn=poslinestfn(cp,pan)
  local tx=pan:textrange(ap,ep)
  local ps=cp-ap
--~   print("findonposline:'"..tx.."'",ps)
--~   print("pos",ps)
  tx,a,e = terminstring(tx,pt,ps)
--~   print("findonposline:'"..tx.."'")
  if a then 
--~   print("foundonpos:"..tx)
  return tx,ap+a-1,ap+e end --returns pane range (caret between positions)
end

function terminstring(tx,pt,ps) --ret leftmost match and lua bounds touching ps:0indexed caret pos
  local fp,a,e=0,0,-1
  if not tx:find(pt) then return false end
  while fp<=ps do
    a,e=tx:find(pt,fp)
    if a then 
      if ps==tain(ps,a-1,e) then 
        return tx:sub(a,e),a,e --returns string sub pos
      else
        fp=a
      end
      fp=fp+1
    else 
      return false 
    end
  end
  return false
end

function freshline() 
  if not (output.CurrentPos == lineancpos(output)) then print() end
end

function printeach(t,b,d,x,y) --table, first, last, prepend, append 
  x=x or ""  y=y or "" d=d or #t  b=b or 1
  for i=b,d do print(x..t[i]..y) end
end

function prntd(...)  print(dumpt(...)) outpdown() end

function prnte(...) --prints on freshline at bottom of pane, and restores any selection
  local a,b=output.Anchor,output.CurrentPos
  output.CurrentPos=output.Length
  freshline()
  print(...)
  output.Anchor,output.CurrentPos=a,b
end

function prntb(...) -- insert messages above the input prompt
  local a,b=output.Anchor,output.CurrentPos
  local bla,ble = poslinestfn(output.Length,output)
  if bla==ble then bla,ble = poslinestfn(output.Length,output,-1) end
  local u=dumpr(...)..nlchar()
  output:InsertText(bla,u)
  if a>bla then a=a+#u end
  if b>bla then b=b+#u end
  output:SetSel(a,b)
  outpdown()
end

-- have debug printing commands
-- debug.getinfo(1)

local debugttables={}
local debugttabmeta = {
  
  __index = function (t,k)
    local tb=debug.traceback():match("^.-[\r\n]+.-[\r\n]+\t?(.-)[\r\n]+")
    prntb("--- dbg table element : " .. tostring(k) .." accessed ".."\n"..tb)
    if debugttables[t] then return debugttables[t][k] end
  end,

  __newindex = function (t,k,v)
    local tb=debug.traceback():match("^.-[\r\n]+.-[\r\n]+\t?(.-)[\r\n]+")
    prntb("--- dbg table element : " .. tostring(k) ..
                         " updated to " .. tostring(v).."\n"..tb)
    if not debugttables[t] then debugttables[t]={} end
    debugttables[t][k]=v
  end
}

function dbgtable(y,rem)
  y=y or {}
  if rem then setmetatable(y, {}) else setmetatable(y, debugttabmeta) end
  return y
end

function tracerr(...)
  local tb=debug.traceback():match("^.-[\r\n]+.-[\r\n]+\t?(.-[\r\n]+.-)[\r\n]+")
  local t={...}
  if #t>0 then 
    table.insert(t,1,"--- tracerr :")
    prntb(table.unpack(t)) 
  end
  prntb(tb)
end

function tracemore(...)
  local tb=debug.traceback():match("^.-[\r\n]+.-[\r\n]+\t?(.*)")
  local t={...}
  if #t>0 then 
    table.insert(t,1,"--- trace :")
    prntb(table.unpack(t)) 
  end
  prntb(tb)
end

overcounts={} --how to auto reset ... ?
function overloop(limit,tag) --returns true if called over limit and prints tagged msg
  tag=tag or 0 overcounts[tag]=overcounts[tag] or 0
  overcounts[tag]=overcounts[tag]+1
  if overcounts[tag]>(limit or 20000) then
    tracemore("Loop Exceeded Limit of "..overcounts[tag],((tag==0 and "") or ("p:"..tag)))
    overcounts[tag]=0
    return true
  else return false
  end
end

function protect(t,f)
  local prottime=os.time()+(t or 6)
  local function hook() 
    if os.time()<prottime then return end
    debug.sethook()
    prntb(debug.traceback():match("^(.-[\r\n]+.-[\r\n]+\t?.-)[\r\n]+"))
    nil_to_crash_hang() 
  end
  debug.sethook(hook, "c",f or 50)
end

function unprotect()
  debug.sethook()
end


function terminway(tx,pt,cp,lftwz,oncp) --string,pattern,caretp0x,golft (returns caret bounds)

  --to return patt match that is over cp0x or immediately in its way
  --when golft cp0x==tain(cp0x,a-1,e) to be in way 
  --when gorgt cp0x==tain(a,e+1) to be in way
  --when gorght matched patt must not start at later nth than cp+1 
    
  oncp=oncp or false  --trick to fix lftwz match pairs
  
  local fp,a,e=1,0,-1    --test, test, 123 , 456, 789
  if lftwz then
    while fp<=cp do
--    if overloop() then print("excess @"..pt.."@",tx:sub(a,e)) return false end
      a,e=tx:find(pt,fp)
      if not a then return false end --should be safe to bale if no matches
--    print(pt,tx:sub(a,e),a,e,cp)
      if (oncp and e==cp) or not oncp and a and cp>=a and cp<=e then 
        return tx:sub(a,e),a-1,e 
      end 
      fp=a+1
    end
  else
    if not tx:find(pt) then return false end --pat is nowhere
    cp=cp+1
    fp=cp
    while fp>0 do         --fp begins just touching cp
      a,e=tx:find(pt,fp)  --moves backwards to find first match that contacts cp 
--    if a then print(pt,tx:sub(fp),tx:sub(a,e),tx:sub(cp,cp)) end
      if (oncp and a==cp) or not oncp and a and cp>=a and cp<=e then
        local aa,ee=a,e              --extends match that contacts cp
        while ee==e and fp>1 do
          a,e,fp=aa,ee,fp-1
          aa,ee=tx:find(pt,fp)
        end
--      print(pt,tx:sub(a,e),a,e,cp)
        return tx:sub(a,e),a-1,e 
      end
      fp=fp-1
    end
  end
--print("failed,",pt)
  return false
end

local caret,dr

--find is indexed 1 to #hay, find can search from the very last position of hay
--find searching from 0 is identical to searching from 1
--caret may be at position 0, if at position 1 it is between chars 1 and 2

--termin string goright has a problem that find looks rightways
-- ...aaapaaa... looking for aaa touching p
-- going left searches
-- aaa
--  aaa
--   aaa
--    aaa  finds and touches p
-- as soon as match reaches p it matches so if ends at p leftways word is known
-- going right searches from p and finds immediately
-- but doesnt see rightways earliest match, it only sees from search position
-- so going right has to find the earliest match
-- it has to keep stepping back until the match end moves or dissappears
-- some patterns will also not work cleanly for rightways term detection

function scite_UserListShow(list,start,fn,pane)
  local separators = {'<','>','^','#','%','!', ';', '@', '?', '~', ':','&'}
  local separator 
  local s = table.concat(list)
  for i, sep in ipairs(separators) do
    separator = sep
    if not s:find(sep, 1, true) then break end
  end

  s = table.concat(list, separator, start)
 
  _UserListSelection = fn
  local pane = editor
  if not editor.Focus then pane = output end
  pane.AutoCSeparator = string.byte(separator)
  local mystery_user_id = 13
  pane:UserListShow(mystery_user_id,s)
  pane.AutoCSeparator = string.byte(' ')
  return true
end

function iocatfile(fp,src)

  local h,rt=nlchar()
  if src==editor then
    rt="buffer" src = editor:GetText()
  elseif src==output then
    rt="output" src = h..output:GetText()
  else
    rt="clipboard" 
    if not ( src:find("^["..h.."]") or src:find("["..h.."]$") ) then 
      src = h..src
    end
  end

  if fileexists(fp) then 
    rt="append "..rt.." to "..fp
  else
    local ret=runcommand('touch "'..fp..'"')
    if ret[3]~=0 then
      tracerr("could not touch "..fp,ret)
      return
    end
    rt="saved "..rt.." to "..fp
  end
  local fio,msg = io.open(fp, "a")
  
  tracerr(fio,msg)
  if not fio then return "problem opening file, "..msg end
  
  fio:write(src)
  fio:close()

  print("\n- "..rt)
  return 
  
--if tt and pane==editor and filen~="temp" then
--  scite.Open(filenm)
--  tracePrompt("Li,"..props['FileNameExt'].." ~"
--    ..match(filen,"[^.]*"),nil,1) 
--  --scite.MenuCommand(421)
--  return true
--end
    
end

function filesize(f) 
  local fio = io.open(f,"r")
  if not fio then return false else
    local size = fio:seek("end") fio:close() return size end
end

function printwaypoint(q)
  local _,_,line=poslinestfn(editor.CurrentPos,editor) 
  print(props['FilePath']..":"..(line+1)..": ~<( "..q.." )>~")
end

function fillpath(f,p)
  p=p or props['FileDir']
  local pp=dircheck(p)
  if not p then tracerr("- Bad path seen:",p) return false end 
 
  local fp,ff = matchout(f,"[^\\/]+$")
  local fpp = dircheck(fp,pp)
  if not fpp then tracerr("- Bad path seen:",fpp) return false end 

  return slashdir(fpp)..ff
end

lastpathpos=-1 --hope these reset between function calls
lastpathdet={} --but not till end of thread ...

function findfirstup(pane,patts,cp,dp,dwn,ispath,isplain)
  cp=cp or pane.CurrentPos
  dp=dp or (dwn and pane.Length) or 0
  
  local a,e,aa,ee
  local first=0   --  SCFIND_MATCHCASE + SCFIND_WHOLEWORD + SCFIND_WORDSTART + SCFIND_REGEXP
  local findtype = isplain and SCFIND_MATCHCASE or SCFIND_REGEXP
  if type(patts) ~= 'table' then patts={patts} end
  for i=1,#patts do
    a,e=pane:findtext(patts[i], findtype ,cp,dp) 
    if ispath~=nil then a,e=cleanpathx(a,e) end
    if a~=nil then 
      if dwn then dp,aa,ee =a,a,e else --??
        dp,aa,ee =e,a,e end
    end
  end
  return aa,ee
end

function rgxfinddwn(pane,rgxpats,nth,sp) return rgxfindup(pane,rgxpats,nth,sp,true) end
function rgxfindup(pane,patts,nth,cp,dwn,ispath,isplain) 
  --
  if type(patts) =='string' then patts={patts} end
  local dp,str = 0
  if dwn==nil then 
    cp,dp=cp or pane.Length, 0 --go to line end cp?
  else
    cp,dp=cp or 0, pane.Length --back to line start cp?
  end
  local a,e,aa,ee
  local i=0
  
  while i<nth do i=i+1  --for does not allow setting i
    aa,ee=a,e
    a,e=findfirstup(pane,patts,cp,dp,dwn,ispath,isplain)
    if a then aa,ee=a,e 
      --print("aza"..pane:textrange(a,e).."aza")
      if dwn==nil then cp=tain(a) else cp=tain(e,0,pane.Length) end --check ob1
    end
  end
  if aa then return true, pane:textrange(aa,ee), aa,ee
    else return false, ""
  end
end

rgxpatha='^/[\\w.,#@-~_][\\w\\s/.,#@-~_]*'
rgxpathb='/[\\w.,#@-~_][\\w\\s/.,#@-~_]*'

function rgxpathdwn(nth,sp) return rgxfindup(output,{rgxpatha},nth,sp,true,true) end
function rgxpathup(nth,sp)
--~   if not dwn and lastpathpos~=1 and lastpathpos<sp 
--~     then print("lusci_sci.lua:213: works")
--~     return table.unpack(lastpathdet) end
  local fnd,str,a,e = rgxfindup(output,{rgxpatha},nth,sp,nil,true)
  lastpathpos,lastpathdet=e,{fnd,str,a,e}
  return fnd,str,a,e
end

local tilddir-- = lc_init('tildedir',false)
function gettilddir() --'/home/yezer'
  if not tilddir then tilddir=runcommand("echo ~","",true)[1] end
  return tilddir
end

function fullpathinstr(p,d) --these things need 
  p=cleanpath(p or "") or "" --take off comments

  local _,_,prev,pres = p:find("^([%~%.][\\/])(.*[%w%.%-])") -- a linux relpth
  if pres and prev=="~/" then prev=gettilddir().."/" end       --linux home
  --luscihome is no use for relative, work out policy for this ...
  if pres and prev=="./" then prev=(d or props['luSciHome']).."/" end  --linux rel
  if pres and prev==".\\" then prev=(d or props['luSciHome']).."\\" end  --win rel

  if not (prev and pres) then --a windows fllpth
    _,_,prev,pres = p:find("(%w:[\\/][\\/])(.*[%w%.%-])")
  end
  if not (prev and pres) then --a linux fllpth
    _,_,prev,pres = p:find("(/%w%w?%w?%w?%w?%w?%w?/)(.*[%w%.%-])")
  end
  
  if not pres then
    _,_,pres = string.find(p,"([^\\/%s] ?[^\\/%s]*)%s*$")
    if pres then prev = d or "" end
  end
  
  local ln=p:find(txttopatt(pres or "")..":(%d+) ?:")
  ln=ln and tonum(ln)
  if prev and pres then
    return joinpath(prev,pres),ln
  end
end

function fillpathinstr(pth,root,noexist) --base path has no / at end
  pth=cleanpath(pth) 
  
  if not pth:match("[\\/]") then --if path has no \\/ then add root
    pth=root..pth
  else
    local p,rel=matchout(pth,"^[.][\\/]") --add root if ./
    if rel~="" then pth=root..p else
      p,rel=matchout(pth,"^[.][.][\\/]")  --add cropped root if ../
      if rel~="" then pth=(root:match("(.*[\\/])[^\\/]$") or "")..p end
    end
  end
  
  if not noexist and not fileexists(p) then p=false end
  return p 
end

function joinpath(p,f)
 if not p or p=="" then  return f  end
 p=p:match("^.*[^\\/]") or "" 
 f=f:match("[^\\/].*$") or ""
 return p..platsep..f
end

function cleanpath(p)
  p=p:gsub("//",";")
  p=p:gsub("[|&*=]",";")
  p=p:gsub("  +",";") --mark non pathy sequences
  p=p:match("^%s*([^;]+[^ ;])")
  return p
end

function cleanpathx(a,e) --poor function used by findfirstup 
  if a==nil then return end
  local p,ln = cleanpath(output:textrange(a,e))
  p,ln=matchout(p,":(%d+ ?):.*")
  if not dircheck(p) then return nil else return a,a+#p end
end

function outcarets(v,w) -- like pane:SetSel(  but side effects ?
  w = w or v
  if not v then return output.CurrentPos,output.AnchorPos end 
  output:ClearSelections() --editor:SetEmptySelection(editor.CurrentPos)
  output.CurrentPos,output.Anchor = v,w 
end

function panefocus(f) 
  if f==nil then return editor.Focus and editor or output end
  if f==output and f.Focus then return end --try and avoid glitch loosing output scroll/cp
  scite.MenuCommand(IDM_SWITCHPANE) --one switch necessary to take focus from strip or other 
  if not f.Focus then scite.MenuCommand(IDM_SWITCHPANE) end --switch back if neccessary
end

local firstlo=true --quick fix occasional 'editor pane not accessible at this time'
                   --change startup opening to not include panefocusing...

local function nagtoomanyopen()
  local n=dictlen(Lc_.buftim,true)
  if n > 90 and n%3==0 then
    prnte("--- "..n.." buffers currently open - unsaved files may be evicted after 99")
  end --arrange disallow file opening at 99...
end   --arrange unsaved files never evicted...

function sc_open(f) --opens as buffer without checks
  --if firstlo then firstlo=false scite.Open(f) return end
  local u=panefocus() scite.Open(f) nagtoomanyopen() panefocus(u)
end

function lc_open(f,la,lb) --lb is optional linenm to focus range
  local fio = io.open(f) --opens as buf with checks
  
  if not fio then        --a silent fp switch, problematic
    f=slashdir(props['FileDir'])
    fio = io.open(f)
  end 
  if fio then
    fio:close()
    local u=panefocus() 
    scite.Open(f)
    nagtoomanyopen()
    local la,lb=tonum(la)-1,tonum(lb)-1
    if lb>0 then --helps focus on range
      editor:SetSel(editor:PositionFromLine(la),editor:PositionFromLine(lb))
      editor:GotoLine(lb)
    end
    if la>-1 then
      editor:GotoLine(la)
    end
    enviscp(editor)
    panefocus(u)
    return true
  end
  nagtoomanyopen()
end

function gogoline(l,p)
  p=p or editor  p:GotoLine(l)  enviscp(p,l)
end 

function outpdown() --pushes output all down, for unknown problem scrolling to bottom line
  output.FirstVisibleLine = output.FirstVisibleLine*2
end

--[[
IDM_BOOKMARK_NEXT,
  ["Previous Bookmark"] = IDM_BOOKMARK_PREV, ["Toggle Bookmark"] = IDM_BOOKMARK_TOGGLE,
  ["Clear All Bookmarks"] = IDM_BOOKMARK_CLEARALL,

--]]

--unfinished path detection in tty and jumping...

winpthrx={
  "^[-][\\w\\s.\\']+ as \\w:?\\\\?[\\w\\\\\\@]+\\.?\\w\\w\\w?$",
  "^  File \\\"\\w[^\\,]+\\\", line \\d+",
  " [-~]. \\[\\w:?\\\\?[\\w\\\\\\@]+\\.\\w\\w\\w? \\]",
  "^\\w:?\\\\?[\\w\\\\\\@]+\\.\\w+:\\d+",
  "^\\w:\\\\[^\\;]+; \\w:\\\\[\\w\\\\\\@]+\\.\\w\\w\\w?:\\d+" 
}	
winpthpt={
  "^([%a][%:][\\/][%w%@%-%#_.\\/%s]+[%w]):([%d]+)",        --errlist
  "^%-[.%w_ %']*as ([%a][%:][\\/][%w%@%-%#_.\\/%s]+[%w])", --copying..
  "[-~][-~] %[([%a][%:][\\/][%w%@%-%#_.\\/%s]+[%w]) %]",   --summarylist
  "^([%a][%:][\\/][%w%@%-%#_.\\/%s]+[%w])",                --plainfile 
  "%; ([%a][%:][\\/][%w%@%-%#_.\\/%s]+[%w]):(%d+)"         --borked lua
}
linpthrx={ 
  '^  File \\\"\\w[^/,]+\\\", line \\d+', --example test filejump format ...
  '^[-~]\\s[\\w\\s.,"#@-_]+\\[[/\\w\\s.,"#@-_]+\\s\\]', --summary lists
  '^[/\\w\\s.,"#@-~_]+\\s*:\\d+\\s?\\s?:', --std jumplists ./bla/bla  /bla/bla  bla  :11:
  '^/[\\w.,#@-~_][/\\w\\s.,"#@-~_]+',    --full filepath
  '^[./]*[\\w\\s/.,"#@-~_]+'         --plain filepath
}	
linpthpt={
  '^([%w%s/.,"#@-~_]*[%w/.,"#@-~_])%s*:s*([%d]+)',    --errlist
  '%[(.+) %]',                                      --summarylist
  '^([%w%s/.,"#@~_%-]+)$'                         --plainfile 
}

local jmpt,jmrx,jmrxx
if props['PLAT_GTK'] then
  jmpt,jmrx=linpthpt,linpthrx
else
  jmpt,jmrx=winpthpt,winpthrx
end

function jumpfile(dwn) --not finished
  lastpathpos=-1 nxtjumpfile(output.CurrentPos,dwn)
  return true
end

function nxtjumpfile(sp,dwn) --not finished 
--jump to next mention of a file location above or below cp
--cp of line start or end should be supplied accordingly
--we use regex search to locate next file description templates
--then (dont) use regex search on hitrange again to determine which pattern hit
--then use lua pattern search to extract file name,path and line number
--then check file is a reasonable size before opening (not done)

--then nextfile jump hotkey, can automatically close the previous jumpfile if it was not editted or scrolled?
  if dwn==nil then dwn=false else dwn=true end
  
  while sp==tain(sp,1,output.Length) do
    local fnd,str,ax,ex = rgxfindup(output,jmrx,1,sp,dwn)
    if not fnd then return false end
    
    --catch some by removing unpathy sequences
    str=str:gsub("  ",";") --no paths should have dbl spaces -for crissake
    str=str:gsub("//",";") --or that
    
    for i=1,#jmpt do
      local a,e,ca,cb=str:find(jmpt[i])
      if ca~=nil then
        --if checks out
        --if ca needs directory get directory or cur working dir
        if ca:find("^[.][\\/]") or not ca:find("[\\/]") then
          ca=ca:gsub("^.*[\\/]","")
          local q=500 --max search loops
          while q>1 do
            local ft,dr,fa,fe=rgxpathup(1,a)
            if not ft then q=1 else
              if not fileexists(dr.."/"..ca) then a,q = fa,q-1 else 
                ca=dr.."/"..ca
                q=0
            end end
          end
        end 
        
        if q==1 then ca=props['FilePath'].."/"..ca end
        if fileexists(ca) then
          if filesize(ca) > 500000 then --dont open it
            outcarets(ex,ax) --highlight
            enviscp(output) --show
          return end
          
          if not editor.Modify then --[[close]] end --is dirty
          lc_open(ca)
          outcarets(0)
          enviscp(editor) 
          enviscp(output) 
          --open ca,cb w/ good visible
        end
      end
    end
    if dwn then sp=e else sp=a end
  end
  
  --no jmps were found
  if dwn then outcarets(0) else outcarets(output.Length) end
  enviscp(output)
  return
end

function pathsabove(cp,f) --not finished
  local fnd,a,e = true,pathuppos,pathuppos
  if not pathuppos<cp then fnd,pathup,pathuppos = rgxfindup(output,rgxpatha,1,cp,dwn) end
  if fnd and fileexists(pathup.."/"..f) then return pathup end
  fnd,pathup,pathuppos = rgxfindup(output,rgxpathb,1,cp,dwn)
  if fnd and fileexists(pathup.."/"..f) then return pathup end
end

function pathabove(cp,pan) --this finds only prompted directory lines
  pan=pan or output
  local wp='^>[a-zA-Z]:[\\/][^;]+[\\/;]'
  local gp='^>[/]\\w\\w\\w?\\w?\\w?\\w?\\w?[/][^;]+[\\/;]'
  local fnd,pth = rgxfindup(pan,(GTK and gp) or wp, 1,cp,dwn)
  if fnd then 
    pth=matchout(pth,"^>") pth=matchout(pth,"[\\/;]$")
    pth=pth..platsep
    return pth
  end
end

function pathabove2(pan,cp)
  pan=pan or output cp=cp or pan.CurrentPos
  local wp='^>?[a-zA-Z]:[\\/][^;]+[\\w\\/;]\\s*$'
  local gp='^>?~?[/]\\w\\w\\w?\\w?\\w?\\w?\\w?[/][^;]+[\\w/;]\\s*$'
  local fnd,pth = rgxfindup(pan,(GTK and gp) or wp, 1,cp,dwn)
  if fnd then 
    pth=matchout(pth,"^>") pth=matchout(pth,"[\\/;]\\s*$")
    return pth
  end
end

--from lua-users org wiki

-- Find the length of a file
--   filename: file name
-- returns
--   len: length of file
--   asserts on error
function length_of_file(filename)
  local fh = assert(io.open(filename, "rb"))
  local len = assert(fh:seek("end"))
  fh:close()
  return len
end

-- Return true if file exists and is readable.
function file_exists(path)
  local file = io.open(path, "rb")
  if file then file:close() end
  return file ~= nil
end

-- Read an entire file.
-- Use "a" in Lua 5.3; "*a" in Lua 5.1 and 5.2
function lufreadall(filename)
  local fh = assert(io.open(filename, "rb"))
  local contents = assert(fh:read(_VERSION <= "Lua 5.2" and "*a" or "a"))
  fh:close()
  return contents
end

-- Write a string to a file.
function lufwrite(filename, contents)
  local fh = assert(io.open(filename, "wb"))
  fh:write(contents)
  fh:flush()
  fh:close()
end

-- Read, process file contents, write.
function lufreplace(filename, replace_func)
  local contents = lufreadall(filename)
  contents = replace_func(contents)
  lufwrite(filename, contents)
end

function argtable(...)  --snusmumriken's method
  local r,n = {},select("#", ...)
  for c = 1,n do  r[c] = select(c, ...)  end
  return r
end
