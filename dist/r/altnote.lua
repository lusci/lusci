
rexfind is a small dependency free lua module which expands luas native string pattern matching
to be, almost, equivalent to common regex implimentation like PCRE

it is lightweight and quite fast as it employs luas native matching engine as much as possible 

a quick drop in file for programmers missing key regex features or unfamilar with lua pattern matching syntax

either backslashes or percents to be used as the escape character

alt subgroups          (ok|alright)
capture backreference  (\w+)=\1+1
subgroup quantifiers   ((ok|alright))(\1 ){2,5}
caseless subgroup      (?i:laser |)beam

(two|three|four)  --any group containing unescaped |symbol is an alt group
(two|three|)      --an empty |element makes group unessential
(|two|three)      --placement of empty element affects 'greed' of matching 
(one) ((1|one))   --any bare group is a capture group

to be added
(?=five|six) and (?=seven) --group must be present and forgotten, group cannot be captured,
                           --enables positive lookback and lookforward type action 
(?!five|six) and (?!seven) --group cannot be present, is forgotten. negative 'lookhere'
(five|six){2,4} --group is present 2 to 4 times frugal, equiv to >
                  (five|six)(five|six)(|five|six)(|five|six)
(five|six){4,2} --group is present 2 to 4 times greedy, equiv to >
                  (five|six)(five|six)(five|six|)(five|six|)

this is not intended to be an optimised or fully featured regex engine,
but to be a quick drop in file for programmers missing key regex features or unfamilar with lua pattern matching syntax 

(one|two).*(three|four)

find a branch which is open
every pit needs a boolean that says if it needs added, to be searched
to say if it can be an unsized result?
separate sized find loop
or a switched size extend

tests to extend 
the extend test mechanics require extend test code to be common
to the different test mechanisms

proceed with chain
proceed over alts

recoil / retract to last open alt
or return fails if no open extend-test pits

when pattern is broke into tree
each segment of pattern is called a pit
some pits can only be tested as a tail on the following pit
following any pit containing unescaped + - ? *
all extend tests will need to be 'tailed'
they will extend from the magit pits stern not its bow
a non magic section that has a bow, cannot be trusted not to leap?

--[[

altfind will robustly find the earliest fit, with the earliest alts tried first
and in nested precidence.

altfind does not try every combination of alts before returning a match
it returns the first match of 
 ([A-Z]|[0-9])([a-z]+|[A-Z])
 searches first for [A-Z] and exhausts all proceeding combinations extending A-Z
 before trying [0-9] and its proceeding combinations
 alt group can contain a missing entry, the solver will try matching proceeding 
 combinations 
 (|to|at)restofpattern the proceeding alts are only searched for if 
 (to||at)restofpattern the preceeding alts are searched for but optional
 (to|at|)restofpattern the preceeding alts are searched for but optional
 
 [hij] then if no match, for [abc][klm] then if not match, for [def][hij] then [def][klm]
 in that order, returning as soon as a match is found,
 
 
]]

stepseq is called on a table seq to resolve an extension to the pattern which matches
it increases the matching pattern by its elements
when an element fails it seeks back to an open alt element
the alt tables maintain breadcrumb info
breadcrumb points to the ultimate open alt element
a simple alt element is closed as soon at it is tried and fails
a compound alt element is not closed until all its descendents are tried and failed

stepseq can initiate seekback itself, due to an element failing
or it can return seekback signal to its calling alt or stepseq

seek retries all the most recent leafs
simply when a compound alt element is followed, breadcrumb is left
compound alt is followed on part success
when it returns with success (a string) 
the success is returned back to the root 
when it returns with a fail
the fail is not returned back, if previous local breadcrumbs are open
the penultimate breadcrumb is followed

the fail signal is seek route exhausted, or 
fail always seeks for an open route,
but closes route when signal is 'seek &route exhausted'

but success may return with route open, or route exhausted
fail always means leaf exhausted
fail returns to callees,
callees retract to their last open route or return fail to its callee
success returns to callees, leaving open route breadcrumbs if open signal
open route breadcrumbs all the way

how does it leave closed route breadcrumbs?
if it leaves all the way, can it close an open route?
doe it have to leave them? 

all routes open until closed, or all closed till open ?

after a multiple routeback, all routes are open again to match

   b3 
   b2 
a  b1  c  d1 



--[[
function afind(hy,pt,st,fn)
  local pt=gobble(pt)
  
  local 
  
  
  repeat
  
  fnd=hy:find(pre,st,fn)
  
  until fnd



solvenextalt(hy,st,pre,agob)
  for i,a in ipairs(agob)

    if type(a)=="table" then
      va,ve,a=solvenextalt(hy,st,pre,a)
      if not ve then return false end
    end
    
    va,ve= hy:find(pre..v,st)
    
    if ve then return va,ve,v end
    
  end
end

solvenextalt(hy,st,pre,agob)
  
  if agob.mix then
    c=1
    repeat
      if type(agob[c])=="string" then
        sb=agob[c]
        c=c+1
        d=c+1
        if agob[c+1] and type(agob[c+1])=="string" then
          sd=agob[c+1]
          d=c+2
        end
        a,e,pat,ix=justalt(hy,alt,st,pre=sb,pos=sd,e,ix)
      end
      
      until done
    
  for i,a in ipairs(agob)

    if type(a)=="table" then
      va,ve,a=solvenextalt(hy,st,pre,a)
      if not ve then return false end
    end
    
    va,ve= hy:find(pre..v,st)
    
    if ve then return va,ve,v end
    
  end
end

solvemix
  hardb
  hardd
  softb
  softd



moz	moz
{ 
  [1]={ ["e"]=1, ["p"]='vooze' }, 
  [2]={ ["e"]=1, 
  ["p"]={ 
    [1]={ ["e"]=1, ["a"]=0, ["p"]='puck' }, 
    [2]={ ["e"]=1, ["a"]=1, ["p"]='styme' } } }, 
    [3]={ ["e"]=1, 
    ["p"]={ 
    [1]={ ["e"]=1, ["a"]=0, ["p"]='merk' }, 
    [2]={ ["e"]=1, ["a"]=1, ["p"]='feld' }, 
    [3]={ ["e"]=1, ["a"]=2, 
    ["p"]={ 
      [1]={ ["e"]=1, ["a"]=0, ["p"]='putt' }, 
      [2]={ ["e"]=1, ["a"]=1, ["p"]='ball' }
    } } } }, 
  [4]={ ["e"]=1, ["p"]='moz' } 
}

]]


--process rollback, in context of a chain is straightfoward (backward)
--when an open subpat is found normal matching proceeds from their,
--with the seekable option on until leaf is reached
--normal matching assumes ult_sub is spent and tries next sub
--seekable matching assumes ult_sub is open and 
-- resume

-- extend_match_in_chain, 
-- rollback_in_chain,
-- resume_chain
-- chain resume key is set to last open alt element

-- when a leaf alt match is found, if it has next key is set as resume
-- local alt=chain[alt_chain_ix]
-- if match_ix<#alt then
--   alt.rsm=match_ix+1
--   alt.prv_opn=chain.opn
--   chain.op=alt_chain_ix


function chain(...)
-- structuring function performs different purposes
-- is called to either:
--   1. match chain of patterns 
--   2. roll_back_to_open_route..
--      (then attempt to match chain from it, or return roll_on_back)
--   3. follow_open_route_to_leaf and integrate
-- metaconditions may imply purposes
-- purposes are determined 
--   after a match attembt
--     1. success: match next in chain
--     2. failure: try alt
--        no alts: roll_back_to_open_route
--          after no-alt condition, there may be open alts
--          earlier in the chain
--          these should be revisited if the relevant patterns are not simple
--          so rollback compares patterns to see if they should be tried
-- during in_chain_rollback
-- an alt contains last_matched registering the last matched element
--  if it is a chain, the chain have registed spent or open
--  if open then must call rollback_solve on the chain
--  chains visted in rollbacks always had a match
-- so, 
-- each element in chain is match tested in sequence
-- simple elements are match tested in flow
-- alt elements are also match tested in flow if simple
-- but in that flow alt elements are recursed if they are chains
-- the result of a simple match test is
--   1. success so next
--   2. fail so try next alt or, if no open alt > roll_back_to_open
-- 

--  chain(tbl,st,fn,phase)


function stepseq(tbl,nxel,st,fn)
  
  -- res,sig= stepseq(tbl,entry,mtchst,mtchfn)
  -- if sig is open tbl.nxel.sig==open ? it already is
  -- if sig is empty 
  local altpos,altel,altpre={},{},{} pr
  rt_tbl=rt_tbl or tbl
  
  -- searches improved by prefixing and postfixing available non-alt els
  -- whole intro prefixing is based
  -- last el prefix at st+last may be a possible optimisation, but
  -- unsure if secure, try after secure and simpler version complete
  
  while nxel < #tbl do
  
  --phase: prepare element 1 
  if nxel==1 then 
    --first element moves start
    st=st+1 
    --[[but if patt starts with ^ give up here
    --else, reorder and prune first alt
    --combine to a seq if only one match
    --repeat for next alt]]
  end --

  --phase: extend match
  el=tbl[nxel]
  if type(el)=="table" then --alt tabls from seqs are alts or caps (todo single alt)
    
    if not alts[nxalt] then alts[nxalt]=1 end
    
    --when earliest alt, loop by lowest match st
    if alts[nxalt]==1 and rt_tbl==tbl then
      alts[nxalt],pat = forealt(hy,el,alts[nxalt],st,fn) --alt finding functions may be rolled in
      --do each alt and return by sort order
    else 
      alts[nxalt],pat = stepalt(hy,el,alts[nxalt],st,fn) --alt finding functions may be rolled in
    end
    
    --an alt can return
    --no matches (elements exhausted)
    --a match and elements exhausted
    --match with unexhausted elements
    --the touch state is noted in the containing table,
    --and in the alt table..?
    if alts[nxalt] == false then --no alt matches on this leaf
      --[[rollback to prev leaf or branch ?
      --alt tables have a key stating if open or closed
      -- o: index of untried el
      -- o: index of el with untried descendant
      -- return 'seek token for
      -- alt may note follow_unexhausted, or untried
      -- return a seek token, for acts functions to search for untried descendents
      -- and return act(tbl,seektoken) (into them)
      -- or return seektoken to tell parent function to seek
      -- breadcrumbs cannot point to previous child element,
      -- as that child will have previous before.
      -- the breadcrumb must be in the child element
      -- saying fresh, unexhausted, or exhausted
      -- fresh is nil, unexhausted is nxel, exhausted is 0
      
      -- if r==seek
      -- while branchi>1
      -- if not branch.o or branch.o>0 then return follow(branch)
      -- branchi--
      -- end
      -- return seek
      
      --while r = seek
      --prev el
      ]]
    else --an alt matched to extend
      --alts[nxalt],pat
      --latest matching alt element, pattern in latest matching alt element
    end
  else
    a,e = hy:find(hy,el,st,fn)
    if not a then return backnx end
    pat=el
  end
  
  --phase: seek on fail
  --after an matchfail on an element or return from an alt
  --we seek back to an open alt, and then call alt to seek into it
  if not pat then
    while ~pat and el>1 do
      el=el-1
      if type(el)=="table" and altpos[el]<#el then
        match=altseek(el) --hunt into alt
      end
    end
    if el==0 then return exhausted end
  end-- if we exit, we have a match, and el to proceed

  nxel=nxel+1
  end --end while unmatched els 

  tbl.exhausted=true or false
  return table.concat(tbl,"")
end

 
--------------

--chain rewrite,
-- can we have alts, in an alt?

local hay

local function chain(tbl,st,fn,pre,resume)

  local foc=1 --focus on table element
  local elm,fnd,opn,elx,cnd,a,e
  
  if resume then 
    foc=tbl.opn
    pre=tbl[foc].pre
  end
  
  --all failures in chain trigger rollback
  --failed rollback results in chain returning false
  
  while foc <= #tbl do
    
    if overloop() then return false end 
  
    --first element moves start
    --if foc==1 then 
      --[[but if patt starts with ^ give up here
      --else, reorder and prune first alt
      --combine to a seq if only one match
      --repeat for next alt]]
    --end --

    elm=tbl[foc]
    if type(elm)=="table" then --alt tabls from seqs are alts or caps (todo single alt)
            
      --when earliest alt, loop by lowest match st
      a,opn,elx = false,false,0
      if resume then elx=elm.rsm-1 end
      
      if 1==2 then -- arrange earliest alt 
        --do each alt and return in sort order
        a,cnd,opn = forealt(elm,st,fn,pre,noseek)
        elm.rsm= (opn and elx) or false --resume is set and left 0 if not fnd
        elm.cur=cnd --? needed ?
      else 
        while elx < #elm and a~=st do
          if overloop() then return false end 
          --possible to weave in pre..patt optimisations
          --initially whole pre, is found each time, to allow pattern lookback
          elx=elx+1
          if type(elm[elx])=="string" then
            cnd=pre..elm[elx]
            a,e = string.find(hay,cnd,st)
            print("alt["..elx.."]",pre.." < "..elm[elx],a,e,st,fn,hay:sub(st,fn))
            if a==st then
              elm.pre=pre elm.rsm=elx+1 
            else 
              elm.rsm=false
            end
          else --type is chain table
            a,cnd,opn = chain(elm,st,fn,pre,resume)
            elm.rsm= (opn and elx) or false --resume is set and left 0 if not fnd
            if a ==st then
              elm.pre=pre 
              elm.rsm=(opn and elx) or ((elx<#elm) and elx+1) or false
            end
          end
        end  --while alts finished sweep of focused alt
      end --the false clause to special case ealiest alt selection
      
      --mark this focus last-open in case alt is resumed later
      if elm.rsm ~=0 then tbl.opn = foc end 
    else -- elm is string
      cnd=pre..elm
      a,e = string.find(hay,cnd,st)
      print("str",pre.." < "..elm,a,e,st,fn)
    end
  
    if a==st then
      resume=false
      pre=cnd
      foc=foc+1 
    else 
      print("rollback")
      --no match for element, rollback to last resume and seek leaf ..
      while foc>0 and not ( type(tbl[foc])=="table" and tbl[foc].rsm ) do
        print(tbl[foc])
        foc=foc-1 
      end
    
      resume=true
    
      if foc==0 then 
        st=st+1
        foc=1
        resume=false
        if st==fn then return end
      end --try move st 
    
      pre=tbl[foc].pre or ""
    end
    
  end

  print("ook")
  return a,cnd,tbl.opn
end







local function nextalt(hy,a,e)
  local q,b = 0,a 

  while a<=e do
    a=nextnoesc(hy,"%%*[()|]",a,e)
    if not a then return false,b end
    local hs=hy:sub(a,a)
--~     print(hs)
    if hs=="(" then q=q+1 elseif hs==")" then q=q-1 
    elseif q == 0 then 
      return hy:sub(b,a-1),a+1 --gobble()
    end
    a=a+1
  end
  return false,b  --gobble() 
end 
   
local function nextparth(hy,a,e)
--~   print("xpar")
  local d=nextnoesc(hy,"%%*[(]",a,e) or (e+1)
  if d>a then return hy:sub(a,d-1),d,false end
  --ate any lead-in, dont gobble it must be string
  --because it has been checked for | already
  
  --otherwise ( was at pos 1 so find closing
  local q = 1
  while q~=0 do
    d=nextnoesc(hy,"%%*[()]",d+1,e)
    if not d then
      prnte("--- Invalid pattern warning -"..hy:sub(a,e))
      return hy:sub(a,e),e+1,false 
    end --kind of error
    if hy:sub(d,d)=="(" then q=q+1 else q=q-1 end
  end
 
  --found a (group)
  --if it doesnt contain an altsplit, it is a bare capture
  --but the capture cant be inlined since a nested altsplit, will break it
  --capturing chains cant explicate themselves until the whole chain is solved
  --so, look for any unescaped | in the content
  --also cope with chain nesting in solver,
  --the solver must cope with chain nesting
  --in cases they do need to occur
  --  ab(cd(e|f)gh)ij  -- a chain capture is needed here,
  --they can be minimised when no alts are nested at any depth

  local inner = hy:sub(a+1,d-1)
  local prepa = inner:match("^(%?[i]?<?[!#]?)")
  
  local alts = nextnoesc(hy,"%%*[|]",a+1,d-1)
  
  if alts or prepa then
    inner=inner:sub((prepa and #prepa or 0)+1)
    e=gobble(inner)
    if e.chn then e.cap=true end
    --add attributes from prepa
  else
    e="("..inner..")"
  end
  
  --~   if hy:sub(d,d)=="{" then
  --~     local j,k=hy:match("{(%d),(%d)}",d)
  --~     if j and k then
  --~       multichain(gobble(e),at)
  --~     end 
  --~ --at = detect attributes, case capture type etc.
  --~   end
  
  return e,d+1
end 


--gobble is building chain tables, and alt tables
--chain tables contain string and alt elements
--alt tables contain string and chain elements
--syntax to split both is same
--...
--situations
--parsing in a chain, to create chain elements
--  m((n|o)|(p|q))
function gobble(hy,a,e) --hay, anchor end
--~   print("gob")
  a=a or 1    e=e or #hy  --(a)nchor (e)nd
  if not string.find(hy,"[(|]",a) then return hy:sub(a,e) end
  
  local rt = {}
  
--~   print("tryinga",hy:sub(a,e))
  local ms,d=nextalt(hy,a,e)   --(m)atch(s)tring  (d)ue pos
  if ms then                   --split as alts
    rt["alt"]=1 
    while ms do
      if overloop() then return rt end
      rt[#rt+1]= gobble(ms)
      ms,d=nextalt(hy,d,e)
    end
    if true then rt[#rt+1]= gobble(hy:sub(d,e)) end
    return rt
  else             --splitting as chain
    rt["chn"]=1
    local x,y
    while d<=e do
      ms,d,x=nextparth(hy,d,e)  --ms gobbled, (d)ue pos, (a)ttributes
      rt[#rt+1]=ms
      y=y or x
    end
    return rt
  end
end







local function base(a,e) 
   
  if (ptu:find("[(|]",a) or 10000)>e then return ptt:sub(a,e) end  --leaf

  local r,d,b = {}
  
  if (ptu:find("|",a,true) or 10000)<=e then 
    b,d=altway(a,e) 
  end
  -- altway returns alt.st to alt.fn  or alt.st to end if no open pipes
  if d and d<e then --there are open pipes
    while d<=e do
      r[#r+1]=base(b,d)
      b,d=altway(d+2,e) 
      --loop advances 2 from last slice-end
      --so it is finished when a==e+2 (slice ended at e)
      --when slice ended at e-1, a pipe is left, so substr "" needs sliced off
      --altway should not be called with a>e
      --so is not called when 
    end --exit when d==e+1 or e+2
    r.alt=true
    
  else
    r.chn=true
    --[[
      this table may contain a single alt, not sure how right this is
    --]]
    
    d=ptu:find("(",a,true) -- here this is already known to be <= e

    local ct,s,h=1
    while d<=e do -- d is pos of next (
    
      if d~=a then -- stash preceeding unenclosed
        r[ct]=(r[ct] or "") .. (ptt:sub(a,d-1))
      end
      
--~   b=ptu:find(")",d+1,true) or 10000
      h,b=ptu:find("%b()",d) -- find matched brak
      
      b = b or 10000
      
      if b>e then --no closures to continue
        invalpat(r,d,e)
        d=e+1
      else --consume next closure, contents d+1,b-1 
       
        s=ptu:find("|",d+1,true) or 10000 --detected an alt
        
        -- so try putting an alt in chn instead of a chnlink
        
        if s>b then --alt not found in closure, so add to (c)ur (t)xt
          r[ct]=(r[ct] or "") .. (ptt:sub(d,b))  --(sub includes braks)
        else        --alt was found in closure, add as table
          h=base(d+1,b-1) --get tables featuring alts
--~           print(ptt:sub(d+1,b-1))
--~           print(dump(h))
          if type(h)=="table" and #h==1 and type(h[1])=="table" then
            h=h[1] h.cap=true --extraneous group is capture
          end
          r[#r+1]=h
          ct = #r+1
        end 
        --to handle b+1...
        a = b+1
      end
      -- b+1 is untaken pos
      d=ptu:find("(",b+1,true) or 10000 
    end --end while all chainlinks
  
  if b+1<=e then
    d=ptu:find(")",b+1,true) or 10000
    if d<=e then invalpat(r,b+1,e) else
      r[#r+1]=ptt:sub(b+1,e)
    end
  end 
  
  end --end if chainlink
  
--~   if a<=e then 
--~     print("slippage")
--~     r[#r+1]=ptt:sub(a,e) 
--~   end
  
  return r
  
end

--[[
  pros cons of finding without captures until required by %1
  
  when not required, capture can slow find alot
  how much ?
  tests indicate minor slowdown even from multiple large captures of changing contents
  so search will not use patterns with capturing removed,
  it will not calculate and store non capturing versions of patlets
  however it will need to remove braks from nodes, until they complete
  as a non completed node would lack the closing brak
  a non completed node cannot be captured
  there will also be a possibility of making captures containing wildcards, 
  their own node
  making captures containing wildcards their own node
  has difficulties as wildcards rely on proceeding ptxt to resolve
  difficulties such as .*(one|other)
  so, we dont remove braks from ptxts, but have to add braks to capturing nodes
  
  


--[[
do i need a hotkey to scan through previous git versions of a file ?
it gets git to checkout older version of file and opens in readonly

abcdefghijklmnopqrstuvwxyz  bdhfkl  irt  aceous  jgpqy  zxwv

kok-e

]]
--*

print( ("a 123 b 123 c"):find("a(.*)b.?"))

