function altfind_top2(hy,pt,st,fn)
  local tbl=makepatt(pt)
  st=st or 1   fn=fn or #hy   hay=hy
  a,cnd,tbl.opn= winnow(tbl,st,fn,"",false)
  if not a then return end
  local rets={string.find(hy,cnd,st)}
  table.insert(rets,cnd)
  table.insert(rets,hy:sub(a,e))
  return table.unpack(rets)
end

function winnow(tbl,st,fn,pre,resume)
  local foc=1 --focus on table element
  local elm,fnd,opn,elx,cnd,a,e
  
  if resume then 
    foc=tbl.opn
    pre=tbl[foc].pre
  end
  
  --all failures in chain trigger rollback
  --failed rollback results in chain returning false
  
  while foc <= #tbl do
    
    if overloop() then return false end 
  
    --first element moves start
    --if foc==1 then 
      --[[but if patt starts with ^ give up here
      --else, reorder and prune first alt
      --combine to a seq if only one match
      --repeat for next alt]]
    --end --

    elm=tbl[foc]
    if type(elm)=="table" then --alt tabls from seqs are alts or caps (todo single alt)
            
      --when earliest alt, loop by lowest match st
      a,opn,elx = false,false,0
      if resume then elx=elm.rsm-1 end
      
      if 1==2 then -- arrange earliest alt 
        --do each alt and return in sort order
        a,cnd,opn = forealt(elm,st,fn,pre,noseek) --not done yet
        elm.rsm= (opn and elx) or false --resume is set and left 0 if not fnd
        elm.cur=cnd --? needed ?
      else 
        while elx < #elm and a~=st do
          if overloop() then return false end 
          --possible to weave in pre..patt optimisations
          --initially whole pre, is found each time, to allow pattern lookback
          elx=elx+1
          if type(elm[elx])=="string" then
            cnd=pre..elm[elx]
            a,e = string.find(hay,cnd,st)
            print("alt["..elx.."]",pre.." < "..elm[elx],a,e,st,fn,hay:sub(st,fn))
            if a==st then
              elm.pre=pre elm.rsm=elx+1 
            else 
              elm.rsm=false
            end
          else --type is chain table
            a,cnd,opn = chain(elm,st,fn,pre,resume)
            elm.rsm= (opn and elx) or false --resume is set and left 0 if not fnd
            if a ==st then
              elm.pre=pre 
              elm.rsm=(opn and elx) or ((elx<#elm) and elx+1) or false
            end
          end
        end  --while alts finished sweep of focused alt
      end --the false clause to special case ealiest alt selection
      
      --mark this focus last-open in case alt is resumed later
      if elm.rsm ~=0 then tbl.opn = foc end 
    else -- elm is string
      cnd=pre..elm
      a,e = string.find(hay,cnd,st)
      print("str",pre.." < "..elm,a,e,st,fn)
    end
  
    if a==st then
      resume=false
      pre=cnd
      foc=foc+1 
    else 
      print("rollback")
      --no match for element, rollback to last resume and seek leaf ..
      while foc>0 and not ( type(tbl[foc])=="table" and tbl[foc].rsm ) do
        print(tbl[foc])
        foc=foc-1 
      end
    
      resume=true
    
      if foc==0 then 
        st=st+1
        foc=1
        resume=false
        if st==fn then return end
      end --try move st 
    
      pre=tbl[foc].pre or ""
    end
    
  end

  print("ook")
  return a,cnd,tbl.opn
end
