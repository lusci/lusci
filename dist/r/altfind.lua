
function gsubhammer(txt,pat,rep,lim) --repeat gsub until limit or no more replacements
  lim=lim or 100000              --100k is moderate safety limit
  local og,rps=lim
  while lim>0 do
    txt,rps=string.gsub(txt,pat,rep,lim)
    if rps==0 then return txt,og-lim end
    lim=lim-rps
    if lim==0 or #txt>lim*5 then prnte("+ gsubhammer replacement limit was reached "..og) end
  end
  return txt,og
end


function maskparens(c)
  if not c:find("%(.-|.-%)") then return c end --no split parens
  c=string.gsub(c,"%%[%%()%[%]]","==") --mask escape pairs and escaped parens
  --%frontier lookbehind not required? escaped ]['s already masked
  --c=gsubhammer(c,"(%f[\\%[%%]%[.-)[%(%)](.-%f[\\%]%%]%])","%1=%2",10000) --mask p in ranges
  c=gsubhammer(c,"(%[.-)[%(%)](.-%])","%1=%2",10000) --mask p in ranges
  return c
end

--*


local function nextnoesc(ptt,nd,st,fn)
--~   print("?(",ptt,nd,st,fn)
  while st<=fn do
    if overloop() then return false end
    local a,e = ptt:find(nd,st)
    if (not a) or (e >fn) then return false end
    if (e-a)&1==1 then --its escaped
      st=e+1
    else
      return e
    end
  end
end

   
local function nextparth(ptt,a,e)
--~   print("xpar")
  local d=nextnoesc(ptt,"%%*[(]",a,e) or (e+1)
  if d>a then return ptt:sub(a,d-1),d,false end
  --ate any lead-in, dont gobble it must be string
  --because it has been checked for | already
  
  --otherwise ( was at pos 1 so find closing
  local q = 1
  while q~=0 do
    d=nextnoesc(ptt,"%%*[()]",d+1,e)
    if not d then
      prnte("--- Invalid pattern warning -"..ptt:sub(a,e))
      return ptt:sub(a,e),e+1,false 
    end --kind of error
    if ptt:sub(d,d)=="(" then q=q+1 else q=q-1 end
  end
 
  --found a (group)
  --if it doesnt contain an altsplit, it is a bare capture
  --but the capture cant be inlined since a nested altsplit, will break it
  --capturing chains cant explicate themselves until the whole chain is solved
  --so, look for any unescaped | in the content
  --also cope with chain nesting in solver,
  --the solver must cope with chain nesting
  --in cases they do need to occur
  --  ab(cd(e|f)gh)ij  -- a chain capture is needed here,
  --they can be minimised when no alts are nested at any depth

  local inner = ptt:sub(a+1,d-1)
  local prepa = inner:match("^(%?[i]?<?[!#]?)")
  
  local alts = nextnoesc(ptt,"%%*[|]",a+1,d-1)
  
  if alts or prepa then
    inner=inner:sub((prepa and #prepa or 0)+1)
    e=gobble(inner)
    if e.chn then e.cap=true end
    --add attributes from prepa
  else
    e="("..inner..")"
  end
  
  --~   if ptt:sub(d,d)=="{" then
  --~     local j,k=ptt:match("{(%d),(%d)}",d)
  --~     if j and k then
  --~       multichain(gobble(e),at)
  --~     end 
  --~ --at = detect attributes, case capture type etc.
  --~   end
  
  return e,d+1
end 




overcounts={}

local function overloop(o,x)
  o=o or 0 overcounts[o]=overcounts[o] or 0
  overcounts[o]=overcounts[o]+1
  if overcounts[o]>(x or 100) then
    prnte("Hiccup, OC is"..overcounts[o],((o==0 and "") or ("p:"..o)))
    return true
  else return false
  end
end

local function invalpat(r,d,e)
  r[#r+1] = "_ERR_>"..(ptt:sub(d,e)).."<_ERR_"
  prnte("--- Invalid pattern warning > "..ptt:sub(1,d-1)..r[#r]..ptt:sub(e+1))
end


--[[
local function chain()

  when chain returns:
    a pattern which solves its position
    
  chain 
    returns for alts,
    returns for chns,
    returns for pats,
    
    pat returns 
          or    "failed
    
    tables return "successfully extended so proceed to open or closed"
    or "failed so closed, recoil to opened point ..."
    
    chain can build an 'earliest' table
    
    chain quickly discerns table it is working on
    builds 'earliest' when called from a 
    'recoil signal' vs 'finish parents' signal

  use prev_cap_len
      
      onhay positions
      
      hysrchpos1
      hysrchpos2
      hyoriginpos  - 1st search position in hay like find("a",".",n)
      hy

      pattern solution tables
      true found pattern snips
      captureless copy of found pattern snips?
      a replace function to call on extension
      the extension function is either 
        simple text find, of raw pat string

end
--]]

--node properties
-- gsb - a number representing the greatest capture index, used to prime pats 
-- par - parent reference
-- vsx - visited index of alt or chn pat
-- on signal : failed_seek back to
-- when seekback occurs, callee recieves seekback signal
-- signals: fallback openback
-- openback means exts are completed but there are open branches to try
-- closback means exts are completed there are no open branches
-- fallback means exts dont fit, no open branch
-- node returns searchresult, signal
-- callee node recieves signal
-- marks childnode open or closed, notes search result and extends itself
-- or if no search result, it winds back, to its last open node
-- and reopens the branch
-- it then calls reopen on the node
-- when an alt node gets called to reopen, it doesnt start at first
-- it expects success at its last opened node 
-- it doesnt recheck previous nodes, it calls reopen into last open node

-- extends must have access to previous nodes prefixpat

-- when chain recieves fallback, it must loop back through its elements
-- and 'seekleaf' into any open element.
-- ( any element with child.active_el < #child ) - no because this misses open_active_els
-- so any element that has marked itself open
-- alt element marks itself open, with an index to its open el, which is always
-- distinct, alt elements can have only one open el - the latest untried
-- chain elements can only mark they have open els before their active
-- on fallback their active el will be closed
-- for convienience chain.open_el is the ultimate open_el to start the 
-- backloop on, or nil or false if known all closed

local function extends(node,par_node,el,fndposa,fndposb)

each node may discern or store:

open_el  -- slightly different meaning for chain and alt
active_el   -- slightly different meaning for chain and alt
chain_prefix   -- only exists for chains 

meta details
whole_match_start_pos   -- this is the active root elements match start pos 
complex match start  -- changes extends discovery mode, is match start of first complex element
                  -- complex element contain + * - ?


  tst_pat = node[el]
  
  if node.gsb then
    
    if node.gsb > #gcap then --recalc gsubs
      local capget=""
      for i,v in node.fllpats do      --fllpats is created when an extend node completes
        capget=capget..v                  --successfuly, capture braks are added if node.cap
      end
      local gcaps = { hay:match(capget,fndposa) }
    end
    
    --gsub cap replace is vulnerable to escapes %%, but use for now..
    tst_pat= tst_pat:gsub( "%1",gcaps[1] )
    
    for i =2,#gsubs do  tst_pat = tst_pat:gsub( cpt,"%"..i,gsubs[i] )  end
    
    node.hdpats[c]=tst_pat
  end
   
  if lst.hrd or lat.hrd then 
    cnd=fpre..lat[cnx] cp=ap
  else 
    cnd=lat[cnx] cp=bp..#fpre
  end
  
  res=hy:find(cnd,
  
end


local function landing()

  --landing is called to extend the current alt or chn
  --or to seek to objectives:
  -- last open branch
  -- last open leaf
  --object to return to previous callee, and to last open branch
  --or call into last 'open' leaf, the noted leaf
  --eg if called into open node that has another open node
  --calls into its open node again
  --and so on until the open node called into
  --has no open nodes only unchecked leaf/s
  --then signal changes to normal extend
  --where breadcrumbs are ignored and overwritten
  --until the next leaf failure
  --signals/modes
  --leaf failure > back to inexhausted leaf > forward to openleaf > normal extend
  
  --in normal extend a node is passed string of preceeding pat
  --in normal success leaf passes only the successful pat (sometimes gsubbed)
  --when alt completes through leafs or nodes it passes its successful pat (sometimes braked)
  --successful pattern strings find their way to a chain node
  --chain node grows its preceeding pat as it extends,
  --alt nodes preceeding text is fixed
  
  --or proceed

  --when it seeking forward it calls into its ancestors open alts
    
  --when node fail, return back if back has no open alts,
  --it fails, when has open alt, it tries
    
  --visited alt, virgin alt
  
--~ print( dump( altfind("abcdefghijkl","(ld|fgd|ded|c(ij|d))") ) )

--when call gobble, say if coming from a chain,
--then if gobble chains again
--make a chain capture, only when content contains alts

-- At time of writing, this developer is dead broke. So donations
-- to my buymeacoffee account, might actually get used to improve the supply of coffee.
