
--function WReplace(c,nosumm,multifile)

--Lister 
--Luscis list and replace command performs operations using a shorthand notation
--that is very simple for common usage, and progressively a little more 
--obscure for rarest usage.
--Performs listings and replacement of words or patterns in current or multiple files
--The List syntax is punctuated by the comma normally. 

--Basic operations: 
--li,findthis            -- lists wholeword 'findthis' in selected text or current file
--li,zap,,               -- replaces "zap" with what follows ,, (here it deletes) 
--li,fixthis,,fixedit    -- simply replaces 'fixthis' with 'fixedit' in current text
--Li,FixThis,,FixedIt    -- to match Case Sensitive ( and replace )
--Li,ixThis,,ixedIt,     -- put a comma at the end to match wordpieces
--li,day,                -- will list MonDay TuesdaY etc. 
--Li,Day                 -- will only list Day
--li,oo,,,               -- deletes all oo from wood brood mood shrood...

--More:
--The default escape is the comma, it can be changed to any of the 
--following 6 characters: ,./;'` by putting it immediately at the 
--start of the instruction. 
--If the find/replace terms contain commas, change the escape to one of the other 5
-- . / ; ' ` (these should be unshifted and live close on the keyboard)
--If any of those characters is placed before the find term, it becomes the escape for 
--that instruction
--li,/,//, /            -- puts a space after all commas in doc, escape is /
--li,,.txt,,.doc        -- need to set comma as escape here or . would be set
--li,.txt,,.doc         -- almost replaces "txt,," with "doc" (error)

--At the very beginning ~ chars change the pattern type
--li~,[wh?][ou]?man,,be a person     --basic lua pattern find (and replace)
--li~~,([wh?][ou]?man),,be a %1      --basic lua pattern with captures in replace
--li~~~,(man|woman|human),,be a %1   --lua pattern with | separated alts and captures in replace

--li@5,red,,pink  --replaces word red with pink in the 5 most recent files
--li@3,           --prints 3 last most recent filepaths

--li@@12,red,,pink  --performs replace on 12 most recent filepaths printed in terminal
--li@@4,            --prints 4 most recent filepaths printed in the terminal

--This last 'magic' term ~<> enables multiple operations in one command 
--It must be followed by a valid escape character, or also by the pattern type
--character '~'

--If this term is placed at the end of the lister command, without search text following
--it causes a dry run.

--the reason why things are not named consistently in this codebase
--is not due to any deficincy in this renaming feature
--using this system it may be possible to accidently enter a command which shreds
--a partition or overheats hardware but it would happen vanishingly rarely, surely.
--for less severe errors, ctrl+z or revert should restore immediately 

--[[ spec
  
 listhotkey - creates command string...
 spothotkey - creates command string for tty sp, and runs it, runs on windows
 MAReplace - contains stuff to breakup syntax, preload files, calls a filesweep at a time
 WReplace - contains stuff to read syntax and run a sweep on the current file,
           prints hits while looping, and lead-in and lead-out info
           
 synput   - parses input and prepares a finalized table of sweeps to do
 listhtky - creates a command string from keypress and calls lister with it
 lister   - calls synput on instring, and does whatever synput responds
          -   cando: set and print target filepaths 
          -   identify filepaths implied (current or a list)
          -   loop through filepaths calling sweep with all req params, printing
          -   leadin and leadout.
          -   much taken from MAReplace, and lead-in-out from WReplace
 sweep    - the guts of WReplace with lead-inout removed, returns num of reps and details
]]

function lister(str)
  
  local mag,mfiles,mfty
  local act=synput(str)
  if act.mulfile then
    act.mulfile,mag=matchout(acton.mulfile,"@+")
    mfiles=tonum(act.mulfile)
    if #mag==1 then
      mfty=" MRU files"
      mfiles= ipaircopy( lc_init('fpstack'),mfiles )
    else
      mfty=" upscreen files - not implemented .. MRU here as stub"
      mfiles=ipaircopy( lc_init('fpstack'),mfiles )
    end
    print("-Lister Multifile "..#mag..mfty)
    
    if act.onlyfiles then 
      printeach(mfiles,1,#mag," ")
      traceprompt() 
    return true end
  end

  if #mfiles==0 then
    table.insert(mfiles,props['FilePath'])
  end
  
  act.waytext = props['FilePath']..":"..(line+1)..": ~<( "..act.cmdstr.." )>~"
  
  for k,cfile in ipairs(mfiles) do
    if props['FilePath']~=cfile then
      --open mfile
      print()
    end
    
    for kk, ac in ipairs(act) do
      if not act.mulfile then
        local pan,b,d = get_pane(editor)
        local jls=poslinestfn(b)
        local _,klf=poslinestfn(d)
        if d>=klf and b<=jls then  --sel must be full line or more 
          ac.st=b ac.fn=d 
        end
      else
        ac.st,ac.fn = 0,editor.Length
      end
      xWReplace2(ac)
    end
  end
  
end

function synput(str)
  local c,ca,cb,cd,mulfile,patset,prepatset,acook = str
  c,ca = matchout(c,"^[Ll][Ii][Ss]?[Tt]?")
  
  local nocase = ca==string.lower(ca or "")
  local actons = {}
  actons.nocase = nocase
  local acmd =nocase and "li," or "LI,"
  
  local capes=",./;'#"
  local mc=","
  local actons,dryrun={},false
  local intro = true
  
  -- get intro cookies in either order
  c,mulfile = matchout(c,"^@@?%d?%d?%d?")
  if mulfile~="" then 
    actons.mulfile=mulfile 
    if not c:match("%S") then 
      actons.onlyfiles = true 
      return actons
    end
  end

  c,dryrun = matchout(c," ~<> ?$") --empty rat at end for dryrun
  
  actons.dryrun = dryrun~=""
  
  c,prepatset = matchout(c,"^~~?~?")
  if mulfile=="" then  c,mulfile = matchout(c,"^@@?%d?%d?%d?")  end

  c,vld = matchout(c,"^,") --comma required 
  if vld=="" then prnte("- List syntax error. No initial comma") return end
  
  c=prepatset..c
  local ovf=0 --overflow
  while #c>0 and ovf<1000 do
    
    ovf=ovf+1
    c,patset = matchout(c,"^~~?~?") --sets pattern type
    
    c,mc = matchout(c,"^["..capes.."]")  --sets the magic char 'mc'
    
    if intro and #mc==0 and (mulfile..patset ~= "") then
      prnte("- No magic char found after intro ("..mulfile..patset..")") return
    end

    intro=false

    if #mc==0 then mc="," end
    pmc="["..mc.."]"
    nmc="[^"..mc.."]"
      
    c,due = matchout(c," ~<>(~?~?~?[,./;'#].+)") --removes any subsequent actons
 
    c,ndl = matchout(c,"^"..nmc.."+")  --oo,   oo,,xo   ndl=oo  c=,,xo 

    if ndl=="" then  prnte("- List, syntax error. No search text : "..c) return  end
    
    if ndl~="" then 
      c,rpx = matchout(c,"^"..pmc..pmc.."("..nmc.."*)")
      c,ntwd = matchout(c,pmc.."$")
    end
    
--~     a="xabbbcde" print(a:match("aa(.*)"))
    
    local pn = ndl:match("~<>")
    local pr = rpx:match("~<>")
    
    if pn or pr then
      prnte( "- List, syntax error problem sequence : "..((pn and ndl) or (pr and rpx)) )
      return
    end
    
    local bird =nil
    if patset ~="" then bird=bird.."~<>"..patset end
    
    local act={}
    act.needle = ndl
    act.rep = rpx
    act.notwords = ntwd~=""
    act.patset = #patset
    act.dryrun = dryrun~=""
    
    act.cmdstr = acmd..","..mc..ndl
    
    if rpx then act.cmdstr=act.cmdstr..mc..mc..rpx end
    if bird then act.cmdstr=act.cmdstr..bird end
    if act.notwords then act.cmdstr=act.cmdstr..mc end
--~     if act.dryrun then act.cmdstr=act.cmdstr.." ~<>" end
    
    table.insert(actons,act)
    
    c=due
  end --while 
  return actons 

end

print( dump( synput("list,poo") ) )

--~     lp,matchout("wdjo,dedk,","^.-[,]")
    
  --hi,pong,,pang, <>,bong,,bang,    diamond cookie, including a mage
  --hi,pong,,pang, <>,bong,,bang, 
  --hi,pong,,,
  --hi,,,pong,,pang, <>, --dry run, is empty diamond cookie at the end
  --hi,//pong//pang
  --hi,;;pong;;pang
  
--[[
function preparse(b,c)

  local patrep,plainmtch=nil,true
  local inst=c

  local nocase
  
  if sub(b,1,1)==string.lower(sub(b,1,1)) then nocase=true end
  
  local wrdbnd=true
  local xfindx=false

  local xp=","
  local c,xp=matchout(c,"^[,./;'#:@~`¬]")
  if xp=="" then
  
  -- prefixed ¬ for xfind ?
  if sub(c,1,1)=="¬" then xfindx=true  -- ¬ for xfindx
    c=sub(c,2)
  end

  -- postfixed ¬ for plainmatch ?
  if sub(c,#c)=="¬" then plainmtch=nil 
    c=sub(c,1,#c-1)
  end

  -- second postfixed ¬ for patter replace ?
  if sub(c,#c)=="¬" then patrep=true 
    c=sub(c,1,#c-1)
  end

--~ 	if sub(c,-2)==" ~" then wrdbnd=nil
--~ 		c=sub(c,1,#c-2)
--~ 	else
  if 
    sub(c,#c)=="~" then wrdbnd=nil
    c=sub(c,1,#c-1)
  end 
  
  if sub(c,1,2)=="\\~" then c=sub(c,3) end --an escaped ~
  
  local fndpatw=c
  local reppatw,reppatv
  
  if find(c," ~") then
    fndpatw=match(c,"(.*) ~")
    reppatw=match(c," ~(.*)")
    reppatv=reppatw
    if not reppatw then reppatw="" end
  end
end
]]--
--WReplace
-- 
function xWReplace2(act)
    
  --synput reader must set buffer, and these params :
  
    --og params
  local  c             = act.comtext -- raw text of command (,be ~bo~) 
  local  nosumm        = act.nosummary -- dont print a summary 
  local  multifile     = act.printcom -- when not 'multifile' the command is printed
  local  targetfile    = act.file       -- filename to open and operate on
  local  stp,fnp       = act.st,act.fn  -- operation range 0ix caret: sel.lft,sel.rght

  --local  --from og (c) = p.
  local  q             = act.waytext -- string for a waypoint print taken from c 
  local  plainmtch     = act.patset==0 --boolean do plain
  local  patrep        = act.patset==1 --boolean do pattern replace 
  local  xfindx        = act.patset==2 --boolean do pattern (with xfindx)
  local  nocase        = p.nocase --boolean
  local  wrdbnd        = not act.notwrds --word bound matching
  local  fndpatw       = act.needle
  local  reppatw       = act.rep
  local  reppatv       = reppatw
  
  --check targetfile is open
  --open it without changing dwell order ?
  --if doesnt open print error and return
  
  local find = string.find
  local cc=string.lower(c)

  if not multifile then --prints here, before 1st op
    uptrace("\n>"..props["FileDir"].."; "..c.."\n",0,1)
  end

  local _,_,q=find(c,"[:,](.*)")  q=q or ""
  printwaypoint(act.waytext)
  --todo: note output.CurrentPos before matchprintout
  --print different waypoint / header info depending on length of matches
  
  local tnam=props['FileNameExt']
  local hfs={lua=1,java=1,js=1,php=1,thtml=1,cxx=1,h=1,py=1,html=1} 
  local hasfun=hfs[(props['FileExt'])] --print also function above match
      
  noteSelection()
  
  local stp,fnp=0,editor.Length
  if editor.CurrentPos~=editor.Anchor then
    fnp,stp=editor.CurrentPos,editor.Anchor
    if fnp<stp then fnp,stp=stp,fnp end
    if (fnp-stp<500) and not find(editor:textrange(stp,fnp),"[\r\n]")
    then stp=0 fnp=editor.Length end
  end

  editor:BeginUndoAction()

  local len=string.len
  
  if reppatv and wrdbnd then
    reppatv=wwordpatt(reppatv) --complications from not using frontier pattern
--~ 		print("reppatv#"..reppatv)
--~ 		reppatv="[^%w_]"..reppatv.."[^%w_]"
  end
  
  if fndpatw=="" then fndpatw=nil end 
  if not fndpatw then fndpatw="whaddyamean?" reppatw=nil end
  
  local lenchng=0 --init not used
  
  local se,fwd=false,false
  local nxfndpos
  local otabl={}
  local ortabl={}
  local fndpatcas=fndpatw
  if nocase then fndpatcas= string.lower(fndpatcas) end
  
  local haswdst,haswdfn --complications from not using frontier pattern
  if xfindx or find(fndpatcas,"^[$%w_]") then haswdst = true else haswdst=false end
  if xfindx or find(fndpatcas,"[%w_]$") then haswdfn = true else haswdst=false end
  local xf
  
  --local ooo=sub("~ooOO8O8O8O8O8888888",1,math.floor((len(fndpatw)+1)/2))..sub("8888888O8O8O8O8OOoo~",21-math.floor((len(fndpatw))/2))
  
  local reps,treps=0,0
  local colides=0
  local edst=editor:LineFromPosition(stp)
  local eden=editor:LineFromPosition(fnp)
  
  if not fndpatw then print("- lost innocence") return end

  local mtchst,mtchfn,clnstp,clnfnp,padlntxt,caslntxt,prtlntxt,lndif,prtdif,insr
  local lower=string.lower
  
  if xfindx then tfind=xfindpattern else tfind=find end
  
  -- linesweep here, printed line by line, results of find replace line by line
  
  
--~ 	print("fndpatcas#"..fndpatcas) 

--~ 	if wrdbnd then print("wrdbnd") else print("no wrdbnd") end
--~ 	if haswdst then print("haswdst") else print("no haswdst") end
--~ 	if haswdfn then print("haswdfn") else print("no haswdfn") end
--~ 	if fndpatcas then print("fndpatcas#"..fndpatcas) else print("no fndpatcas") end
--~ 	if plainmtch then print("plainmtch") else print("no plainmtch") end
--~ 
--~ 	a,b,c,d=tfind("boot tweek",fndpatcas,0,plainmtch)
--~ 	print(a,b,c,d)
  
--~ 	if true then return end
  
  for cln=edst,eden do
    
    clnstp=editor:PositionFromLine(cln)
    clnfnp=editor.LineEndPosition[cln]
    
    if clnstp<stp and clnfnp>stp then clnstp=stp end
    if clnfnp>fnp then clnfnp=fnp end
    
    if clnstp==0 then padlntxt="\n"..editor:textrange(clnstp,clnfnp)
    else padlntxt=editor:textrange(clnstp-1,clnfnp) 
    end  --one extra char at start to assist word finds
    
    caslntxt, prtlntxt =padlntxt, padlntxt
    lndif, nxfndpos, prtdif=0, 2, 0 
    if nocase then caslntxt= lower(caslntxt) end
  
    if reppatv and not patrep then
      local v=0 
      for _ in string.gmatch(padlntxt, reppatv) do --!
        v = v + 1	
      end
      if v>0 then
        colides=colides+v
        insert(ortabl, tnam..":"..(cln+1)..":"..sub(prtlntxt,2))
      end
    end
    
    repeat --process one line (padlntxt)
        
      mtchst,mtchfn=tfind(caslntxt,fndpatcas,nxfndpos,plainmtch)
      
--~ 			if mtchst then print(nxfndpos,sub(caslntxt,mtchst,mtchfn),fndpatcas) end
      
      if wrdbnd then
        if haswdst and mtchst and find(sub(caslntxt,mtchst-1,mtchst-1),"[%w$_]") 
        then mtchfn=nil end
        if haswdfn and mtchfn and find(sub(caslntxt,mtchfn+1,mtchfn+1),"[%w$_]") 
        then mtchfn=nil end
      end
      
      if mtchfn then nxfndpos=mtchfn+1 
      else
        if mtchst then nxfndpos=mtchst+1 end
      end
        
      if mtchfn then	
        if reppatw then	
          if patrep then 
            if xfindx then
              insr=gsub(sub(padlntxt,mtchst,mtchfn), 
                sub(padlntxt,mtchst,mtchfn), reppatw) 
              or "!!"..sub(padlntxt,mtchst,mtchfn).."!!"
            else
              insr=gsub(sub(padlntxt,mtchst,mtchfn), fndpatcas, reppatw) 
              or "!!"..sub(padlntxt,mtchst,mtchfn).."!!"
            end
            
            prtlntxt=sub(prtlntxt,1,mtchst-1+lndif+prtdif).."@"..insr
                .."@"..sub(prtlntxt,mtchfn+1+lndif+prtdif)
            prtdif=prtdif+2
          else 
            insr=reppatw 
          end
          
          padlntxt=sub(padlntxt,1,mtchst-1)..insr..sub(padlntxt,mtchfn+1) 
          caslntxt=sub(caslntxt,1,mtchst-1)..insr..sub(caslntxt,mtchfn+1) 
          
          lenchng=#insr-(mtchfn-mtchst+1)
          nxfndpos=nxfndpos+lenchng
          if clnstp+mtchst<stp then stp=stp+lenchng end
          if clnstp+mtchst<fnp then fnp=fnp+lenchng end
        end
        
        reps,treps=reps+1,treps+1
      end	
    until not mtchst --(no more finds in line)
    
    if hasfun and find(padlntxt,"function") then 
      --function: test1.lua:3:
      ffunc=tnam..":"..(cln+1)..":"..sub(prtlntxt,2)
      fll=cln
    end
    
    if reps>0 then
      if ffunc and not (fll==cln) then
        insert(otabl, ffunc)
        ffunc=nil
      end
      
      insert(otabl, tnam..":"..(cln+1)..":"..sub(prtlntxt,2))
      if reppatw then
        editor:SetSel(clnstp,clnfnp) 
        editor:ReplaceSel(sub(padlntxt,2))
      end
    end
    reps=0
    
  end --all lines done	

  -- collected reports to ouput :
  -- computes  collides --count of collisions made during loop
  
  editor:EndUndoAction()
  
  local tr="Text"
  if wrdbnd then tr="Word" end
  
  if treps==0 then
    if (not nosumm) then 
      print("-"..tr.." not found: '"..fndpatw.."'")
    end
  else
    print(table.concat(otabl,"\n"))
    
    if not reppatw then
      print("-"..tr.." '"..fndpatw.."' found "..treps.." times")
    else
      print("-"..tr.." '"..fndpatw.."' replaced with '"..reppatw.."' "..treps.." times")
      if colides>0 then
        print(table.concat(ortabl,"\n"))
        print("-Colliding with "..colides.." previous occurences of '"..reppatw.."'")
      end
    end
  end

  restoreSelection()	
  if act.save then --idm save end
  return treps
  end
end




--------------------------

-- old functions

--[[
function xMAReplace(c)
  
  local cc=string.lower(c)
  local _,_,isrep=find(c,"^([Ll]is?t?[:,])")
  local _,_,fxx=find(cc,"^lis?t?[:,](~~?)")
  local _,_,fscope=find(cc,"^lis?t?[:,]~~?([%d][%d]?)")
  local _,_,rpexpr=find(c,"^[Ll]is?t?[:,]~~?[%d%~][%d]?[%d]? (.+)")
  if not fxx then WReplace(c) return end 
  
  local farray={}
  local cfp=props['FilePath']
  Lc_.repfiles=Lc_.repfiles or {}
  
  if (not fscope) or fscope=="0" then 
    if Lc_.repfiles[cfp] then 
      farray=Lc_.repfiles[cfp]
    else
      table.insert(farray,cfp)
    end
    print("- File Scope:")
    print(table.concat(farray,"\n"))
    Lc_.repfiles[cfp]=farray
    --print(isrep..rpexpr)
    --~ return false
  elseif find(c,"...?.?[:,]~%d+") then
    farray,farr={},fhistory(tain(tonumber(fscope),1,50),true)
    if fscope then
      for i=#farr,1,-1 do
        insert(farray,farr[i])
      end
      print(table.concat(farray,"\n"))
      print("- These recent files scoped")
      Lc_.repfiles[cfp]=farray
      if not rpexpr then return false end
    end
  else
    farray,farr={},{}--getofiles(tain(tonumber(fscope),1,50))
    if fscope then
      for i=#farr,1,-1 do
        insert(farray,farr[i])
      end
      print(table.concat(farray,"\n"))
      print("- These upscreen files scoped")
      Lc_.repfiles[cfp]=farray
      if not rpexpr then return false end
    end
  end
  
  if not rpexpr then return false end
  
  for i,v in ipairs(farray) do 
    scite.Open(v)
    WReplace(isrep..rpexpr,i~=#farray,true)
  end
  scite.Open(cfp)
  return false
end
]]--