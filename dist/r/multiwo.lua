local mwrdtbl = {}                -- multiword table
local mrnga,mrnge,mrncp = 1,1,1   -- mul range anchor,end,curpos
local mwrdc,mwrdb = "",""         -- mul word current, before 
local htwrda,htwrde = -1,0        -- mul word anchor,end
local mwpan,mwbuf                 -- multiword panel (editor output) buffer
local nowrd="esCaPe~C0ok1e"       -- no one will type that

-- ccccc cccac cece bc c c c c c c c c c 
local function paintrange(pan,a,e)

  local cla=tain((a or 0)-200)    -- avoid full sweep of large files
  local clb=tain((e or pan.Length)+200,0,pan.Length)
  
  local send = pan==editor and scite.SendEditor or scite.SendOutput
  send(SCI_SETINDICATORCURRENT, 0) -- use different number for overlapping indications? 
  send(SCI_INDICATORCLEARRANGE, cla, clb)
  send(SCI_INDICSETALPHA, 0, 35)
  send(SCI_INDICSETOUTLINEALPHA, 0, 125)
  send(SCI_INDICSETUNDER, 0, 1)
  send(SCI_INDICSETFORE, 0, 0x00aaff) -- bgr
  send(SCI_INDICSETSTYLE, 0, 7)
  if e and e>a then send(SCI_INDICATORFILLRANGE, a, e - a) end

  mrngax=poslinestfn(a,pan,-1)
  _,mrngex=poslinestfn(e,pan,1)

end

local function stopmwrd()
  paintrange(mwpan)
  --clear interpose
  setinterposekeys(false)
  mwpan,mwbuf=false,false
end

--replace wrds in the mw range with due wrd except the curpos wrd
--move curpos after so it is in same place after
--due wrd may be +1 or -1 size, it may go to 0, but not under 
--if it goes to 0 dont destroy mwt , incase fresh word is typed in place

local function updtmwt(wrdd,wrdb,crp,arp) --dueword , beforword , caret position
  if htwrd==nowrd then return end  -- do nothing if new table found no hits
  if wrdb~=htwrd then prnte("wrdb~=htwrd:",wrdb,htwrd) return end
  htwrd=wrdd
  mwpan:BeginUndoAction()
  
  -- p , i, r   pristine , intermediate, resulting
  
  local ld,lb,lz = #wrdd, #wrdb, #wrdd-#wrdb
  local oz,rz,sz = 0,0,0  -- accrued output shift, caret shift, caret skip shift
  for i,wp in pairs(mwrdtbl) do
    mwrdtbl[i] = wp+oz+sz --
    htwrde=htwrde+lz
    if crp~=tain(crp,wp,wp+lb) then -- only change outside current pos wrdd
      mwpan:SetSel(wp+oz,wp+oz+lb)
--~       prnte("sel :"..mwpan:textrange(wp+oz,wp+oz+lb))
      mwpan:ReplaceSel(wrdd) 
      oz=oz+lz
      if crp>wp then rz=rz+lz end
      htwrda=htwrda+lz
    else
--~       prnte("xsel :"..mwpan:textrange(wp+oz,wp+oz+lb))
      sz=lz
    end 
    mrnge=mrnge+lz  -- range end adjusted for every word including excluded caret word
  end
  
  if crp==ape then
    mwpan:GotoPos(crp + rz)
  else
    mwpan:SetSel(arp+rz, crp + rz)
  end
  
  mwpan:EndUndoAction()
  paintrange(mwpan,mrnga,mrnge)
  return  -- pos to put caret in when scite is allowed to handle keypress
end
-- eefef    eefe     fefffl     eeeee   eee 
--makes a list of matches in the range
local function newmwt(wrd,flags)
  local z, x, a,e,d = 0, 0, mrnga,mrnge,1
  while d and z<1234 do
    b,d=mwpan:findtext(wrd, flags ,a,e+1)  -- e+1 tried for an offbyone? 
    if d then  x=x+1 mwrdtbl[x]=b a=d prnte("b d",b,d)  end -- should a=d+1 ?
    z=z+1
  end 
  if z>=1234 then prnte("woah there !!!!!!!!!!!") end
  
  if x<2 then htwrda,htwrde,htwrd=-1,-1,"" end --invalidate 
  --clear rest of table
  while x<#mwrdtbl do  x=x+1 mwrdtbl[x]=nil  end
  htwrd = wrd
end

--correct mwt after the wrd has changed
local function mwtshft(crp,x)
  local xs=0
  for i,v in pairs(mwrdtbl) do
    if v<crp then crp=crp+x end --only word anchors before cp will move cp
    mwrdtbl[i]=v+xs       --xs is initially 0 as first anchor not moved
    xs=xs+x 
  end
  return crp --rtrn probably not required as updatemwt moves cp
end

local function mwtaftcp(crp,x,z)
  local bv=0
  for i,v in pairs(mwrdtbl) do 
    if v>=crp then 
      if bv+z >=v+x then htwrd=nowrd return end --? hit previous wd so invalidate mwrange
      mwrdtbl[i]=v+x 
    end 
    bv=v
  end
end

----------------------------------------------------------------------------
-- we just make list of current word in range locwds
-- which is the start positions of the current word in the document, in the range
-- if has selection, make selection the current word
--   make new locwds of selection but dont require separation
-- if edit not occuring during current locwds
--   get word it is occuring at,
--     make new locwds...
--   if not occuring in word, allow normal edit (return false)
-- calculate what editted word will be 
--   use locwds to change all other occs and change cp and ank
--   return false to allow normal edit

Lc_shortcut("matchedit|startmultiword|Ctrl+;")

function startmultiword()
  prnte("mw started")
--calculate range
  local pan,ap,ep,rv=get_pane()
  if ap==ep then ap,ep = poslinestfn(ap,pan) end
  if ap==ep then return true end

--highlight range
  mwpan,mwbuf=pan,props['FilePath']
  mrnga,mrnge=ap,ep
  paintrange(mwpan,mrnga,mrnge)
  htwrda,htwrde,htwrd = -1,-1,""
--register range
--interpose okmultiword
  setinterposekeys(okmultiword)
  mwpan.Anchor=mwpan.CurrentPos
  return true
end

function okmultiword(key, shift, ctrl, alt)
  local pan,d,c,rv=get_pane()
  local issel = d~=c
  local cpe = rv and c or d
  local ape = rv and d or c
  
  if pan~=mwpan or props['FilePath']~=mwbuf then return false end
  --paintrange(mwpan,mrnga,mrnge) --dev
  if c~=tain(c,mrngax,mrngex) or d~=tain(d,mrngax,mrngex) then
    return stopmwrd()
  end
  
  if (key>kyvalcrpre and key<kyvalcraft) --cursor 
  or c~=tain(c,mrnga,mrnge) then       --or just outside range do nut 
    return false 
  end
  prnte("c htwrdaa htwrde htwrd pantxt",c,htwrda,htwrde,htwrd,pan:textrange(tain(htwrda),tain(htwrde)))

  --   c d a  b aaab b c deek e f 
  if issel then -- makenewlist of selected thing
    newmwt(pan:textrange(d,c),SCFIND_MATCHCASE)
    prnte("selction mwt of:"..pan:textrange(d,c))
  elseif htwrd==nowrd or c~=tain(c,htwrda,htwrde) or pan:textrange(htwrda,htwrde) ~= htwrd then 
    prnte("rebottle")
    htwrd,htwrda,htwrde=findonpos("[%w_]+",pan) --to make new list of word thing 
--~     if not htwrd then htwrd,htwrda,htwrde = findonpos("[%p]+",pan) end --punctuation?? space?? 
    if not htwrd then prnte("nowrd") htwrd=nowrd else
      prnte("new mwt of:"..htwrd)
      newmwt(htwrd,SCFIND_MATCHCASE + SCFIND_WHOLEWORD)
    end
  end
  
  key=tostring(key)
  ckey=vkkycd[key]
  
  if ckey==nil then return false end
  
  wrdcp=c-htwrda --cp in word
  
  --space tab enter - shift with these characters does plain operation
  --space aa bbb c d e f g aa aaa a a bb 
  --tab
  --enter
  
  local wrdx=htwrd
  local wrdb=htwrd:sub(1,wrdcp)
  local wrdd=htwrd:sub(wrdcp+1,#htwrd)

  prnte("htwrd :"..wrdb.."@"..wrdd..":")
  
  if #ckey==1 then --ckey is most any character
    if shift then ckey=string.upper(ckey) end
    if issel then wrdx=ckey else --sel replaces
      wrdx=wrdb..ckey..wrdd     --no sel inserts
      prnte("tobe :"..wrdx)
    end
    if ctrl then
      if ckey =="v" then ckey=Lc_.clippet:pop(1)
      elseif ckey =="c" then return
      elseif ckey=="x" then mrnge=mrnge-math.abs(d-c) return
      else ckey="" end
    end
    if htwrd==nowrd then 
      mrnge=mrnge+#ckey mrngex=mrngex+#ckey 
      prnte("just mrnge",#ckey)
      return false 
    end
  elseif key==kvkycd['space'] then
    if issel then 
      wrdx=" " 
    elseif shift or #wrdd==0 or #wrdb==0 then 
      htwrd=nowrd       --reset wrdx and ignore 
      mrnge=mrnge+1 mrngex=mrngex+1
      return false 
    else
      if htwrd==nowrd then 
        mrnge=mrnge+1 mrngex=mrngex+1 
        return false 
      end
      wrdx=wrdb.." "..wrdd
    end 
  elseif shift and key==kvkycd['tab'] then htwrd=nowrd return false
  elseif key==kvkycd['tab'] then
    if issel then wrdx="\t"
    elseif #wrdd==0 or #wrdb==0 or htwrd==nowrd then 
      htwrd=nowrd 
      mrnge=mrnge+1 mrngex=mrngex+1
      --paintrange(mwpan,mrnga,mrnge)
      return false 
    else
      wrdx=wrdb.."\t"..wrdd
    end 
  elseif key==kvkycd['delete'] then
    if issel or #wrdd==0 then wrdx="" --delete selection just deletes selection
      htwrd=nowrd mrnge=mrnge-1 mrngex=mrngex-1 
      --paintrange(mwpan,mrnga,mrnge)
      return false 
    end 
    wrdx = wrdb..wrdd:sub(2)   --else wrdx is calculated here
  elseif key==kvkycd['backspace'] then --backspace selection deletes selection everywhere,
    if issel then 
      wrdx=""               --maintain mask to allow recreation ?? yes
    elseif #wrdb==0 then    --drag wrdx back so not to alter a joined word 
      htwrda=htwrda-1 htwrde=htwrde-1 mrnge=mrnge-1 mrngex=mrngex-1 
      mwtaftcp(c,-1,#wrdx)  --but matches in table after cp need moved
      --paintrange(mwpan,mrnga,mrnge)
      return false 
    end
    if htwrd==nowrd then  mrnge=mrnge-1 mrngex=mrngex-1  return false  end
    wrdx = wrdb:sub(1,#wrdb-1) .. wrdd
  end
  
  prnte("axmwrd:"..wrdx..":")
  
  --if true then return false end
  
  if htwrd~=nowrd and wrdx~=htwrd then updtmwt(wrdx,htwrd,cpe,ape) end

end


--recalc mwmap when ?
--when edit outside lastwrd
--when lastwrd empty
--after shift-space
--[[

eee e eee eeee ebe ebe eeeee ebe ebe ebe
e
e
e
eoekemeueleteieweoerede e
e e eeeveeereye epereeesese
e e e edeieseceeerenemeeenetese
e
e e e ekeeeye eiese eceuereseoere
e e e e
e e e eeedeiete ekeeeye epereeeseseeede
e e e e e eieneheoeteweoerede e-eienehewerenege e&e eheoeteweoerede eiese eseaemeee e
e e e e e eeexeheoeteweoerede e-eeexehewerenege eoere ehewerede eiese edeiefefe eoere e
e e e e e e
e e e e e e
e e e e
e e e ekeeeye eiese eieneseeerete eceheaereaeceteeere
e e e ekeeeye eiese eieneseeerete eceleiepebeoeaerede e
e e e ekeeeye eiese edeeeleeeteee eceheaereaeceteeere
e e e ekeeeye eiese ebeaecekesepeaeceee
e e e ekeeeye eiese eceuete e
e
etetetete e ebebe ece e edede
ete e ebebe ece e edede
e ewewewewewewewewewewewewe eweweeededededededewewewewewewewewewewewewexexexexexexe ededexexexexexexe
e
e e e e e e eae e
ete
e]e]e
efeueneceteieoene eoekemeueleteieweoerede(ekeeeye,e eseheiefete,e eceterele,e eaelete)e
e e eleoeceaele epeaene,ede,ece,ereve=egeeete_epeaeneee(e)e
e e eleoeceaele eieseseeele e=e ede~e=ece
e e eleoeceaele ecepeee e=e ereve eaenede ece eoere ede
e e eleoeceaele eaepeee e=e ereve eaenede ede eoere ece
e e e
e e ece=ecepeee e-e-eseteietecehe eteoe eseeete ecepe e
e e e
e e eiefe epeaene~e=emewepeaene eoere epereoepese[e'eFeieleeePeaetehe'e]e~e=emewebeuefe eteheeene ereeeteuerene efeaeleseee eeenede
e e e-e-epeaeienetereaenegeee(emewepeaene,emerenegeae,emerenegeee)e e-e-edeeeve
e e eiefe ece~e=eteaeiene(ece,emerenegeaexe,emerenegeeexe)e eoere ede~e=eteaeiene(ede,emerenegeaexe,emerenegeeexe)e eteheeene
e e e e ereeeteuerene eseteoepemewerede(e)e
e e eeenede
e e e
e e eiefe e(ekeeeye>ekeyeveaelecerepereee eaenede ekeeeye<ekeyeveaelecereaefete)e e-e-eceuereseoere e
e e eoere ece~e=eteaeiene(ece,emerenegeae,emerenegeee)e eteheeene e e e e e e e-e-eoere ejeuesete eoeueteseiedeee ereaenegeee edeoe eneuete e
e e e e ereeeteuerene efeaeleseee e
e e eeenede
e e edepereienete(e"ece eheteweredeaeae eheteweredeee ehetewerede epeaenetexete"e,ece,ehetewrda,htwrde,htwrd,pan:textrange(tain(htwrda),tain(htwrde)))

  --   c d a  b aaab b c deek e f 
  if issel then -- makenewlist of selected thing
    newmwt(pan:textrange(d,c),SCFIND_MATCHCASE)
    prnte("selction mwt of:"..pan:textrange(d,c))
  elseif htwrd==nowrd or c~=tain(c,htwrda,htwrde) or pan:textrange(htwrda,htwrde) ~= htwrd then 
    prnte("rebottle")
    htwrd,htwrda,htwrde=findonpos("[%w_]+",pan) --to make new list of word thing 
--~     if not htwrd then htwrd,htwrda,htwrde = findonpos("[%p]+",pan) end --punctuation?? space?? 
    if not htwrd then prnte("nowrd") htwrd=nowrd else
      prnte("new mwt of:"..htwrd)
      newmwt(htwrd,SCFIND_MATCHCASE + SCFIND_WHOLEWORD)
    end
  end
  
  key=tostring(key)
  ckey=vkkycd[key]
  
  if ckey==nil then return false end
  
  wrdcp=c-htwrda --cp in word
  
  --space tab enter - shift with these characters does plain operation
  --space aa bbb c d e f g aa aaa a a bb 
  --tab
  --enter
  
  local wrdx=htwrd
  local wrdb=htwrd:sub(1,wrdcp)
  local wrdd=htwrd:sub(wrdcp+1,#htwrd)

  prnte("htwrd :"..wrdb.."@"..wrdd..":")
  
  if #ckey==1 then --ckey is most any character
    if shift then ckey=string.upper(ckey) end
    if issel then wrdx=ckey else --sel replaces
      wrdx=wrdb..ckey..wrdd     --no sel inserts
      prnte("tobe :"..wrdx)
    end
    if ctrl then
      if ckey =="v" then ckey=Lc_.clippet:pop(1)
      elseif ckey =="c" then return
      elseif ckey=="x" then mrnge=mrnge-math.abs(d-c) return
      else ckey="" end
    end
    if htwrd==nowrd then 
      mrnge=mrnge+#ckey mrngex=mrngex+#ckey 
      prnte("just mrnge",#ckey)
      return false 
    end
  elseif key==kvkycd['space'] then
    if issel then 
      wrdx=" " 
    elseif shift or #wrdd==0 or #wrdb==0 then 
      htwrd=nowrd       --reset wrdx and ignore 
      mrnge=mrnge+1 mrngex=mrngex+1
      return false 
    else
      if htwrd==nowrd then 
        mrnge=mrnge+1 mrngex=mrngex+1 
        return false 
      end
      wrdx=wrdb.." "..wrdd
    end 
  elseif shift and key==kvkycd['tab'] then htwrd=nowrd return false
  elseif key==kvkycd['tab'] then
    if issel then wrdx="\t"
    elseif #wrdd==0 or #wrdb==0 or htwrd==nowrd then 
      htwrd=nowrd 
      mrnge=mrnge+1 mrngex=mrngex+1
      --paintrange(mwpan,mrnga,mrnge)
      return false 
    else
      wrdx=wrdb.."\t"..wrdd
    end 
  elseif key==kvkycd['delete'] then
    if issel or #wrdd==0 then wrdx="" --delete selection just deletes selection
      htwrd=nowrd mrnge=mrnge-1 mrngex=mrngex-1 
      --paintrange(mwpan,mrnga,mrnge)
      return false 
    end 
    wrdx = wrdb..wrdd:sub(2)   --else wrdx is calculated here
  elseif key==kvkycd['backspace'] then --backspace selection deletes selection everywhere,
    if issel then 
      wrdx=""               --maintain mask to allow recreation ?? yes
    elseif #wrdb==0 then    --drag wrdx back so not to alter a joined word 
      htwrda=htwrda-1 htwrde=htwrde-1 mrnge=mrnge-1 mrngex=mrngex-1 
      mwtaftcp(c,-1,#wrdx)  --but matches in table after cp need moved
      --paintrange(mwpan,mrnga,mrnge)
      return false 
    end
    if htwrd==nowrd then  mrnge=mrnge-1 mrngex=mrngex-1  return false  end
    wrdx = wrdb:sub(1,#wrdb-1) .. wrdd
  end
  
  prnte("axmwrd:"..wrdx..":")
  
  --if true then return false end
  
  if htwrd~=nowrd and wrdx~=htwrd then updtmwt(wrdx,htwrd,cpe,ape) end

end

--[[



atrotty booooooo cobling boooooooz atrotty
b331z atr1tty c1bling atr1tty b331131f1fz
c1blibb331131f1fz/1tty wwr1bb33b33b331131f1fz/bhijr bhijr coorrppppy bhijr bhijr
bhijr coorrppppy bhijr coorrppppy bhijr
coorrppppy bhijr cccoorrrpppcoorrppppyppy


vvv QQQoo owwwQQ QQhhoo vvvs
QQQoo owwwQeeee vvvs owwwQQ QQbovvvvvsvvvvsvsvsvvso Wqqweeee


aasssaaagaaaaa beee c beee aasssaaagaaaaa
beee c aasssaaagaaaaa c beee
c aasssaaagaaaaa beee aasssaaagaaaaa aassab aassa aassaa c

o dasaby crabsss dasaby o
dasaby crabsss o crabsss dasaby
crabsss dasaby o dasaby crabsss

aa bbbbbbbbbbbbbbbbbb cccccc bbbbbbbbbbbbbbbbbb aa
bbbbbbbbbbbbbbbbbb cccccc aa cccccc bbbbbbbbbbbbbbbbbb
cccccc bbbbbbbbbbbbbbbbbb aa bccccccccccbbbbbbbbbbbb cccccc

a babby crabs babby a
babby crabs a crab babby
crabs babby a babby crabs

ttrav ttrrrroo cco ttrrrroo ttrav
ttrrrroo cops ttrav cop ttrrrroo
cco ttrav ttrrrroo ttracopso


andyyyes blindman copyes blindman andyyyes
blindman copyes andyyyes copyes blindman
copyes andyyyes blindman andyyyes copyes


  croat 
 croat  croat 
croat    croat

a b c  a
 c a c 
c a  a c


a  c  a
 c a c 
c a  a c

]]