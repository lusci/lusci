--[[ lua function altfind works identically to string.find except regex like alt groups are possible
  altfind("apple pear banana","(orange|apple|grape)")   - finds orange or apple or grape
 an alt group is declared with () brackets and alternative patterns within are separated with |
pattern contains a maximum of two alt groups. alt groups can contain up to 10 alternative
patterns. 
--]]

function altfind(text,pattern)
  -- find the first alt group
  local start,finish,alt1,alt2 = string.find(pattern,"%(.-%)")
  -- if no alt group, then just use string.find
  if not start then
    return string.find(text,pattern)
  end
  -- store alternative patterns
  local alts = {}
  for alt in string.gmatch(string.sub(pattern,start+1,finish-1),"[^|]+") do
    table.insert(alts,alt)
  end
  -- make a pattern of alternatives
  local altpat = "("..table.concat(alts,")|(")..")"
  -- replace the first alt group with the alternative pattern
  pattern = string.gsub(pattern,alt1,altpat,1)
  -- search the text for the new pattern
  return string.find(text,pattern)
end

-- test
print(altfind("apple pear banana","(orange|apple|grape)"))   -- 1