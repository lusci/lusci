local ptt,ptu,hay --pattern, pattern unescaped, hay

--if call for eg. altway(5,5) returns 5,4 if 5 is | so substring is ""
--and loop is over by d==e
--if 5 is normal char, return is 5,5 to include it
--again loop is over as d==e
--when | is in a-e altway(a,e) returns a to pos before pipe

local function altway(a,e)
  local b , q = a-1 , 0 
  
  while b<e do
    b=ptu:find("[()|]",b+1) or 10000   --pos of next delimiter 
    if b>e then return a,e end         --no alt at q level 0, shouldnt happen
    
    if ptu:find("^%(",b) then q=q+1 elseif ptu:find("^%)",b) then q=q-1 
    elseif q == 0 then 
      return a,b-1 --gobble()
    end
  end
  
  if a<=e+1 then 
    return a,e    --found no pipes so return all, or e+1,e for end substr ""
  else
    return 0,e+1  --0,e+1 exits callees action loop, mad magic behaviour 
  end
end 


local function bloompatt(a,e) 
   
  if (ptu:find("[(|]",a) or 10000)>e then return ptt:sub(a,e) end  --leaf

  local r,d,b = {}
  
  if (ptu:find("|",a,true) or 10000)<=e then 
    b,d=altway(a,e) 
  end
  -- altway returns alt.st to alt.fn  or alt.st to end if no open pipes
  if d and d<e then --there are open pipes
    while d<=e do
      r[#r+1]=bloompatt(b,d)
      b,d=altway(d+2,e) 
      --loop advances 2 from last slice-end
      --so it is finished when a==e+2 (slice ended at e)
      --when slice ended at e-1, a pipe is left, so substr "" needs sliced off
      --altway should not be called with a>e
      --so is not called when 
    end --exit when d==e+1 or e+2
    r.alt=true
    
  else
    r.chn=true
    --[[
      this table may contain a single alt, not sure how right this is
    --]]
    
    d=ptu:find("(",a,true) -- here this is already known to be <= e

    local ct,s,h=1
    while d<=e do -- d is pos of next (
    
      if d~=a then -- stash preceeding unenclosed
        r[ct]=(r[ct] or "") .. (ptt:sub(a,d-1))
      end
      
--~   b=ptu:find(")",d+1,true) or 10000
      h,b=ptu:find("%b()",d) -- find matched brak
      
      b = b or 10000
      
      if b>e then --no closures to continue
        invalpat(r,d,e)
        d=e+1
      else --consume next closure, contents d+1,b-1 
       
        s=ptu:find("|",d+1,true) or 10000 --detected an alt
        
        -- so try putting an alt in chn instead of a chnlink
        
        if s>b then --alt not found in closure, so add to (c)ur (t)xt
          r[ct]=(r[ct] or "") .. (ptt:sub(d,b))  --(sub includes braks)
        else        --alt was found in closure, add as table
          h=bloompatt(d+1,b-1) --get tables featuring alts
--~           print(ptt:sub(d+1,b-1))
--~           print(dump(h))
          if type(h)=="table" and #h==1 and type(h[1])=="table" then
            h=h[1] h.cap=true --extraneous group is capture
          end
          
          if a==d and b==e and #r==0 then r=h else r[#r+1]=h end
          
          ct = #r+1
        end 
        --to handle b+1...
        a = b+1
      end
      -- b+1 is untaken pos
      d=ptu:find("(",b+1,true) or 10000 
    end --end while all chainlinks
  
  if b+1<=e then
    d=ptu:find(")",b+1,true) or 10000
    if d<=e then invalpat(r,b+1,e) else
      r[#r+1]=ptt:sub(b+1,e)
    end
  end 
  
  end --end if chainlink
  
--~   if a<=e then 
--~     print("slippage")
--~     r[#r+1]=ptt:sub(a,e) 
--~   end
  
  return r
end


function makepatt(pat)
  ptu=pat ptt=pat
  if ptu:find("%",1,true) then ptu=ptu:gsub("%%.","xx") end
  if ptu:find("[",1,true) then 
    ptu=ptu:gsub("[%[][^%]]*[%]]",function(c) return string.rep("x",#c) end )
  end
  
  --if no %%%d then make a no brackets version ?
  local ptree=bloompatt(1,#ptt)
  return ptree
end



--~ print( makepatt("%[%]b[bb%%n]n[   ]") )

--~ print( dump(makepatt("(ad|ss)|(ef|dd)")) )
--~ print( dump(makepatt("(ad|ss|)|((p|q))")) )
--~ print( dump( makepatt( "fsk|a(ko|kl)" ) ) )

print( dump( makepatt( "(fsk|.-(ko|kl))cc" ) ) )

print( dump( makepatt( "((a|b)((c|e)|d))(e|f|g)" ) ) )



---------------------

--gobble is building chain tables, and alt tables
--chain tables contain string and alt elements
--alt tables contain string and chain elements
--syntax to split both is same
--...
--situations
--parsing in a chain, to create chain elements
--  m((n|o)|(p|q))
function gobble(ptt,a,e) --hay, anchor end
--~   print("gob")
  a=a or 1    e=e or #ptt  --(a)nchor (e)nd
  if not string.find(ptt,"[(|]",a) then return ptt:sub(a,e) end
  
  local rt = {}
  
--~   print("tryinga",ptt:sub(a,e))
  local ms,d=altway(ptt,a,e)   --(m)atch(s)tring  (d)ue pos
  if ms then                   --split as alts
    rt["alt"]=1 
    while ms do
      if overloop() then return rt end
      rt[#rt+1]= gobble(ms)
      ms,d=altway(ptt,d,e)
    end
    if true then rt[#rt+1]= gobble(ptt:sub(d,e)) end
    return rt
  else             --splitting as chain
    rt["chn"]=1
    local x,y
    while d<=e do
      ms,d,x=nextparth(ptt,d,e)  --ms gobbled, (d)ue pos, (a)ttributes
      rt[#rt+1]=ms
      y=y or x
    end
    return rt
  end
end

function gobbleup(ptt)
  return gobble("("..ptt..")")
end

local hay


--~ print(dump( gobble("vooze((dub|brakd))(merk|feld|(putt|ball))moz") ) )
--~ print(dump( gobble("vooze(sin|brakd)(merk|feld|(putt|ball))moz") ) )

ptt="abcde(fghij)klmnop"
pt="a(.?.?|.?).?(e|f|d)(x|x|)(ef|f|g)(|h|)"
pt="e|f||(d(|x|x)e|f|d(x|x))"
pt="abba.*[%(])"
--~ pt="a(.?.?|.?)"
--~ print( dump(altfind(ptt,pt,st,fn)) )
--~ print(dump( nextnoesc(pt,"%%*%(",1,#ptt) ) )
--~ print(dump(nextnoesc(ptt,"%%*%(",1,#ptt)))
--~ print(dump(nextparth(pt,1,#ptt)))
--~ print(dump(nextparth(pt,2,#ptt)))
--~ print(dump( gobble("m((n|q)|(p|q))") ) )
pt="(ad|ss)|(ef|dd)"
-- print(dump( gobble(pt) ) )
 print(dump( gobbleup(pt) ) )

--*
