
local function nearbymark(l)
  local q=1
  while math.abs(q)<8 do
    if editor:MarkerGet(tain(l+q,0,editor.LineCount))&2==2 then return l+q end
    if q>0 then q=0-q else q=1-q end
  end
end

Lc_['bkstack'] = lc_init('bkstack',Stacket())
local stk = Lc_['bkstack']

local function bkeq(a,b)
  return a and b and a.fp==b.fp and a.bl==b.bl 
end

local function bkfnd(a,b)
  return a and b and a.fp==b.fp 
end


local function marko(bl,fp)
  return {fp= fp or props['filepath'], bl= bl or editor:LineFromPosition(editor.CurrentPos)}
end 

function bmcorrectlines()  --only works when buffer is current
  local cfp,filemks = props['FilePath'],{}  --currentfp, markedlines
  local n = stk:len()
--print(dump(stk))
--tracerr(ipaircopy(stk))
  for c = 1,n do
    if stk[c].fp==cfp then --only collect info of current open file to be scanned through
--    print("inserting",n,dump(stk[c]))
      table.insert(filemks,{mkx=c,mkl=stk[c].bl}) --markstack index, markedline
--    tracerr(filemks)
    end
  end
  --filemks is sortable metainfo of bookmarks in file,lines and their ix in the stacket
  if #filemks==0 then return end
  
  --sort mlines by mkl 
  table.sort(filemks,function(x,y) return x.mkl < y.mkl end) 

  --loop through bookmarks in file, 
  --for each one update the stacket entries corresponding to its number 
  local cl,lnc,cm = 0,editor.LineCount,1
  local mask=2
  
  while not overloop(100) do
    local dl=editor:MarkerNext(cl,mask)
--  print("dl",dl)
    if dl ==-1 then break else 
      if filemks[cm] then
--      print("move line",stk[filemks[cm].mkx].bl,">",dl)
        stk[ filemks[cm].mkx ].bl = dl   -- filemks[cm].mkx is index in stack 
      else
--      print("add missing",dump(marko(dl,cfp)))
        stk:under( marko(dl,cfp), bkeq ) --must add missing bookmarks ...
--      tracerr(ipaircopy(stk))
      end
      cm = cm+1
      cl=dl+1
    end
  end
--tracerr(ipaircopy(stk))
end

function bkmhopset(anti)
  bmcorrectlines()

  local cl=editor:LineFromPosition(editor.CurrentPos)
  local cbk={} cbk.fp=props['FilePath'] cbk.bl=cl 
  local sbk=nil 
  
  if editor:MarkerGet(cl)&2==2 then   -- cursor is on a bkmk
    if anti then 
      scite.MenuCommand(IDM_BOOKMARK_TOGGLE) 
      stk:rmv(cbk, bkeq)
      return true
    else
      sbk=stk:hop(bkeq)
      tracerr(ipaircopy(stk))
      --takes extra hop if pogo
      if bkeq(sbk,cbk) then sbk=stk:hop(bkeq) end
      if sbk and cbk.fp~=sbk.fp then 
        sc_open(sbk.fp) 
        bmcorrectlines()
        --get entry again after correction
      end
      
      if sbk then gogoline(sbk.bl) end
      return true
    end
  end
  
  local nl=nearbymark(cl)
  
  if nl then  gogoline(nl)  return  end
  
  if anti then
    sbk=stk:unhop(bkeq) or cbk
    sc_open(sbk.fp)
    bmcorrectlines() --get entry again after corrections
    gogoline(sbk.bl)
  else
    scite.MenuCommand(IDM_BOOKMARK_TOGGLE)
    stk:push( cbk, bkeq )
--  tracerr(ipaircopy(stk))
  end
  return true
end


local function listbm(p,mask) --old
  local bms,p,mask = {} , p or props['FilePath'] , mask or 255
  
  local cl,el=0,editor.LineCount
  while cl~=el+1 do
    local bl=editor:MarkerNext(cl,mask)
    if bl ==-1 then cl=el+1 else
--~       print("debug", editor:MarkerGet(bl), editor:GetLine(bl)) 
      cl=bl+1
      table.insert(bms,bl)
    end
  end
  return bms
end

function bkstackmove(dp,bp) --change bp to dp in bookmarket stacket
  bp=bp or props['FilePath']
  for c=1,stk:len(),1 do
    if stk[c].fp==bp then stk[c].fp=dp end
  end
end
 
Lc_shortcut("Bookmark set and next|bkmhopset|ctrl+\\")
Lc_shortcut("Bookmark unset and prev|bkmhopset 'rv'|Ctrl+Shift+|")

function bkmjump(dwn)
  local cl,dl=editor:LineFromPosition(editor.CurrentPos)
  
  if dwn then dl=editor:MarkerNext(cl,2) else dl=editor:MarkerPrev(cl,2) end
  
  if dl>-1 then
    stk:pre( marko(dl) )
    gogoline(dl)
  end
  
  return true
end

--Lc_shortcut("Jump to Bookmark above|bkmjump|Ctrl+Shift+{")
--Lc_shortcut("Jump to Bookmark below|bkmjump 'dwn'|Ctrl+Shift+}")

function dumpmarks()
  print('\n- Bookmark list -')
  local el=editor.LineCount
  local bufy=props["FilePath"]
  local bms=listbm()
  for _,cl in ipairs(blk) do
    local t=editor:MarkerGet(cl)
    if t==2 then t="" else t="("..tostring(t)..")" end 
    trace(bufy..":"..(cl+1)..":"..t..editor:GetLine(cl)) 
  end
  tracePrompt()
  return true
end

--~ scite_Command("List BkMarks|dumpmarks")

--sends caret back to previous undo point or 2 before if same (toggles)
Lc_shortcut("Last position|goback|Ctrl+#")
local gobp={}
function goback()
  local pane=get_pane()

  gobp[props["gg"..'FilePath']]=gobp[props['FilePath']]
  gobp[props['FilePath']]=pane.CurrentPos
  
  local gpos=pane.CurrentPos
  local tpos=pane.CurrentPos
  if pane:CanUndo() then pane:Undo()
    tpos=pane.CurrentPos
    if pane:CanUndo() then pane:Undo()
    tpos=pane.CurrentPos
    pane:Redo()
    end
    pane:Redo()
  end
  
  if tpos==gpos then
    pane:GotoPos(gobp[props["gg"..'FilePath']] or tpos)
  else
--~ 		gobp[props['FilePath']]=gobp[props["gg"..'FilePath']]
    if tpos==gobp[props['FilePath']] then tpos=gobp[props["gg"..'FilePath']] end
    pane:GotoPos(tpos)
  end
  return true
end